
개발 시, 주의 사항
---
* 신규 기능 혹은 개선 작업 시엔, 항상 새로운 브랜치를 만들어서 작업한다.
* branch 
  - 신규/개선 : feature/xxxx_0925
  - 버그 : bugfix/xxxx_0925
  - 긴급 수정 : hotfix/xxxx_0925
  - 배포 : release/xxxx_0925
<br>
<br>

개발 환경 설정 가이드
------------

1. 톰캣 설정
    - 주소: 8080
    - VM Option: -Dspring.profiles.active=local
    - Deployment 추가
    
2. 이니시스 설정
    * 결제 키 파일 설치
       - WEB-INF/files/key.zip 파일을 C:/home/minds/maum/payment에 압축을 푼다.<br>
          ? 드라이버명은 톰캣 설치 폴더 기준!!<br>
          압축을 풀면 C:/home/minds/maum/payment/key 폴더가 생성되어야 한다. <br>
          혹은 압축을 풀면 D:/home/minds/maum/payment/key 폴더가 생성되어야 한다.<br>
          
    * 라이브러리 설치<br>
       ... 준비중
             