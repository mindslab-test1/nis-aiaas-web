package ai.square.anomalyReverse.controller;


import ai.square.anomalyReverse.service.AnomalyReverseService;
import ai.square.common.service.CommonService;
import ai.square.common.util.PropertyUtil;
import ai.square.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/anomalyReverse")
public class anomalyReverseController {

    private static final Logger logger = LoggerFactory.getLogger(anomalyReverseController.class);

    @Autowired
    private CommonService commonService;

    @Autowired
    private AnomalyReverseService anomalyReverseService;
    /**
     * anomalyReverse Main
     * @return
     */
    @RequestMapping(value = "/krAnomalyReverseMain")
    public String krAnomalyReverseMain(HttpServletRequest request) {
        logger.info("Welcome krAnomalyReverseMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "anomalyReverseMain");
        httpSession.setAttribute("menuName", "API Services");
        httpSession.setAttribute("subMenuName", "이상행동 감지");
        httpSession.setAttribute("menuGrpName", "시각");

        String returnUri = "/kr/anomalyReverse/anomalyReverseMain.pg";

        return returnUri;
    }


    @PostMapping(value="/multiAnomaly")
    @ResponseBody
    public ResponseEntity<byte[]> multiAnomaly(@RequestParam MultipartFile file, HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getApiId();

        commonService.recordLog(memberVo.getUserId(), "anomaly", "api_call", "");

        return anomalyReverseService.AnomalyMultipart(apiId, apiKey, file, PropertyUtil.getUploadPath()+"/anomaly/", savedId);
    }


}