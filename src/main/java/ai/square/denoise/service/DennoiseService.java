package ai.square.denoise.service;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import ai.square.common.service.CommonService;
import ai.square.common.util.FileConvertUtil;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",

})
@Service
public class DennoiseService {

    private static final Logger logger = LoggerFactory.getLogger(DennoiseService.class);

    @Value("${api.url}")
    private String apiDomain;

    public Map<String, Object> getApiDenoise (String apiId, String apiKey,
                                                 MultipartFile noiseFile, String uploadPath, String saveId) {

        if (noiseFile == null) {
            throw new RuntimeException("You must select the a file for uploading");
        }

        String url = apiDomain + "/api/dap/denoise";

        String fileName = noiseFile.getOriginalFilename();
        fileName = fileName.substring(fileName.lastIndexOf("\\")+1);

        String logMsg = "\n===========================================================================\n";
        logMsg += "Denoise API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", fileName);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try {

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File inputFile = new File(uploadPath);


            if (!inputFile.exists()) {
                logger.info("create Dir : {}", inputFile.getPath());
                inputFile.mkdirs();
            }

            inputFile = new File(uploadPath + saveId + "_" + fileName);
            noiseFile.transferTo(inputFile);
            FileBody noiseFileBody = new FileBody(inputFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("file", noiseFileBody);
            builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            HttpEntity responseEntity = response.getEntity();

            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] wavAudioArray = IOUtils.toByteArray(in);

            // 로컬 서버에 wav 파일 저장
            File cnnoiseFile = new File(uploadPath + "out");

            if(!cnnoiseFile.exists()){
                try {
                    logger.info("create out dir : {}", cnnoiseFile.getPath());
                    cnnoiseFile.mkdirs();
                }catch (Exception e){
                    e.getStackTrace();
                }
            }

            FileOutputStream fos = new FileOutputStream(cnnoiseFile +"/"+ saveId + "_denoise.wav");
            fos.write(wavAudioArray);
            fos.flush();
            fos.close();

            // MP3로 변환 및 return
//            Path target = Paths.get(cnnoiseFile +"/"+ saveId + "_denoise.wav"); // 파일 저장 경로
            File target = new File(cnnoiseFile +"/"+ saveId + "_denoise.wav");

            try {
//                FileCopyUtils.copy(wavAudioArray, target.toFile());

                String orgMp3FilePath = FileConvertUtil.changeFileExtesion(inputFile.toString());
                String changedMp3FilePath = FileConvertUtil.changeFileExtesion(target.toString());

                FileConvertUtil.changePermission( inputFile.toString() );
                FileConvertUtil.changePermission( target.toString() );

                FileConvertUtil.convertWavToMp3( inputFile.toString(), orgMp3FilePath );
                FileConvertUtil.convertWavToMp3( target.toString(), changedMp3FilePath );

                File orgMp3file = new File(orgMp3FilePath);
                File changedMp3file = new File(changedMp3FilePath);

                FileInputStream orgInputStream = new FileInputStream(orgMp3file);
                FileInputStream changedInputStream = new FileInputStream(changedMp3file);
                byte[] orgMp3AudioArray = IOUtils.toByteArray(orgInputStream);
                byte[] changedMp3AudioArray = IOUtils.toByteArray(changedInputStream);

                headers.setCacheControl(CacheControl.noCache().getHeaderValue());
                ResponseEntity<byte[]> orgMp3Entity = new ResponseEntity<>(orgMp3AudioArray, headers, HttpStatus.OK);
                ResponseEntity<byte[]> changedMp3Entity = new ResponseEntity<>(changedMp3AudioArray, headers, HttpStatus.OK);
                ResponseEntity<byte[]> wavEntity = new ResponseEntity<>(wavAudioArray, headers, HttpStatus.OK);

                Map<String, Object> resultMap = new HashMap<>();

                resultMap.put("orgMp3File", orgMp3Entity);
                resultMap.put("changedMp3File", changedMp3Entity);
                resultMap.put("wavFile", wavEntity);

                inputFile.delete();
                orgMp3file.delete();
                target.delete();
                changedMp3file.delete();

                logger.info(" @ Denoise service delete MP3 & WAV files complete! ==> apiId : {}", apiId);

                return resultMap;

            } catch (IOException e) {
                logger.error("Exception in Denoise service's file writing process ! : " + e);
            }


        } catch (Exception e) {
            logger.error("API exception : {}", e.toString());
            e.printStackTrace();

        }
        return null;
    }

}
