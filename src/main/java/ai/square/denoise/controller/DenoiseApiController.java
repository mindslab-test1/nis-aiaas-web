package ai.square.denoise.controller;

import ai.square.common.service.CommonService;
import ai.square.common.util.PropertyUtil;
import ai.square.denoise.service.DennoiseService;
import ai.square.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/*
 * API Controller
 */

@RestController
public class DenoiseApiController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private DennoiseService denoiseService;


    @PostMapping("/api/dap/denoise")
    @ResponseBody
    public Map<String, Object> getApiDenoise (@RequestParam("noiseFile")MultipartFile noiseFile, HttpServletRequest request){

        HttpSession httpSession= request.getSession(true);

        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getApiId();

        commonService.recordLog(memberVo.getUserId(), "denoise", "api_call", "");

        return denoiseService.getApiDenoise(apiId, apiKey, noiseFile, PropertyUtil.getUploadPath() +"/denoise/", savedId);

    }

}
