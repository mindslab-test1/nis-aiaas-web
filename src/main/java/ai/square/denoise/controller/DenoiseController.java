package ai.square.denoise.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/denoise")
public class DenoiseController {

	private static final Logger logger = LoggerFactory.getLogger(DenoiseController.class);

	/**
	 * denoise Main
	 * @return
	 */
	@RequestMapping(value = "/krDenoiseMain")
	public String krDenoiseMain(HttpServletRequest request) {
		logger.info("Welcome krDenoiseMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "denoiseMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "음성 정제");
		httpSession.setAttribute("menuGrpName", "음성");

		String returnUri = "/kr/denoise/denoiseMain.pg";
		
		return returnUri;
	}
}