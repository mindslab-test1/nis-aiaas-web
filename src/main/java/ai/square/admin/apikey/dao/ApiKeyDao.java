package ai.square.admin.apikey.dao;

import ai.square.admin.apikey.model.ApiKeyVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class ApiKeyDao {

    @Resource(name = "sqlSession")
    private SqlSession sqlSession;

    private static final String NAMESPACE = "ai.square.model.registerApiIdMapper";

    public List<String> getAllApiId() throws Exception{
        return sqlSession.selectList(NAMESPACE+".getAllApiId");
    }

    public ApiKeyVo getUserApi(String userId) throws Exception{
        return sqlSession.selectOne(NAMESPACE+".getUserApi", userId);
    }

    public void pauseApiKey(String userId) throws Exception{
        sqlSession.update(NAMESPACE+".pauseApiKey", userId);
    }

    public void registApiId(ApiKeyVo registApiVo) throws Exception{
        sqlSession.update(NAMESPACE+".registApiId", registApiVo);
    }

}
