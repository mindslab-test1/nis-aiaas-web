package ai.square.admin.apikey.controller;

import ai.square.admin.apikey.service.ApiKeyService;
import ai.square.admin.apikey.service.ApiKeyService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ApiKeyApiController {

    private static final Logger logger = LoggerFactory.getLogger(ApiKeyApiController.class);

    @Autowired
    private ApiKeyService registApiService;

    /*@PostMapping("/admin/apikey/register")
    public ResponseEntity<String> getApiRegistApi(HttpServletRequest request, @RequestParam(value="apiId") String apiId, @RequestParam(value="email") String email,
                                                  @RequestParam(value="name") String name) throws IOException, Exception {
        System.out.println("this is /admin/apikey/register");
        HttpSession session = request.getSession(true);

        Map<String, String> usrMap = new HashMap<>();
        usrMap.put("apiId", apiId);
        usrMap.put("email", email);
        usrMap.put("name", name);

        JSONObject jsonapi = new JSONObject();
        Map<String, String> result = new HashMap<>();
        result = registApiService.addApiKey(usrMap);

        for(Map.Entry<String, String> elem : result.entrySet())
        {
            jsonapi.put(elem.getKey(), elem.getValue());
        }

        return new ResponseEntity<String>(jsonapi.toString(), HttpStatus.OK);
    }*/

    /*
    ** API ID/KEY 조회
    */
    /*@PostMapping("/admin/getClient")
    public ResponseEntity<String> getClient(HttpServletRequest request,
                                            @RequestParam(value="email") String email) throws IOException, Exception {
        System.out.println("this is /admin/getClient");
        HttpSession session = request.getSession(true);

        Map<String, String> usrMap = new HashMap<>();
        usrMap.put("apiId", email.substring(0, email.indexOf("@")) + "-" + (System.currentTimeMillis()%10000));
        usrMap.put("email", email);
        usrMap.put("name", email);

        JSONObject jsonapi = new JSONObject();
        Map<String, String> result = new HashMap<>();
        result = registApiService.getApiKey(email);
        if(result == null) {
            result = registApiService.addApiKey(usrMap);
            if(result != null) {
                result.put("apiId", result.get("id"));
                result.put("apiKey", result.get("key"));

                result.remove("id");
                result.remove("key");
                result.remove("message");
            }
        }

        for(Map.Entry<String, String> elem : result.entrySet())
        {
            jsonapi.put(elem.getKey(), elem.getValue());
        }

        return new ResponseEntity<String>(jsonapi.toString(), HttpStatus.OK);
    }*/
}