package ai.square.admin.apikey.service;

import ai.square.admin.apikey.dao.ApiKeyDao;
import ai.square.admin.apikey.model.ApiKeyVo;
import ai.square.member.dao.MemberDao;
import ai.square.member.model.MemberDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",

})
@Service
public class ApiKeyService {

    @Value("${api.url}")
    private String apiDomain;

    @Value("${api.account.add}")
    private String apiAccountAddUrl;

    @Value("${api.account.delete}")
    private String apiAccountDeleteUrl;

    @Value("${api.account.resume}")
    private String apiAccountResumeUrl;


    private static final Logger logger = LoggerFactory.getLogger(ApiKeyService.class);

    @Autowired
    private ApiKeyDao apiDao;

    @Autowired
    private MemberDao memberDao;

    /*
    ** 임시 코드 (차후에 API 서버에서 조회 기능을 제공하면 삭제)
    */
    public Map<String, String> getApiKey(String email) throws Exception {
        System.out.println("this is getApi");

        Map<String, String> apiMap = new HashMap<>();
        MemberDto member = memberDao.getMemberDetail(email);
        if(member == null) return null;

        apiMap.put("apiId", member.getApiId());
        apiMap.put("apiKey", member.getApiKey());

        return apiMap;
    }

    public Map<String, String> addApiKey(Map<String, String> usrMap) throws Exception {
        logger.info("addApiKey()");

        String apiUrl = apiDomain + apiAccountAddUrl;
        return sendApiPost(apiUrl, usrMap);
    }

    private Map<String, String> sendApiPost(String sendUrl, Map<String, String> usrMap) throws Exception{
        StringBuilder result = new StringBuilder();
        ApiKeyVo registApiVo = new ApiKeyVo();
        Map<String, String> apiMap = new HashMap<>();

        List<String> allApiId = apiDao.getAllApiId();

        for(int idNo=0; idNo<allApiId.size(); idNo++)
        {
            if(usrMap.get("apiId").equals(allApiId.get(idNo)))
            {
                apiMap.put("message", "IdOverlap");
                return apiMap;
            }
        }


        //api 등록
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonValue = mapper.writeValueAsString(usrMap);

            URL url = new URL(sendUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");

            OutputStream os = conn.getOutputStream();
            os.write(jsonValue.getBytes("UTF-8"));
            os.flush();

            int nResponseCode = conn.getResponseCode();

            //api 등록
            if (nResponseCode == HttpURLConnection.HTTP_OK) {
                logger.info("httpConnection success...");
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
                String inputLine;

                while ((inputLine = br.readLine()) != null)
                {
                    try {
                        result.append(URLDecoder.decode(inputLine, "UTF-8"));
                    } catch (Exception ex) {
                        result.append(inputLine);
                    }
                }

                logger.info(result.toString());
                br.close();

            } else {
                throw new Error("httpConnection error...");
            }
            conn.disconnect();

        }catch (Exception e){
            apiMap.put("message", "fail");
            logger.info("api서버 에러");
            return apiMap;
        }

        //apiKey 가져옴
        JsonParser parser = new JsonParser();
        Object obj = parser.parse(result.toString());
        JsonObject apiJson = (JsonObject)obj;
        String message = apiJson.get("message").toString().replace("\"", "");


        //db에 api 값 저장
        try {
            if (message.equals("Success")) {
                String apiKey = apiJson.getAsJsonObject("data").get("apiKey").toString().replace("\"", "");
                logger.info("apiKey : {}, message : {}", apiKey, message);

                apiMap.put("message", "Success");
                apiMap.put("id", usrMap.get("apiId"));
                apiMap.put("key", apiKey);
                registApiVo.setApiId(usrMap.get("apiId"));
                registApiVo.setApiKey(apiKey);
                registApiVo.setUserId(usrMap.get("email"));

                apiDao.registApiId(registApiVo);

            } else {
                apiMap.put("message", "fail");
            }
        }catch(Exception e){
            logger.debug("DB error...");
            apiMap.put("message", "fail");
        }

        return apiMap;
    }

    public Map<String, String> pauseApiId(String userId) throws Exception{
        String apiUrl = apiDomain + apiAccountDeleteUrl;

        Map<String, String> deleteResult = new HashMap<>();
        Map<String, String> userApi = new HashMap<>();

        ApiKeyVo api = apiDao.getUserApi(userId);

        if(api == null){ //회원가입 되지 않은 계정일 경우
            deleteResult.put("SCUSSES", "0");
            deleteResult.put("MESSAGE", "This account is not registered");
            return deleteResult;
        }else if(api.getApiKey() == null && api.getApiId() == null){    //apiId가 등록되지 않았을 경우
            deleteResult.put("SCUSSES", "0");
            deleteResult.put("MESSAGE", "Unregisterd ApiId");
            return deleteResult;
        }else if(api.getApiId() != null && api.getApiKey() == null){   //apiId가 이미 삭제된 경우
            deleteResult.put("SUCCESS", "0");
            deleteResult.put("MESSAGE", "This apiKey is already deleted.");
            return deleteResult;
        }
        else{
            userApi.put("apiId", api.getApiId());
        }

        try{
            URL url = new URL(apiUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            ObjectMapper mapper = new ObjectMapper();
            String jsonValue = mapper.writeValueAsString(userApi);
            conn.setDoOutput(true);
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accetp-Charset", "UTF-8");

            OutputStream os = conn.getOutputStream();
            os.write(jsonValue.getBytes("UTF-8"));
            os.flush();

            int nResponseCode = conn.getResponseCode();
            logger.info("this is nResponseCode : {}", nResponseCode);

            if (nResponseCode == HttpURLConnection.HTTP_OK) { //200 성공
                logger.info("httpConnection success");
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
                String inputLine;
                StringBuilder result = new StringBuilder();

                while ((inputLine = br.readLine()) != null) {
                    try {
                        result.append(URLDecoder.decode(inputLine, "UTF-8"));
                    } catch (Exception ex) {
                        result.append(inputLine);
                    }
                }

                logger.info(result.toString());
                deleteResult.put("SUCCESS", "1");
                deleteResult.put("MESSAGE", "delete success.");
                br.close();

            } else {
                throw new Exception();
            }
            conn.disconnect();
        }catch(Exception e){
            deleteResult.put("SUCCESS", "0");
            deleteResult.put("MESSAGE", e.getMessage());
            return deleteResult;
        }

        return deleteResult;
    }


    /* API 계정 resume */
    public String resumeApiAccount(String apiId){

        String apiUrl = apiDomain + apiAccountResumeUrl;
        HttpClient client = HttpClients.createDefault();
        HttpPatch httpPatch = new HttpPatch(apiUrl);
        httpPatch.addHeader("Content-Type", "application/json");
        httpPatch.addHeader("cache-control", "no-cache");

        try {

            httpPatch.setEntity(new StringEntity("{ \"apiId\" : \"" + apiId + "\"}", "utf-8"));
            HttpResponse response = client.execute(httpPatch);
            int respCode = response.getStatusLine().getStatusCode();

            if(respCode >= 200 && respCode < 300){
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }
                logger.info("Response Result : {}", result);

                JsonParser jsonParser = new JsonParser();
                return jsonParser.parse(result.toString()).getAsJsonObject().get("message").getAsString(); // "Success"
            }
            else{
                logger.error("Resume API account error : {}", respCode);
                return "Resume API account error : " + respCode;
            }

        } catch (Exception e) {
            logger.error("Resume API account exception : {}", e.toString());
            return "Resume API account exception : " + e.toString();
        }

    }


}