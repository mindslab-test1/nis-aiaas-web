package ai.square.member.controller;

import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",

})
@Controller
@RequestMapping(value="/member")
public class MemberController {

}
