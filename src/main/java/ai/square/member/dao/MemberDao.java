package ai.square.member.dao;

import ai.square.member.model.MemberDto;
import ai.square.member.model.MemberDto;
import ai.square.member.model.MemberForm;
import ai.square.member.model.MemberSecurityVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class MemberDao {


	@Resource(name = "sqlSession")
	private SqlSession sqlSession;

	private static final String NAMESPACE = "ai.square.model.memberMapper";


	public MemberSecurityVo getUserById(String userId) {
		return sqlSession.selectOne(NAMESPACE + ".getUserById", userId);
	}

	/**
	 * getLogInMemberDetail (session info)
	 */
	public MemberDto getLogInMemberDetail(String id) throws Exception {

		return sqlSession.selectOne(NAMESPACE + ".getLogInMemberDetail", id);
	}

	/**
	 * 계정 - 등록
	 */
	public int insertMember(MemberForm memberForm) throws Exception {
		return sqlSession.insert(NAMESPACE + ".insertMember", memberForm);
	}


	/** 프로필 조회 */
	public MemberDto getMemberDetail(String email) throws Exception {

		return sqlSession.selectOne(NAMESPACE + ".getMemberDetail", email);
	}
}