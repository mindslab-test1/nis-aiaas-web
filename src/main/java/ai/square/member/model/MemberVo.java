package ai.square.member.model;

import ai.square.common.model.CommonDto;

public class MemberVo extends CommonDto {
	
	private String userId;
	private int userNo ;
	private String apiKey;
	private String apiId;
	private String authority;
	private String password;
	private int enabled;

	public int getUserNo() {
		return userNo;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	@Override
	public String toString() {
		return "MemberVo [userId=" + userId + ", userno=" + userNo + ", password=" + password
				+ ", authority=" + authority + ", apiId=" + apiId + ", apiKey=" + apiKey
				+ ", enabled=" + enabled + "]";
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public int getEnabled() {
		return enabled;
	}
	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getApiId() {
		return apiId;
	}
	public void setApiId(String apiId) {
		this.apiId = apiId;
	}
}