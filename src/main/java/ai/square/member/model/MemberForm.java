package ai.square.member.model;

import ai.square.common.model.CommonForm;

public class MemberForm extends CommonForm {

	private int userNo;
	private String userId;
	private String apiId;
	private String apiKey;
	private String password;
	private String authority;
	private int enabled;

	public int getUserNo() {
		return userNo;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public int getEnabled() {
		return enabled;
	}
	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}
	public String getApiId() { return apiId; }
	public void setApiId(String apiId) { this.apiId = apiId; }
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
}
