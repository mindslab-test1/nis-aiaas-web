package ai.square.member.service;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import ai.square.member.dao.MemberDao;
import ai.square.member.model.MemberForm;
import ai.square.member.model.MemberSecurityVo;

public class MemberDetailsService implements UserDetailsService {
	private static final Logger logger = LoggerFactory.getLogger(MemberDetailsService.class);
    @Autowired
    private MemberDao memberDao;
    
	@Autowired
	private MemberService memberService;     
    
    @SuppressWarnings("unused")
	public UserDetails loadUserByUsername(MemberForm memberForm) throws UsernameNotFoundException {
    	MemberSecurityVo user = memberDao.getUserById(memberForm.getUserId());
		Gson gson = new Gson();
		logger.info("=====유저 조회 : {}",gson.toJson(user));
        if(user==null) {
			logger.info("=====새 유저=====");
			try {
				memberService.insertMember(memberForm);
				user = memberDao.getUserById(memberForm.getUserId());
				logger.info("=====새 유저 : {}",gson.toJson(user));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error("passwordEncoder(MemberForm) 새 유저 생성 Exception :: {}", e.toString());
				throw new UsernameNotFoundException(memberForm.getUserId());
			}
        }else{
			try {
				user = memberDao.getUserById(memberForm.getUserId());
				logger.info("=====기존 유저 : {}",gson.toJson(user));
			}catch (Exception e){
				e.printStackTrace();
				logger.error("passwordEncoder(MemberForm) 기존 유저 조회 Exception :: {}", e.toString());
			}
		}
        return user;
    }


    @Override
	public UserDetails loadUserByUsername(String email){

    	MemberSecurityVo user = memberDao.getUserById(email);
    	logger.info("USER : {}", user.getUsername());

    	if (user == null){
			throw new UsernameNotFoundException(email);
		}else{
    		return user;
		}
	}

 
}