package ai.square.member.service;

import ai.square.member.model.MemberDto;
import com.google.gson.Gson;
import ai.square.member.dao.MemberDao;
import ai.square.member.model.MemberForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class MemberService {

    private final static Logger logger = LoggerFactory.getLogger(MemberService.class);

	@Autowired
	private MemberDao memberDao;

	
	public MemberDto getLogInMemberDetail(String id) throws Exception {
		return memberDao.getLogInMemberDetail(id);
	}
	
	/** 계정 - 등록 */
	MemberDto insertMember(MemberForm memberForm) throws Exception {

		MemberDto memberDto = new MemberDto();
		String id = memberForm.getUserId();
		
		memberForm.setUserId(id);
		memberForm.setAuthority("ROLE_USER");
		memberForm.setEnabled(1);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		memberForm.setPassword(passwordEncoder.encode("nisUserPW"));//임시 BCrypt 비번

		int insertCnt = 0;
		insertCnt = memberDao.insertMember(memberForm);

		if (insertCnt > 0) {
			memberDto.setResult("SUCCESS");
		} else {
			memberDto.setResult("FAIL");
		}

		return memberDto;
	}


	/** 사용자 프로필 조회 */
	public MemberDto getMemberDetail(String userId) throws Exception {

		MemberDto memberDto;

		memberDto = memberDao.getMemberDetail(userId);
		Gson gson = new Gson();
		logger.info("result Dto ::: {}  ", gson.toJson(memberDto));
		return memberDto;
	}

}