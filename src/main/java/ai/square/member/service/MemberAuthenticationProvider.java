package ai.square.member.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import ai.square.member.model.MemberSecurityVo;
import ai.square.member.model.MemberVo;

public class MemberAuthenticationProvider implements AuthenticationProvider {
    
    @Autowired
    private MemberDetailsService memberDetailsService;  
 
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    	
    	MemberVo memberVo = new MemberVo();
        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();
        
        MemberSecurityVo user = (MemberSecurityVo) memberDetailsService.loadUserByUsername(username);
		
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        
        memberVo.setUserId(username);
        //비밀번호 확인
        if(!passwordEncoder.matches(password, user.getPassword())) {       	
        	throw new BadCredentialsException(username);
        }
        
        //계정상태 확인
        if(!user.isEnabled()) {
            throw new LockedException(username);
        }
        
        return new UsernamePasswordAuthenticationToken(username, password, user.getAuthorities());
    }
 
    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
 
}