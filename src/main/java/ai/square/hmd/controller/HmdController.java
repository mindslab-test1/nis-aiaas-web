package ai.square.hmd.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/hmd")
public class HmdController {

	private static final Logger logger = LoggerFactory.getLogger(HmdController.class);
	
	/**
	 * hmd Main
	 * @return
	 */
	@RequestMapping(value = "/krHmdMain")
	public String krHmdMain(HttpServletRequest request) {
		logger.info("Welcome krHmdMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "hmdMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "패턴 분류");
		httpSession.setAttribute("menuGrpName", "언어");

		String returnUri = "/kr/hmd/hmdMain.pg";
		
		return returnUri;
	}
}
