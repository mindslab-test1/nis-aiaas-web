package ai.square.hmd.controller;

import ai.square.common.service.CommonService;
import ai.square.hmd.service.HmdService;
import ai.square.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/*
 * API Controller
 */

@RestController
public class HmdApiController {
    private final static Logger logger = LoggerFactory.getLogger(HmdApiController.class);

    @Autowired
    private CommonService commonService;

    @Autowired
    private HmdService hmdService;

    @PostMapping(value= "/hmd/hmdApi")
    public String hmdApi(@RequestParam(value="lang") String lang, @RequestParam(value="reqText") String text, HttpServletRequest request) throws UnsupportedEncodingException {

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        commonService.recordLog(memberVo.getUserId(), "hmd", "api_call", "");

        String result = hmdService.hmdApi(apiId, apiKey, lang, text);
        String encodedResult = URLEncoder.encode(result, "UTF-8");

        return encodedResult;
    }
}
