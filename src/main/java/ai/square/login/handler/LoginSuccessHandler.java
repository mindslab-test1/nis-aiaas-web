package ai.square.login.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginSuccessHandler implements AuthenticationSuccessHandler {
	private static int TIME = 60 * 60; // 1시간
	
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException {
		
		request.getSession().setMaxInactiveInterval(TIME);
		
		String returnUri = "/";
		
		response.sendRedirect(request.getContextPath() + returnUri);
	}
}