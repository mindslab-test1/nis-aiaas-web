package ai.square.login.controller;

import ai.square.common.service.CommonService;
import ai.square.login.service.LoginService;
import ai.square.member.model.MemberDto;
import ai.square.member.model.MemberForm;
import ai.square.member.model.MemberSecurityVo;
import ai.square.member.model.MemberVo;
import ai.square.member.service.MemberDetailsService;
import ai.square.member.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",

})
@Controller
@RequestMapping(value = "/login")
public class LoginController {

	@Autowired
	private CommonService commonService;

    @Autowired
    private MemberDetailsService memberDetailsService;

	@Autowired
	private MemberService memberService;

	@Autowired
	private LoginService loginService;

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	// TODO: 임시 함수임!!
//	public String testLoginFunc(){
//		return "333333"; //사번 넘겨주는 임시 함수
//	}



	@RequestMapping(value="/nisLogin")
	public String nisLogin(HttpServletRequest request, HttpServletResponse response, RedirectAttributes rediectAttr){

		HttpSession session;
		MemberDto memberDto = null;
		MemberVo memberVo;


		session = request.getSession();
		String userId = (String)session.getAttribute("ID");

//		String userId = testLoginFunc(); // TODO : 로그인 요청 수정해야함

		logger.info("============= Start AIaaS login! ============== \nuser: {}", userId);

		if(userId != null && !userId.equals("")){

			try {
				memberDto = memberService.getMemberDetail(userId);
				memberVo = memberDto;

				// DB에 user가 없는 경우
				if(memberDto == null) {
					MemberForm memberForm = new MemberForm();
					memberForm.setUserId(userId);
					MemberSecurityVo user = (MemberSecurityVo) memberDetailsService.loadUserByUsername(memberForm);
					logger.info("새 user 생성 완료!! -- {}", user.getUsername());

					memberDto = memberService.getMemberDetail(user.getUsername());
					memberVo = memberDto;

					// API id, key 발급
					loginService.createOrResumeApiAccount(memberVo);
				}

				session.setAttribute("accessUser", memberVo);

			} catch (Exception e) {
				if(memberDto == null){
					logger.error("User insert 실패 Exception : {}", e.toString());
					rediectAttr.addAttribute("redirectErrMsg", "로그인 User insert 실패 Exception");
				}else{
					logger.error("로그인 Exception : {}", e.toString());
					rediectAttr.addAttribute("redirectErrMsg", "로그인 Exception");
				}
				return "redirect:/errorPage";
			}


			if(securityLogin(memberVo.getUserId(), request).equals("SUCCESS")){
				logger.info("로그인 성공");
				commonService.recordLog(memberVo.getApiId(), "login", "login_success", "");
				return "redirect:/";
			}
			else{
				logger.info("로그인 에러");
				rediectAttr.addAttribute("redirectErrMsg", "security login error");
				return "redirect:/errorPage";
			}

		}else{
			logger.info("UserId 없음!!");
			rediectAttr.addAttribute("redirectErrMsg", "UserId doesn't exist.");
			return "redirect:/errorPage";
		}

	}


	private String securityLogin(String email, HttpServletRequest request){

		logger.info("------------------ SECURITY START -----------------------");

		try{

			MemberVo memberVo = memberService.getLogInMemberDetail(email);

			// security login
			MemberSecurityVo user = (MemberSecurityVo) memberDetailsService.loadUserByUsername(email);
			SecurityContext sc = SecurityContextHolder.getContext();
			sc.setAuthentication(new UsernamePasswordAuthenticationToken(email, "", user.getAuthorities()));


			HttpSession session = request.getSession();
			session.setAttribute("accessUser", memberVo);
			session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc);

			return "SUCCESS";

		}catch (Exception e){
			logger.error("---------- SECURITY 로그인 실패 ------");
			logger.error(e.toString());
			return "FAIL";
		}
	}


	/**
	 * accessDenied
	 * @return
	 */
	@RequestMapping(value = "/accessDenied")
	public String accessDenied(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {

		logger.info("AccessDenied!!");
		redirectAttributes.addAttribute("redirectErrMsg", "accessDenied");

		return "redirect:/errorPage";
	}

}