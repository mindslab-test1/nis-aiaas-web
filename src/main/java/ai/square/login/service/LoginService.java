package ai.square.login.service;

import ai.square.common.service.CommonService;
import ai.square.member.model.MemberVo;
import ai.square.admin.apikey.dao.ApiKeyDao;
import ai.square.admin.apikey.model.ApiKeyVo;
import ai.square.admin.apikey.service.ApiKeyService;
import ai.square.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class LoginService {

    @Autowired
    private CommonService commonService;

    @Autowired
    private ApiKeyDao apiDao;

    @Autowired
    private ApiKeyService registApiService;

    private static final Logger logger = LoggerFactory.getLogger(LoginService.class);


    public void createOrResumeApiAccount(MemberVo memberVo){
        ApiKeyVo apiVo = null;

        try {
            apiVo = apiDao.getUserApi(memberVo.getUserId());
        } catch (Exception e) {
            logger.error("getUserAPI exception : {}", e);
        }

        if(apiVo == null){
            logger.info("API ID, Key 발급 실패 --- userId : {}", memberVo.getUserId());
            return;
        }

        String apiId = apiVo.getApiId();
        String apiKey = apiVo.getApiKey();
        Map<String, String> result = new HashMap<>();

        if( apiId == null && apiKey == null) {
            logger.info("{} ==> API ID, Key 발급 시작", memberVo.getUserId());
            do {
                UUID uuid = UUID.randomUUID();
                apiId = uuid.toString().split("-")[0] + uuid.toString().split("-")[1];

                Map<String, String> usrMap = new HashMap<>();
                usrMap.put("apiId", apiId);
                usrMap.put("email", memberVo.getUserId());
                usrMap.put("name", memberVo.getUserId());

                try {
                    result = registApiService.addApiKey(usrMap);
                } catch (Exception e) {
                    logger.error("API ID,key 발급 Exception : {}", e);
                }
            } while (result.get("message").equals("IdOverlap"));

            if ("Success".equals(result.get("message"))) {
                logger.info("{}'s Get API account SUCCESS.", memberVo.getUserId());

                commonService.recordLog(memberVo.getApiId(), "registerApiId", "registerSuccess", "");

            } else {
                logger.error("{}'s Get API account FAIL.", memberVo.getUserId());
            }

        }else{
            logger.info("{} already have API account ==> ID : {} , KEY : {}", memberVo.getUserId(), apiId, apiKey);
            String resumeStr = registApiService.resumeApiAccount(apiId); // account resume

            if(!resumeStr.equals("Success")){
                logger.info("실패 -- {}", resumeStr);
            }
        }


    }

}
