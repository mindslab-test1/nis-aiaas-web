package ai.square.vr.service;

import ai.square.common.service.CommonService;
import ai.square.common.util.FileConvertUtil;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",

})

@Service
public class VrService {

    private static final Logger logger = LoggerFactory.getLogger(VrService.class);

    @Value("${api.url}")
    private String apiDomain;


    public void setVoice(String apiId, String apiKey, String dbId, String voiceId, MultipartFile file,
                         String uploadPath, String saveId) throws Exception {

        String url = apiDomain + "/dap/app/setVoice";

        String fileName = file.getOriginalFilename();
        fileName = fileName.substring(fileName.lastIndexOf("\\")+1);

        String logMsg = "\n===========================================================================\n";
        logMsg += "VoiceRecog(setVoice) API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", fileName);
        logMsg += String.format(":: %-10s = %s%n", "dbId", dbId);
        logMsg += String.format(":: %-10s = %s%n", "voiceId", voiceId);
        logMsg += "===========================================================================";
        logger.info(logMsg);

            HttpClient client = HttpClients.createDefault();
            HttpPut put = new HttpPut(url);
            File voiceRecogFile = new File(uploadPath + "set/");

            if(!voiceRecogFile.exists()) {
                logger.info("create Dir : {}", voiceRecogFile.getPath());
                voiceRecogFile.mkdirs();
            }

            voiceRecogFile = new File(uploadPath + saveId + "_" + fileName);
            file.transferTo(voiceRecogFile);
            FileBody txtrFileBody = new FileBody(voiceRecogFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("file", txtrFileBody);
            builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("voiceId", new StringBody(voiceId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("dbId", new StringBody(dbId, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            put.setEntity(entity);
            HttpResponse response = client.execute(put);

            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            voiceRecogFile.delete();

            if(responseCode != 200) {
                logger.error("API fail.");
                throw new Exception("setVoice api fail");
            }
    }

    public void deleteVoice(String apiId, String apiKey, String voiceId, String dbId) {
        String url = apiDomain + "/dap/app/deleteVoice";
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "3FS4PKI7YH9S";

        String logMsg = "\n===========================================================================\n";
        logMsg += "VoiceRecog(deleteVoice) API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "dbId", dbId);
        logMsg += String.format(":: %-10s = %s%n", "voiceId", voiceId);
        logMsg += "===========================================================================";
        logger.info(logMsg);


        try{
            URL connectURL = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Connection", "Keep-Alive");    // connection을 위함
            conn.setRequestProperty("Content-type", "multipart/form-data;boundary=" + boundary);
            conn.setConnectTimeout(15000);

            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

            StringBuffer sb = new StringBuffer();
            sb.append(twoHyphens+boundary+lineEnd);
            sb.append("Content-Disposition: form-data; name=\"apiId\"" + lineEnd);

            sb.append(lineEnd);
            sb.append(apiId+lineEnd);

            sb.append(twoHyphens+boundary+lineEnd);
            sb.append("Content-Disposition: form-data; name=\"apiKey\"" + lineEnd);
            sb.append(lineEnd);
            sb.append(apiKey+lineEnd);

            sb.append(twoHyphens+boundary+lineEnd);
            sb.append("Content-Disposition: form-data; name=\"voiceId\"" + lineEnd);
            sb.append(lineEnd);
            sb.append(voiceId+lineEnd);

            sb.append(twoHyphens+boundary+lineEnd);
            sb.append("Content-Disposition: form-data; name=\"dbId\"" + lineEnd);
            sb.append(lineEnd);
            sb.append(dbId+lineEnd);

            sb.append(twoHyphens+boundary+twoHyphens+lineEnd);

            dos.writeUTF(sb.toString());
            dos.flush();

            int responseCode = conn.getResponseCode();
            logger.info("responseCode = {}" , responseCode);

            if(responseCode != 200){
                logger.error("API fail.");
                throw new Exception("deleteVoice api fail");
            }
        } catch (Exception e) {
            logger.error("API exception : {}", e.toString());
            e.printStackTrace();
        }
    }

    public Map<String, Object> recogVoice(String apiId, String apiKey, MultipartFile file, String dbId,
                                          String uploadPath, String saveId) {

        String url = apiDomain + "/dap/app/recogVoice";

        String fileName = file.getOriginalFilename();
        fileName = fileName.substring(fileName.lastIndexOf("\\")+1);

        String logMsg = "\n===========================================================================\n";
        logMsg += "VoiceRecog(recogVoice) API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", fileName);
        logMsg += String.format(":: %-10s = %s%n", "dbId", dbId);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try{
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File recogVoiceFile = new File(uploadPath + "recog/");

            if(!recogVoiceFile.exists()) {
                logger.info("create Dir : {}", recogVoiceFile.getPath());
                recogVoiceFile.mkdirs();
            }

            recogVoiceFile = new File(uploadPath + saveId + "_" + fileName);
            file.transferTo(recogVoiceFile);
            FileBody recogVoiceFileBody = new FileBody(recogVoiceFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("file", recogVoiceFileBody);
            builder.addPart("dbId", new StringBody(dbId, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            if(responseCode == 200){
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

                StringBuilder result = new StringBuilder();
                String line = "";
                while((line = rd.readLine()) != null){
                    result.append(line);
                }

                // 기존 파일 경로
                String origFilePath = recogVoiceFile.getAbsolutePath();
                // mp3 파일 경로
                String mp3FilePath = FileConvertUtil.changeFileExtesion(recogVoiceFile.getAbsolutePath());

                FileConvertUtil.changePermission( origFilePath );
                FileConvertUtil.convertWavToMp3( origFilePath, mp3FilePath );

                File mp3file = new File(mp3FilePath);

                FileInputStream inputStream = new FileInputStream(mp3file);
                byte[] audioArray = IOUtils.toByteArray(inputStream);

                HttpHeaders headers = new HttpHeaders();
                headers.setCacheControl(CacheControl.noCache().getHeaderValue());

                ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(audioArray, headers, HttpStatus.OK);

                mp3file.delete();
                recogVoiceFile.delete();

                logger.info(" @ VoiceRecognition service delete MP3 file complete! ==> apiId : {}", apiId);

                Map<String, Object> resultMap = new HashMap<>();

                resultMap.put("mp3File", resultEntity);
                resultMap.put("resultString", result.toString());

                return resultMap;

/*
                HttpHeaders headers = new HttpHeaders();
                headers.add("Content-Type", "application/json;charset=UTF-8");

                ResponseEntity<String> resultEntity = new ResponseEntity<>(result.toString(), headers, HttpStatus.OK);
                recogVoiceFile.delete();

                return resultEntity;*/

            } else {
                recogVoiceFile.delete();
                logger.error("API Fail");
                throw new Exception("VoiceRecognition api response fail");
            }
        } catch (Exception e) {
            logger.error("API exception : {}", e.toString());
        }

        return null;
    }
}
