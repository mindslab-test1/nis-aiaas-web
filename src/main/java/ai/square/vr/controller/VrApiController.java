package ai.square.vr.controller;

import ai.square.common.service.CommonService;
import ai.square.common.util.PropertyUtil;
import ai.square.member.model.MemberVo;
import ai.square.vr.service.VrService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class VrApiController {

    private static final Logger logger = LoggerFactory.getLogger(VrApiController.class);

    @Autowired
    private CommonService commonService;

    @Autowired
    private VrService vrService;

    @RequestMapping(value="/api/vr/getVoiceRecog", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getVoiceRecog(@RequestParam(value = "demoFile1") MultipartFile soundFile1,
                                                @RequestParam(value = "demoFile2") MultipartFile soundFile2,
                                                @RequestParam(value = "demoFile3") MultipartFile soundFile3,
                                                @RequestParam(value = "nameArray") List<String> nameArray,
                                                @RequestParam(value = "demoFile") MultipartFile recogSound,
                                                HttpServletRequest request) {

        HttpSession session = request.getSession(true);
        MemberVo memberVo = (MemberVo) session.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getApiId();
        String dbId = "demo_" + savedId;

        commonService.recordLog(memberVo.getUserId(), "voiceRecognition", "api_call", "");

        List<MultipartFile> soundFile = new ArrayList<>();
        soundFile.add(soundFile1);
        soundFile.add(soundFile2);
        soundFile.add(soundFile3);

        try {
            for (int i = 0; i < soundFile.size(); i++) {
                /* 사운드 파일의 길이가 0보다 큰 경우 setVoice */
                if ((soundFile.get(i).getBytes().length) > 0) {
                    vrService.setVoice(apiId, apiKey, dbId, nameArray.get(i), soundFile.get(i), PropertyUtil.getUploadPath() + "/voiceRecog/", savedId);
                }
            }

            /* voice 인식 */
            Map<String, Object> responseEntity = vrService.recogVoice(apiId, apiKey, recogSound, dbId,
                                                                    PropertyUtil.getUploadPath() + "/voiceRecog/", savedId);

            /* Demo 이기 때문에 즉시 삭제 */
            for (int i = 0; i < soundFile.size(); i++) {
                if ((soundFile.get(i).getBytes().length) > 0) {
                    vrService.deleteVoice(apiId, apiKey, nameArray.get(i), dbId);
                }
            }

            return responseEntity;

        } catch (Exception e) {
            logger.info(" @ api fail!! => " + e);

            for (int i = 0; i < soundFile.size(); i++) {
                vrService.deleteVoice(apiId, apiKey, nameArray.get(i), dbId);
            }
        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("mp3File", null);
        resultMap.put("resultString", HttpStatus.INTERNAL_SERVER_ERROR);

        return resultMap;
        //return (new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    @RequestMapping(value = "/api/vr/sampleVoiceRecog", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getSampleVoiceRecog(@RequestParam(value = "sampleAudio1") MultipartFile sampleVoice1,
                                                   @RequestParam(value = "sampleAudio2") MultipartFile sampleVoice2,
                                                   @RequestParam(value = "sampleAudio3") MultipartFile sampleVoice3,
                                                   @RequestParam(value = "sampleAudio4") MultipartFile sampleVoice4,
                                                   @RequestParam(value = "sample") MultipartFile sample,
                                                   HttpServletRequest request) {
        String dbId = "sample";

        System.out.println(" @ Hello VoiceRecognition Controller.getSampleVoiceRecog ! ");

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getApiId();

        commonService.recordLog(memberVo.getUserId(), "voiceRecognition", "api_call", "");

        try {

            vrService.setVoice(apiId, apiKey, dbId, "선균", sampleVoice1, PropertyUtil.getUploadPath() + "/voiceRecog/", savedId);
            vrService.setVoice(apiId, apiKey, dbId, "수지", sampleVoice2, PropertyUtil.getUploadPath() + "/voiceRecog/", savedId);
            vrService.setVoice(apiId, apiKey, dbId, "Woo", sampleVoice3, PropertyUtil.getUploadPath() + "/voiceRecog/", savedId);
            vrService.setVoice(apiId, apiKey, dbId, "영애", sampleVoice4, PropertyUtil.getUploadPath() + "/voiceRecog/", savedId);

            Map<String, Object> responseEntity = vrService.recogVoice(apiId, apiKey, sample, dbId,
                                                                          PropertyUtil.getUploadPath() + "/voiceRecog/", savedId);

            vrService.deleteVoice(apiId, apiKey, "선균", dbId);
            vrService.deleteVoice(apiId, apiKey, "수지", dbId);
            vrService.deleteVoice(apiId, apiKey, "Woo", dbId);
            vrService.deleteVoice(apiId, apiKey, "영애", dbId);

            return responseEntity;
        } catch (Exception e) {
            logger.info(" @ api. fail!! => " + e);

            vrService.deleteVoice(apiId, apiKey, "선균", dbId);
            vrService.deleteVoice(apiId, apiKey, "수지", dbId);
            vrService.deleteVoice(apiId, apiKey, "Woo", dbId);
            vrService.deleteVoice(apiId, apiKey, "영애", dbId);
        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("mp3File", null);
        resultMap.put("resultString", HttpStatus.INTERNAL_SERVER_ERROR);

        return resultMap;
        //return (new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR));
        }
}
