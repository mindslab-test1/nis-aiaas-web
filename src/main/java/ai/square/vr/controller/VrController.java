package ai.square.vr.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/vr")
public class VrController {

    private static final Logger logger = LoggerFactory.getLogger(VrController.class);

    @RequestMapping(value = "/krVoiceRecogMain")
    public String krVoiceRecogMain(HttpServletRequest request) {
        logger.info("Welcome krVoiceRecogMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "voiceRecogMain");
        httpSession.setAttribute("menuName", "API Services");
        httpSession.setAttribute("subMenuName", "Voice Recognition");
        httpSession.setAttribute("menuGrpName", "음성");

        String returnUri = "/kr/vr/voiceRecogMain.pg";

        return returnUri;
    }
}
