package ai.square.stt.service;

import java.io.*;

import ai.square.common.service.CommonService;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",

})
@Service
public class SttApiService {

	private static final Logger logger = LoggerFactory.getLogger(SttApiService.class);

	@Value("${api.url}")
	private String apiDomain;
		
	public String getApiStt(String apiId, String apiKey, MultipartFile file, String uploadPath, String saveId) {

		if (file == null) {
			throw new RuntimeException("You must select the a file for uploading");
		}

		String url = apiDomain + "/stt/cnnStt";

		String fileName = file.getOriginalFilename();
		fileName = fileName.substring(fileName.lastIndexOf("\\")+1);

		String logMsg = "\n===========================================================================\n";
		logMsg += "STT API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "file", fileName);
		logMsg += "===========================================================================";
		logger.info(logMsg);

		try {

			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);

			File varfile = new File(uploadPath);

			// 디렉토리가 없으면 생성
			if (!varfile.exists()) {
				logger.info("create Dir : {}", varfile.getPath());
				varfile.mkdirs();
			}

			varfile = new File(uploadPath + saveId + "_" + fileName);
			file.transferTo(varfile);

			FileBody fileBody = new FileBody(varfile);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

			builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
			builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
			builder.addPart("file", fileBody);

			HttpEntity entity = builder.build();
			post.setEntity(entity);
			HttpResponse response = client.execute(post);

			int responseCode = response.getStatusLine().getStatusCode();
			logger.info("responseCode = {}" , responseCode);

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));
			StringBuilder result = new StringBuilder();
			String line = "";
			while ((line = rd.readLine()) != null) {
				// result.append(URLDecoder.decode(line, "UTF-8"));
				result.append(line);
			}

			varfile.delete();

			if (responseCode == 200) {
				return result.toString();
			} else {
				logger.info("API fail : {}" , result);
				return "{ \"status\": \"error\" }";

			}
		} catch (Exception e) {
			logger.error("API exception : {}", e.toString());
			return "{ \"status\": \"error\" }";
		}
	}

}