package ai.square.stt.controller;

import ai.square.common.service.CommonService;
import ai.square.common.util.PropertyUtil;
import ai.square.member.model.MemberVo;
import ai.square.stt.service.SttApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RestController
public class SttApiController {

	@Autowired
	private CommonService commonService;

	@Autowired
	private SttApiService sttApiService;

	@PostMapping(value = "/api/stt", produces = "application/text; charset=utf8")
	public String getApiStt(@RequestParam("file") MultipartFile file,
							HttpServletRequest request) throws IOException {

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();
		String savedId = memberVo.getApiId();

		commonService.recordLog(memberVo.getUserId(), "stt", "api_call", "");

		String result = sttApiService.getApiStt(apiId, apiKey, file, PropertyUtil.getUploadPath() +"/stt/", savedId);
		
		return result;

	}

}
