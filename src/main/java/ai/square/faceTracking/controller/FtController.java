package ai.square.faceTracking.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/faceTracking")
public class FtController {

    private static final Logger logger = LoggerFactory.getLogger(FtController.class);

    /**
     * faceTracking Main
     * @return page_url
     */
    @RequestMapping(value = "/krFaceTrackingMain")
    public String krFaceTracking(HttpServletRequest request) {
        logger.info("Welcome krFaceTrackingMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "faceTrackingMain");
        httpSession.setAttribute("menuName", "API Services");
        httpSession.setAttribute("subMenuName", "얼굴 추적");
        httpSession.setAttribute("menuGrpName", "시각");

        String returnUri = "/kr/faceTracking/faceTrackingMain.pg";

        return returnUri;
    }
}