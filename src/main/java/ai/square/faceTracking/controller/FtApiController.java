package ai.square.faceTracking.controller;

import ai.square.common.service.CommonService;
import ai.square.faceTracking.service.FtService;
import ai.square.common.util.PropertyUtil;
import ai.square.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class FtApiController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private FtService ftService;

    @RequestMapping(value = "/api/faceTracking")
    @ResponseBody
    public ResponseEntity<byte[]> multiFaceTracking(@RequestParam MultipartFile file, HttpServletRequest request) {

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getApiId();

        commonService.recordLog(memberVo.getUserId(), "faceTracking", "api_call", "");

        return ftService.getApiFt(apiId, apiKey, file, PropertyUtil.getUploadPath() + "/faceTracking/", savedId);
    }
}
