package ai.square.faceTracking.service;

import ai.square.common.service.CommonService;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",

})

@Service
public class FtService {

    private static final Logger logger = LoggerFactory.getLogger(FtService.class);

    @Value("${api.url}")
    private String apiDomain;


    public ResponseEntity<byte[]> getApiFt(String apiId, String apiKey, MultipartFile file,
                                           String uploadPath, String saveId){

        if(file == null) {
            throw new RuntimeException("File is not exist!");
        }

        String url = apiDomain + "/Vca/faceTracking";

        String fileName = file.getOriginalFilename();
        fileName = fileName.substring(fileName.lastIndexOf("\\")+1);

        String logMsg = "\n===========================================================================\n";
        logMsg += "FaceTracking API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "video", fileName);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try{
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            File faceTrackingVarFile = new File(uploadPath);

            if(!faceTrackingVarFile.exists()) {
                logger.info("create Dir : {}", faceTrackingVarFile.getPath());
                faceTrackingVarFile.mkdirs();
            }

            faceTrackingVarFile = new File(uploadPath + saveId + "_" + fileName);
            file.transferTo(faceTrackingVarFile);
            FileBody fileBody = new FileBody(faceTrackingVarFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("video", fileBody);

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            if (responseCode != 200) {
                faceTrackingVarFile.delete();
                logger.error("API fail : {}", response.getEntity());
                return null;
            }

            HttpEntity responseEntity = response.getEntity();
            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] resultArray = IOUtils.toByteArray(in);

            ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(resultArray, headers, HttpStatus.OK);

            faceTrackingVarFile.delete();

            return resultEntity;

        } catch (Exception e) {
            logger.error("API exception : {}", e.toString());
            e.printStackTrace();
        }

        return null;
    }
}
