package ai.square.tti.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/tti")
public class TtiController {

	private static final Logger logger = LoggerFactory.getLogger(TtiController.class);

	/**
	 * tti Main
	 * @return
	 */
	@RequestMapping(value = "/krTtiMain")
	public String krTtiMain(HttpServletRequest request) {
		logger.info("Welcome krTtiMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "ttiMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "AI 스타일링");
		httpSession.setAttribute("menuGrpName", "시각");

		String returnUri = "/kr/tti/ttiMain.pg";	
		
		return returnUri;
	}

}