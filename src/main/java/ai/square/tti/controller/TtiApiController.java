package ai.square.tti.controller;

import ai.square.common.service.CommonService;
import ai.square.member.model.MemberVo;
import ai.square.tti.service.TtiApiService;
import ai.square.member.model.MemberVo;
import ai.square.tti.service.TtiApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
 * API Controller
 */

@RestController
public class  TtiApiController {
	private final static Logger logger = LoggerFactory.getLogger(TtiApiController.class);

	@Autowired
	private CommonService commonService;

	@Autowired
	private TtiApiService ttiApiService;
	
	@RequestMapping(value = "/api/getTtiImage")
	@ResponseBody
    public ResponseEntity<byte[]> getTtiImage(@RequestParam(value = "reqText") String reqText, HttpServletRequest request) {

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();

		commonService.recordLog(memberVo.getUserId(), "tti", "api_call", "");

		return ttiApiService.getTtiImage(apiId, apiKey, reqText);
	}	
}
