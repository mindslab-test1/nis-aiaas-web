package ai.square.tti.service;

import java.io.InputStream;

import ai.square.common.service.CommonService;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",

})
@Service
public class TtiApiService {

	private static final Logger logger = LoggerFactory.getLogger(TtiApiService.class);

	@Value("${api.url}")
	private String apiDomain;

    public ResponseEntity<byte[]> getTtiImage(String apiId, String apiKey, String reqText) {

		String url = apiDomain + "/api/tti_image";

		String logMsg = "\n===========================================================================\n";
		logMsg += "TTI API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "reqText", reqText);
		logMsg += "===========================================================================";
		logger.info(logMsg);

    	try {

            JSONObject json = new JSONObject();
            json.put("reqText", reqText);
            json.put("apiId", apiId);
            json.put("apiKey", apiKey);

	    	CloseableHttpClient client = HttpClientBuilder.create().build();
			
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type", "application/json");	
			StringEntity entity = new StringEntity(json.toString());
		    post.setEntity(entity);
			HttpResponse response = client.execute(post);
			int responseCode = response.getStatusLine().getStatusCode();
			logger.info("responseCode = {}" , responseCode);
			
			HttpEntity responseEntity = response.getEntity();

			HttpHeaders headers = new HttpHeaders();
			InputStream in = responseEntity.getContent();
			byte[] imageArray = IOUtils.toByteArray(in);			

			ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(imageArray, headers, HttpStatus.OK);

			return resultEntity;
			
		} catch (Exception e) {
			logger.error("API exception : {}", e.toString());
			e.printStackTrace();
		}
		
		return null;			
	}	

}