package ai.square.voicefilter.service;


import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import ai.square.common.service.CommonService;
import ai.square.common.util.FileConvertUtil;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",

})
@Service
public class VoicefilterService {

    private static final Logger logger = LoggerFactory.getLogger(VoicefilterService.class);

    @Value("${api.url}")
    private String apiDomain;

    public Map<String, Object> getApiVoiceFilter(String apiId, String apiKey, MultipartFile mixedVoice, MultipartFile reqVoice,
                                                    String uploadPath, String saveId) {

        if (mixedVoice == null) {
            throw new RuntimeException("You must select the a file for uploading");
        }

        String url = apiDomain + "/api/dap/voiceFilter";

        String mixedFileName = mixedVoice.getOriginalFilename();
        mixedFileName = mixedFileName.substring(mixedFileName.lastIndexOf("\\")+1);
        String refFileName = reqVoice.getOriginalFilename();
        refFileName = refFileName.substring(refFileName.lastIndexOf("\\")+1);

        String logMsg = "\n===========================================================================\n";
        logMsg += "VoiceFilter API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "mixedVoice", mixedFileName);
        logMsg += String.format(":: %-10s = %s%n", "reqVoice", refFileName);
        logMsg += "===========================================================================";
        logger.info(logMsg);


        try {
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File mixedVarfile = new File(uploadPath + "mixed/");
            File reqVarfile = new File(uploadPath + "req/");


            if (!mixedVarfile .exists()) {
                logger.info("create Dir : {}", mixedVarfile.getPath());
                mixedVarfile .mkdirs();
            }

            if (!reqVarfile .exists()) {
                logger.info("create Dir : {}", reqVarfile.getPath());
                reqVarfile .mkdirs();
            }

            mixedVarfile  = new File(uploadPath + saveId + "_" + mixedFileName);
            reqVarfile  = new File(uploadPath + saveId + "_" + refFileName);

            mixedVoice.transferTo(mixedVarfile);
            reqVoice.transferTo(reqVarfile);

            FileBody mixedFileBody = new FileBody(mixedVarfile);
            FileBody reqFileBody = new FileBody(reqVarfile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("mixedVoice", mixedFileBody);
            builder.addPart("reqVoice", reqFileBody);
            builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            HttpEntity responseEntity = response.getEntity();

            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] wavAudioArray = IOUtils.toByteArray(in);


            // 로컬 서버에 wav 파일 저장
            File voiceFilterFile = new File(uploadPath + "out");

            if(!voiceFilterFile.exists()){
                try {
                    logger.info("create out dir : {}", voiceFilterFile.getPath());
                    voiceFilterFile.mkdirs();
                }catch (Exception e){
                    logger.error("Exception in VoiceFilter service ==> " + e);
                }
            }

            FileOutputStream fos = new FileOutputStream(voiceFilterFile +"/"+ saveId + "_voiceFilter.wav");
            fos.write(wavAudioArray);
            fos.flush();
            fos.close();

            // MP3로 변환 및 return
            File target = new File(voiceFilterFile +"/"+ saveId + "_voiceFilter.wav"); // 파일 저장 경로
            try {
                String refMp3FilePath = FileConvertUtil.changeFileExtesion(reqVarfile.toString());
                String mixedMp3FilePath = FileConvertUtil.changeFileExtesion(mixedVarfile.toString());
                String resultMp3FilePath = FileConvertUtil.changeFileExtesion(target.toString());

                FileConvertUtil.changePermission( reqVarfile.toString() );
                FileConvertUtil.changePermission( mixedVarfile.toString() );
                FileConvertUtil.changePermission( target.toString() );

                FileConvertUtil.convertWavToMp3( reqVarfile.toString(), refMp3FilePath );
                FileConvertUtil.convertWavToMp3( mixedVarfile.toString(), mixedMp3FilePath );
                FileConvertUtil.convertWavToMp3( target.toString(), resultMp3FilePath );

                File refMp3file = new File(refMp3FilePath);
                File mixedMp3file = new File(mixedMp3FilePath);
                File resultMp3file = new File(resultMp3FilePath);

                FileInputStream refInputStream = new FileInputStream(refMp3file);
                FileInputStream mixedInputStream = new FileInputStream(mixedMp3file);
                FileInputStream resultInputStream = new FileInputStream(resultMp3file);

                byte[] refMp3AudioArray = IOUtils.toByteArray(refInputStream);
                byte[] mixedMp3AudioArray = IOUtils.toByteArray(mixedInputStream);
                byte[] resultMp3AudioArray = IOUtils.toByteArray(resultInputStream);

                headers.setCacheControl(CacheControl.noCache().getHeaderValue());

                ResponseEntity<byte[]> refMp3Entity = new ResponseEntity<>(refMp3AudioArray, headers, HttpStatus.OK);
                ResponseEntity<byte[]> mixedMp3Entity = new ResponseEntity<>(mixedMp3AudioArray, headers, HttpStatus.OK);
                ResponseEntity<byte[]> resultMp3Entity = new ResponseEntity<>(resultMp3AudioArray, headers, HttpStatus.OK);
                ResponseEntity<byte[]> wavEntity = new ResponseEntity<>(wavAudioArray, headers, HttpStatus.OK);

                Map<String, Object> resultMap = new HashMap<>();

                resultMap.put("refMp3File", refMp3Entity);
                resultMap.put("mixedMp3File", mixedMp3Entity);
                resultMap.put("mp3File", resultMp3Entity);
                resultMap.put("wavFile", wavEntity);

                reqVarfile.delete();
                refMp3file.delete();
                mixedVarfile.delete();
                mixedMp3file.delete();
                target.delete();
                resultMp3file.delete();

                logger.info(" @ VoiceFilter service delete MP3 & WAV files complete! ==> apiId : {}", apiId);

                return resultMap;

            } catch (IOException e) {
                logger.error("Exception in VoiceFilter service's file writing process ! : {}" + e);
            }

        } catch (Exception e) {
            logger.error("API exception : {}", e.toString());
            e.printStackTrace();
        }
        return null;
    }

}