package ai.square.voicefilter.controller;

import ai.square.common.service.CommonService;
import ai.square.voicefilter.service.VoicefilterService;
import ai.square.common.util.PropertyUtil;
import ai.square.member.model.MemberVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/*
 * API Controller
 */


@RestController
public class VoicefilterApiController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private VoicefilterService voicefilterService;

    @PostMapping("/api/dap/voicefilter")
    @ResponseBody
    public Map<String, Object> getApiVoiceFilter(@RequestParam("mixedVoice") MultipartFile mixedVoice,
                                                 @RequestParam("reqVoice")MultipartFile reqVoice , HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);

        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getApiId();

        commonService.recordLog(memberVo.getUserId(), "voiceFilter", "api_call", "");

        return voicefilterService.getApiVoiceFilter(apiId, apiKey, mixedVoice, reqVoice,
                                                    PropertyUtil.getUploadPath()+"/voiceFilter/", savedId);
    }
	
}
