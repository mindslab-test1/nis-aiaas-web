package ai.square.voicefilter.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/voicefilter")
public class VoicefilterController {

	private static final Logger logger = LoggerFactory.getLogger(VoicefilterController.class);

	/**
	 * Voicefilter Main
	 * @return
	 */
	@RequestMapping(value = "/krVoicefilterMain")
	public String krVoicefilterMain(HttpServletRequest request) {
		logger.info("Welcome krVoicefilterMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "voicefilterMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "Voice Filter");
		httpSession.setAttribute("menuGrpName", "음성");

		String returnUri = "/kr/voicefilter/voicefilterMain.pg";	
		
		return returnUri;
	}
}