package ai.square.chatbot.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ai.square.chatbot.service.ChatbotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/chatbot")
public class ChatbotController {

	private static final Logger logger = LoggerFactory.getLogger(ChatbotController.class);

	@Autowired
	private ChatbotService chatbotService;
	
	/**
	 * chatbot Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krChatbotMain")
	public String krChatbotMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krChatbotMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "chatbotMain");
		httpSession.setAttribute("menuName", "API Services");	
		httpSession.setAttribute("subMenuName", "날씨봇 · 위키봇");	
		httpSession.setAttribute("menuGrpName", "대화");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/chatbot/chatbotMain.pg";
		
		return returnUri;
	}

	/**
	 * chatbot Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enChatbotMain")
	public String enChatbotMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enChatbotMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "chatbotMain");
		httpSession.setAttribute("menuName", "API Services");	
		httpSession.setAttribute("subMenuName", "Ready-made Bots");	
		httpSession.setAttribute("menuGrpName", "Conversation");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/chatbot/chatbotMain.pg";
		
		return returnUri;
	}
}
