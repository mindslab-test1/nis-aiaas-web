package ai.square.common.util;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",

})
public class Util {

    protected Util() {
        throw new UnsupportedOperationException(); // prevents calls from
        // subclass
    }

    /**
     * SimpleDateFormat, yyyyMMdd
     */
    static SimpleDateFormat sdf;
    /**
     * SimpleDateFormat, yyyyMMddHHmmss
     */
    static SimpleDateFormat stf;
    /**
     * SimpleDateFormat, yyyy-MM-dd HH:mm:ss
     */
    static SimpleDateFormat stfs;

    static {
        sdf = getDateFormat("yyyyMMdd", Locale.getDefault());
        stf = getDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        stfs = getDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    }

    /**
     * 원하는 날짜 형식으로 SimpleDateFormat 설정하기.
     *
     * @param s 날짜 형식.(예)"yyyy-MM-dd HH:mm:ss"
     * @param locale 로케일 정보
     * @return SimpleDateFormat
     */
    public static SimpleDateFormat getDateFormat(String s, Locale locale) {
        SimpleDateFormat simpledateformat = new SimpleDateFormat(s, locale);
        simpledateformat.setTimeZone(TimeZone.getDefault());
        return simpledateformat;
    }

    /**
     * "yyyyMMdd" 형식으로 date 일자 가져오기
     *
     * @param date Date 객체
     * @return date 일자 문자열
     */
    public static String format(Date date) {
        return sdf.format(date);
    }

    /**
     * "yyyyMMddHHmmss" 형식으로 date 일시 가져오기
     *
     * @param date Date 객체
     * @return date 일시 문자열
     */
    public static String formatTime(Date date) {
        return stf.format(date);
    }

    /**
     * "yyyy-MM-dd HH:mm:ss" 형식으로 date 일시 가져오기
     *
     * @param date Date 객체
     * @return date 일시 문자열
     */
    public static String formatTimeDisp(Date date) {
        return stfs.format(date);
    }

    /**
     * String형으로 전달받은시간 정보를 원하는 포맷형식으로 리턴해주기.
     * @param yyyymmddhh24miss 년월일시분초
     * @param format "yyyy-MM-dd HH:mm:ss"같은 자바 날짜 포맷형식
     * @return format으로 전달받은 날짜 포맷 형식으로 변환된 시간
     */
    public static String getFormatTime(String yyyymmddhh24miss, String format) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(Integer.parseInt(yyyymmddhh24miss.substring(0, 4)), Integer.parseInt(yyyymmddhh24miss.substring(4, 6)), Integer.parseInt(yyyymmddhh24miss.substring(6, 8)), Integer.parseInt(yyyymmddhh24miss.substring(8, 10)), Integer.parseInt(yyyymmddhh24miss.substring(10, 12)), Integer.parseInt(yyyymmddhh24miss.substring(12, 14)));
        return getDateFormat(format, Locale.getDefault()).format(cal.getTime());
    }

    /**
     * 현재시간을 정보를 원하는 포맷형식으로 리턴해주기.
     * @param format "yyyy-MM-dd HH:mm:ss"같은 자바 날짜 포맷형식
     * @return format으로 전달받은 날짜 포맷 형식으로 변환된 시간
     */
    public static String getFormatCurrTime(String format) {
        GregorianCalendar cal = new GregorianCalendar();
        return getDateFormat(format, Locale.getDefault()).format(cal.getTime());
    }

    /**
     * String 으로 넘어온 8자리 숫자를 yyyy.MM.DD 형식으로 변환하여 리턴.
     */
    public static String formatTimeDot(String date) {
        if (date.length() < 8) {
            String g = "";
            return g;
        } else {
            String a = date.substring(0, 4);
            String b = date.substring(4, 6);
            String c = date.substring(6, 8);
            String d = a + "." + b + "." + c;
            return d;
        }
    }

    public static String formatTimeSign(String date, String sign) {
        if (date.length() < 8) {
            String g = "";
            return g;
        } else {
            String a = date.substring(0, 4);
            String b = date.substring(4, 6);
            String c = date.substring(6, 8);
            String d = a + sign + b + sign + c;
            return d;
        }
    }

    /**
     * Calendar 객체 생성
     *
     * @return Calendar
     */
    public static Calendar getCalendar() {
        return Calendar.getInstance();
    }

    /**
     * Calendar 객체 생성
     *
     * @param date
     *            Date 객체
     * @return Calendar
     */
    public static Calendar getCalendar(Date date) {
        Calendar calendar = getCalendar();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * "yyyyMMdd" 형식으로 date 일자 가져오기
     *
     * @return 오늘 일자
     */
    public static String getToDay() {
        Calendar ca = getCalendar();
        Date da = ca.getTime();
        return format(da);

    }

    /**
     * "yyyyMMddHHmmss" 형식으로 현재 일시 가져오기
     *
     * @return 현재 일시 문자열
     */
    public static String getToDayTime() {
        Calendar ca = getCalendar();
        Date da = ca.getTime();
        return formatTime(da);
    }

    /**
     * "yyyy-MM-dd HH:mm:ss" 형식으로 현재 일시 가져오기
     *
     * @return 현재 일시 문자열
     */
    public static String getToDayTimeDisp() {
        Calendar ca = getCalendar();
        Date da = ca.getTime();
        return formatTimeDisp(da);
    }

    /**
     * 현재 일자에서 지정한 개월수 이전의 일자 가져오기
     *
     * @param submonth 이전 개월수
     * @return "yyyyMMdd" 형식의 해당 일자일
     */
    public static String getPrevToMonth(int submonth) {
        Calendar ca = getCalendar();
        ca.set(ca.get(Calendar.YEAR), ca.get(Calendar.MONTH) - submonth, ca.get(Calendar.DATE));
        Date da = ca.getTime();

        return format(da);
    }

    /**
     * 현재 일자에서 지정한 개월수 이후의 일자 가져오기
     *
     * @param submonth 이후 개월수
     * @return "yyyyMMdd" 형식의 해당 일자일
     */
    public static String getMoveToMonth(int submonth) {
        Calendar ca = getCalendar();
        ca.set(ca.get(Calendar.YEAR), ca.get(Calendar.MONTH) + submonth, ca.get(Calendar.DATE));
        Date da = ca.getTime();

        return format(da);
    }

    /**
     * 현재 일자에서 지정한 개월수 이후의 일자 -1일 가져오기
     *
     * @param submonth
     *            이후 개월수
     * @return "yyyyMMdd" 형식의 해당 일자일
     */

    public static String getMoveToMonthBfDay(int submonth) {
        Calendar ca = getCalendar();
        ca.set(ca.get(Calendar.YEAR), ca.get(Calendar.MONTH) + submonth, ca.get(Calendar.DATE) - 1);
        Date da = ca.getTime();

        return format(da);
    }

    /**
     * 년월 정보를 스트링형으로 넘긴 후 그 년월 정보를 기준으로 몇달전, 몇개월 후의 년월값을 구하기.
     *
     * @param yyyymm
     *            기준 년월
     * @param submonth
     *            음수면 이전 월, 양수면 이후 월, 예) -1이면 1개월 전. +3 이면 3개월 후
     * @return 새로운 연월 정보가 YYYYMM 형식으로
     */
    public static String getMoveToMonth(String yyyymm, int submonth) {
        Calendar ca = getCalendar();
        int iY = 0;
        int iM = 0;

        if (yyyymm.length() < 6) {
            return "00000000";
        } else {
            iY = chkNum(yyyymm.substring(0, 4)) ? Integer.parseInt(yyyymm.substring(0, 4)) : 0;
            iM = chkNum(yyyymm.substring(4, 6)) ? Integer.parseInt(yyyymm.substring(4, 6)) - 1 : -1; // 월은
            // 0부터
            // 시작
        }

        if (iY < 1 || iM < 0) { // 월은 0부터 시작
            return "00000000";
        }
        ca.set(iY, iM + submonth, 1);
        Date da = ca.getTime();

        return format(da);
    }

    /**
     * 날짜,시각호출(GregorianCalendar)
     *
     * @return 현재 시간이 YYYYMMDDHH24MISS 형식으로
     */
    public static String getPlainDateTime() {
        GregorianCalendar cal = new GregorianCalendar();
        StringBuffer date = new StringBuffer();
        date.append(cal.get(1));
        if (cal.get(2) < 9) date.append('0');
        date.append(cal.get(2) + 1);
        if (cal.get(5) < 10) date.append('0');
        date.append(cal.get(5));
        if (cal.get(11) < 10) date.append('0');
        date.append(cal.get(11));
        if (cal.get(12) < 10) date.append('0');
        date.append(cal.get(12));
        return date.toString();
    }

    /**
     * 숫자 검증
     *
     * @param value
     *            검증할 문자열
     * @return boolean형 : true면 숫자만, false면 문자가 껴있음
     * @author 1010
     */
    public static boolean chkNum(String value) {
        boolean numOk = false;
        String sVerifi = value.trim();
        int minus = 0;
        int plus = 0;
        int dot = 0;
        char c = 0;
        for (int i = 0; i < sVerifi.length(); i++) {
            c = sVerifi.charAt(i);
            if (c >= '0' && c <= '9') { // 숫자
                numOk = true;
            } else if (c == '-') { // 예외허용1 : 음수 표시 기호
                minus++;
            } else if (c == '+') { // 예외허용2 : 양수 표시 기호
                plus++;
            } else if (c == '.') { // 예외허용3 : 소수점 표시 기호
                dot++;
            } else {
                numOk = false;
                return numOk;
            }
        }
        if (minus > 1 || dot > 1 || plus > 1) { // 하나의 숫자에 예외 허용은 한 종류에 하나만
            // 허용한다.
            return false; // 둘 이상일 경우 하나를 제외하고는 나머지는 기호가 아니라 문자일 가능성이 절대적임.
        }
        return numOk;
    }

    /**
     * 결제금액검증
     *
     * @param value
     *            검증할 문자열
     * @return boolean형 : true면 결제가능 숫자만
     * @author 1010
     */
    public static boolean chkPay(String value) {
        if (value==null || value.equals("")) return false;
        boolean numOk = false;
        String sVerifi = value.trim();
        //int minus = 0;
        //int plus = 0;
        int dot = 0;
        char c = 0;
        for (int i = 0; i < sVerifi.length(); i++) {
            c = sVerifi.charAt(i);
            if (c >= '0' && c <= '9') { // 숫자
                numOk = true;
            } else if (c == '.') { // 예외허용 : 소수점 표시 기호
                dot++;
            } else {
                numOk = false;
                return numOk;
            }
        }
        if (dot > 1) { // 하나의 숫자에 예외 허용은 한번만 허용한다
            return false;
        }
        //경민씨 요청으로 주석 PRMP-3628
        //if (Double.parseDouble(sVerifi) < 2000 || (Double.parseDouble(sVerifi) % 1000 != 0)) { return false; }
        return numOk;
    }

    /**
     * 결제금액검증(10원이상)
     *
     * @param value
     *            검증할 문자열
     * @return boolean형 : true면 결제가능 숫자만
     * @author 1010
     */
    public static boolean chkPayZero(String value) {
        boolean numOk = false;
        String sVerifi = value.trim();
        //int minus = 0;
        //int plus = 0;
        int dot = 0;
        char c = 0;
        for (int i = 0; i < sVerifi.length(); i++) {
            c = sVerifi.charAt(i);
            if (c >= '0' && c <= '9') { // 숫자
                numOk = true;
            } else if (c == '.') { // 예외허용 : 소수점 표시 기호
                dot++;
            } else {
                numOk = false;
                return numOk;
            }
        }
        if (dot > 1) { // 하나의 숫자에 예외 허용은 한번만 허용한다
            return false;
        }
        if (Double.parseDouble(sVerifi) < 10 || (Double.parseDouble(sVerifi) % 10 != 0)) { return false; }
        return numOk;
    }

    /**
     * 문자열이 널이나 공백일 경우 지정 문자열로 치환.
     *
     * @param str
     *            비교 대상 문자열
     * @param defaultStr
     *            치환 문자열
     * @return 문자열
     */
    public static String chkNull(String str, String defaultStr) {
        if (str != null && !str.equals("")) {
            return str;
        } else {
            return defaultStr;
        }
    }

    /**
     * 문자열이 널이나 공백일 경우 defaultStr로 치환, 공백이 아닐경우 changeStr로 치환.
     *
     * @param str
     *            비교 대상 문자열
     * @param defaultStr
     *            치환 문자열
     * @param changeStr
     *            치환 문자열
     * @return 문자열
     */
    public static String replaceStr(String str, String defaultStr, String changeStr) {
        if (str != null && !str.equals("")) {
            return changeStr;
        } else {
            return defaultStr;
        }
    }

    /**
     * 문자열이 널일 경우 ""으로 치환.
     *
     * @param str
     *            비교 대상 문자열
     * @return 문자열
     */
    public static String chkNull(String str) {
        return chkNull(str, "");
    }

    /**
     * 오브젝트의 널 여부 체크. 널이면 true를 리턴.
     *
     * @param obj
     *            비교 대상 객체
     * @return 문자열
     */
    public static boolean isNull(Object obj) {
        if (obj != null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 해당 객체의 널 여부를 파악하고, 널일 경우 공백으로, 널이 아닌 경우 해당 객체를 String로 변환 후 리턴.
     *
     * @param obj
     *            변환 대상 객체.
     * @return String형 값.
     */
    public static String getReqAtt(Object obj) {
        if (obj == null) {
            return "";
        } else {
            return (String) obj;
        }
    }

    /**
     * 해당 객체의 널 여부를 파악하고, 널일 경우 defaultValue로, 널이 아닌 경우 해당 객체를 String형으로 변환 후
     * 리턴.
     *
     * @param obj
     *            변환 대상 객체.
     * @param defaultValue
     *            널일 경우 대입할 기본 값.
     * @return String형 값.
     */
    public static String getReqAtt(Object obj, String defaultValue) {
        if (obj == null) {
            return defaultValue;
        } else {
            return (String) obj;
        }
    }

    /**
     * 해당 객체의 널 여부를 파악하고, 널일 경우 defaultValue로, 널이 아닌 경우 해당 객체를 int형으로 변환 후 리턴.
     *
     * @param obj
     *            변환 대상 객체.
     * @param defaultValue
     *            널일 경우 대입할 기본 값.
     * @return int형 값.
     */
    public static int getReqAtt(Object obj, int defaultValue) {
        if (obj == null) {
            return defaultValue;
        } else {
            int retVal = 0;
            try {
                retVal = Integer.parseInt((String) obj);
            } catch (ClassCastException cce) {
                retVal = (Integer) obj;
            } catch (Exception e) {
                retVal = defaultValue;
            }
            return retVal;
        }
    }

    /**
     * 해당 객체의 널 여부를 파악하고, 널일 경우 defaultValue로, 널이 아닌 경우 해당 객체를 long형으로 변환 후 리턴.
     *
     * @param obj
     *            변환 대상 객체.
     * @param defaultValue
     *            널일 경우 대입할 기본 값.
     * @return long형 값.
     */
    public static long getReqAtt(Object obj, long defaultValue) {
        if (obj == null) {
            return defaultValue;
        } else {
            long retVal = 0;
            try {
                retVal = Long.parseLong((String) obj);
            } catch (ClassCastException cce) {
                retVal = (Long) obj;
            } catch (Exception e) {
                retVal = defaultValue;
            }
            return retVal;
        }
    }

    /**
     * 프로퍼티 값 가져오기
     *
     * @param sc
     *            ServletContext
     * @param properyKey
     *            properyKey
     * @return value
     * @throws IOException
     *             IOException
     */
    public static String getProp(ServletContext sc, String properyKey) throws IOException {
        String value = "";

        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
        PropertiesFactoryBean conf = ctx.getBean(PropertiesFactoryBean.class);

        value = conf.getObject().getProperty(properyKey);

        return value;
    }

    /**
     * 프로퍼티 값 가져오기
     *
     * @param ctx
     *            WebApplicationContext
     * @param properyKey
     *            properyKey
     * @return value
     * @throws IOException
     *             IOException
     */
    public static String getProp(WebApplicationContext ctx, String properyKey) throws IOException {
        String value = "";

        PropertiesFactoryBean conf = ctx.getBean(PropertiesFactoryBean.class);

        value = conf.getObject().getProperty(properyKey);

        return value;
    }

    /**
     * 프로퍼티 값 가져오기 (예외처리를 내부에서 함.)
     *
     * @param ctx
     *            WebApplicationContext
     * @param properyKey
     *            properyKey
     * @return value
     */
    public static String getPropNoThrows(WebApplicationContext ctx, String properyKey) {
        String value = "";

        try {
            PropertiesFactoryBean conf = ctx.getBean(PropertiesFactoryBean.class);
            value = conf.getObject().getProperty(properyKey);

            if (value == null) {
                value = "";
            }
        } catch (Exception e) {
            value = "";
        }

        return value;
    }

    /**
     *
     *
     * @param str
     * @param pattern
     * @param replace
     * @return
     */
    public static String replace(String str, String pattern, String replace) {
        int s = 0, e = 0;

        StringBuffer result = new StringBuffer();

        while ((e = str.indexOf(pattern, s)) >= 0) {
            result.append(str.substring(s, e));
            result.append(replace);
            s = e + pattern.length();
        }

        result.append(str.substring(s));
        return result.toString();
    }

    /**
     *
     *
     * @param str
     * @param limit
     * @return
     */
    public static String shortCutString(String str, int limit) {

        if (str == null) return str;

        if (limit <= 0) { return str; }

        byte[] strbyte = str.getBytes();

        if (strbyte.length <= limit) { return str; }

        char[] charArray = str.toCharArray();

        int checkLimit = limit;
        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] < 256) {
                checkLimit -= 1;
            } else {
                checkLimit -= 2;
            }

            if (checkLimit <= 0) {
                break;
            }
        }

        byte[] newByte = new byte[limit + checkLimit];

        for (int i = 0; i < newByte.length; i++) {
            newByte[i] = strbyte[i];
        }

        return new String(newByte);

    }

    /**
     *
     *
     * @param strArg
     * @return
     */
    public static String getPhoneFormat(String strArg) {
        String strNumber = replace(strArg, "-", "");
        try {
            if (strNumber.length() > 3) {
                String strFir = strNumber.substring(0, 3);
                if (strNumber.substring(0, 2).equals("02")) {
                    strNumber = strNumber.substring(0, 2) + "-" + strNumber.substring(2, strNumber.length() - 4) + "-" + strNumber.substring(strNumber.length() - 4, strNumber.length());
                } else if (strFir.equals("010") || strFir.equals("011") || strFir.equals("016") || strFir.equals("017") || strFir.equals("018") || strFir.equals("019") || strNumber.length() == 10 || strNumber.length() == 11) {
                    strNumber = strNumber.substring(0, 3) + "-" + strNumber.substring(3, strNumber.length() - 4) + "-" + strNumber.substring(strNumber.length() - 4, strNumber.length());
                } else if (strFir.equals("158")) {
                    // strNumber = strNumber.substring(0,4) + "-" +
                    // strNumber.substring(strNumber.length()-3,strNumber.length());
                    strNumber = strNumber.substring(0, 4) + "-" + strNumber.substring(strNumber.length() - 4, strNumber.length());
                } else {
                    if (strNumber.length() == 7) {
                        strNumber = strNumber.substring(0, 3) + "-" + strNumber.substring(strNumber.length() - 4, strNumber.length());
                    } else if (strNumber.length() == 8) {
                        strNumber = strNumber.substring(0, 4) + "-" + strNumber.substring(strNumber.length() - 4, strNumber.length());
                    }
                }
            }
        } catch (Exception e) {
        }
        return strNumber;
    }

    /**
     *
     *
     * @param data
     * @return
     */
    public static String insertComma(String data) {
        StringBuffer sb = new StringBuffer();
        int loc = data.length() - 1;
        int cursor = 0;
        char c = 0;
        for (int i = 0; i < data.length(); i++) {
            cursor = loc - i;
            c = data.charAt(cursor);
            sb.insert(0, c);
            if (i % 3 == 2) {
                if (i != loc) {
                    sb.insert(0, ',');
                }
            }
        }
        String result = sb.toString();
        return result;
    }

    /**
     *
     *
     * @return
     */
    public static long getStamp() {
        Date date = new Date();
        return date.getTime();
    }

    /**
     * @desc determine give file is modified after last access
     * @author haward
     */
    public static boolean isModified(String fileName, long lastModified) throws Exception {

        boolean needUpdate = false;

        File file = new File(fileName);

        if (!file.canRead()) { throw new Exception("Util Can't open file: " + fileName); }

        if (lastModified != file.lastModified()) {

            needUpdate = true;

        }

        return needUpdate;

    }

    public static long getModified(String fileName) {

        File file = new File(fileName);

        return file.lastModified();

    }

    /**
     * replaceAll 명령어. 스트링버퍼로 변환해서 치환하므로 빠르다?
     *
     * @param str 원문
     * @param oldStr 치환 대상 단어
     * @param newStr 치환할 단어
     * @return 치환환 문자열
     */
    public static String replaceString(String str, String oldStr, String newStr) {
        if (str == null) { return null; }
        if (oldStr == null || newStr == null || oldStr.length() == 0) { return str; }

        int i = str.lastIndexOf(oldStr);
        if (i < 0) return str;

        StringBuffer sbuf = new StringBuffer(str);

        while (i >= 0) {
            sbuf.replace(i, (i + oldStr.length()), newStr);
            i = str.lastIndexOf(oldStr, i - 1);
        }
        return sbuf.toString();
    }

    /**
     * @param str
     * @return
     */
    public static String emptyNull(String str) {

        if (str == null) str = "";

        return str;
    }

    /**
     *
     *
     * @param str
     * @return
     */
    public static boolean isEmpty(String str){
		if(str != null && !"".equals(str.trim())){
			return false;
		}

		return true;
	}

    /**
     *
     *
     * @param prefix_userid
     * @param id
     * @return
     */
	public static String inputPrefixUserid(String prefix_userid, Object id) {
		String tempid = emptyNull((String) id);

		if (!("".equals(tempid)))
			tempid = prefix_userid + tempid;
		return tempid;
	}

    /**
     *
     *
     * @param prefix_userid
     * @param id
     * @return
     */
    public static String outputPrefixUserid(String prefix_userid, Object id) {
        return (emptyNull((String) id)).replaceFirst(prefix_userid, "");
    }

    /**
     * 토큰으로 연결된 스트링을 스트링 배열로 변환
     *
     * @param String s 변환할 스트링
     * @param String t 토큰
     * @return String[] 배열
     */
	public static String[] stringToArray(String s, String t) {
		if (isNull(s))
			return null;

		StringTokenizer st = new StringTokenizer(s, t);
		int size = st.countTokens();
		if (size <= 0)
			return null;

		String[] result = new String[size];
		for (int i = 0; i < size && st.hasMoreTokens(); i++) {
			result[i] = st.nextToken();
		}
		return result;
	}

	public static String stringCheck(String str) {
		if (str == null || str.equals("")) {
			return "";
		}

		str = str.replaceAll("'", "&#39;");
		str = str.replaceAll("--", "__");
		str = str.replaceAll("<", "&lt;");
		str = str.replaceAll(">", "&gt;");
		str = str.replaceAll("/", "&#47;");
		str = str.replaceAll("[(]", "&#40;");
		str = str.replaceAll("[)]", "&#41;");
		str = str.replaceAll("\"", "&quot;");

		return str;
    }

	public static String stringCheckReverse(String str) {
		if (str == null || str.equals("")) {
			return "";
		}

		str = str.replaceAll("&#39;", "'");
		str = str.replaceAll("__", "--");
		str = str.replaceAll("&lt;", "<");
		str = str.replaceAll("&gt;", ">");
		str = str.replaceAll("&#47;", "/");
		str = str.replaceAll("&#40;", "[(]");
		str = str.replaceAll("&#41;", "[)]");
		str = str.replaceAll("\"", "&quot;");

		return str;
	}

    /**
     * 배열로 넘어온 파라미터 키를 querystring문자열로 변환
     *
     * @param String s 변환할 스트링
     * @param String t 토큰
     * @return String[] 배열
     */
    public static String makeQueryString(String[] paramNames, HttpServletRequest request) {
    	StringBuffer sb = new StringBuffer();
    	String query = "";
    	String s = null;

	    	for(int i = 0, n = 0; i < paramNames.length; i++){
	    		s = request.getParameter(paramNames[i]);
	    		if(s != null && !"".equals(s.trim())){
	    			try {
	    				if (n > 0) {
	    					sb.append("&");
	    				}
						query = sb.append(paramNames[i]).append("=").append(URLEncoder.encode(s, "UTF-8")).toString();
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
	    			n++;
	    		}
	    	}

    	return query;
    }

    /**
     * 배열로 넘어온 파라미터 키를 querystring문자열로 변환
     *
     * @param String s 변환할 스트링
     * @param String t 토큰
     * @return String[] 배열
     */
    public static String htmlcrop(String str, String tail) {
    	if (str == null) {
    		return "";
    	}

    	//int remain = 0;
		StringBuffer result = new StringBuffer();

		//int position = 0;
		int bIndex = -1;
		int eIndex = -1;
		String temp = str;

		while (temp.length() > 0) {
			bIndex = temp.indexOf("<");
			eIndex = temp.indexOf(">", bIndex);

			if (bIndex > -1 && eIndex > bIndex) {
				if (bIndex > 0) result.append(temp.substring(0, bIndex));
				temp = temp.substring(eIndex + 1, temp.length());
			} else {
				result.append(temp);
				temp = "";
			}
		}
		return result.toString();
	}

    public static String crop(String source, int length, String tail) {
    	int remain = 0;
		if (source == null) return "";

		String result = source;
		int sLength = 0;
		int bLength = 0;
		char c;

		if ( result.getBytes().length > length) {
			while ( (bLength + 1)  < length) {
				c = result.charAt(sLength);
				bLength++;
				sLength++;
				if (c > 127) bLength++;
			}
			result = result.substring(0, sLength) + tail;

		}

		remain = remain - result.length();
		return result;
	}


    /**
	 * ARIA 암호화
	 * @param str
	 * @return
	 * @throws InvalidKeyException
	 * @throws UnsupportedEncodingException

	public static String encryptARIA(String str) throws InvalidKeyException, UnsupportedEncodingException{

		String ret = "";
		if (str != null && !str.equals("")){
			byte[] mk = new byte[32];
			String key = "";	//32자

			if(str != null  && !str.equals("")){
				mk = key.getBytes();
				//AriaCipher cipher = new AriaCipher();
				ret = AriaCipher.encrypt(str, mk, null);
			}
		}
		return ret;
	}

	/**
	 * ARIA 복호화
	 * @param str
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeyException

	public static String decryptARIA(String str) throws InvalidKeyException,
			UnsupportedEncodingException {

		String ret = "";
		if (str != null && !str.equals("")){
			byte[] mk = new byte[32];
			String key = "";	//32자

			if(str != null  && !str.equals("")){
				mk = key.getBytes();
				//AriaCipher cipher = new AriaCipher();
				ret = AriaCipher.decrypt(str, mk, null);
			}
		}
		return ret;
	}

	public static byte[] hexToByte(String hex) {
		if (hex == null || hex.length() == 0)
			return null;

		byte[] ba = new byte[hex.length() / 2];

		for (int i = 0; i < ba.length; i++)
			ba[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		return ba;

	}
	*/

	/**
	 * byte to hex
	 * @param ba
	 * @return
	 */
	public static String byteToHex(byte[] ba) {
		if (ba == null || ba.length == 0)
			return null;

		String hexNumber = "";
		StringBuffer sb = new StringBuffer(ba.length * 2);

		for (int x = 0; x < ba.length; x++) {
			hexNumber = "0" + Integer.toHexString(0xff & ba[x]);
			sb.append(hexNumber.substring(hexNumber.length() - 2));

		}

		return sb.toString();
	}

	public static boolean isHangul(char ch) {
		Character.UnicodeBlock block = Character.UnicodeBlock.of(ch);

		if (block == Character.UnicodeBlock.HANGUL_SYLLABLES
				|| block == Character.UnicodeBlock.HANGUL_JAMO
				|| block == Character.UnicodeBlock.HANGUL_COMPATIBILITY_JAMO) {
			return true;
		}
		return false;
	}




	/**
	 * 랜덤숫자생성
	 * @param len 자리수
	 * @return 랜덤숫자문자열
	 */
	public static String getRandomNum(int len) {

		char[] tmpChar = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < len; i++) {
			int seq = (int) (tmpChar.length * Math.random());
			sb.append(tmpChar[seq]);
		}

		return sb.toString();
	}



    /**
     * 구분자로 넘어온 문자열을 랜덤 배열로 리턴한다.
     * @param str
     * @return
     */
    public static String[] getRandomArray(String str) {
        if (!"".equals(str)) {
            String[] alpb = str.split(",");
            int[] arrNum = getRandomNumber(alpb.length);
            String[] result = new String[alpb.length];

            for(int i=0; i<arrNum.length; i++){
                result[i] = alpb[(arrNum[i]-1)];
            }

            return result;
        } else {
            return null;
        }
    }


    /**
     * 정해진 길이 안에서 배열 숫자를 가져온다.
     * @param length
     * @return
     */
    public static int[] getRandomNumber(int length){
        int[] arr = new int[length];
        int ran = 0;
        boolean cheak;
        Random r = new Random();

        for (int i = 0; i < arr.length; i++) {
            ran = r.nextInt(length) + 1;
            cheak = true;
            for (int j = 0; j < i; j++) {
                if (arr[j] == ran) {
                    i--;
                    cheak = false;
                }
            }
            if (cheak){
                arr[i] = ran;
            }
        }

        return arr;
    }



	/**
	 * @param txt
	 * @return
	 * @throws Exception
	 */
	public static String getEncSHA256(String txt) throws Exception{
	    StringBuffer sbuf = new StringBuffer();

	    MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
	    mDigest.update(txt.getBytes());

	    byte[] msgStr = mDigest.digest() ;

	    for(int i=0; i < msgStr.length; i++){
	        byte tmpStrByte = msgStr[i];
	        String tmpEncTxt = Integer.toString((tmpStrByte & 0xff) + 0x100, 16).substring(1);

	        sbuf.append(tmpEncTxt) ;
	    }

	    return sbuf.toString();
	}



}
