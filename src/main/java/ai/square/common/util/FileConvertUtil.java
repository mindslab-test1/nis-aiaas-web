package ai.square.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class FileConvertUtil {
    private static final Logger logger = LoggerFactory.getLogger(FileConvertUtil.class);

    /* ======================================================================================================================= */
    // wav 파일 확장자를 mp3로 변환
    /* ======================================================================================================================= */
    public static String changeFileExtesion(String orgFilePath){

        if(orgFilePath.lastIndexOf(".") != -1){
            String extension = orgFilePath.substring(orgFilePath.lastIndexOf("."));
            logger.info("file extension : {}",extension);

            if(extension.equals(".wav")){
                orgFilePath = orgFilePath.replace(".wav", ".mp3");
            }
            else
                orgFilePath += ".mp3";
        }else{
            orgFilePath += ".mp3";
        }

        logger.info("changed file path : {}",orgFilePath);
        return orgFilePath;
    }


    /* ======================================================================================================================= */
    // wav 파일의 접근 권한 변경
    /* ======================================================================================================================= */
    public static void changePermission(String wavName) {
        Runtime rt = Runtime.getRuntime();
        Process pc = null;

        try {
            pc = rt.exec("chmod 777 " + wavName);
            pc.waitFor();
            pc.destroy();
            logger.info(" @ changePermission complete! {}", wavName);

        } catch (IOException | InterruptedException e) {
            logger.error("Exception in changePermission : " + wavName);
        }
    }

    /* ======================================================================================================================= */
    // lame을 이용한 파일 변환 (wav --> mp3)
    /* ======================================================================================================================= */

    public static void convertWavToMp3(String wavName, String mp3Name) {

        Runtime rt = Runtime.getRuntime();
        Process pc = null;

        try {
            //외부 프로세스 실행
            pc = rt.exec("lame " + wavName + " " + mp3Name);
            pc.waitFor();
            pc.destroy();
            logger.info(" @ convertWavToMp3 complete! {} ==> {}", wavName, mp3Name);

        } catch (Exception e) {
            logger.error("Exception in TTS service's writeWavToMp3 : " + e.getMessage() + " ==> Name : " + wavName);
        }
    }


}
