package ai.square.common.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",

})
@Component
public class PropertyUtil {

    @Value("${file.upload}")
    String mUploadPath;
    static String mUploadPath_Static;

    @PostConstruct
    public void init() {
        mUploadPath_Static = mUploadPath;
    }

    /*
    ** 파일 업로드 패스
    ** 윈도우즈 환경인 경우, 드라이브 명이 존재하지 않으면 에러가 발생하여 함.
    */
    public static String getUploadPath() {
        String os = System.getProperty("os.name").toLowerCase();
        if(os.contains("window")) return "c:" + mUploadPath_Static;
        return mUploadPath_Static;
    }
}
