package ai.square.common.service;

import ai.square.common.dao.CommonDao;
import ai.square.common.model.LogVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommonService {

    @Autowired
    CommonDao commonDao;

    private final static Logger logger = LoggerFactory.getLogger(CommonService.class);

    /* ======================================================================================================================= */
    // 로그 쌓는 함수
    /* ======================================================================================================================= */
    public int recordLog(String apiId, String page, String detail, String action) {

        System.out.println(" @ recordLog !!! ");

        LogVo logVo = new LogVo();

        logVo.setUserId(apiId);
        logVo.setTitle(page);
        logVo.setDetail(detail);
        logVo.setAction(action);

        return commonDao.insertLog(logVo);
    }

}