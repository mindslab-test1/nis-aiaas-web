package ai.square.common.model;

import lombok.Data;

@Data
public class LogVo {

    String userId;
    String title;
    String detail;
    String action;
    String date;

}
