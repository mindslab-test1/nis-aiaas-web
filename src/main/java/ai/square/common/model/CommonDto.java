package ai.square.common.model;

public class CommonDto extends CommonVo {
	
	private String createDateTime;	// 생성일
	private String updateDateTime;	// 수정일
	
	public String getCreateDateTime() {
		return createDateTime;
	}
	public void setCreateDateTime(String createDateTime) {
		this.createDateTime = createDateTime;
	}
	public String getUpdateDateT() {
		return updateDateTime;
	}
	public void setUpdateDateT(String updateDateTime) {
		this.updateDateTime = updateDateTime;
	}		
}
