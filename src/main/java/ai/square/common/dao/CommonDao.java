package ai.square.common.dao;

import ai.square.common.model.LogVo;
import ai.square.home.model.VideoVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class CommonDao {

	@Resource(name = "sqlSession")
	private SqlSession sqlSession;

	private static final String NAMESPACE = "ai.square.model.logMapper";

	public int insertLog(LogVo logVo) {
		return sqlSession.insert(NAMESPACE + ".insertLog", logVo);
	}
}