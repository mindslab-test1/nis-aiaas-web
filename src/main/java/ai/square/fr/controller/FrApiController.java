package ai.square.fr.controller;

import ai.square.common.service.CommonService;
import ai.square.fr.service.FrService;
import ai.square.common.util.PropertyUtil;
import ai.square.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RestController
public class FrApiController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private FrService frService;


    @RequestMapping(value = "/api/getFaceRecog")
    @ResponseBody
    public ResponseEntity<String> getFaceRecog(@RequestParam(value = "demoFile1") MultipartFile imageFile1,
                                               @RequestParam(value="demoFile2") MultipartFile imageFile2,
                                               @RequestParam(value="demoFile3") MultipartFile imageFile3,
                                               @RequestParam(value="nameArray") List<String> nameArray,
                                               @RequestParam(value = "demoFile") MultipartFile recogImage,
                                               HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getApiId();
        String dbId = "demo_" + savedId;

        commonService.recordLog(memberVo.getUserId(), "faceRecognition", "api_call", "");

        List<MultipartFile> imageFile = new ArrayList<>();
        imageFile.add(imageFile1);
        imageFile.add(imageFile2);
        imageFile.add(imageFile3);

        try{
            for(int i=0;i<imageFile.size();i++){
                /* 이미지의 byte 길이가 0보다 큰 경우 setFace */
                if( (imageFile.get(i).getBytes().length>0)) {
                    frService.setFace(apiId, apiKey, nameArray.get(i), dbId, imageFile.get(i), PropertyUtil.getUploadPath()+"/faceRecog/", savedId);
                }
            }

            /* 얼굴인식 */
            ResponseEntity<String> responseEntity = frService.recogFace(apiId, apiKey, recogImage, dbId, PropertyUtil.getUploadPath()+"/faceRecog/", savedId);

            /* set한 이미지 모두 삭제 */
            for(int i=0;i<imageFile.size();i++){
                frService.deleteFace(apiId, apiKey, nameArray.get(i), dbId);
            }
            return responseEntity;
        }catch(Exception e){
            System.out.println(e.getMessage());
            for(int i=0;i<imageFile.size();i++){
                frService.deleteFace(apiId, apiKey, nameArray.get(i), dbId);
            }
        }

        return (new ResponseEntity<String>(HttpStatus.OK));
    }

    @RequestMapping(value = "/api/sampleFaceRecog")
    @ResponseBody
    public ResponseEntity<String> getFaceRecog(@RequestParam(value = "sampleImage1") MultipartFile sampleImage1,
                                               @RequestParam(value = "sampleImage2") MultipartFile sampleImage2,
                                               @RequestParam(value = "sampleImage3") MultipartFile sampleImage3,
                                               @RequestParam(value = "sampleImage4") MultipartFile sampleImage4,
                                               @RequestParam(value = "sample") MultipartFile sample,
                                               HttpServletRequest request){
        String dbId = "sampleDB2";


        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getApiId();

        commonService.recordLog(memberVo.getUserId(), "faceRecognition", "api_call", "");

        try{
            frService.setFace(apiId, apiKey, "Anne", dbId, sampleImage1, PropertyUtil.getUploadPath()+"/faceRecog/", savedId);
            frService.setFace(apiId, apiKey, "Edward", dbId, sampleImage2, PropertyUtil.getUploadPath()+"/faceRecog/", savedId);
            frService.setFace(apiId, apiKey, "Nancy", dbId, sampleImage3, PropertyUtil.getUploadPath()+"/faceRecog/", savedId);
            frService.setFace(apiId, apiKey, "Marquis", dbId, sampleImage4, PropertyUtil.getUploadPath()+"/faceRecog/", savedId);

            ResponseEntity<String> responseEntity = frService.recogFace(apiId, apiKey, sample, dbId, PropertyUtil.getUploadPath()+"/faceRecog/", savedId);

            frService.deleteFace(apiId, apiKey, "Anne", dbId);
            frService.deleteFace(apiId, apiKey, "Edward", dbId);
            frService.deleteFace(apiId, apiKey, "Nancy", dbId);
            frService.deleteFace(apiId, apiKey, "Marquis", dbId);

            return responseEntity;
        }catch(Exception e){
            System.out.println(e.getMessage());

            frService.deleteFace(apiId, apiKey, "Anne", dbId);
            frService.deleteFace(apiId, apiKey, "Edward", dbId);
            frService.deleteFace(apiId, apiKey, "Nancy", dbId);
            frService.deleteFace(apiId, apiKey, "Marquis", dbId);
        }

        return (new ResponseEntity<String>(HttpStatus.OK));
    }
}
