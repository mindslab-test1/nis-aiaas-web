package ai.square.fr.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/fr")
public class FrController {

    private static final Logger logger = LoggerFactory.getLogger(FrController.class);

    /**
     * faceRecog Main
     * @return
     */
    @RequestMapping(value = "/krFaceRecogMain")
    public String krfaceRecogMain(HttpServletRequest request) {
        logger.info("Welcome krfaceRecogMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "faceRecogMain");
        httpSession.setAttribute("menuName", "API Services");
        httpSession.setAttribute("subMenuName", "Face Recognition");
        httpSession.setAttribute("menuGrpName", "시각");

        String returnUri = "/kr/fr/faceRecogMain.pg";

        return returnUri;
    }
}
