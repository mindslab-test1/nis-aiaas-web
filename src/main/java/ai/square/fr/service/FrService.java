package ai.square.fr.service;

import ai.square.common.service.CommonService;
import org.apache.http.HttpEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.apache.http.client.HttpClient;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",

})

@Service
public class FrService {

    private static final Logger logger = LoggerFactory.getLogger(FrService.class);

    @Value("${api.url}")
    private String apiDomain;

    public void setFace(String apiId, String apiKey, String faceId, String dbId, MultipartFile file, String uploadPath, String saveId){

        String url = apiDomain+"/insight/app/setFace";

        String fileName = file.getOriginalFilename();
        fileName = fileName.substring(fileName.lastIndexOf("\\")+1);

        String logMsg = "\n===========================================================================\n";
        logMsg += "FaceRecog(setFace) API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", fileName);
        logMsg += String.format(":: %-10s = %s%n", "dbId", dbId);
        logMsg += String.format(":: %-10s = %s%n", "faceId", faceId);
        logMsg += "===========================================================================";
        logger.info(logMsg);


        try{
            HttpClient clinet = HttpClients.createDefault();
            HttpPut put = new HttpPut(url);
            File faceRecogFile = new File(uploadPath + "set/");

            if (!faceRecogFile.exists()) {
                logger.info("create Dir : {}", faceRecogFile.getPath());
                faceRecogFile.mkdirs();
            }


            faceRecogFile= new File(uploadPath + saveId + "_" + fileName);
            file.transferTo(faceRecogFile);
            FileBody faceRecogFileBody = new FileBody(faceRecogFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("file", faceRecogFileBody);
            builder.addPart("faceId", new StringBody(faceId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("dbId", new StringBody(dbId, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            put.setEntity(entity);
            HttpResponse response = clinet.execute(put);

            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            faceRecogFile.delete();

            if(responseCode != 200){
                logger.error("API fail.");
                throw new Error("setFace api fail");
            }

        }catch(Exception e){
           logger.error(e.getMessage());
        }
    }

    public void deleteFace(String apiId, String apiKey, String faceId, String dbId){

        String url = apiDomain+"/insight/app/deleteFace";
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "3FS4PKI7YH9S";

        String logMsg = "\n===========================================================================\n";
        logMsg += "FaceRecog(deleteFace) API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "dbId", dbId);
        logMsg += String.format(":: %-10s = %s%n", "faceId", faceId);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try {
            URL connectURL = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Connection", "Keep-Alive");    //연결을 지속하기 위해
            conn.setRequestProperty("Content-Type","multipart/form-data;boundary=" + boundary); //multipart/form으로 보낼 때는 boundary를 넣어줘야 함
            conn.setConnectTimeout(15000);

            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

            StringBuffer pd = new StringBuffer();
            pd.append(twoHyphens+boundary+lineEnd);
            pd.append("Content-Disposition: form-data; name=\"apiId\"" + lineEnd);

            pd.append(lineEnd);
            pd.append(apiId+lineEnd);

            pd.append(twoHyphens+boundary+lineEnd);
            pd.append("Content-Disposition: form-data; name=\"apiKey\"" + lineEnd);
            pd.append(lineEnd);
            pd.append(apiKey+lineEnd);

            pd.append(twoHyphens+boundary+lineEnd);
            pd.append("Content-Disposition: form-data; name=\"faceId\"" + lineEnd);
            pd.append(lineEnd);
            pd.append(faceId+lineEnd);

            pd.append(twoHyphens+boundary+lineEnd);
            pd.append("Content-Disposition: form-data; name=\"dbId\"" + lineEnd);
            pd.append(lineEnd);
            pd.append(dbId+lineEnd);

            pd.append(twoHyphens+boundary+twoHyphens+lineEnd);

            dos.writeUTF(pd.toString());
            dos.flush();

            int responseCode = conn.getResponseCode();

            if(responseCode != 200){
                logger.error("API fail.");
                throw new Exception("deleteFace api fail");
            }
        }catch(Exception e){
            logger.error(e.getMessage());
        }
    }

    public ResponseEntity<String> recogFace(String apiId, String apiKey, MultipartFile file, String dbId,
                                            String uploadPath, String saveId){

        String url = apiDomain + "/insight/app/recogFace";

        String fileName = file.getOriginalFilename();
        fileName = fileName.substring(fileName.lastIndexOf("\\")+1);

        String logMsg = "\n===========================================================================\n";
        logMsg += "FaceRecog(recogFace) API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", fileName);
        logMsg += String.format(":: %-10s = %s%n", "dbId", dbId);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try {
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File recogFaceFile = new File(uploadPath + "recog/");

            if (!recogFaceFile.exists()) {
                logger.info("create Dir : {}", recogFaceFile.getPath());
                recogFaceFile.mkdirs();
            }

            recogFaceFile = new File(uploadPath + saveId + "_" + fileName);
            file.transferTo(recogFaceFile);
            FileBody recogFaceFileBody = new FileBody(recogFaceFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("file", recogFaceFileBody);
            builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("dbId", new StringBody(dbId, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);


            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json;charset=UTF-8");

            if(responseCode == 200){
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

                StringBuilder result = new StringBuilder();
                String line = "";
                while((line = rd.readLine()) != null){
                    result.append(line);
                }

                ResponseEntity<String> resultEntity = new ResponseEntity<String>(result.toString(), headers, HttpStatus.OK);
                recogFaceFile.delete();

                return resultEntity;
            }else{
                recogFaceFile.delete();
                logger.error("API Fail");
                throw new Exception("recogFace api response fail");
            }

        }catch(Exception e){
            logger.error("API exception : {}", e.toString());
            e.printStackTrace();
        }

        return null;
    }
}
