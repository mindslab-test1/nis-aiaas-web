package ai.square.superResolution.controller;

import ai.square.common.service.CommonService;
import ai.square.superResolution.service.SuperResolutionService;
import ai.square.common.util.PropertyUtil;
import ai.square.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@RestController
public class SuperResolutionApiController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private SuperResolutionService superResolutionService;

    @RequestMapping(value = "/api/getSuperResolution")
    @ResponseBody
    public ResponseEntity<byte[]> getSuperResolutionImage(@RequestParam(value = "file") MultipartFile imageFile, HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getApiId();

        commonService.recordLog(memberVo.getUserId(), "ESR", "api_call", "");

        return superResolutionService.getApiSuperResolution(apiId, apiKey, imageFile,
                                                            PropertyUtil.getUploadPath()+"/superResolution/", savedId);

    }
}
