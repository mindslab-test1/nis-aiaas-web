package ai.square.superResolution.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
 * 화면 Controller
 */

@Controller
@RequestMapping(value = "/superResolution")
public class SuperResolutionController {

	private static final Logger logger = LoggerFactory.getLogger(SuperResolutionController.class);

	/**
	 * superResolution Main
	 * @return
	 */
	@RequestMapping(value = "/krSuperResolutionMain")
	public String krSuperResolutionMain(HttpServletRequest request) {
		logger.info("Welcome krSuperResolutionMain!");

		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "superResolutionMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "Enhanced Super Resolution");
		httpSession.setAttribute("menuGrpName", "시각");

		String returnUri = "/kr/superResolution/superResolutionMain.pg";
		
		return returnUri;
	}

}