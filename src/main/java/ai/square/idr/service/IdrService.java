package ai.square.idr.service;

import java.io.*;

import ai.square.common.service.CommonService;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",

})
@Service
public class IdrService {

	private static final Logger logger = LoggerFactory.getLogger(IdrService.class);

	@Value("${api.url}")
	private String apiDomain;

	public ResponseEntity<byte[]> idrBillNote(String apiId, String apiKey, MultipartFile file, String uploadPath, String saveId) {

		if(file == null) {
			throw new RuntimeException("File is not exist!");
		}

		String url = apiDomain + "/api/idr";

		String logMsg = "\n===========================================================================\n";
		logMsg += "IDR_NOTE_BILL API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "file", file.getOriginalFilename());
		logMsg += "===========================================================================";
		logger.info(logMsg);

		try {
			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);
			File idrVarFile = new File(uploadPath);

			if(!idrVarFile.exists()) {
				logger.info("create Dir : {}", idrVarFile.getPath());
				idrVarFile.mkdirs();
			}

			idrVarFile = new File(uploadPath + saveId + "_" + file.getOriginalFilename());
			file.transferTo(idrVarFile);
			FileBody fileBody = new FileBody(idrVarFile);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("file", fileBody);

			HttpEntity entity = builder.build();
			post.setEntity(entity);
			HttpResponse response = client.execute(post);
			int responseCode = response.getStatusLine().getStatusCode();
			logger.info("responseCode = {}" , responseCode);

			if (responseCode != 200) {
//				throw new RuntimeException(" @ Idr_Hospital ErrCode : " + responseCode);
				idrVarFile.delete();
				logger.error("API fail : {}", response.getEntity());
				return null;
			}

			HttpEntity responseEntity = response.getEntity();
			HttpHeaders headers = new HttpHeaders();
			InputStream in = responseEntity.getContent();
			byte[] resultArray = IOUtils.toByteArray(in);

			ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(resultArray, headers, HttpStatus.OK);

			idrVarFile.delete();

			return resultEntity;

		} catch (Exception e) {
			logger.error("API exception : {}", e.toString());
			e.printStackTrace();
		}
		return null;

	}


}