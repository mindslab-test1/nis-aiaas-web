package ai.square.idr.controller;

import ai.square.common.service.CommonService;
import ai.square.idr.service.IdrService;
import ai.square.common.util.PropertyUtil;
import ai.square.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/api/idr")
public class IdrApiController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private IdrService idrService;


    @RequestMapping(value = "/callRecog", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<byte[]> idrBillNote(@RequestParam MultipartFile file, HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getApiId();

        commonService.recordLog(memberVo.getUserId(), "idr", "api_call", "");

        return idrService.idrBillNote(apiId, apiKey, file, PropertyUtil.getUploadPath() + "/idr_note_bill/", savedId);
    }

}
