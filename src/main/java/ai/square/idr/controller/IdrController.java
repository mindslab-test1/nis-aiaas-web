package ai.square.idr.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping(value = "/idr")
public class IdrController {

	private static final Logger logger = LoggerFactory.getLogger(IdrController.class);

	/**
	 * idr Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krIdrMain")
	public String krIdrMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krIdrMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "idrMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "문서 이미지 분석");
		httpSession.setAttribute("menuGrpName", "시각");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/idr/idrMain.pg";

		return returnUri;
	}

	
}
