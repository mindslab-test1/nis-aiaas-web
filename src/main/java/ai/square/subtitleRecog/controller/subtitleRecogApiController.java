package ai.square.subtitleRecog.controller;

import ai.square.common.service.CommonService;
import ai.square.subtitleRecog.service.SubtitleRecogService;
import ai.square.common.util.PropertyUtil;
import ai.square.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class subtitleRecogApiController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private SubtitleRecogService service;

    @PostMapping(value = "/api/subtitleRecog")
    public ResponseEntity<byte[]> subtitleRecog(@RequestParam MultipartFile file, HttpServletRequest request) {

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getApiId();

        commonService.recordLog(memberVo.getUserId(), "subtitleRecog", "api_call", "");

        return service.recog(apiId, apiKey, file, PropertyUtil.getUploadPath() + "/subTitRecog/", savedId);
    }
}
