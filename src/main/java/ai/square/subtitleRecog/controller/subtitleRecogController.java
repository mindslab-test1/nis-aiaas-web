package ai.square.subtitleRecog.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping(value = "/subtitleRecog")


public class subtitleRecogController {
    private static final Logger logger = LoggerFactory.getLogger(subtitleRecogController.class);

    /**
     * subtitleRecog Main
     * @return
     */
    @RequestMapping(value = "/krSubtitleRecogMain")
    public String krSubtitleRecogMain(HttpServletRequest request) {
        logger.info("Welcome krSubtitleRecogMain");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "subtitleRecogMain");
        httpSession.setAttribute("menuName", "API Services");
        httpSession.setAttribute("subMenuName", "이미지 자막 인식");
        httpSession.setAttribute("menuGrpName", "시각");

        String returnUri = "/kr/subtitleRecog/subtitleRecogMain.pg";

        return returnUri;

    }
}
