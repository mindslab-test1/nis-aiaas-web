package ai.square.mrc.service;

import ai.square.common.service.CommonService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",

})
@Service
public class MrcApiService {

	private static final Logger logger = LoggerFactory.getLogger(MrcApiService.class);

	@Value("${api.url}")
	private String apiDomain;

	public String getApiMrc(String apiId, String apiKey, String sentence, String question, String lang) throws IOException {

		String url = apiDomain + "/api/mrc";

		String logMsg = "\n===========================================================================\n";
		logMsg += "MRC API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "lang", lang);
		logMsg += String.format(":: %-10s = %s%n", "sentence", sentence);
		logMsg += String.format(":: %-10s = %s%n", "question", question);
		logMsg += "===========================================================================";
		logger.info(logMsg);

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("id", apiId);
		paramMap.put("key", apiKey);
		paramMap.put("lang", lang);
		paramMap.put("sentence", sentence);
		paramMap.put("question", question);
		
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(paramMap);
		String resultMsg = sendPost(url, json);
		
		return resultMsg;
	}
	
	private String sendPost(String sendUrl, String jsonValue) throws IOException {
		StringBuilder result = new StringBuilder();

		URL url = new URL(sendUrl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Accetp-Charset", "UTF-8");
		conn.setConnectTimeout(10000);
		conn.setReadTimeout(60000);

		OutputStream os = conn.getOutputStream();
		os.write(jsonValue.getBytes("UTF-8"));
		os.flush();

		int responseCode = conn.getResponseCode();
		logger.info("responseCode = {}" , responseCode);

		if(responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
			String inputLine = "";
			while((inputLine = br.readLine()) != null) {
				try {
					result.append(new String(URLDecoder.decode(inputLine, "UTF-8")));
				} catch(Exception ex) {
					result.append(inputLine);	
				}
			}

			br.close();
		} else {
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
			String inputLine = "";
			while((inputLine = br.readLine()) != null) {
				try {
					result.append(new String(URLDecoder.decode(inputLine, "UTF-8")));
				} catch(Exception ex) {
					result.append(inputLine);	
				}
			}
			logger.error("API fail.");
			return "{ \"status\": \"error\" }";
		}
		conn.disconnect();
		
		return result.toString();
	}
}