package ai.square.mrc.controller;

import java.io.IOException;
import java.util.List;

import ai.square.common.service.CommonService;
import ai.square.mrc.service.MrcApiService;
import ai.square.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import ai.square.mrc.service.MrcApiService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class MrcApiController {

	@Autowired
	private CommonService commonService;

	@Autowired
	private MrcApiService mrcApiService;
	
	/**
	 * 답변 찾기 클릭 이벤트
	 * @param sentence
	 * @param question
	 * @return 응답 본문 String
	 * @throws IOException 
	 */
	@RequestMapping(value="/api/mrc", produces = "application/text; charset=utf8")
	@ResponseBody
    public String getApiMrc(
			@RequestParam(value = "sentence") String sentence
    		, @RequestParam(value = "question") String question
    		, @RequestParam(value = "lang") String lang
			, HttpServletRequest request) throws IOException {

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();

		commonService.recordLog(memberVo.getUserId(), "mrc", "api_call", "");

		return mrcApiService.getApiMrc(apiId, apiKey, sentence, question, lang);
	}
}
