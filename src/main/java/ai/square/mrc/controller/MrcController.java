package ai.square.mrc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/mrc")
public class MrcController {

	private static final Logger logger = LoggerFactory.getLogger(MrcController.class);

	/**
	 * mrc Main
	 * @return
	 */
	@RequestMapping(value = "/krMrcMain")
	public String krMrcMain(HttpServletRequest request) {
		logger.info("Welcome krMrcMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "mrcMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "AI 독해");
		httpSession.setAttribute("menuGrpName", "언어");

		String returnUri = "/kr/mrc/mrcMain.pg";
		
		return returnUri;
	}

}