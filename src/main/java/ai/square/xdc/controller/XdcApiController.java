package ai.square.xdc.controller;

import ai.square.common.service.CommonService;
import ai.square.member.model.MemberVo;
import ai.square.xdc.service.XdcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/xdc")
public class XdcApiController {

	private static final Logger logger = LoggerFactory.getLogger(XdcController.class);

	@Autowired
	private CommonService commonService;

	@Autowired
	private XdcService xdcService;
	
	/**
	 * 분석하기
	 * @param text
	 * @return
	 */
	@RequestMapping(value = "/xdcApiv20", produces = "application/json; charset=utf8")
	@ResponseBody
	public String xdcApiv20(@RequestParam(value = "context") String text, HttpServletRequest request){

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();

		commonService.recordLog(memberVo.getUserId(), "xdc", "api_call", "");

		return xdcService.xdcApiV20(apiId, apiKey, text);
	}
}
