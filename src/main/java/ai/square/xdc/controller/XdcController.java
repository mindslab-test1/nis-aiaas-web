package ai.square.xdc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/xdc")
public class XdcController {

	private static final Logger logger = LoggerFactory.getLogger(XdcController.class);

	/**
	 * xdc Main
	 * @return
	 */
	@RequestMapping(value = "/krXdcMain")
	public String krXdcMain(HttpServletRequest request) {
		logger.info("Welcome krXdcMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "xdcMain");
		httpSession.setAttribute("menuName", "API Services");		
		httpSession.setAttribute("subMenuName", "텍스트 분류");	
		httpSession.setAttribute("menuGrpName", "언어");

		String returnUri = "/kr/xdc/xdcMain.pg";
		return returnUri;
	}
}
