package ai.square.gpt.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import ai.square.common.service.CommonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",

})
@Service
public class GptApiService {

	private static final Logger logger = LoggerFactory.getLogger(GptApiService.class);

	@Value("${api.url}")
	private String apiDomain;
	
	public String getApiGpt(String apiId, String apiKey, String context, String lang) throws IOException {

		String url = apiDomain + "/api/gpt/";

		String logMsg = "\n===========================================================================\n";
		logMsg += "GPT API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "lang", lang);
		logMsg += String.format(":: %-10s = %s%n", "context", context);
		logMsg += "===========================================================================";
		logger.info(logMsg);

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("apiId", apiId);
		paramMap.put("apiKey", apiKey);
		paramMap.put("lang", lang);
		paramMap.put("context", context);

		String resultMsg = "";

		try {
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writeValueAsString(paramMap);
			resultMsg = sendPost(url, json);
		} catch (Exception e) {
			logger.error("API exception : {}", e.toString());
			e.printStackTrace();
			return "{ \"status\": \"error\" }";
		}
		
		return resultMsg;
	}
	
	private String sendPost(String sendUrl, String jsonValue) throws IOException {
		StringBuffer result = new StringBuffer();

		URL url = new URL(sendUrl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Accetp-Charset", "UTF-8");
		conn.setConnectTimeout(10000);
		conn.setReadTimeout(60000);

		OutputStream os = conn.getOutputStream();
		os.write(jsonValue.getBytes("UTF-8"));
		os.flush();

		int responseCode = conn.getResponseCode();
		logger.info("responseCode = {}" , responseCode);

		if(responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
			String inputLine = "";
			while((inputLine = br.readLine()) != null) {
				inputLine = inputLine.replace("\\n", "<br>");
				try {
					result.append(URLDecoder.decode(inputLine, "UTF-8"));
				} catch(Exception ex) {
					result.append(inputLine);	
				}
			}
			br.close();
		} else {
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
			String inputLine = "";
			while((inputLine = br.readLine()) != null) {
				inputLine = inputLine.replace("\\n", "<br>");
				try {
					result.append(URLDecoder.decode(inputLine, "UTF-8"));
				} catch(Exception ex) {
					result.append(inputLine);	
				}
			}

			logger.error("API fail.");
			return "{ \"status\": \"error\" }";
		}
		conn.disconnect();
		
		return result.toString();
	}
}
