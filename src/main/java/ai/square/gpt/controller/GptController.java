package ai.square.gpt.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/gpt")
public class GptController {

	private static final Logger logger = LoggerFactory.getLogger(GptController.class);

	/**
	 * gpt Main
	 * @return
	 */
	@RequestMapping(value = "/krGptMain")
	public String krGptMain(HttpServletRequest request) {
		logger.info("Welcome krGptMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "gptMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "문장 생성");
		httpSession.setAttribute("menuGrpName", "언어");

		String returnUri = "/kr/gpt/gptMain.pg";	
		
		return returnUri;
	}

}