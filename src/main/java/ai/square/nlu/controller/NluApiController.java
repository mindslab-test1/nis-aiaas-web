package ai.square.nlu.controller;

import ai.square.common.service.CommonService;
import ai.square.nlu.service.NluService;
import ai.square.member.model.MemberVo;
import ai.square.nlu.service.NluService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/*
 * API Controller
 */

@RestController
public class NluApiController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private NluService nluService;

    @PostMapping(value= "/nlu/nluApi")
    public String nluApi(@RequestParam(value="lang") String lang, @RequestParam(value="reqText") String text,
                         HttpServletRequest request) throws UnsupportedEncodingException {

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        commonService.recordLog(memberVo.getUserId(), "nlu", "api_call", "");

        String result = nluService.nluApi(apiId, apiKey, lang, text);
        String encodedResult = URLEncoder.encode(result, "UTF-8");

        return encodedResult;
    }
}
