package ai.square.nlu.controller;

import ai.square.nlu.service.NluService;
import ai.square.nlu.service.NluService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/nlu")
public class NluController {

	private static final Logger logger = LoggerFactory.getLogger(NluController.class);

	@Autowired
	private NluService nluService;
	
	/**
	 * nlu Main
	 * @return
	 */
	@RequestMapping(value = "/krNluMain")
	public String krNluMain(HttpServletRequest request) {
		logger.info("Welcome krNluMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "nluMain");
		httpSession.setAttribute("menuName", "API Services");		
		httpSession.setAttribute("subMenuName", "자연어 이해");
		httpSession.setAttribute("menuGrpName", "언어");

		String returnUri = "/kr/nlu/nluMain.pg";	
		
		return returnUri;
	}

}
