package ai.square.nlu.service;

import ai.square.common.service.CommonService;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",

})
@Service
public class NluService {

    private static final Logger logger = LoggerFactory.getLogger(NluService.class);

    @Value("${api.url}")
    private String apiDomain;

    
    public String nluApi(String apiId, String apiKey, String lang, String text) {

        String url = apiDomain + "/api/nlu";

        String logMsg = "\n===========================================================================\n";
        logMsg += "NLU API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "lang", lang);
        logMsg += String.format(":: %-10s = %s%n", "text", text);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try {

            JSONObject json = new JSONObject();
            json.put("apiId", apiId);
            json.put("apiKey", apiKey);
            json.put("lang", lang);
            json.put("reqText", text);

            CloseableHttpClient client = HttpClientBuilder.create().build();

            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            StringEntity entity = new StringEntity(json.toString(), "UTF-8");
            post.setEntity(entity);

            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            String resData = EntityUtils.toString(response.getEntity(), "UTF-8");

            return resData;

        } catch (Exception e) {
            logger.error("API exception : {}", e.toString());
            e.printStackTrace();
        }

        return null;
    }
}