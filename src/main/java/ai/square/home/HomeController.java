package ai.square.home;


import ai.square.home.service.HomeService;
import ai.square.home.service.HomeService;
import ai.square.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@Controller
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private HomeService homeService;


    /**
     * landing_page
     * @param model
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String landingPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        logger.info("Welcome landingPage");

        model.addAttribute("list", homeService.selectVideoList());
        String returnUri = "noLayout/kr/aiSquare/landingPage.pg";

        return returnUri;
    }


    /**
     * faqPage
     * @return
     */
    @RequestMapping(value = "/faqPage")
    public String faqPage() {

        logger.info("Welcome faqPage");
        String returnUri = "noLayout/kr/aiSquare/faqPage.pg";

        return returnUri;
    }

    /**
     * trendPage
     * @return
     */
    @RequestMapping(value = "/trendPage")
    public String trendPage() {

        logger.info("Welcome trendPage");
        String returnUri = "noLayout/kr/aiSquare/trendPage.pg";

        return returnUri;
    }


    /**
     * errorPage
     * @return
     */
    @RequestMapping(value = "/errorPage")
    public String errorPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        logger.info("Welcome errorPage");

        String errMsg = request.getParameter("errorMsg");
        model.addAttribute("errMsg", errMsg);

        String returnUri = "noLayout/kr/aiSquare/errorPage.pg";

        return returnUri;
    }
}
