package ai.square.home.dao;

import ai.square.home.model.VideoVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class HomeDao {

    @Resource(name = "sqlSession")
    private SqlSession sqlSession;

    private static final String NAMESPACE = "ai.square.model.homeMapper";


    public List<VideoVo> selectVideoList() {
        return sqlSession.selectList(NAMESPACE + ".selectVideoList");
    }


}
