package ai.square.home.service;


import ai.square.home.model.VideoVo;
import ai.square.home.dao.HomeDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomeService {
    private final static Logger logger = LoggerFactory.getLogger(HomeService.class);

    @Autowired
    private HomeDao homeDao;

    public List<VideoVo> selectVideoList(){

        List<VideoVo> videoList = homeDao.selectVideoList();
        logger.info(videoList.toString());
        return videoList;
    }


}
