package ai.square.home.model;

import lombok.Data;

@Data
public class VideoVo {

    int id;
    String videoPath;
    String imgPath;
    String category;
    String title;
    String fontColor;
    String createDate;
    String updateDate;

}
