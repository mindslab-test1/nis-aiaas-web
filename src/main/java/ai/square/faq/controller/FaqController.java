package ai.square.faq.controller;

import ai.square.faq.model.FaqVo;
import ai.square.faq.model.QuestionVo;
import ai.square.faq.service.FaqService;
import lombok.extern.slf4j.Slf4j;
import ai.square.faq.model.FaqVo;
import ai.square.faq.model.QuestionVo;
import ai.square.faq.service.FaqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/faq")
public class FaqController {

    @Autowired
    private FaqService faqService;

    /* FAQ contents 가져오기 */
    @RequestMapping(value = "/getFaqContents", method = RequestMethod.POST)
    @ResponseBody
    public List<FaqVo> getFaqContents() {
        log.debug(" @ Hello FaqController.getFaqContents ! ");

        return faqService.getFaqContents();
    }

    /* 문의하기 게시판 insert */
    @RequestMapping(value = "/insertQuestion", method = RequestMethod.POST)
    @ResponseBody
    public int insertQuestion(@RequestParam("question") String question,
                              @RequestParam("userNo") int userNo,
                              @RequestParam("userId") String userId) {
        log.debug(" @ Hello FaqController.insertQuestion ! ==> question: {} || userNo: {} || userId: {}", question, userNo, userId);

        QuestionVo questionVo = new QuestionVo();
        questionVo.setQuestion(question);
        questionVo.setUserNo(userNo);
        questionVo.setUserId(userId);

        return faqService.insertQuestion(questionVo);
    }


    /* 문의하기 게시판 가져오기 */
    @RequestMapping(value = "/getQuestion", method = RequestMethod.POST)
    @ResponseBody
    public List<QuestionVo> getQuestion() {
        log.debug(" @ Hello FaqController.getQuestion ! ");
        return faqService.getQuestion();

    }



}
