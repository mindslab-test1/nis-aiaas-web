package ai.square.faq.dao;

import ai.square.faq.model.FaqVo;
import ai.square.faq.model.QuestionVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Repository
public class FaqDao {

    @Resource(name = "sqlSession")
    private SqlSession sqlSession;

    private static final String NAMESPACE = "ai.square.model.faqMapper";

    /* FAQ contents 가져오기 */
    public List<FaqVo> getFaqContents() {
        log.debug(" @ Hello FaqDao.getFaqContents ! ");
        return sqlSession.selectList(NAMESPACE + ".getFaqContents");
    }

    /* Question_t insert */
    public int insertQuestion(QuestionVo questionVo) {
        log.debug(" @ Hello FaqDao.insertQuestion ! ");
        return sqlSession.insert(NAMESPACE + ".insertQuestion", questionVo);
    }

    /* Question contents 가져오기 */
    public List<QuestionVo> getQuestion() {
        log.debug(" @ Hello QuestionVo.getQuestion ! ");
        return sqlSession.selectList(NAMESPACE + ".getQuestion");
    }
}
