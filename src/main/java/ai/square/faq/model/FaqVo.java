package ai.square.faq.model;

import lombok.Data;

@Data
public class FaqVo {

    int id;
    String question;
    String answer;
    int userNo;
    String createDateTime;
    String updateDateTime;
}
