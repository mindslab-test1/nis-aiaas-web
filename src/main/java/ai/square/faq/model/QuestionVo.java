package ai.square.faq.model;

import lombok.Data;

@Data
public class QuestionVo {
    int id;
    String question;
    String answer;
    int userNo;
    String userId;
    String createDateTime;
    String updateDateTime;
}
