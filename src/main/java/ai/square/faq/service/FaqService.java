package ai.square.faq.service;

import lombok.extern.slf4j.Slf4j;
import ai.square.faq.dao.FaqDao;
import ai.square.faq.model.FaqVo;
import ai.square.faq.model.QuestionVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class FaqService {
    private static final Logger logger = LoggerFactory.getLogger(FaqService.class);
    @Autowired
    private FaqDao faqDao;

    /* FAQ contents 가져오기 */
    public List<FaqVo> getFaqContents() {
        log.debug(" @ Hello FaqService.getFaqContents ! ");

        return faqDao.getFaqContents();
    }

    /* Question_t insert */
    public int insertQuestion(QuestionVo questionVo) {
        log.debug(" @ Hello FaqService.insertQuestion ! ");

        return faqDao.insertQuestion(questionVo);
    }

    /* Question_t 가져오기 */
    public List<QuestionVo> getQuestion() {
        log.debug(" @ Hello FaqService.getQuestion ! ");
        List<QuestionVo> q_list = faqDao.getQuestion();

        logger.info (q_list.toString());
        return q_list;

    }
}
