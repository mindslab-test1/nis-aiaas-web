package ai.square.textRemoval.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
 * 화면 Controller
 */

@Controller
@RequestMapping(value = "/textRemoval")
public class TextRemovalController {

	private static final Logger logger = LoggerFactory.getLogger(TextRemovalController.class);

	/**
	 * textRemoval Main
	 * @return
	 */
	@RequestMapping(value = "/krTextRemovalMain")
	public String krTextRemovalMain(HttpServletRequest request) {
		logger.info("Welcome krTextRemovalMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "textRemovalMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "텍스트 제거");
		httpSession.setAttribute("menuGrpName", "시각");

		String returnUri = "/kr/textRemoval/textRemovalMain.pg";
		
		return returnUri;
	}

}