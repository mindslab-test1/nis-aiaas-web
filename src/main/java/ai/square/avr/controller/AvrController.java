package ai.square.avr.controller;

import ai.square.avr.service.AvrService;
import ai.square.common.service.CommonService;
import ai.square.common.util.PropertyUtil;
import ai.square.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
public class AvrController {

	private static final Logger logger = LoggerFactory.getLogger(AvrController.class);

	@Autowired
	private CommonService commonService;

	@Autowired
	private AvrService avrService;

	/**
	 * avr Main
	 * @return
	 */
	@RequestMapping(value = "/avr/krAvrMain")
	public String krAvrMain(HttpServletRequest request) {
		logger.info("Welcome krAvrMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "avrMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "도로상의 객체 인식");
		httpSession.setAttribute("menuGrpName", "시각");

		String returnUri = "/kr/avr/avrMain.pg";
		
		return returnUri;
	}

	@RequestMapping(value="/api/avr/multiAvr", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<byte[]> multiAvr(@RequestParam MultipartFile file, HttpServletRequest request){

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();
		String savedId = memberVo.getApiId();

		commonService.recordLog(memberVo.getUserId(), "avr", "api_call", "");

		return avrService.AvrMultipart(apiId, apiKey, file,
							PropertyUtil.getUploadPath()+"/avr/", savedId);
	}


}