package ai.square.avr.service;

import ai.square.common.service.CommonService;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;
import java.util.Base64;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",

})
@Service
public class AvrService {

	private static final Logger logger = LoggerFactory.getLogger(AvrService.class);

	@Value("${api.url}")
	private String apiDomain;

	public ResponseEntity<byte[]> AvrMultipart( String apiId, String apiKey, MultipartFile orgFile,
													  String uploadPath, String saveId){

		String url = apiDomain + "/smartXLoad/PlateRecog";

		String logMsg = "\n===========================================================================\n";
		logMsg += "AVR(plate) API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "car_img", orgFile.getOriginalFilename());
		logMsg += "===========================================================================";
		logger.info(logMsg);

		try {
			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);
			File avrFile = new File(uploadPath);


			if (!avrFile.exists()) {
				logger.info("create Dir : {}", avrFile.getPath());
				avrFile.mkdirs();
			}

			avrFile = new File(uploadPath + saveId + "_" + orgFile.getOriginalFilename());
			orgFile.transferTo(avrFile);
			FileBody avrFileBody = new FileBody(avrFile);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

			builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("car_img", avrFileBody);

			HttpEntity entity = builder.build();
			post.setEntity(entity);
			HttpResponse response = client.execute(post);
			int responseCode = response.getStatusLine().getStatusCode();
			logger.info("responseCode = {}" , responseCode);

			if (responseCode != 200) {
				avrFile.delete();
				throw new RuntimeException("API fail : " + response);
			}

			HttpEntity responseEntity = response.getEntity();
			HttpHeaders headers = new HttpHeaders();
			InputStream in = responseEntity.getContent();
			byte[] imageArray = IOUtils.toByteArray(in);

			ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(imageArray, headers, HttpStatus.OK);

			avrFile.delete();

			return resultEntity;

		}catch(Exception e){
			logger.error("API exception : {}", e.toString());
			e.printStackTrace();
		}

		return null;
	}

}

