package ai.square.tts.controller;

import ai.square.common.service.CommonService;
import ai.square.common.util.PropertyUtil;
import ai.square.member.model.MemberVo;
import ai.square.tts.service.TtsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;


@Controller
public class TtsController {

	private static final Logger logger = LoggerFactory.getLogger(TtsController.class);

	@Autowired
	private CommonService commonService;

	@Autowired
	private TtsService ttsService;

	
	/**
	 * tts Main
	 * @return
	 */
	@RequestMapping(value = "/tts/krTtsMain")
	public String krTtsMain(HttpServletRequest request) {
		logger.info("Welcome krTtsMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "ttsMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "음성생성(TTS)");
		httpSession.setAttribute("menuGrpName", "음성");

		String returnUri = "/kr/tts/ttsMain.pg";
		return returnUri;
	}


	@PostMapping(value = "/api/tts/ttsKidsApi")
	@ResponseBody
	public Map<String, Object> ttsKidsApi(
			@RequestParam("text") String text,
			@RequestParam("voiceName") String voiceName,
			HttpServletRequest request, HttpServletResponse response)
	{

		HttpSession httpSession = request.getSession(true);

		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");

		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();
		String savedId = memberVo.getUserId();

		commonService.recordLog(memberVo.getUserId(), "tts", "api_call", "");
		logger.info("uploadPath : {}" , PropertyUtil.getUploadPath());

		Map<String, Object> result = ttsService.ttsKidsApi(apiId, apiKey, savedId, text, voiceName, PropertyUtil.getUploadPath()+ "/tts/");
		if(result == null){
			response.setStatus(500);
		}

		return result;

	}


	/** TTS 결과 파일 다운로드 */
	@RequestMapping(value = "/api/tts/ttsDwn")
	public ModelAndView fileDownload(HttpServletRequest request,
									 @RequestParam("fileNameKey") String fileNameKey,
									 @RequestParam("fileName") String fileName) {

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String savedId = memberVo.getUserId();

		/** 첨부파일 정보 조회 */
		Map<String, Object> fileInfo = new HashMap<String, Object>();
		fileInfo.put("fileNameKey", fileNameKey);
		fileInfo.put("fileName", fileName);
		fileInfo.put("filePath", PropertyUtil.getUploadPath()+"/tts/"+savedId+"/");

		System.out.println("fileDownload fileNameKey ::::::  "+ fileNameKey);
		System.out.println("fileDownload fileName ::::::  "+ fileName);
		System.out.println("fileDownload filePath ::::::  "+ PropertyUtil.getUploadPath()+"/tts/"+savedId+"/");
		return new ModelAndView("fileDownloadUtil", "fileInfo", fileInfo);
	}
}
