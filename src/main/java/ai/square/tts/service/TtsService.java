package ai.square.tts.service;

import ai.square.common.util.FileConvertUtil;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",

})
@Service
public class TtsService {

	private static final Logger logger = LoggerFactory.getLogger(TtsService.class);

	@Value("${api.url}")
	private String apiDomain;

	public Map<String, Object> ttsKidsApi(String apiId, String apiKey, String saveId,
										  String text, String voiceName, String uploadPath) {

		String url = apiDomain + "/tts/stream";
//		String url = apiDomain + "/tts/makeFile";

		String logMsg = "\n===========================================================================\n";
		logMsg += "TTS(makeFile) API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "text", text);
		logMsg += String.format(":: %-10s = %s%n", "voiceName", voiceName);
		logMsg += "===========================================================================";
		logger.info(logMsg);

		try {

			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("text", new StringBody(text, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("voiceName", new StringBody(voiceName, ContentType.TEXT_PLAIN.withCharset("UTF-8")));

			HttpEntity entity = builder.build();

			post.setEntity(entity);
			HttpResponse response = client.execute(post);
			int responseCode = response.getStatusLine().getStatusCode();
			logger.info("responseCode = {}" , responseCode);

			HttpHeaders headers = new HttpHeaders();

			if(responseCode == 200) {

				HttpEntity responseEntity = response.getEntity();
				InputStream in = responseEntity.getContent();
				byte[] wavAudioArray = IOUtils.toByteArray(in);

				// 로컬 서버에 wav 파일 저장
				File ttsWavFile = new File(uploadPath + "out");

				if(!ttsWavFile.exists()){
					try {
						logger.info("create out dir : {}", ttsWavFile.getPath());
						ttsWavFile.mkdirs();
					}catch (Exception e){
						e.getStackTrace();
					}
				}

				FileOutputStream fos = new FileOutputStream(ttsWavFile +"/"+ saveId + "tts.wav");
				fos.write(wavAudioArray);
				fos.flush();
				fos.close();

//				Path target = Paths.get(ttsWavFile +"/"+ saveId + "tts.wav"); // 파일 저장 경로
				File target = new File(ttsWavFile +"/"+ saveId + "tts.wav");

				try {
//					FileCopyUtils.copy(wavAudioArray, target.toFile());

					String ttsMp3FilePath = FileConvertUtil.changeFileExtesion(target.toString());

					FileConvertUtil.changePermission( target.toString() );

					FileConvertUtil.convertWavToMp3( target.toString(), ttsMp3FilePath );

					File ttsMp3file = new File(ttsMp3FilePath);

					FileInputStream changedInputStream = new FileInputStream(ttsMp3file);
					byte[] mp3AudioArray = IOUtils.toByteArray(changedInputStream);

					headers.setCacheControl(CacheControl.noCache().getHeaderValue());

					ResponseEntity<byte[]> mp3Entity = new ResponseEntity<>(mp3AudioArray, headers, HttpStatus.OK);
					ResponseEntity<byte[]> wavEntity = new ResponseEntity<>(wavAudioArray, headers, HttpStatus.OK);

					Map<String, Object> resultMap = new HashMap<>();

					resultMap.put("mp3File", mp3Entity);
					resultMap.put("wavFile", wavEntity);

					target.delete();
					ttsMp3file.delete();

					logger.info(" @ TTS service delete MP3 & WAV files complete! ==> apiId : {}", apiId);

					return resultMap;

				} catch (IOException e) {
					logger.error("Exception in TTS service's file writing process ! : " + e);
				}

			}
		} catch (Exception e) {
			logger.error("API exception : {}", e.toString());
		}

		return null;
	}

}