package ai.square.main.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/main")
public class MainController {

	private static final Logger logger = LoggerFactory.getLogger(MainController.class);


	@RequestMapping(value = "/krMainHome")
	public String krMainHome(HttpServletRequest request, Model model) {
		logger.info("Welcome krMainHome!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "mainHome");
		//httpSession.setAttribute("menuName", "HOME");
		httpSession.setAttribute("menuName", "");
		httpSession.setAttribute("subMenuName", "");
		httpSession.setAttribute("lang", "ko");
		httpSession.setAttribute("menuGrpName", "");
		model.addAttribute("lang", "ko");

		String returnUri = "/kr/main/mainHome.pg";	
		
		return returnUri;
	}

}