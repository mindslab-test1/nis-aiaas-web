package ai.square.diarize.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/diarize")
public class DiarizeController {

	private static final Logger logger = LoggerFactory.getLogger(DiarizeController.class);

	/**
	 * diarize Main
	 * @return
	 */
	@RequestMapping(value = "/krDiarizeMain")
	public String krDiarizeMain(HttpServletRequest request) {
		logger.info("Welcome krDiarizeMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "diarizeMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "화자 분리");
		httpSession.setAttribute("menuGrpName", "음성");

		String returnUri = "/kr/diarize/diarizeMain.pg";

		return returnUri;
	}

}