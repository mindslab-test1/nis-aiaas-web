package ai.square.diarize.controller;

import ai.square.common.service.CommonService;
import ai.square.common.util.PropertyUtil;
import ai.square.diarize.service.DiarizeService;
import ai.square.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/*
 * API Controller
 */

@RestController
public class DiarizeApiController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private DiarizeService diarizeService;

    @PostMapping("/api/dap/diarize")
    public Map<String, Object> getApiDiarize(@RequestParam("file")MultipartFile file, HttpServletRequest request){

        HttpSession httpSession= request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String savedId = memberVo.getApiId();
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        commonService.recordLog(memberVo.getUserId(), "diarize", "api_call", "");

        return diarizeService.getApiDiarize(apiId, apiKey, file, savedId, PropertyUtil.getUploadPath() +"/diarize/");

    }
}
