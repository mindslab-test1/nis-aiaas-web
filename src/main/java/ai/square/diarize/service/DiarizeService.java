package ai.square.diarize.service;


import ai.square.common.service.CommonService;
import ai.square.common.util.FileConvertUtil;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",

})
@Service
public class DiarizeService {

    private static final Logger logger = LoggerFactory.getLogger(DiarizeService.class);

    @Value("${api.url}")
    private String apiDomain;

    public Map<String, Object> getApiDiarize( String apiId, String apiKey, MultipartFile file,
                                 String savedId, String uploadPath){

        if(file==null){
            throw new RuntimeException("You must select the a file for uploading");
        }

        String url = apiDomain + "/api/dap/diarize";

        String logMsg = "\n===========================================================================\n";
        logMsg += "Diarize API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", file.getOriginalFilename());
        logMsg += "===========================================================================";
        logger.info(logMsg);

        Map<String, Object> resultMap = new HashMap<>();

        try {

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            File varfile = new File(uploadPath);

            // 디렉토리가 없으면 생성
            if (!varfile.exists()) {
                logger.info("create Dir : {}", varfile.getPath());
                varfile.mkdirs();
            }

            varfile = new File(uploadPath + savedId + "_" + file.getOriginalFilename());
            file.transferTo(varfile);

            logger.info(varfile.toString());

            FileBody fileBody = new FileBody(varfile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("reqVoice", fileBody);
            builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            if (responseCode == 200) {
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    // result.append(URLDecoder.decode(line, "UTF-8"));
                    result.append(line);
                }
                logger.info("Response Result : {}", result);

                JSONParser jsonParser = new JSONParser();
                JSONObject object = (JSONObject)jsonParser.parse(result.toString());

                HttpHeaders headers = new HttpHeaders();
                try {
                    String mp3FilePath = FileConvertUtil.changeFileExtesion(varfile.toString());

                    FileConvertUtil.changePermission( varfile.toString() );
                    FileConvertUtil.convertWavToMp3( varfile.toString(), mp3FilePath );

                    File mp3file = new File(mp3FilePath);

                    FileInputStream inputStream = new FileInputStream(mp3file);
                    byte[] mp3AudioArray = IOUtils.toByteArray(inputStream);

                    headers.setCacheControl(CacheControl.noCache().getHeaderValue());
                    ResponseEntity<byte[]> mp3Entity = new ResponseEntity<>(mp3AudioArray, headers, HttpStatus.OK);

                    resultMap.put("mp3File", mp3Entity);
                    resultMap.put("json", object);

                    mp3file.delete();
                    varfile.delete();
                    logger.info(" @ Diarize service delete MP3 & WAV files complete! ==> apiId : {}", apiId);

                    return resultMap;

                } catch (IOException e) {
                    logger.error("Exception in Diarize service's file writing process ! : " + e);
                }

            } else {

                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }

                logger.info("Response Result : {}", result.toString().replace('+', ' '));

                varfile.delete();

            }
        } catch (Exception e) {
            logger.error("API exception : {}", e.toString());
            resultMap.put("mp3File", null);
            resultMap.put("json", null);
        }

        return resultMap;

    }
}
