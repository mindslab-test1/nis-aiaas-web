package ai.square.batch.controller;

import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;

/**
 * BatchController
 */

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",

})
@Controller
public class BatchController {

}
