package ai.square.poseRecog.service;

import ai.square.common.service.CommonService;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",

})
@Service
public class poseRecognitionService {

    private static final Logger logger = LoggerFactory.getLogger(poseRecognitionService.class);

    @Value("${api.url}")
    private String apiDomain;

    public ResponseEntity<byte[]> runPoseRecog(String apiId, String apiKey, MultipartFile orgFile,
                                                     String uploadPath, String saveId){

        if(orgFile == null){
            throw new RuntimeException("You must select the a file for uploading");
        }

        String url = apiDomain + "/smartXLoad/poseExtract/";

        String fileName = orgFile.getOriginalFilename();
        fileName = fileName.substring(fileName.lastIndexOf("\\")+1);

        String logMsg = "\n===========================================================================\n";
        logMsg += "PoseRecog API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", fileName);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try{
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File poseRecogFile = new File(uploadPath);

            if (!poseRecogFile.exists()) {
                logger.info("create Dir : {}", poseRecogFile.getPath());
                poseRecogFile.mkdirs();
            }

            poseRecogFile = new File(uploadPath + saveId + "_" + fileName);
            orgFile.transferTo(poseRecogFile);
            FileBody poseRecogFileBody = new FileBody(poseRecogFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("pose_img", poseRecogFileBody);

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            if (responseCode != 200) {
                poseRecogFile.delete();
                throw new RuntimeException("API fail : " + response);
            }

            HttpEntity responseEntity = response.getEntity();
            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] imageArray = IOUtils.toByteArray(in);

            ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(imageArray, headers, HttpStatus.OK);

            poseRecogFile.delete();

            return resultEntity;

        }catch(Exception e){
            logger.error("API exception : {}", e.toString());
            e.printStackTrace();
        }

        return null;
    }
}
