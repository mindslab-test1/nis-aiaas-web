package ai.square.poseRecog.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping(value = "/poseRecog")

public class poseRecognitionController {


    private static final Logger logger = LoggerFactory.getLogger(poseRecognitionController.class);

    /**
     * poseRecog Main
     * @return
     */
    @RequestMapping(value = "/krPoseRecogMain")
    public String krPoseRecogMain(HttpServletRequest request) {
        logger.info("Welcome krPoseRecogMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "poseRecogMain");
        httpSession.setAttribute("menuName", "API Services");
        httpSession.setAttribute("subMenuName", "인물 포즈 인식");
        httpSession.setAttribute("menuGrpName", "시각");

        String returnUri = "/kr/poseRecog/poseRecogMain.pg";

        return returnUri;
    }

}
