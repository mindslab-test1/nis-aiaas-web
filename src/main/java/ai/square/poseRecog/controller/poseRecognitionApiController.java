package ai.square.poseRecog.controller;

import ai.square.common.service.CommonService;
import ai.square.common.util.PropertyUtil;
import ai.square.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class poseRecognitionApiController {
    private final static Logger logger = LoggerFactory.getLogger(poseRecognitionApiController.class);

    @Autowired
    private CommonService commonService;

    @Autowired
    private ai.square.poseRecog.service.poseRecognitionService poseRecognitionService;

    @PostMapping(value="/poseRecog/poseApi")
    public ResponseEntity<byte[]> poseRecogApi(@RequestParam(value = "file")MultipartFile imageFile, HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getApiId();

        commonService.recordLog(memberVo.getUserId(), "poseRecog", "api_call", "");

        return poseRecognitionService.runPoseRecog(apiId, apiKey, imageFile, PropertyUtil.getUploadPath() + "/poseRecog/", savedId);
    }
}
