package ai.square.itf.service;

import ai.square.common.service.CommonService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",

})
@Service
public class ItfApiService {

    private static final Logger logger = LoggerFactory.getLogger(ItfApiService.class);

    @Value("${api.url}")
    private String apiDomain;


    public String getApiItf(String apiId, String apiKey, String utter, String lang) throws IOException{

        String url = apiDomain + "/api/itf";

        String logMsg = "\n===========================================================================\n";
        logMsg += "ITF API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "lang", lang);
        logMsg += String.format(":: %-10s = %s%n", "utter", utter);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("apiId", apiId);
        paramMap.put("apiKey", apiKey);
        paramMap.put("utter", utter);
        paramMap.put("lang", lang);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(paramMap);
        String resultMsg = sendPost(url, json);

        return resultMsg;
    }

    private String sendPost(String sendUrl, String jsonValue) throws IOException{
        StringBuilder result = new StringBuilder();

        URL url = new URL(sendUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept-Charset", "UTF-8");
        conn.setConnectTimeout(10000);
        conn.setReadTimeout(60000);

        OutputStream os = conn.getOutputStream();
        os.write(jsonValue.getBytes("UTF-8"));
        os.flush();

        int responseCode = conn.getResponseCode();
        logger.info("responseCode = {}" , responseCode);

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
        String inputLine = "";
        while((inputLine = br.readLine()) != null) {
            try {
                result.append(URLDecoder.decode(inputLine, "UTF-8"));
            } catch(Exception ex) {
                result.append(inputLine);
            }
        }

        br.close();

        if(responseCode != HttpURLConnection.HTTP_OK){
            logger.error("API fail.");
            return "{ \"status\": \"error\" }";
        }

        conn.disconnect();
        return result.toString();
    }
}
