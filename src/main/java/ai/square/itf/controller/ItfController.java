package ai.square.itf.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/itf")
public class ItfController {

	private static final Logger logger = LoggerFactory.getLogger(ItfController.class);

	/**
	 * itf Main
	 * @return
	 */
	@RequestMapping(value = "/krItfMain")
	public String krItfMain(HttpServletRequest request) {
		logger.info("Welcome krItfMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "itfMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "의도 분류");
		httpSession.setAttribute("menuGrpName", "언어");

		String returnUri = "/kr/itf/itfMain.pg";

		return returnUri;
	}
	
}
