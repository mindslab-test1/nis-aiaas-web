<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%--<c:if test="${empty fn:escapeXml(sessionScope.accessUser)}">--%>
<%--	<script>--%>
<%--		window.location.href = "/?lang=ko";--%>
<%--	</script>--%>
<%--</c:if>--%>


<sec:authorize access="isAnonymous()">
		<script>
			window.location.href = "${pageContext.request.contextPath}/errorPage";
		</script>
</sec:authorize>

<form id="logout-form" action='${pageContext.request.contextPath}/logout' method="POST">
   <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>

        <h1><a href="${pageContext.request.contextPath}/" target="_self">AI Square</a></h1>
		<div class="sta">			
			<!-- .titArea -->
			<div class="titArea">               
                <div class="path">
                    <span><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_path_home_bk.png" alt="HOME"></span>
                    <span>${fn:escapeXml(sessionScope.menuName)}</span>
<c:if test="${fn:escapeXml(sessionScope.subMenuName) ne ''}">                    
                    <span>${fn:escapeXml(sessionScope.subMenuName)}</span>
</c:if>
                </div>
				<div class="linkBox">
					<ul>
						<li><a class="" href="${pageContext.request.contextPath}/main/krMainHome" target="_self">AI 서비스</a></li>
						<li><a href="${pageContext.request.contextPath}/trendPage" target="_self">AI 기술 트렌드</a></li>
						<li><a href="${pageContext.request.contextPath}/faqPage" target="_self">FAQ</a></li>
						<li><a class="" href="<spring:eval expression="@property['dataEditTool.url']" />" target="_self">Data Edit Tool</a></li>
					</ul>
				</div>
            </div>


			<div class="etcmenu">
				<div class="userBox">
<%--					<span class="user_num">사번 ${fn:escapeXml(sessionScope.accessUser.userId)}</span>--%>
					<span id="key_btn"><em class="fas fa-key"></em><strong>API<br>KEY</strong></span>
				</div>
			</div>
			<!-- //.etcmenu -->
			<div class="idkey_box">
				<ul class="idpw">
					<li><strong>API ID </strong><span>${fn:escapeXml(sessionScope.accessUser.apiId)}</span></li>
					<li><strong>API KEY </strong><span>${fn:escapeXml(sessionScope.accessUser.apiKey)}</span></li>
				</ul>
				<em class="fas fa-times"></em>
			</div>
		</div>

<script>

	$(document).ready(function() {
		$('#key_btn').on('click', function(){
			$(this).parent().parent().toggleClass('active');
		});
		$('.idkey_box .fa-times, #container ').on('click', function(){
			$('.etcmenu').removeClass('active');
		})
	})

</script>