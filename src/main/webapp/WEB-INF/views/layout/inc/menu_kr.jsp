<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%
	Date lastModifiedStyle = new Date();
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/menu.css?ver=<%=fmt.format(lastModifiedStyle)%>" />

<!-- .snb 한글 -->
<div class="snb">
	<ul class="api_list">
		<!--음성-->
		<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq '음성'}">
		<li class="collapsible list1 dropdwn active">
		</c:if>
		<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne '음성'}">
		<li class="collapsible list1 dropdwn">
		</c:if>
			<span><a href="#none" target="_self"><em class="fas fa-microphone"></em> 음성</a></span>
		</li>

		<li class="sublist list1_sub">

<!-- [API Services/음성 생성] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'ttsMain'}">
			<a class="twodepth active" href="${pageContext.request.contextPath}/tts/krTtsMain">음성생성(TTS)</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'ttsMain'}">
			<a class="twodepth" href="${pageContext.request.contextPath}/tts/krTtsMain">음성생성(TTS)</a>
</c:if>

<!-- [API Services/음성 인식] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'sttMain'}">
			<a class="twodepth active" href="${pageContext.request.contextPath}/stt/krSttMain">음성인식(STT)</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'sttMain'}">
			<a class="twodepth" href="${pageContext.request.contextPath}/stt/krSttMain">음성인식(STT)</a>
</c:if>

<!-- [API Services/음성 정제] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'denoiseMain'}">
			<a class="twodepth active" href="${pageContext.request.contextPath}/denoise/krDenoiseMain">음성 정제</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'denoiseMain'}">
			<a class="twodepth" href="${pageContext.request.contextPath}/denoise/krDenoiseMain">음성 정제</a>
</c:if>

<!-- [API Services/화자 분리] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'diarizeMain'}">
	<a class="twodepth active" href="${pageContext.request.contextPath}/diarize/krDiarizeMain">화자 분리</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'diarizeMain'}">
	<a class="twodepth" href="${pageContext.request.contextPath}/diarize/krDiarizeMain">화자 분리</a>
</c:if>

<!-- [API Services/음성 분리] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'voicefilterMain'}">
			<a class="twodepth active" href="${pageContext.request.contextPath}/voicefilter/krVoicefilterMain">Voice Filter</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'voicefilterMain'}">
			<a class="twodepth" href="${pageContext.request.contextPath}/voicefilter/krVoicefilterMain">Voice Filter</a>
</c:if>

<!-- [API Services/음성 분리] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'voiceRecogMain'}">
			<a class="twodepth active" href="${pageContext.request.contextPath}/vr/krVoiceRecogMain">Voice Recognition</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'voiceRecogMain'}">
			<a class="twodepth" href="${pageContext.request.contextPath}/vr/krVoiceRecogMain">Voice Recognition</a>
</c:if>


		</li>

		<!--시각-->
		<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq '시각'}">
		<li class="collapsible list2 dropdwn active">
		</c:if>
		<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne '시각'}">
		<li class="collapsible list2 dropdwn">
		</c:if>
			<span><em class="fas fa-eye"></em> 시각</span>
		</li>

		<li class="sublist list2_sub">

<!-- [API Services/Face Recognition] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'faceRecogMain'}">
<a class="twodepth active" href="${pageContext.request.contextPath}/fr/krFaceRecogMain">Face Recognition</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'faceRecogMain'}">
<a class="twodepth" href="${pageContext.request.contextPath}/fr/krFaceRecogMain">Face Recognition</a>
</c:if>

<!-- [API Services/Super Resolution] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'superResolutionMain'}">
<a class="twodepth active" href="${pageContext.request.contextPath}/superResolution/krSuperResolutionMain">Enhanced Super Resolution</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'superResolutionMain'}">
<a class="twodepth" href="${pageContext.request.contextPath}/superResolution/krSuperResolutionMain">Enhanced Super Resolution</a>
</c:if>


<!-- [API Services/이미지 생성] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'ttiMain'}">
<a class="twodepth active" href="${pageContext.request.contextPath}/tti/krTtiMain">AI 스타일링</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'ttiMain'}">
<a class="twodepth" href="${pageContext.request.contextPath}/tti/krTtiMain">AI 스타일링</a>
</c:if>

<!-- [API Services/텍스트 제거] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'textRemovalMain'}">
			<a class="twodepth active" href="${pageContext.request.contextPath}/textRemoval/krTextRemovalMain">텍스트 제거</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'textRemovalMain'}">
			<a class="twodepth" href="${pageContext.request.contextPath}/textRemoval/krTextRemovalMain">텍스트 제거</a>
</c:if>

<!-- [API Services/물체/얼굴 검출] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'avrMain'}">
<a class="twodepth active" href="${pageContext.request.contextPath}/avr/krAvrMain">도로상의 객체 인식</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'avrMain'}">
<a class="twodepth" href="${pageContext.request.contextPath}/avr/krAvrMain">도로상의 객체 인식</a>
</c:if>

<!-- [API Services/인물 포즈 인식] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'poseRecogMain'}">
<a class="twodepth active" href="${pageContext.request.contextPath}/poseRecog/krPoseRecogMain">인물 포즈 인식</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'poseRecogMain'}">
<a class="twodepth" href="${pageContext.request.contextPath}/poseRecog/krPoseRecogMain">인물 포즈 인식</a>
</c:if>

<!-- [API Services/얼굴 추적] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'faceTrackingMain'}">
<a class="twodepth active" href="${pageContext.request.contextPath}/faceTracking/krFaceTrackingMain">얼굴 추적</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'faceTrackingMain'}">
<a class="twodepth" href="${pageContext.request.contextPath}/faceTracking/krFaceTrackingMain">얼굴 추적</a>
</c:if>

<!-- [API Services/이상 행동 감지] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'anomalyReverseMain'}">
<a class="twodepth active" href="${pageContext.request.contextPath}/anomalyReverse/krAnomalyReverseMain">이상행동 감지</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'anomalyReverseMain'}">
<a class="twodepth" href="${pageContext.request.contextPath}/anomalyReverse/krAnomalyReverseMain">이상행동 감지</a>
</c:if>

<!-- [API Services/이미지 자막 인식 ] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'subtitleRecogMain'}">
<a class="twodepth active" href="${pageContext.request.contextPath}/subtitleRecog/krSubtitleRecogMain">이미지 자막 인식</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'subtitleRecogMain'}">
<a class="twodepth" href="${pageContext.request.contextPath}/subtitleRecog/krSubtitleRecogMain">이미지 자막 인식</a>
</c:if>

<!-- [API Services/문서 이미지 분석 ] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'idrMain'}">
	<a class="twodepth active" href="${pageContext.request.contextPath}/idr/krIdrMain">문서 이미지 분석</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'idrMain'}">
	<a class="twodepth" href="${pageContext.request.contextPath}/idr/krIdrMain">문서 이미지 분석</a>
</c:if>

		</li>

		<!--언어-->
		<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq '언어'}">
		<li class="collapsible list3 dropdwn active">
		</c:if>
		<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne '언어'}">
		<li class="collapsible list3 dropdwn">
		</c:if>
			<span><em class="fas fa-file-alt"></em> 언어</span>
		</li>

		<li class="sublist list3_sub">
<!-- [API Services/자연어 이해] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'nluMain'}">
			<a class="twodepth active" href="${pageContext.request.contextPath}/nlu/krNluMain">자연어 이해</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'nluMain'}">
			<a class="twodepth" href="${pageContext.request.contextPath}/nlu/krNluMain">자연어 이해</a>
</c:if>				

<!-- [API Services/AI 독해] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'mrcMain'}">
			<a class="twodepth active" href="${pageContext.request.contextPath}/mrc/krMrcMain">AI 독해</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'mrcMain'}">
			<a class="twodepth" href="${pageContext.request.contextPath}/mrc/krMrcMain">AI 독해</a>
</c:if>

<!-- [API Services/텍스트 분류] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'xdcMain'}">
			<a class="twodepth active" href="${pageContext.request.contextPath}/xdc/krXdcMain">텍스트 분류</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'xdcMain'}">
			<a class="twodepth" href="${pageContext.request.contextPath}/xdc/krXdcMain">텍스트 분류</a>
</c:if>

<!-- [API Services/AI 작문] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'gptMain'}">
			<a class="twodepth active" href="${pageContext.request.contextPath}/gpt/krGptMain">문장 생성</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'gptMain'}">
			<a class="twodepth" href="${pageContext.request.contextPath}/gpt/krGptMain">문장 생성</a>
</c:if>

<!-- [API Services/패턴 분류] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'hmdMain'}">
			<a class="twodepth active" href="${pageContext.request.contextPath}/hmd/krHmdMain">패턴 분류</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'hmdMain'}">
			<a class="twodepth" href="${pageContext.request.contextPath}/hmd/krHmdMain">패턴 분류</a>
</c:if>

<!-- [API Services/의도 분류] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'itfMain'}">
			<a class="twodepth active" href="${pageContext.request.contextPath}/itf/krItfMain">의도 분류</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'itfMain'}">
			<a class="twodepth" href="${pageContext.request.contextPath}/itf/krItfMain">의도 분류</a>
</c:if>
		</li>
	</ul>
</div>
<!-- //.snb 한글 -->
		
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/menu.js"></script>		