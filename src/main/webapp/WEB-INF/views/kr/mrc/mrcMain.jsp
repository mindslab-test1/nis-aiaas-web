<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">AI 독해</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'mrcdemo')" id="defaultOpen"><button type="button">데모</button></li>
			<li class="tablinks" onclick="openTap(event, 'mrcexample')"><button type="button">적용사례</button></li>
			<li class="tablinks" onclick="openTap(event, 'mrcmenu')"><button type="button">매뉴얼</button></li>
<%--			<li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
		</ul>
		<!--.demobox_mrc-->
		<div class="demobox demobox_mrc" id="mrcdemo">
			<p>정확하게 읽고 답하는 <span>AI 독해</span> <small>(MRC, Machine Reading Comprehension)</small></p>
			<span class="sub">내용을 읽고 분석하여 질문에 맞는 답을 정확하게 찾습니다.</span>
			<!--.mrc_box-->
			<div class="mrc_box">
				<div class="mrclang_select">
					<span>언어 선택</span>
					<div class="radio">
						<input type="radio" id="radioType01" name="radio" checked="checked" value="kor"><label for="radioType01">한국어</label>
					</div>
					<div class="radio">
						<input type="radio" id="radioType02" name="radio" value="eng"><label for="radioType02">English</label>
					</div>
				</div>

				<!--.step01-->
				<div class="step01">
					<div class="demo_top">
						<p>본문넣기</p>
					</div>
<%--					<div class="selectarea">--%>
<%--						<div class="select_box">--%>
<%--							<label for="lang">언어</label>--%>
<%--							<select id="lang">--%>
<%--								<option value="kor">한국어</option>--%>
<%--								<option value="eng">영어</option>--%>
<%--							</select>--%>
<%--						</div>--%>
<%--					</div>--%>
					<div class="tab">
						<button class="tablink" onclick="clickTap(this, 'example')" id="defaultOpen2" >예문</button>
					</div>
					<div class="text_area tabcontent"  id="example">
						<textarea rows="7" id="id_input_text" placeholder="문장을 입력해 주세요.">인공지능(AI) 전문기업 마인즈랩이 캐나다 3대 인공지능 연구기관 에이미(AMII: Alberta Machine Intelligence Institute)에 합류하고, 구글 딥마인드를 비롯한 세계 최고 수준의 AI 연구진과 함께 딥러닝 분야의 연구를 함께한다. 마인즈랩은 에이미의 회원사로서 에이미 소속의 연구진들과 함께 공동 연구를 추진한다. 인공지능 강국으로 평가받는 캐나다에서 엘리먼트AI(Element AI), 벡터 연구소(Vector Institute)와 함께 3대 인공지능 연구기관 중 하나로 꼽히는 에이미는 강화학습의 창시자 리차드 서튼 교수의 주도 아래 알파고를 개발한 구글 딥마인드와 앨버타 대학 등이 모여 각 분야에서의 딥러닝 연구를 활발하게 이끌고 있다.</textarea>
					</div>
				</div>
				<!--//.step01-->
				<!--.step02-->

				<div class="step02">
					<div class="demo_top">
						<p>질문하기</p>
					</div>
					<div class="text_area">
						<textarea class="example_area" id="id_input_question" placeholder="질문을 입력해 주세요.">어디에 합류했나요?</textarea>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_search" id="find_answer_btn"><em class="fas fa-file-invoice"></em>답변찾기
						</button>
						<%--<span class="tooltiptext">버튼 클릭</span>--%>
						<!--							<span class="disBox"></span>-->
					</div>
				</div>
				<!--//.step02-->

				<!--.step03-->
				<div class="step03">
					<div class="demo_top">
						<p style="width: 155px;">AI 독해 분석 결과</p>
					</div>
					<div class="resultBox">
						신뢰도: <span>93.25%</span>
					</div>
					<div class="result_txt">
						신성 로마 제국(라틴어: Sacrum Romanum Imperium)은 중세에서 근대 초까지 이어진 기독교 성향이 강한 유럽 국가들의 정치적 연방체이다. 프랑크 왕국이 베르됭 조약(843년)으로 나뉜 동쪽에서 독일 왕이 마자르족을 격퇴한 후 <strong>교황</strong>으로부터 황제의 관을 수여받아 신성 로마 제국 건국을 선포하였다. 신성 로마 제국은 초기에는 강력한 중앙집권 국가였으나, 점차 이탈리아에 대한 간섭으로 독일 지역에 소홀히 하면서 여러 제후들에 의해 분할 상태가 되었다. <strong>30년 전쟁(1618~48년)</strong>이 일어난 나라로 유명하며, 30년 전쟁에 패배하여 베스트팔렌 조약(1648년)으로 많은 영토를 잃었다
					</div>
					<div class="btn_area">
						<button type="button" class="btn_another">다른 질문하기</button>
						<button type="button" class="btn_reset">처음으로</button>
					</div>
				</div>
				<!--//.step03-->
			</div>
			<!--//.mrc_box-->

		</div>
		<!-- .demobox_mrc -->

		<!--.mrcmenu-->
		<div class="demobox" id="mrcmenu">

			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API 공통 가이드
					</div>
					<p class="sub_title">개발 환경 세팅</p>
					<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
					<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

				</div>
				<div class="guide_group">
					<div class="title">
						Bert MRC <small>(Machine Reading Comprehension)</small>
					</div>
					<p class="sub_txt">MRC는 내용이나 분량에 관계없이 어떠한 문서든 즉시 독해하고 사용자의 질문에 정확하게 답할 수 있는 혁신적인 인공지능(AI) 기술입니다.</p>

					<span class="sub_title">
								준비사항
							</span>
					<p class="sub_txt">① Input: 지문 (텍스트), 질문 (텍스트)</p>
					<p class="sub_txt">② 아래 Model 중 택 1  </p>
					<ul>
						<li>한글 (kor)</li>
						<li>영어 (eng)</li>

					</ul>
					<span class="sub_title">
								 실행 가이드
							</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/bert.mrc/</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId </td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey</td>
							<td>사용자의 고유 key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>lang </td>
							<td>사용할 모델의 언어 (kor / eng)  </td>
							<td>string</td>
						</tr>
						<tr>
							<td>context </td>
							<td>기계 독해를 실행할 지문 (텍스트) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>question</td>
							<td>답을 요구하는 질문 (텍스트)  </td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
						curl -X POST \ <br>
						https://api.maum.ai/api/bert.mrc/ \<br>
						-H 'Content-Type: application/json' \<br>
						-d '{<br>
						<span>"apiId":"(*ID 요청 필요)",</span><br>
						<span>"apiKey":"(*key 요청 필요)",</span><br>
						<span>"lang":"eng",</span><br>
						<span>"context":"Born in Hungary in 1913 as Friedmann
Endre Ernő, Capa was forced to leave his native country
after his involvement in anti government protests. Capa
had originally wanted to become a writer, but after his arrival in Berlin had first found work as a photographer.
He later left Germany and moved to France due to the rise
in Nazism. He tried to find work as a freelance
journalist and it was here that he changed his name to
Robert Capa, mainly because he thought it would sound
more American.",</span><br>
						<span>"question":"Why did Capa changed his name?"</span><br>
						}'

					</div>

					<p class="sub_txt">④ Response 파라미터 설명 </p>
					<span class="table_tit">Response</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>message </td>
							<td>API 동작여부 </td>
							<td>list</td>
						</tr>
						<tr>
							<td>answer </td>
							<td>지문 독해 후 입력한 질문에 대한 답변 </td>
							<td>string</td>
						</tr>
						<tr>
							<td>prob </td>
							<td>답변의 신뢰도 (0~1) </td>
							<td>number</td>
						</tr>
						<tr>
							<td>startIdx  </td>
							<td>지문 내 답변 위치의 시작 지점 </td>
							<td>number</td>
						</tr>
						<tr>
							<td>endIdx </td>
							<td>지문 내 답변 위치의 끝 지점  </td>
							<td>number</td>
						</tr>
					</table>
					<span class="table_tit">message: API 동작여부</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>message </td>
							<td>요청 처리 상태를 설명하는 문자열 (Success/ Fail) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>status</td>
							<td>요청 처리 상태에 대한 status code (0: Success)  </td>
							<td>number</td>
						</tr>
					</table>

					<p class="sub_txt">⑤ Response 예제 </p>
					<div class="code_box">
<pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "answer": "he thought it would sound more American",
    "prob": 0.2143673,
    "startIdx": 105,
    "endIdx": 111
}
</pre>
					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--//mrcmenu-->
		<!--.mrcexample-->
		<div class="demobox" id="mrcexample">
			<p><em style="color:#27c1c1;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

			<!-- AI 독해(MRC) -->
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>비정형 데이터 <strong>인식</strong></span>
							</dt>
							<dd class="txt">형식이 정해져 있지 않은 다양한 비정형 데이터를 정확하게 인식할 수 있습니다. 뉴스, 블로그, 매뉴얼 데이터 등 여러 형태의 데이터에 대한 학습 및 질의응답(QA) 시스템을 통해 높은 데이터 인식률과 정확한 답변을 제공합니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_mrc"><span>MRC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>영어 교육 서비스</span>
							</dt>
							<dd class="txt">아이들이 영문 책을 읽고 나서 책 내용에 해당되는 질문을 묻고, 답변을 듣고 검색하여 찾아줍니다. 아이들의 영어 교육 서비스에 다양하게 활용할 수 있습니다. </dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_mrc"><span>MRC</span></li>
									<li class="ico_stt"><span>STT</span></li>
									<li class="ico_tts"><span>TTS</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //AI 독해(MRC) -->
		</div>


	</div>

</div>
<!-- //.contents -->


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/mrc.js"></script>

<script type="text/javascript">
	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){


			$("#find_answer_btn").on('click',function(){
				let sentence = $('#id_input_text').val();
				let question = $('#id_input_question').val();
				let language = $("input[type=radio][name=radio]:checked").val();
				if(sentence == "" || $.trim(sentence)==""){
					alert("본문을 넣어 주세요!");
					return false;
				}
				if(question == "" || $.trim(question)==""){
					alert("질문을 입력해 주세요!");
					$("#id_input_question").val("");
					$("#id_input_question").focus();
					return false;
				}else{
					let result="";
					let original="";
					let param = {
						'sentence' : sentence,
						'question' : question,
						'lang' : language,
						'${_csrf.parameterName}' : '${_csrf.token}'
					};

					let resultStr ="";
					let confidenceLv = null;
					$.ajax({
						url: '${pageContext.request.contextPath}/api/mrc/',
						async: false,
						type: 'POST',
						headers: {
							"Content-Type": "application/x-www-form-urlencoded"
						},
						data: param,
						timeout: 20000,
						error: function(request){
							if (request.statusText == ""){
								alert("서버에 연결할 수 없습니다.");
							}else if (request.statusText == "INTERNAL SERVER ERROR"){
								alert("서버에서 답을 불러올 수 없습니다.");
							}else{
								alert(request.statusText);
							}
						},
						success: function(data){
							let responseData = JSON.parse(data);

							result = responseData['data'][0]['answers'][0]['answer'];
							original = responseData['data'][0]['original'];
							resultStr = original.toString().replace(result.toString(), "<strong>"+result.toString()+"</strong>");
							confidenceLv = (responseData['data'][0]['answers'][0]['prob']*100).toFixed(2);
						}

					});

					//결과 화면
					$(".resultBox").text("신뢰도: "+confidenceLv+" %");
					$('.step03 .result_txt').html(resultStr);

					$('.mrc_box .step01').hide();
					$('.mrc_box .step02').hide();
					$('.mrc_box .step03').fadeIn();
				}

			});

		});

	});


</script>

