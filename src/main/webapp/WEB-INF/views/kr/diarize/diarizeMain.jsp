<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">


<!-- 5 .pop_simple -->
<div class="pop_simple" id="api_upload_fail_popup">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap pop_sr_noti">
		<button class="pop_close" type="button">닫기</button>
		<!-- .pop_bd -->
		<div class="pop_bd">
			<em class="fas fa-sad-cry"></em>
			<h5>파일 업로드 실패</h5>
			<p>파일 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
				업로드 되지 않았습니다.</p>
			<span>* 지원가능 파일 확장자: .wav<br>
* sample rate 16000 / channels mono<br>
* 5MB 이하의 음성 파일을 이용해주세요.</span>

		</div>
		<!-- //.pop_bd -->
		<div class="btn">
			<a class="#">확인</a>
		</div>
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- 5 .pop_simple -->
<div class="pop_simple " id="api_fail_popup">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap">
		<button class="pop_close" type="button">팝업 닫기</button>
		<!-- .pop_bd -->
		<div class="pop_bd">
			<em class="fas fa-exclamation-triangle"></em>
			<p>Timeout error</p>
			<p style="font-size: small"><strong>업로드한 파일을 확인해주세요</strong></p>
			<p style="font-size: small">(samplerate = 16000, channel = mono)</p>

		</div>
		<!-- //.pop_bd -->
		<div class="btn" id="close_api_fail_popup">
			<a class="">확인</a>
		</div>
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

		<!-- .contents -->
		<div class="contents">
			<!-- .content -->
			<div class="content api_content">
				<h1 class="api_tit">화자분리</h1>
				<ul class="menu_lst voice_menulst">
					<li class="tablinks" onclick="openTap(event, 'diardemo')" id="defaultOpen"><button type="button">데모</button></li>
					<li class="tablinks" onclick="openTap(event, 'diarexample')"><button type="button">적용사례</button></li>
					<li class="tablinks" onclick="openTap(event, 'diarmenu')"><button type="button">매뉴얼</button></li>
<%--					<li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
				</ul>

				<!-- .demobox -->
				<div class="demobox" id="diardemo">
					<p><span>화자분리</span> <small>(Speaker Diarization)</small></p>
					<span class="sub">다양한 화자의 음성이 담겨있는 목소리를 듣고 각각의 화자를 인식해줍니다. </span>
					<!--diarize_box-->
					<div class="demo_layout diarize_box">
						<!--diarize_1-->
						<div class="diarize_1" >
							<div class="fl_box">
								<p><em class="far fa-file-audio"></em><strong>샘플 파일</strong>로 테스트 하기</p>
								<div class="sample_box">
									<div class="sample first_sample">
										<div class="radio">
											<input type="radio" id="sample1" name="option" value="샘플1" checked>
											<label for="sample1" class="">샘플1</label>
										</div>
										<!--player-->
										<div class="player">
											<div class="button-items">
												<audio id="music" class="music" preload="auto"  onended="audioEnded($(this))">
													<source src="${pageContext.request.contextPath}/aiaas/kr/audio/diarization/diarization-kor.mp3" type="audio/mp3">
													<p>Alas, your browser doesn't support html5 audio.</p>
												</audio>
												<div id="slider" class="slider">
													<div id="elapsed" class="elapsed"></div>
												</div>
												<p id="timer" class="timer">0:00</p>
												<p class="timer_fr">0:00</p>
												<div class="controls">
													<div id="play" class="play sample_play" >
													</div>
													<div id="pause" class="pause"  >
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sample">
										<div class="radio">
											<input type="radio" id="sample2" name="option" value="샘플2">
											<label for="sample2" class="">샘플2</label>
										</div>
										<!--player-->
										<div class="player">
											<div class="button-items">
												<audio id="music2" class="music" preload="auto"  onended="audioEnded($(this))">
													<source src="${pageContext.request.contextPath}/aiaas/kr/audio/diarization/diarization-eng.mp3" type="audio/mp3">
													<p>Alas, your browser doesn't support html5 audio.</p>
												</audio>
												<div class="slider">
													<div class="elapsed"></div>
												</div>
												<p class="timer">0:00</p>
												<p class="timer_fr">0:00</p>
												<div class="controls">
													<div class="play sample_play" >
													</div>
													<div class="pause" >
													</div>
												</div>
											</div>
										</div>
										<!--player-->
									</div>
								</div>
							</div>
							<div class="fr_box">
								<p><em class="far fa-file-audio"></em><strong>내 파일</strong>로 해보기</p>
								<div class="uplode_box">
									<div class="btn" id="uploadFile">
										<em class="fas fa-times hidden close"></em>
										<em class="far fa-file-image hidden"></em>
										<label for="demoFile" class="demolabel">.wav 파일 업로드</label>
										<input type="file" id="demoFile" class="demoFile" accept=".wav">
									</div>
									<ul>
										<li>* 지원가능 파일 확장자: .wav</li>
										<li>* sample rate 16000 / channels mono</li>
										<li>* 2MB 이하의 음성 파일을 이용해주세요.</li>
									</ul>
								</div>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_start" id="change_txt">화자 <span>분리하기</span></button>
							</div>
						</div>
						<!--diarize_1-->
						<!--diarize_2-->
						<div class="diarize_2">
							<p><em class="far fa-file-audio"></em>화자 분리 중</p>
							<div class="loding_box ">
								<!-- ball-scale-rotate -->
								<div class="lds">
									<div class="ball-scale-rotate">
										<span> </span>
										<span> </span>
										<span> </span>
									</div>
								</div>
								<p>약간의 시간이 소요 됩니다.</p>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_back1" id="reload_diarize"><em class="fas fa-redo"></em>처음으로</button>
							</div>


						</div>
						<!--diarize_2-->
						<!--diarize_3-->
						<div class="diarize_3">
							<p><em class="far fa-file-audio"></em>결과 파일</p>
							<div class="result_file" >
								<div class="result">
									<!--player-->
									<div class="player">
										<div class="button-items">
											<audio id="music3" class="music" preload="auto" onended="audioEnded($(this))">
												<source src="" type="audio/mp3">
												<p>Alas, your browser doesn't support html5 audio.</p>
											</audio>
											<div id="slider3" class="slider">
												<div id="elapsed3" class="elapsed"></div>
											</div>
											<div class="divide">
												<ul>
													<li class="start_num">0:00</li>

<%--													<li class="time_txt speaker2">3:50</li>--%>
<%--													<li class="time_txt speaker3">4:50</li>--%>
<%--                                                    <li class="time_txt speaker1">2:50</li>--%>
													<li class="last_num">5:00</li>
												</ul>
											</div>
											<!--
                                                                                        <p id="timer3" class="timer">0:00</p>
                                                                                        <p id="" class="timer_fr">0:00</p>
                                            -->
											<div class="controls">
												<div id="play3" class="play" >
												</div>
												<div id="pause3" class="pause" >
												</div>
											</div>
										</div>
									</div>
									<!--player-->
								</div>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
							</div>
						</div>
						<!--diarize_3-->
					</div>
					<!--// diarize_box-->
				</div>
				<!-- //.demobox -->


				<!--.diarmenu-->
				<div class="demobox voice_menu" id="diarmenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API 공통 가이드
							</div>
							<p class="sub_title">개발 환경 세팅</p>
							<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
							<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

						</div>
						<div class="guide_group">
							<div class="title">
								화자 분리
							</div>
							<p class="sub_txt"> 한 음성파일에 여러 목소리가 담겨져 있는 경우, 각각의 목소리를 인지하여 구분하여 분리해줍니다.</p>

							<span class="sub_title">
								준비사항
							</span>
							<p class="sub_txt">Input: 음성파일  </p>
							<ul>
								<li>확장자: .wav</li>
								<li>Sample rate: 16000</li>
								<li>Channels: mono</li>
								<li>특징: 다수의 화자의 발화가 담겨져있는 음성파일 (최대 5명)</li>
							</ul>
							<span class="sub_title">
								 실행 가이드
							</span>
							<p class="sub_txt">① Request  </p>
							<ul>
								<li>Method : POST</li>
								<li>URL : https://api.maum.ai/api/dap/diarize/</li>
							</ul>
							<p class="sub_txt">② Request 파라미터 설명 </p>
							<table>
								<tr>
									<th>키</th>
									<th>설명</th>
									<th>type</th>
								</tr>
								<tr>
									<td>apiId </td>
									<td>사용자의 고유 ID. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>apiKey </td>
									<td>사용자의 고유 key. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>reqVoice </td>
									<td>type:file (wav) 음성파일 </td>
									<td>string</td>
								</tr>
							</table>
							<p class="sub_txt">③ Request 예제 </p>
							<div class="code_box">
<pre>
curl -X POST \
   'https://api.maum.ai/api/dap/diarize/ \
   -H 'content-type: multipart/form-data;
boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
   -F apiId=(*ID 요청 필요) \
   -F apiKey=(*key 요청 필요) \
   -F 'reqVoice=@sample.wav'
</pre>
							</div>
							<p class="sub_txt">③ Response 파라미터 설명 </p>
							<span class="table_tit">Response</span>
							<table>
								<tr>
									<th>키</th>
									<th>설명</th>
									<th>type</th>
								</tr>
								<tr>
									<td>message </td>
									<td>API 동작여부 </td>
									<td>list</td>
								</tr>
								<tr>
									<td>data </td>
									<td>API 결과 data (json format) </td>
									<td>list</td>
								</tr>
							</table>
							<span class="table_tit">message: API 동작여부</span>
							<table>
								<tr>
									<th>키</th>
									<th>설명</th>
									<th>type</th>
								</tr>
								<tr>
									<td>message </td>
									<td>요청 처리 상태를 설명하는 문자열 (Success/ Fail) </td>
									<td>string</td>
								</tr>
								<tr>
									<td>status </td>
									<td>요청 처리 상태에 대한 status code (0: Success)  </td>
									<td>number</td>
								</tr>
							</table>
							<span class="table_tit">data: 결과값</span>
							<table>
								<tr>
									<th>키</th>
									<th>설명</th>
									<th>type</th>
								</tr>
								<tr>
									<td>speaker </td>
									<td>화자의 고유 번호 (묵음의 경우, ‘0’으로 표기) </td>
									<td>int</td>
								</tr>
								<tr>
									<td>startTime  </td>
									<td>해당 화자의 발화 시작 시점 </td>
									<td>number</td>
								</tr>
								<tr>
									<td>endTime   </td>
									<td>해당 화자의 발화 종료 시점 </td>
									<td>number</td>
								</tr>
							</table>
							<p class="sub_txt">④ Response 예제 </p>

							<div class="code_box">
								{<br>
								<span>"message": { </span><br>
								<span class="twoD">"message": "Success", </span><br>
								<span class="twoD">"status": 0 </span><br>
								<span>}, </span><br>
								<span>"data": [ </span><br>
								<span class="twoD"> { </span><br>
								<span class="thirD"> "speaker": 0, </span><br>
								<span class="thirD"> "startTime": 0.0, </span><br>
								<span class="thirD"> "endTime": 3.6 </span><br>
								<span class="twoD">}, </span><br>
								<span class="twoD"> { </span><br>
								<span class="thirD"> "speaker": 1, </span><br>
								<span class="thirD"> "startTime": 3.6, </span><br>
								<span class="thirD"> "endTime": 4.800000000000001 </span><br>
								<span class="twoD"> }, </span><br>
								<span class="twoD"> { </span><br>
								<span class="thirD">"speaker": 0, </span><br>
								<span class="thirD">"startTime": 4.800000000000001, </span><br>
								<span class="thirD">"endTime": 8.0 </span><br>
								<span class="twoD">}, </span><br>
								<span class="twoD"> { </span><br>
								<span class="thirD">"speaker": 1, </span><br>
								<span class="thirD"> "startTime": 8.0, </span><br>
								<span class="thirD">"endTime": 9.600000000000001 </span><br>
								<span class="twoD">}, </span><br>
								<span class="twoD">{ </span><br>
								<span class="thirD"> "speaker": 0, </span><br>
								<span class="thirD"> "startTime": 9.600000000000001, </span><br>
								<span class="thirD">7 support@mindslab.aiMinds Lab API Documentation July,2019 </span><br>
								<span class="thirD"> "endTime": 10.0 </span><br>
								<span class="twoD">}, </span><br>
								<span class="twoD">{ </span><br>
								<span class="thirD">"speaker": 1, </span><br>
								<span class="thirD">"startTime": 10.0, </span><br>
								<span class="thirD">"endTime": 13.200000000000001 </span><br>
								<span class="twoD">}, </span><br>
								<span class="twoD">{ </span><br>
								<span class="thirD">"speaker": 0, </span><br>
								<span class="thirD">"startTime": 13.200000000000001, </span><br>
								<span class="thirD">"endTime": 14.0 </span><br>
								<span class="twoD">}, </span><br>
								<span class="twoD">{ </span><br>
								<span class="thirD">"speaker": 1, </span><br>
								<span class="thirD">"startTime": 14.0, </span><br>
								<span class="thirD">"endTime": 15.200000000000001 </span><br>
								<span class="twoD">} </span><br>
								<span>] </span><br>
								}

							</div>
						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--.diarmenu-->
				<!--.diarexample-->
				<div class="demobox" id="diarexample">
					<p><em style="font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

					<!-- 화자 분리(Diarization) -->
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>회의록 자동작성</span>
									</dt>
									<dd class="txt">다수의 화자가 참여한 회의 음성 파일에서 누가, 언제 말했는지를 정확하게 분리해서 기록할 수 있습니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_dia"><span>화자 분리</span></li>
											<li class="ico_stt"><span>STT</span></li>
											<li class="ico_nlu"><span>NLU</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>다목적 대화 녹취</span>
									</dt>
									<dd class="txt">강연, 인터뷰, 각종 엔터테인먼트 콘텐츠 등의 다양한 목적의 대화를 화자별로 분리하여 음성인식을 진행할 수 있습니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_dia"><span>화자 분리</span></li>
											<li class="ico_stt"><span>STT</span></li>
											<li class="ico_nlu"><span>NLU</span></li>
										</ul>
									</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //화자 분리(Diarization) -->
				</div>
				<!--//.diarexample-->


			</div>
			<!-- //.content -->
		</div>
		<!-- //.contents -->


<script type="text/javascript">

	var timelineWidth = $('#slider').get(0).offsetWidth;
	var sample1wavFile;
	var sample2wavFile;
	// var sample1SrcUrl;
	// var sample2SrcUrl;


	// Set audio duration
	$('.music').each(function(idx){
		$(this).on("canplay", function () {
			var $parent = $(this).parent();
			var dur = this.duration;
			var fl_dur = Math.floor(dur);
            var endTime = toTimeFormat(fl_dur+"");
			if(idx === 2){
                $('.last_num').text(endTime);
			}else{
				$parent.children('.timer_fr').text(endTime);
			}

		});
	});

	$(document).ready(function () {
		loadSample1();
		loadSample2();


		// Time update event
		$('.music').on("timeupdate", function(){
			var curIdx = $('.music').index($(this));
			timeUpdate($(this), curIdx);
		});

		// Audio play
		$('.play').on('click', function (me) {
			var curIdx = $('.play').index(this);

			$('.play').each(function (idx,e) {
				if (e !== me.currentTarget) {
					$('.pause').eq(idx).css("display","none");
					$('.play').eq(idx).css("display","block");
					$('.music').eq(idx).trigger("pause");
				}
			});

			$('.music').eq(curIdx).trigger("play");
			$(this).css("display","none");
			$('.pause').eq(curIdx).css("display","block");
		});

		// Audio pause
		$('.pause').on('click', function () {
			var curIdx = $('.pause').index(this);

			$('.music').eq(curIdx).trigger("pause");
			$(this).css("display","none");
			$('.play').eq(curIdx).css("display","block");
		});


		$('.sample .sample_play').on('click', function(){
			var sampleNum = $('.sample .sample_play').index(this);
			$('input[type="radio"]:eq('+sampleNum+')').trigger('click');
		});

		$('input[type="radio"]').on('click', function () {
			$('em.close').trigger('click');
		});

		$('.fl_box').on('click', function(){
			if($('.fl_box').css("opacity") === "0.5"){
				$('.fl_box').css("opacity", "1");
			}
		});


		// 파일 업로드 후 이벤트
		document.querySelector("#demoFile").addEventListener('change', function (ev) {
			if(this.files[0] === undefined){
				$('em.close').trigger('click');
				return;
			}

			var file = this.files[0];
			var name = file.name;
			var size = (file.size / 1048576).toFixed(3); //size in mb
			var fileSize= file.size;
			var maxSize = 1024 * 1024 * 2; //2MB

			// console.log(fileSize);
			// console.log(maxSize);

			if (this.files[0].type.match(/audio\/wav/)  && fileSize < maxSize  ){
				$('input[type="radio"]:checked').prop("checked", false);
				$('.demolabel').text(name + ' (' + size + 'MB)');

				$('#uploadFile').removeClass( 'btn' );
				$('#uploadFile').addClass( 'btn_change' );
				$('.fl_box').css("opacity", "0.5");
			}
			else{
				this.value = null;
				$('#api_upload_fail_popup').fadeIn(300);
				$('.pop_close, .pop_bg, .btn a').on('click', function () {
					$('#api_upload_fail_popup').fadeOut(300);
					$('body').css({
						'overflow': '',
					});
				});
			}
		});


		// Remove uploaded file
		$('em.close').on('click', function () {
			$('#demoFile').val(null);
			$('.demolabel').text('.wav 파일 업로드');
			$(this).parent().removeClass("btn_change");
			$(this).parent().addClass("btn");
			$('.fl_box').css('opacity', '1');
		});


		$('#change_txt, .btn_back2, .demolabel').on('click', function(){
			$('.music').each(function(){
				$('.pause').trigger('click');
			});
		});


		// 음성 정제하기
		$('#change_txt').on('click',function (){
			var $checked = $('input[type="radio"]:checked');
			var $demoFile = $("#demoFile");
			var inputFile;
            var url;

			//내 파일로
			if ( $checked.length === 0 ){
				if ( $demoFile.val() === "" || $demoFile.val() === null){
					alert("샘플을 선택하거나 파일을 업로드해 주세요.");
					return 0;
				}
				inputFile = $demoFile.get(0).files[0];
                url = URL.createObjectURL(inputFile);
			}
			//샘플로
			else {
				if( $checked.val() === "샘플1"){
					inputFile = sample1wavFile;
				}else if( $checked.val() === "샘플2"){
					inputFile = sample2wavFile;
				}
			}

            // $('#music3').attr('src',url);

			$('.diarize_1').hide();
			$('.diarize_2').fadeIn(300);

			startRequest(inputFile);

		});


		// Result -> 처음으로
		$('.btn_back2').on('click', function () {
		    $('.time_txt').remove();
			$('.last_speaker').remove();
			$('.diarize_3').hide();
			$('.diarize_1').fadeIn(300);

			// audio UI setting
			$('.music').each(function(){ this.currentTime = 0; });
			$('.elapsed').css("width", "0px");
			$('.timer').text("0:00");
		});

        $('.btn_back1, .btn_back2').on('click', function () {
            timelineWidth = $('#slider').get(0).offsetWidth;
        });

	});



//------------------ Functions --------------------------

	// Update current play time and player bar
    //      obj : current .music jquery object
    //      idx : currnet .music index (= audio index)
	function timeUpdate(obj, idx) {
		var cur_music = obj.get(0);
		var playHead = $('.elapsed').eq(idx).get(0);
		var timer = $('.timer').eq(idx).get(0);
		var playPercent = timelineWidth * (cur_music.currentTime / cur_music.duration);
		playHead.style.width = playPercent + "px";

		var secondsIn = Math.floor(cur_music.currentTime);
		var curTime = toTimeFormat(secondsIn+"");
        if(idx === 2){
            $('.start_num').text(curTime);
        }else{
            timer.innerHTML = curTime;
        }
	}


	function audioEnded(audio){
        $('.elapsed').eq(idx).css("width", "100%");
	    var idx = $('.music').index(audio);
        // audio UI setting
        $('.pause').eq(idx).trigger('click');
        $('.music').eq(idx).get(0).currentTime = 0;
        $('.elapsed').eq(idx).css("width", "0px");
        $('.timer').eq(idx).text("0:00");
    }

	function loadSample1() {
		let blob = null;
		let xhr = new XMLHttpRequest();
		xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/audio/diarization/diarization-kor.wav");
		xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
		xhr.onload = function()
		{
			blob = xhr.response;//xhr.response is now a blob object
			// sample1File = new File([blob], "diarization-kor.wav");
			sample1wavFile = new Blob([blob], {type:'audio/wav'});
			// sample1SrcUrl = URL.createObjectURL(blob);

		};

		xhr.send();
	}

	function loadSample2() {
		let blob = null;
		let xhr = new XMLHttpRequest();
		xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/audio/diarization/diarization-eng.wav");
		xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
		xhr.onload = function()
		{
			blob = xhr.response;//xhr.response is now a blob object
			// sample2File = new File([blob], "diarization-eng.wav");
			sample2wavFile = new Blob([blob], {type:'audio/wav'});
			// sample2SrcUrl = URL.createObjectURL(blob);
		};

		xhr.send();
	}


	function startRequest(inputFile){
		var formData = new FormData();
		formData.append('file',inputFile);
		formData.append('${_csrf.parameterName}', '${_csrf.token}');

		//var request = new XMLHttpRequest();

		xhr = $.ajax({
			type: 'POST',
			async: true,
			url: '${pageContext.request.contextPath}/api/dap/diarize',
			data: formData,
			processData: false,
			contentType: false,
			timeout: 20000,
			success: function(result) {
				console.log(result);

				// let resultObj = JSON.parse(result);


				let jsonResult = result.json.data;
				let mp3Audio = "data:audio/mp3;base64," + result['mp3File'].body;
				let m3duration = null;
				$('#music3').attr('src', mp3Audio);
				$('#music3')[0].onloadedmetadata = function () {
					m3duration = $('#music3')[0].duration;
					let fl_dur = Math.floor(m3duration);
					let AudioendTime = toTimeFormat(fl_dur+"");
					$('.last_num').text(AudioendTime);
					console.log(m3duration);


					var segment = "";
					var prevEndTime = 0;
					var sum_li_length = 0;

					for(var i=0; i < jsonResult.length; i++){
						var endTime = jsonResult[i]['endTime'].toFixed(1);

						console.log(endTime);
						console.log(prevEndTime);
						console.log(m3duration);


						if(i < jsonResult.length-1) {
							segment += "<li class='time_txt' style='width:calc("+((endTime-prevEndTime)/m3duration)*100+"%)'>"+endTime+"<span>"+ (jsonResult[i]['speaker']) +"</span></li>";
							sum_li_length += ((endTime-prevEndTime)/m3duration)*100;
						}else{
							var sub = 100 - sum_li_length;
							segment += "<li class='last_speaker' style='width:calc("+sub+"% - 25px)'><span>"+ (jsonResult[i]['speaker']) +"</span></li>";
							// 25px는 .start_num의 사이즈
						}
						prevEndTime = endTime;
					}

					$('.divide .start_num').after(segment);

					$('.diarize_2').hide();
					$('.diarize_3').fadeIn(300);
					timelineWidth = $('#slider3').get(0).offsetWidth;
				};

			}, error: function(status, error) {
				//console.dir(error);

				$('#api_fail_popup').show();

				$('#close_api_fail_popup').on('click', function () {
					window.location.reload();
				});
				$('.pop_close, .pop_bg, .btn a').on('click', function () {
					$('#api_fail_popup').fadeOut(300);
					$('body').css({
						'overflow': '',
					});
					window.location.reload();
				});
			}

		});


		// request.responseType ="blob";
/*
		request.onreadystatechange = function(){
			if (request.readyState === 4 && request.status !== 0){
				console.log(request.response);

                var jsonResult = JSON.parse(request.response)['data'];
                var segment = "";
                var m3duration = $('#music3').get(0).duration;
                var prevEndTime = 0;
				var sum_li_length = 0;

                for(var i=0; i < jsonResult.length; i++){
                    var endTime = jsonResult[i]['endTime'].toFixed(1);

					if(i < jsonResult.length-1) {
						segment += "<li class='time_txt' style='width:calc("+((endTime-prevEndTime)/m3duration)*100+"%)'>"+endTime+"<span>"+ (jsonResult[i]['speaker']) +"</span></li>";
						sum_li_length += ((endTime-prevEndTime)/m3duration)*100;
					}else{
						var sub = 100 - sum_li_length;
						segment += "<li class='last_speaker' style='width:calc("+sub+"% - 25px)'><span>"+ (jsonResult[i]['speaker']) +"</span></li>";
						// 25px는 .start_num의 사이즈
					}
					prevEndTime = endTime;
                }

                $('.divide .start_num').after(segment);

				$('.diarize_2').hide();
				$('.diarize_3').fadeIn(300);
                timelineWidth = $('#slider3').get(0).offsetWidth;
			}
		};
		request.open('POST', '/api/dap/diarize');
		request.send(formData);*/

		// step2->step1
		$('.btn_back1').on('click', function () {
			if(xhr)
				xhr.abort();

			// audio UI setting
			$('.music').each(function(){ this.currentTime = 0; });
			$('.elapsed').css("width", "0px");
			$('.timer').text("0:00");

			$('.diarize_2').hide();
			$('.diarize_1').fadeIn(300);

			window.location.reload();
		});
	}


    function toTimeFormat (text) {
        var sec_num = parseInt(text, 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        // if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        
        return minutes+':'+seconds;
    }
</script>
<script>
	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
