<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/download.js"></script>
<script type="text/javascript">
	var data;
	$(document).ready(function(){

		$('#sub').on('click',function () {
			var formData = new FormData();

			var length = $("#length").val();
			var type = $("#type").val();
			var color = $("#color").val();

			if(length == ''){
				alert("소매길이를 선택해주세요.");
				$("#length").focus();
				return;
			}
			if(type == ''){
				alert("옷종류를 선택해주세요.");
				$("#type").focus();
				return;
			}
			if(color == ''){
				alert("색깔을 선택해주세요.");
				$("#color").focus();
				return;
			}

			var reqText = '';

			if($("#length").val() != ''){
				reqText = $("#length").val();
			}
			if($("#fabric").val() != ''){
				reqText = reqText+' '+$("#fabric").val();
			}
			if($("#type").val() != ''){
				reqText = reqText+' '+$("#type").val();
			}
			if($("#color").val() != ''){
				reqText = reqText+' in '+$("#color").val()+'.';
			}
			if($("#neck").val() != ''){
				reqText = reqText+' '+$("#neck").val();
			}
			if($("#zip").val() != ''){
				reqText = reqText+' with '+$("#zip").val()+'.';
			}

			$("#reqText").val(reqText);
			//alert(reqText);

			$(".sentence").text(reqText);

			formData.append('reqText',reqText);
			formData.append('${_csrf.parameterName}','${_csrf.token}');

			var request = new XMLHttpRequest();

			// var imageTag = document.querySelector('img');
			//request.responseType="blob";
			request.onloadstart = function(ev) {
				request.responseType = "blob";
			}
			request.onreadystatechange=function () {
				if(request.readyState===4){
					console.log(request.response);
					var blob = request.response;
					var imgSrcURL = URL.createObjectURL(blob);
					console.log(imgSrcURL);
					console.log(blob.size);

					data = new Blob([request.response], {type:'image/jpeg'});
					// downloadResultImg();

					if(blob.size === 1175){
						alert('TTI 이미지가 존재하지 않습니다.');
						// $("#reqText").focus();
						$('.tti_2').hide();
						$('.tti_1').fadeIn(300);
					}else{
						// imageTag.src=imgSrcURL;
						var tti_img = document.getElementById('ttiImg');
						//var blob = request.response;
						tti_img.setAttribute("src", imgSrcURL );
						$('.tti_1').hide();
						$('.tti_2').hide();
						$('.tti_3').fadeIn(300);
						// $('.stylingBoxUl').hide();
					}
				}
			};
			request.timeout = 20000;
			request.ontimeout = function() {
				alert('timeout');
				window.location.reload();
			}
			request.open('POST','${pageContext.request.contextPath}/api/getTtiImage');
			request.send(formData);

			$('.tti_1').hide();
			$('.tti_2').fadeIn(300);

		})
	});

	function downloadResultImg(){
		let fileName = "TTI.jpg";
        download(data, fileName, "image/jpg");
	}

	/** 이미지 생성  */
</script>

<!-- .contents -->
<div class="contents">
	<!-- .content -->
	<div class="content api_content">
		<h1 class="api_tit">AI 스타일링</h1>
		<ul class="menu_lst vision_lst">
			<li class="tablinks" onclick="openTap(event, 'ttidemo')" id="defaultOpen"><button type="button">데모</button></li>
			<li class="tablinks" onclick="openTap(event, 'ttiexample')"><button type="button">적용사례</button></li>
			<li class="tablinks" onclick="openTap(event, 'ttimenu')"><button type="button">매뉴얼</button></li>
<%--			<li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
		</ul>
		<!-- .demobox -->
		<div class="demobox" id="ttidemo">
			<p>TTI<small> (Text-To-Image)</small> 를 이용한 <span>AI 스타일링</span></p>
			<span class="sub">원하는 스타일을 선택해서 만들어 보세요! 텍스트로 표현하면 이미지로 나타내 줍니다.</span>
			<!--tti_box-->
			<div class="tti_box">
				<input type="hidden" id="reqText" name="reqText" value="" />
				<!--tti_box-->
				<div class="demo_layout tti_box">
					<!--tti_1-->
					<div class="tti_1" >
						<div class="tti_selectarea">
							<div class="box_select necessary">
								<span>*필수 선택</span>
								<div class="selectType02">
									<label for="length">소매길이</label>
									<select id="length">
										<option value="">*소매길이</option>
										<option value="Long sleeve">Long sleeve</option>
										<option value="Short sleeve">Short sleeve</option>
										<option value="Sleeveless">Sleeveless</option>
									</select>
								</div>
								<div class="selectType02">
									<label for="type">옷종류</label>
									<select id="type">
										<option value="">*옷종류</option>
										<option value="Jacket">Jacket</option>
										<option value="Maxi Dress">Maxi Dress</option>
										<option value="Shirt">Shirt</option>
										<option value="T-shirt">T-shirt</option>
									</select>
								</div>
								<div class="selectType02">
									<label for="color">색깔</label>
									<select id="color">
										<option value="">*색깔</option>
										<option value="Beige">Beige</option>
										<option value="Black">Black</option>
										<option value="Blue">Blue</option>
										<option value="Brown">Brown</option>
										<option value="Dark blue">Dark blue</option>
										<option value="Green">Green</option>
										<option value="Grey">Grey</option>
										<option value="Indigo blue">Indigo blue</option>
										<option value="Light blue">Light blue</option>
										<option value="Red">Red</option>
										<option value="White">White</option>
									</select>
								</div>
							</div>
							<div class="box_select optional">
								<span>선택 사항</span>
								<div class="selectType02">
									<label for="fabric">옷감</label>
									<select id="fabric">
										<option value="">옷감</option>
										<option value="Cotton">Cotton</option>
										<option value="Knit">Knit</option>
										<option value="Linen">Linen</option>
									</select>
								</div>
								<div class="selectType02">
									<label for="neck">넥타입</label>
									<select id="neck">
										<option value="">넥타입</option>
										<option value="Band collar">Band collar</option>
										<option value="Crewneck">Crewneck</option>
										<option value="V-neck-collar">V-neck collar</option>
									</select>
								</div>
								<div class="selectType02">
									<label for="zip">지퍼/단추</label>
									<select id="zip">
										<option value="">지퍼/단추</option>
										<option value="Button closure">Button closure</option>
										<option value="Zip closure">Zip closure</option>
									</select>
								</div>
							</div>
						</div>
						<div class="styling_box">
							<p><em class="fas fa-tshirt"></em>내 스타일링</p>
							<ul class="stylingBoxUl">

							</ul>
							<div class="sentence_box">
								<p>스타일링 생성문장</p>
								<div class="sentence"></div>
							</div>
						</div>
						<div class="btn_area">
							<button type="button" class="btn_start" id="sub">스타일링<br>생성하기</button>
						</div>
					</div>
					<!--tti_1-->
					<!--tti_2-->
					<div class="tti_2">
						<p><em class="fas fa-tshirt"></em>스타일 생성중</p>
						<div class="loding_box ">

							<!-- ball-scale-rotate -->
							<div class="lds">
								<div class="ball-scale-rotate">
									<span> </span>
									<span> </span>
									<span> </span>
								</div>
							</div>
							<p>약간의 시간이 소요 됩니다. <br>(약 10초 내외)</p>
						</div>
						<div class="btn_area">
							<button type="button" class="btn_back1" id="reload_tti"><em class="fas fa-redo"></em>처음으로</button>
						</div>

					</div>
					<!--tti_2-->
					<!--tti_3-->
					<div class="tti_3">
						<p><em class="fas fa-tshirt"></em>스타일 생성 완료</p>
						<div class="result_img">
							<img id="ttiImg" src="" alt="ttiImg" alt="스타일 생성 완료 이미지">
						</div>
						<a id="save" onclick="downloadResultImg();" class="dwn_link" ><em class="far fa-arrow-alt-circle-down"></em> 결과 파일 다운로드</a>

						<div class="styling_box">
							<ul class="stylingBoxUl">
								<li>Concealed zip closure at side</li>
								<li>Shirt</li>
								<li>Mini Dress</li>
								<li>Dark Blue</li>
								<li>Japanese selvedge denim</li>
								<li>Mini Dress</li>
								<li>Dark Blue</li>
							</ul>
							<div class="sentence_box">
								<p>스타일링 생성문장</p>
								<div class="sentence"></div>
							</div>
						</div>

						<div class="btn_area">
							<button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
						</div>
					</div>
					<!--tti_3-->
				</div>
				<!--tti_box-->
				<span class="remark">*AI 스타일링은 샘플 모델입니다. TTI를 이용하여 다른 모델을 만들고 싶다면 고객센터로 문의하세요.</span>
				<!--// tti-->

			</div>
		</div>
		<!-- //.demobox -->

		<!--.ttimenu-->
		<div class="demobox vision_menu" id="ttimenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API 공통 가이드
					</div>
					<p class="sub_title">개발 환경 세팅</p>
					<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
					<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

				</div>
				<div class="guide_group">
					<div class="title">
						Text-To-Image (TTI)
					</div>
					<p class="sub_txt"> 텍스트로 입력한 것을 이미지로 보여줍니다. 현재는 옷 스타일만 제공됩니다.</p>

					<span class="sub_title">
								준비사항
							</span>
					<p class="sub_txt">- Input: 가이드라인에 따른 영문 텍스트(문장)</p>
					<ul>
						<li>상의 가이드라인 <br>
							(*필수입력정보) 소매 길이 &amp; 종류 &amp; 색<br>
							(부가입력정보) 핏, 넥 타입(ex. v-neck collar or crewneck 등), 단추, 지퍼<br>
							정보 등
						</li>
						<li>하의 가이드라인<br>
							(*필수입력정보) 종류 &amp; 색<br>
							(부가입력정보) 핏, fading, 옷감(ex.denim 등) 단추, 지퍼 정보 등<br>
						</li>
					</ul>
					<p class="sub_txt">- Input 예시 문장</p>
					<ul>
						<li>상의 예시문 1)<br>
							Long sleeve flannel shirt in burgundy and white. Signature check print
							throughout. V-neck collar. Button closure at front. Single-button barrel
							cuffs. Tonal stitching.</li>
						<li> 상의 예시문 2)<br>
							Long sleeve padded nylon bomber jacket in 'khaki' green. Oversized fit.
							Slips on. Rib knit modified stand collar, cuffs, and hem. Vented central
							seam and flap pockets at front. Utility pocket at upper sleeve. Tonal
							stitching.
						</li>
						<li>하의 예시문 1)<br>
							Slim-fit jeans in 'dark vintage' indigo. High-rise. Japanese selvedge
							denim. Fading, whiskering, and honeycombing throughout. cutting left knee. Five-pocket styling. Zip-fly. Silver-tone hardware. Contrast
							stitching in tan.
						</li>
						<li>하의 예시문 2)<br>
							Circle skirt in black. Concealed zip closure at side. Tonal stitching
						</li>
					</ul>
					<span class="sub_title">
								 실행 가이드
							</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/tti_image/</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId </td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>사용자의 고유 key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>reqText </td>
							<td>이미지로 출력하고자하는 의류를 가이드라인에 맞게 표현하는 영문 텍스트 문장 </td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
						curl -X POST \<br>
						https://api.maum.ai/api/tti_image/ \<br>
						-H 'Content-Type: application/json' \<br>
						-d '{<br>
						"apiId": "(*ID 요청 필요)",<br>
						"apiKey": "(*key 요청 필요)",<br>
						"reqText": "Long sleeve flannel shirt in burgundy<br>
						and white. Signature check print throughout. V-neck<br>
						collar. Button closure at front. Single-button barrel<br>
						cuffs. Tonal stitching."<br>
						}<br>
					</div>

					<p class="sub_txt">④ Response 예제 </p>
					<div class="code_box">
						<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_tti.png" alt="text removal image" style="width:20%;">
					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--.ttimenu-->
		<!--.ttiexample-->
		<div class="demobox" id="ttiexample">
			<p><em style="color:#f7778a;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

			<!-- AI 스타일링(TTI) -->
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>쇼핑몰 이미지 <strong>검색</strong></span>
							</dt>
							<dd class="txt">검색하고자 하는 상품의 색상, 사이즈, 패턴 등의 특징을 입력하여 좀 더 쉽게 검색할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_tti"><span>TTI</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>몽타주 작성</span>
							</dt>
							<dd class="txt">눈, 코, 입 등 얼굴의 특징을 묘사하면 이를 그대로 이미지로 생성해냄으로써 특정인의 얼굴을 만들 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_tti"><span>TTI</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 03</em>
								<span>디자인 목업</span>
							</dt>
							<dd class="txt">디자인 아이디에이션 단계에서 편리하게 이미지를 생성하고, 조합할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_tti"><span>TTI</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //AI 스타일링(TTI) -->
		</div>
		<!--//.ttiexample-->

	</div>
	<!-- //.content -->
</div>
<!-- //.contents -->

<script>
	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){

			$('#reload_tti').on('click', function () {
				window.location.reload();
			});

			$('.selectType02').on('click', function () {
				textMake();
				stylingBoxMake();

				var $opt_close = $('span.opt_close');
				var $sel5 = $("#fabric");
				var $sel4 = $("#neck");
				var $sel6 = $("#zip");

				var opt4 = "opt4";
				var opt5 = "opt5";
				var opt6 = "opt6";
				$opt_close.on('click', function () {
					var $li_id = $(this).parent().attr('id');
					if($li_id == opt5 ){
						$sel5.val('');
						$('li#opt5').hide();
						textMake();
					} else if ($li_id == opt4){
						$sel4.val('');
						$('li#opt4').hide();
						textMake();
					}else if ($li_id == opt6){
						$sel6.val('');
						$('li#opt6').hide();
						textMake();
					}

				});
			});

			// step2->step3
			$('.btn_back1').on('click', function () {
				$('.tti_2').hide();
				$('.tti_3').fadeIn(300);
				// $('.stylingBoxUl').hide();
			});

			// step3->step1
			$('.btn_back2').on('click', function () {
				$('.tti_3').hide();
				$('.tti_1').fadeIn(300);
				// $('.stylingBoxUl').show();

			});

			function textMake(){
				var reqText = '';

				if($("#length").val() != ''){
					reqText = $("#length").val();
				}
				if($("#fabric").val() != ''){
					reqText = reqText+' '+$("#fabric").val();
				}
				if($("#type").val() != ''){
					reqText = reqText+' '+$("#type").val();
				}
				if($("#color").val() != ''){
					reqText = reqText+' in '+$("#color").val()+'.';
				}
				if($("#neck").val() != ''){
					reqText = reqText+' '+$("#neck").val();
				}
				if($("#zip").val() != ''){
					reqText = reqText+' with '+$("#zip").val()+'.';
				}

				$(".sentence").text(reqText);
			}

			function stylingBoxMake(){
				$(".styling_box ul li").remove();

				var selected1 = $("#length option:selected");
				var selected2 = $("#type option:selected");
				var selected3 = $("#color option:selected");
				var selected4 = $("#neck option:selected");
				var selected5 = $("#fabric option:selected");
				var selected6 = $("#zip option:selected");

				var output = "";

				if(selected1.val() != 0){
					output = "<li id=\"opt1\">" + selected1.text() + "</li>";
					$(".styling_box ul").append(output);
				}

				if(selected5.val() != 0){
					output = "<li class='optional'  id=\"opt5\">" + selected5.text() +"<span class=\"opt_close\"></span>" + "</li>";
					$(".styling_box ul").append(output);
				}

				if(selected2.val() != 0){
					output = "<li id=\"opt2\">" + selected2.text() + "</li>";
					$(".styling_box ul").append(output);
				}

				if(selected3.val() != 0){
					output = "<li id=\"opt3\">" + selected3.text() + "</li>";
					$(".styling_box ul").append(output);
				}

				if(selected4.val() != 0){
					output = "<li class='optional' id=\"opt4\">" + selected4.text() +"<span class=\"opt_close\"></span>" + "</li>";
					$(".styling_box ul").append(output);
				}

				if(selected6.val() != 0){
					output = "<li class='optional' id=\"opt6\">" + selected6.text() +"<span class=\"opt_close\"></span>" + "</li>";
					$(".styling_box ul").append(output);
				}

				if(output == ''){
					$(".styling_box ul").append(output);
				}
			}

		});
	});

	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();

</script>