<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>파일 업로드 실패</h5>
            <p>파일 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>* 지원가능 파일 확장자: .avi, .mp4<br>
* 동영상파일 용량 50MB 이하만 가능합니다</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">얼굴 추적</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'ftdemo')" id="defaultOpen">
                <button type="button">데모</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'ftexample')">
                <button type="button">적용사례</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'ftmenu')">
                <button type="button">매뉴얼</button>
            </li>
<%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="ftdemo">
            <p><span>얼굴 추적 </span><small>(Face Tracking)</small></p>
            <span class="sub">동영상 내에 있는 얼굴을 인식하여 추출하는 인공지능 엔진입니다.  </span>
            <!--faceTracking_box-->
            <div class="demo_layout faceTracking_box">
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-video"></em><strong>샘플 파일</strong>로 테스트 하기</p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="1" checked>
                                    <label for="sample1" class="">
                                        <video id="sampleVideo1" controls width="300" height="200">
                                            <source src="${pageContext.request.contextPath}/aiaas/kr/video/faceTracking2.mp4"
                                                    type="video/mp4">
                                            IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.
                                        </video>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-video"></em><strong>내 파일</strong>로 해보기</p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-image hidden"></em>
                                <label for="demoFile" class="demolabel">동영상 업로드</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".avi, .mp4">
                            </div>
                            <ul>
                                <li>* 지원가능 파일 확장자: .avi, .mp4</li>
                                <li>* 동영상파일 용량 50MB 이하만 가능합니다.</li>
                                <li>* 사람의 얼굴 정면이 120 x 160 pixel 이상인 경우에 한해서 인식률이 높습니다.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">영상 분석</button>
                    </div>
                </div>
                <!--tr_1-->
                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-video"></em>얼굴 검출 처리중</p>
                    <div class="loding_box ">
                        <!-- ball-scale-rotate -->
                        <div class="lds">
                            <div class="ball-scale-rotate">
                                <span> </span>
                                <span> </span>
                                <span> </span>
                            </div>
                        </div>
                        <p>약간의 시간이 소요 됩니다.</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>
                <!--tr_2-->
                <!--tr_3-->
                <div class="tr_3">

                    <div class="result_file">
                        <p><em class="far fa-file-image"></em>결과 파일</p>
                        <ul class="scenebox">
                            <li class="scene">
                                <div class="imgbox">
                                    <img id="resultImg1" src="" alt="결과 1">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime1">Scene 1 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt1">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox">
                                    <img id="resultImg2" src="" alt="결과 2">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime2">Scene 2 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt2">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox">
                                    <img id="resultImg3" src="" alt="결과 3">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime3">Scene 3 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt3">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox">
                                    <img id="resultImg4" src="" alt="결과 4">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime4">Scene 4 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt4">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox">
                                    <img id="resultImg5" src="" alt="결과 5">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime5">Scene 5 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt5">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox">
                                    <img id="resultImg6" src="" alt="결과 6">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime6">Scene 6 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt6">
                                        </textarea>
                                    </div>
                                </div>
                            </li>

                            <li class="scene">
                                <div class="imgbox">
                                    <img id="resultImg7" src="" alt="결과 7">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime7">Scene 7 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt7">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox">
                                    <img id="resultImg8" src="" alt="결과 8">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime8">Scene 8 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt8">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox">
                                    <img id="resultImg9" src="" alt="결과 9">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime9">Scene 9 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt9">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox">
                                    <img id="resultImg10" src="" alt="결과 10">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime10">Scene 10 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt10">
                                        </textarea>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2" id=""><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.faceTracking_box-->

        </div>
        <!-- //.demobox -->
        <!--.ftmenu-->
        <div class="demobox vision_menu" id="ftmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

                </div>
                <div class="guide_group">
                    <div class="title">
                        얼굴 추적 <small>(Face Tracking)</small>
                    </div>
                    <p class="sub_txt">동영상 내 얼굴을 인식하여 같은 얼굴일 경우, 추적해줍니다. </p>
                    <span class="sub_title">
								준비사항
					</span>
                    <p class="sub_txt">- Input: 비디오 파일 </p>
                    <ul>
                        <li>확장자: .mp4, .avi</li>
                        <li>용량: 50MB 이하  </li>
                    </ul>
                    <span class="sub_title">
								 실행 가이드
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/Vca/faceTracking</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>사용자의 고유 ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>video</td>
                            <td>type:file (.mp4, .avi) 비디오 파일 </td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        curl --location --request POST 'http://api.maum.ai/Vca/faceTracking' \<br>
                        --header 'Content-Type: multipart/form-data' \<br>
                        --form 'apiId= 발급받은 API ID' \<br>
                        --form 'apiKey= 발급받은 API KEY' \<br>
                        --form 'video= 얼굴 추적할 동영상 파일'<br>
                    </div>

                    <p class="sub_txt">④ Response 예제 </p>

                    <div class="code_box">
<pre>
{
    "frame_1": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_1": "[{\"sceneTime\": 80, \"faceId\": null, \"faceRectangle\": {\"left\": null, \"top\": null, \"width\": null, \"height\": null}}]",

    "frame_2": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_2": "[{\"sceneTime\": 82, \"faceId\": null, \"faceRectangle\": {\"left\": null, \"top\": null, \"width\": null, \"height\": null}}]",

    "frame_3": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_3": "[{\"sceneTime\": 84, \"faceId\": \"64\", \"faceRectangle\": {\"left\": 518, \"top\": 166, \"width\": 128, \"height\": 179}}]",

    "frame_4": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_4": "[{\"sceneTime\": 86, \"faceId\": \"64\", \"faceRectangle\": {\"left\": 438, \"top\": 198, \"width\": 131, \"height\": 163}}]",

    "frame_5": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_5": "[{\"sceneTime\": 88, \"faceId\": \"45\", \"faceRectangle\": {\"left\": 267, \"top\": 119, \"width\": 149, \"height\": 180}}]",

    "frame_6": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_6": "[{\"sceneTime\": 90, \"faceId\": \"44\", \"faceRectangle\": {\"left\": 507, \"top\": 144, \"width\": 134, \"height\": 190}}]",

    "frame_7": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_7": "[{\"sceneTime\": 92, \"faceId\": \"64\", \"faceRectangle\": {\"left\": 499, \"top\": 150, \"width\": 134, \"height\": 192}}]",

    "frame_8": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_8": "[{\"sceneTime\": 94, \"faceId\": \"64\", \"faceRectangle\": {\"left\": 481, \"top\": 197, \"width\": 137, \"height\": 184}}]",

    "frame_9": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_9": "[{\"sceneTime\": 96, \"faceId\": \"64\", \"faceRectangle\": {\"left\": 478, \"top\": 183, \"width\": 136, \"height\": 187}}]",

    "frame_10": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_10": "[{\"sceneTime\": 98, \"faceId\": \"64\", \"faceRectangle\": {\"left\": 484, \"top\": 171, \"width\": 137, \"height\": 187}}]"

}
</pre>
                    </div>

                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.ftmenu-->
        <!--.ftexample-->
        <div class="demobox" id="ftexample">
            <p><em style="color:#f7778a;font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>

            <!-- 얼굴 추적 -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>저작권 보호<br>영상 여부 판단</span>
                            </dt>
                            <dd class="txt">비디오 인식을 통해 특정 사람의 추적 및 식별을 수행하여 저작권 보호 영상 여부를 판단합니다.
                                <span><em class="fas fa-book-reader"></em> Reference: 한국저작권보호원</span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_ftr"><span>얼굴 추적</span></li>
                                    <li class="ico_sr"><span>자막 인식</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>범죄자 추적</span>
                            </dt>
                            <dd class="txt">CCTV 내의 얼굴 추적 및 검출을 통하여 특정 범죄자의 이동경로 및 소재를 파악합니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_ftr"><span>얼굴 추적</span></li>
                                    <li class="ico_anor"><span>이상 행동</span></li>
                                    <li class="ico_tr"><span>ESR</span></li>
                                    <li class="ico_fr"><span>얼굴 인증</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>미아 찾기</span>
                            </dt>
                            <dd class="txt">특정 지역 내에서 CCTV를 활용하여 잃어버린 아이를 찾는 서비스에 활용할 수 있습니다.
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_ftr"><span>얼굴 추적</span></li>
                                    <li class="ico_fr"><span>얼굴 인증</span></li>
                                    <li class="ico_tr"><span>ESR</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!--얼굴 추적   -->
        </div>
        <!--//.ftexample-->


    </div>
    <!-- //.content -->
</div>

<script>

    var sample1SrcUrl;
    var sample1File;
    var ajaxXHR;

    let image1_blob;

    //파일명 변경
    document.querySelector("#demoFile").addEventListener('change', function (ev) {

        $('.fl_box').on('click', function () {
            $(this).css("opacity", "1");
            $('.tr_1 .btn_area .disBox').remove();
        });

        //파일 용량 체크
        var demoFileInput = document.getElementById('demoFile');
        var demoFile = demoFileInput.files[0];
        var demoFileSize = demoFile.size;
        var max_demoFileSize = 1024 * 1024 * 50;//1kb는 1024바이트
        var demofile = document.querySelector("#demoFile");

        if (demoFileSize > max_demoFileSize || !demofile.files[0].type.match(/video.mp4/)) {
            $('.pop_simple').show();
            // 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
            $('.fl_box').css("opacity", "0.5");
            $('#demoFile').val('');
            $('.tr_1 .btn_area').append('<span class="disBox"></span>');

        } else {
            document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
            var element = document.getElementById('uploadFile');
            element.classList.remove('btn');
            element.classList.add('btn_change');
            $('.fl_box').css("opacity", "0.5");
            $('.tr_1 .btn_area .disBox').remove();

            $('#sample1').prop('checked', false);
        }

    });

    function sendApiRequest(file) {
        var formData = new FormData();
        formData.append('file', file);
        formData.append('${_csrf.parameterName}', '${_csrf.token}');

        //console.log(" @ Multipart Request start");

        ajaxXHR = $.ajax({
            type: "POST",
            async: true,
            url: "${pageContext.request.contextPath}/api/faceTracking",
            data: formData,
            processData: false,
            contentType: false,
            timeout: 20000,
            success: function (result) {
                $('.tr_2').hide();
                $('.tr_3').fadeIn(300);

                let resultData = JSON.parse(result);

                for(var i=1 ; i<=10 ; i++) {

                    // result image
                    if( typeof resultData["frame_" + i] !== 'undefined')
                        $('#resultImg' + i).attr('src', "data:image/jpeg;base64," + resultData["frame_" + i]);
                    else
                        $('#resultImg' + i).attr('src', '');

                    // result text (time, text)
                    if( typeof resultData["result_json_" + i] !== 'undefined') {
                        // result time
                        $('#resultTime' + i).html("SceneTime : " + JSON.parse(resultData["result_json_" + i])[0]["sceneTime"] + " s");

                        // result text (sceneTime, faceId, faceRectangle)
                        var responseString = JSON.stringify( JSON.parse(resultData["result_json_" + i]), undefined, 8);
                        var textAreaObj = document.getElementById('resultTxt' + i);
                        textAreaObj.scrollTop = textAreaObj.offsetHeight - 300;
                        textAreaObj.innerHTML = responseString;
                    } else {
                        // console.log(typeof result[ (i*2) -1 ]);
                        $('#resultTime' + i).html("SceneTime : ");
                        $('#resultTxt' + i).html("추출된 faceId가 없습니다.");
                    }
                }
            },
            error: function (jqXHR) {
                if (jqXHR.status === 0) {
                    return false;
                }
                alert("ERROR");
                window.location.reload();
            }
        });
    }

    function initResult() {

        for (var i = 1; i <= 10; i++) {
            $('#resultImg' + i).attr('src', '');
            $('#resultTxt' + i).text();
            $('#resultTime' + i).text();
        }
    }

    function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/video/faceTracking2.mp4");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function () {
            blob = xhr.response; //xhr.response is now a blob object
            //sample1File = new File([blob], "sample1.mp4");
            sample1SrcUrl = URL.createObjectURL(blob);

            image1_blob = blob;
        };
        xhr.send();
    }

    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            loadSample1();

            $('.btn_start').on('click', function () {
                //console.log(" @ faceTracking recogButton click");

                if ($('#sample1').prop('checked')) { // 샘플 파일로 해보기
                    // console.log("Sample file !! ");

                    var blob = sample1File;
                    var filename = Date.now() + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).toString(2, 15);
                    /*
                    console.log("blob");
                    console.log(blob);
                    console.log("type");
                    */
                   /* if (blob == null) {
                        alert("동영상 파일 로딩 중입니다. 잠시만 기다려주세요^^");
                        return;

                    } else {*/
                        //console.log(blob.type);
                        //filename += '.' + blob.type.split('/').pop();

                        //var file = new File([blob], filename, {type: blob.type, lastModified: Date.now()});
                        var file = image1_blob;
                    //}
                    // console.log("File size : " + file.size);
                } else { // 내 파일로 해보기
                    // console.log("Your local file !! ");

                    var blob = document.getElementById('demoFile').files[0];
                    var filename = Date.now() + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).toString(2, 15);
                    image1_blob = blob;
                    /*if (blob1 == null) {
                        alert("동영상 파일 로딩 중입니다. 잠시만 기다려주세요.");
                        return;

                    } else {*/
                    console.log(blob.type.split('/'));
                    filename += '.' + blob.type.split('/').pop();

                        //var file = new File([blob], filename, {type: blob.type, lastModified: Date.now()});
                    var file = image1_blob;
                        // console.log("File size : " + file.size);
                    //}
                }
                $('.tr_1').hide();
                $('.tr_2').fadeIn(300);
                sendApiRequest(file);
            });

            // close button
            $('em.close').on('click', function () {
                //console.log('업로드 취소');
                $('.fl_box').css("opacity", "1");
                $(this).parent().removeClass("btn_change");
                $(this).parent().addClass("btn");
                $(this).parent().children('.demolabel').text('동영상 업로드');
                $('#demoFile').val('');
                //파일명 변경
                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
                    var element = document.getElementById('uploadFile');
                    element.classList.remove('btn');
                    element.classList.add('btn_change');
                });
            });

            $('.sample_box').on('click', function () {
                $('em.close').trigger('click');
                $('#sample1').prop('checked', true);
            });

            // 처음으로 버튼
            $('.btn_back1').on('click', function () {
                ajaxXHR.abort();
                // console.log('처음으로 버튼 1번');
                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);
                $('.fl_box').css("opacity", "1");
                $('#sample1').trigger('click');
                $('#demoFile').val("");


            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                // console.log('처음으로 버튼 2번');
                $('.tr_3').hide();
                $('.tr_1').fadeIn(300);
                $('.fl_box').css("opacity", "1");
                $('#sample1').trigger('click');
                $('#demoFile').val("");

            });


            //알림 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
        });
    });

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

</script>