<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
	<!-- .content -->
	<div class="content api_content">
		<h1 class="api_tit">문장 생성</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'gptdemo')" id="defaultOpen"><button type="button">데모</button></li>
			<li class="tablinks" onclick="openTap(event, 'gptexample')"><button type="button">적용사례</button></li>
			<li class="tablinks" onclick="openTap(event, 'gptmenu')"><button type="button">매뉴얼</button></li>
<%--			<li class="tablinks"><a href="/member/krApiAccount" >API ID, key 발급</a></li>--%>
		</ul>
		<!-- .gptdemo -->
		<div class="demobox demobox_gpt" id="gptdemo">
			<p>GPT-2<small style="font-size:14px;">(Generative Pre-Training)</small>을 통한 <span>문장 생성</span></p>
			<span class="sub">주어진 문장에 따라 BERT 모델 기반의 새로운 문장을 여러개 생성해줍니다.</span>
			<div class=" gpt_box">
				<div class="mrclang_select" >
					<span>언어 선택</span>
					<div class="radio">
						<input type="radio" id="radio_lang_kor" name="radio_lang" checked="checked" value="kor"><label for="radio_lang_kor">한국어</label>
					</div>
					<div class="radio">
						<input type="radio" id="radio_lang_eng" name="radio_lang" value="eng"><label for="radio_lang_eng">English</label>
					</div>
				</div>

				<!-- =================================================================================================================== -->
				<!-- 예문 및 사용자 입력 -->
				<!-- =================================================================================================================== -->
				<!--.step01-->
				<div class="step01">
					<div class="demo_top first_top">
						<em class="far fa-list-alt"></em><span>예문 선택</span>
					</div>
					<div id='layer_sample' class="ift_topbox">
						<p>문장을 생성하고 싶은 예문을 선택 해주세요. <span style="color:#576068;font-weight:400;">(뉴스를 학습한 엔진입니다.)</span></p>
						<ul class="ift_lst">
							<li>
								<div class="radio">
									<input type="radio" id="radio_sample_1" name="radio_sample" checked="checked" value="제헌절인 오늘 중부 내륙과 전북 내륙에는 낮 동안 소나기가 오는 곳이 있겠습니다.">
									<label id='label_sample_1' for="radio_sample_1">제헌절인 오늘 중부 내륙과 전북 내륙에는 낮 동안 소나기가 오는 곳이 있겠습니다.</label>
								</div>
							</li>
							<li>
								<div class="radio">
									<input type="radio" id="radio_sample_2" name="radio_sample" value="최저임금위원회는 전원회의를 개최해, 2020년 적용 최저임금 수준을 의결했다.">
									<label id='label_sample_2' for="radio_sample_2">최저임금위원회는 전원회의를 개최해, 2020년 적용 최저임금 수준을 의결했다.</label>
								</div>
							</li>
						</ul>
					</div>
					<p class="txt_or">또는</p>
					<div class="demo_top">
						<em class="far fa-keyboard"></em><span>텍스트 입력</span>
					</div>
					<div class="text_area">
						<textarea id='input_user' rows="7" placeholder="문장 생성을 하고싶은 문장을 직접 써주세요." class="start_input"></textarea>
					</div>

					<div class="btn_area">
						<button type="button" class="btn_start" id="btn_gen">문장생성</button>
					</div>
				</div>
				<!--//.step01-->

				<!-- =================================================================================================================== -->
				<!-- 1차 진행중 -->
				<!-- =================================================================================================================== -->
				<!--step02-->
				<div id='layer_progress_1' class="step02">
					<div class="demo_top">
						<span>문장생성 중</span>
					</div>
					<div class="loding_box ">
						<!-- ball-scale-rotate -->
						<div class="lds">
							<div class="ball-scale-rotate">
								<span> </span>
								<span> </span>
								<span> </span>
							</div>
						</div>
						<p>약간의 시간이 소요 됩니다.</p>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_back1" ><em class="fas fa-redo"></em>처음으로</button>
					</div>

				</div>
				<!--//step02-->



				<!--step03-->
				<div id="house_result" class="step03">

					<!-- =================================================================================================================== -->
					<!-- 1차 결과 -->
					<!-- =================================================================================================================== -->
					<div id="layer_result_1" class="step03_1">
						<div class="demo_top">
							<span>문장생성</span>
						</div>
						<div class="txt_box">
							<p>입력 문장: <span id='text_request_1' class="result_txt requestText">최저임금위원회는 전원회의를 개최해, 2020년 적용 최저임금 수준을 의결했다.</span></p>
							<div class="gpt_result">
								<span>문장생성</span>
								<div id='text_result_1' class="result_txt resultText">20일 개막한 충남 논산 딸기 축제장에 인공지능(AI) 안내 로봇 '먹보 딸기'가 등장해 눈길을 끌고 있다.
									'말하는 인공지능 체험관'에서 만나볼 수 있는 먹보 딸기는 중학생 수준의 언어능력을 가지고 있다.
									관람객과 어린이들을 상대로 딸기 축제 홍보는 물론 관광지 안내, 일상적인 대화까지 척척 해낸다.
									또 어른들에게는 트로트, 아이들에게는 동요를 들려주며 웃음을 유발하는 재롱도 부려 사랑을 듬뿍 받고 있다.
									'상큼한 딸기향에 실려 오는 달콤한 만남'을 주제로 한 올해 논산 딸기 축제는 오는 24일까지 이어진다.</div>
							</div>
						</div>
					</div>

					<!-- =================================================================================================================== -->
					<!-- 2차 결과 -->
					<!-- =================================================================================================================== -->
					<!--문장가상 추가-->
					<div id="layer_result_2" class="step03_2">
						<div class="demo_top">
							<span>문장생성</span>
						</div>
						<div class="txt_box">
							<p>입력 문장: <span id='text_request_2' class="result_txt requestText">최저임금위원회는 전원회의를 개최해, 2020년 적용 최저임금 수준을 의결했다.</span></p>
							<div class="gpt_result">
								<span>문장생성</span>
								<div id='text_result_2' class="result_txt resultText">
								     20일 개막한 충남 논산 딸기 축제장에 인공지능(AI) 안내 로봇 '먹보 딸기'가 등장해 눈길을 끌고 있다.
									'말하는 인공지능 체험관'에서 만나볼 수 있는 먹보 딸기는 중학생 수준의 언어능력을 가지고 있다.
									관람객과 어린이들을 상대로 딸기 축제 홍보는 물론 관광지 안내, 일상적인 대화까지 척척 해낸다.
									또 어른들에게는 트로트, 아이들에게는 동요를 들려주며 웃음을 유발하는 재롱도 부려 사랑을 듬뿍 받고 있다.
									'상큼한 딸기향에 실려 오는 달콤한 만남'을 주제로 한 올해 논산 딸기 축제는 오는 24일까지 이어진다.</div>
							</div>
						</div>
					</div>

					<!-- =================================================================================================================== -->
					<!-- 3차 결과 -->
					<!-- =================================================================================================================== -->
					<!--문장가상 추가-->
					<div id="layer_result_3" class='step03_3'>
						<div class="demo_top">
							<span>문장생성</span>
						</div>
						<div class="txt_box">
							<p>입력 문장: <span id='text_request_3' class="result_txt requestText">최저임금위원회는 전원회의를 개최해, 2020년 적용 최저임금 수준을 의결했다.</span></p>
							<div class="gpt_result">
								<span>문장생성</span>
								<div id='text_result_3' class="result_txt resultText">20일 개막한 충남 논산 딸기 축제장에 인공지능(AI) 안내 로봇 '먹보 딸기'가 등장해 눈길을 끌고 있다.
									'말하는 인공지능 체험관'에서 만나볼 수 있는 먹보 딸기는 중학생 수준의 언어능력을 가지고 있다.
									관람객과 어린이들을 상대로 딸기 축제 홍보는 물론 관광지 안내, 일상적인 대화까지 척척 해낸다.
									또 어른들에게는 트로트, 아이들에게는 동요를 들려주며 웃음을 유발하는 재롱도 부려 사랑을 듬뿍 받고 있다.
									'상큼한 딸기향에 실려 오는 달콤한 만남'을 주제로 한 올해 논산 딸기 축제는 오는 24일까지 이어진다.</div>
							</div>
						</div>
					</div>

					<!-- =================================================================================================================== -->
					<!-- 4차 결과 -->
					<!-- =================================================================================================================== -->
					<!--문장가상 추가-->
					<div id="layer_result_4" class="step03_4">
						<div class="demo_top">
							<span>문장생성</span>
						</div>
						<div class="txt_box">
							<p>입력 문장: <span id='text_request_4' class="result_txt requestText">최저임금위원회는 전원회의를 개최해, 2020년 적용 최저임금 수준을 의결했다.</span></p>
							<div class="gpt_result">
								<span>문장생성</span>
								<div id='text_result_4' class="result_txt resultText">20일 개막한 충남 논산 딸기 축제장에 인공지능(AI) 안내 로봇 '먹보 딸기'가 등장해 눈길을 끌고 있다.
									'말하는 인공지능 체험관'에서 만나볼 수 있는 먹보 딸기는 중학생 수준의 언어능력을 가지고 있다.
									관람객과 어린이들을 상대로 딸기 축제 홍보는 물론 관광지 안내, 일상적인 대화까지 척척 해낸다.
									또 어른들에게는 트로트, 아이들에게는 동요를 들려주며 웃음을 유발하는 재롱도 부려 사랑을 듬뿍 받고 있다.
									'상큼한 딸기향에 실려 오는 달콤한 만남'을 주제로 한 올해 논산 딸기 축제는 오는 24일까지 이어진다.</div>
							</div>
						</div>
					</div>

					<!-- =================================================================================================================== -->
					<!-- 추가 입력 -->
					<!-- =================================================================================================================== -->
					<div id="layer_add_input" class="add_area">
						<div class="demo_top">
							<em class="far fa-keyboard"></em>텍스트 입력
						</div>
						<div class="text_area">
							<textarea id='input_more' rows="7" placeholder="새로운 문장을 추가해 더 많은 문장을 만들어 보세요."></textarea>
						</div>
					</div>

					<!-- =================================================================================================================== -->
					<!-- 추가 진행중 -->
					<!-- =================================================================================================================== -->
					<div id="layer_progress_2" class="add_lording">
						<div class="demo_top">
							<span>문장생성 중</span>
						</div>
						<div class="loding_box " id="lording_temp">
							<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#27c1c1" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

							<p>약간의 시간이 소요 됩니다.</p>
						</div>
					</div>
					<!-- //add_loding-->


					<!-- =================================================================================================================== -->
					<!-- 버턴들 -->
					<!-- =================================================================================================================== -->
					<div id='layer_add_btn' class="btn_area">
						<button type="button" class="btn_back2" ><em class="fas fa-redo"></em>처음으로</button>
						<button type="button" class="btn_add" id="button_add_input">새 문장<span>추가 하기</span></button>
					</div>

				</div>
				<!--//step03-->



			</div>
		</div>
		<!-- .gptdemo -->
		<!--.gptmenu-->
		<div class="demobox" id="gptmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API 공통 가이드
					</div>
					<p class="sub_title">개발 환경 세팅</p>
					<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
					<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

				</div>
				<div class="guide_group">
					<div class="title">
						Generative Pre-Training <small>(GPT-2)</small>
					</div>
					<p class="sub_txt">입력한 문자열과 유사도가 있는 문장들을 여러개 생성해줍니다.</p>

					<span class="sub_title">
							준비사항
						</span>
					<p class="sub_txt">① Input: 문자열 (텍스트)</p>
					<p class="sub_txt">② 아래 언어 중 택 1 </p>
					<ul>
						<li>한글 (kor)</li>
						<li>영어 (eng)</li>
					</ul>
					<span class="sub_title">
							 실행 가이드
						</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/gpt/</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>사용자의 고유 key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>lang</td>
							<td>사용할 모델의 언어 (kor / eng) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>context</td>
							<td>문장 생성을 실행할 지문 (텍스트)</td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
							<pre>
curl -X POST \
https: //api.maum.ai/api/gpt/ \
-H 'Content-Type: application/json' \
-d '{
    "apiId": "(*ID 요청 필요)",
    "apiKey": "(*key 요청 필요)",
    "context": "오늘 날씨는",
    "lang": "kor"
}'
</pre>
					</div>

					<p class="sub_txt">④ Response 파라미터 설명 </p>
					<span class="table_tit">Response</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>message</td>
							<td>API 동작여부</td>
							<td>list</td>
						</tr>
						<tr>
							<td>result</td>
							<td>생성된 문장 (총 3개)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>reqText</td>
							<td>입력된 문자열 </td>
							<td>string</td>
						</tr>
					</table>
					<span class="table_tit">message: API 동작여부</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>message</td>
							<td>요청 처리 상태를 설명하는 문자열 (Success/ Fail)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>status</td>
							<td>요청 처리 상태에 대한 status code (0: Success)</td>
							<td>number</td>
						</tr>
					</table>

					<p class="sub_txt">⑤ Response 예제 </p>
					<div class="code_box">
<pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "result": "전국이 기압골의 영향으로 서울· 경기 지역을 중심으로 폭염이 불겠다” 고 밝혔다.
오늘 날씨에 따르면 오늘 아침 중부지방을 중심으로 많은 비가 내리겠는데, 오늘과 내일 경기북부, 강원북부,
북한 지역에서는 돌풍과 함께 천둥, 번개를 동반한 시간당 20mm 이상의 강한 비가 오는 곳이 있겠다.
오늘 날씨는 중부지방에 기준시점을 찍은 고기압의 영향으로 대체로 맑겠으나, 남부지방은 북태평양 고기압의
가장자리에 들어 가끔 구름 많겠다.",
    "reqText": "오늘 날씨는"
}
</pre>

					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--//gptmenu-->
		<!--.gptexample-->
		<div class="demobox" id="gptexample">
			<p><em style="color:#27c1c1;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

			<%--문장 생성(NLG)--%>
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>학습용 문장 생성</span>
							</dt>
							<dd class="txt">언어 지능 관련 AI 엔진 학습을 위하여 문장을 벌크로 생성합니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_nlg"><span>GPT-2</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //문장 생성(NLG)-->
		</div>

	</div>
	<!-- //.content -->
</div>
<!-- //.contents -->
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/gpt.js"></script>--%>
<script type="text/javascript">
	$(window).load(function() {
		$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
			$(this).remove();
		});
	});
</script>
<script type="text/javascript">

	var hAjax = null;
	var step = 0;

	/* 데이타 로딩 시, 페이지 초기화 */
	jQuery.event.add(window,"load",function(){
		$(document).ready(function() {
			init();
		});
	});

	function init() {
		setSample("kor");

	}


	/* ============================================================================================================== */
	// LANG
	/* ============================================================================================================== */

	$("input:radio[name=radio_lang]").click(function() {
		setSample($("input[type=radio][name=radio_lang]:checked").val());
	});


	/* ============================================================================================================== */
	// INPUT
	/* ============================================================================================================== */

	var g_input_text = null;

	/* 사용자 입력 포커스 */
	$('#input_user').focus(function() {
		g_input_text = $('#input_user').text();
	});
	$('#input_more').focus(function() {
		g_input_text = $('#input_more').text();
	});

	/* 사용자 입력 */
	$('#input_user').on('input keyup paste', function() {
		g_input_text = $(this).val();
	});
	$('#input_more').on('input keyup paste', function() {
		g_input_text = $(this).val();
	});


	//직접 입력할 때 예문 흐리게하기
	$('.start_input').on('click', function () {
		$('.step01 .first_top').css('opacity', '.5');
		$('.step01 .ift_topbox').css('opacity', '.5');
	});

	//다시 예문 클릭하면 선명하게 하기
	$('.ift_topbox').on('click', function () {
		$('.step01 .first_top').css('opacity', '1');
		$('.step01 .ift_topbox').css('opacity', '1');
	});



	/* ============================================================================================================== */
	// SAMPLE
	/* ============================================================================================================== */

	var eng_sample = [
		"Wall Street is growing worried that Netflix's best days could be behind it.",
		"Nearly half of the US population will see temperatures of at least 95 degrees over the next seven days, according to meteorologists."
	];
	var kor_sample = [
		"제헌절인 오늘 중부 내륙과 전북 내륙에는 낮 동안 소나기가 오는 곳이 있겠습니다.",
		"최저임금위원회는 전원회의를 개최해, 2020년 적용 최저임금 수준을 의결했다."
	];

	$('#layer_sample').on('click', function() {
		$('#input_user').val('');

		g_input_text = $("input[type=radio][name=radio_sample]:checked").val();
	});
	$('#layer_sample').click();

	function setSample(lang) {
		if(lang == "kor") {
			for(var xx = 0; xx < 2; xx ++) {
				$('#radio_sample_' + (xx+1)).val(kor_sample[xx]);
				$('#label_sample_' + (xx+1)).html(kor_sample[xx]);
			}
		}
		else {
			for(var xx = 0; xx < 2; xx ++) {
				$('#radio_sample_' + (xx+1)).val(eng_sample[xx]);
				$('#label_sample_' + (xx+1)).html(eng_sample[xx]);
			}
		}
	}


	/* ============================================================================================================== */
	// 돌아가기 버튼
	/* ============================================================================================================== */

	$(".btn_back1").on('click', function() {
		if(hAjax != null) hAjax.abort();
		hAjax = null;

		initUI();
	});

	$(".btn_back2").on('click', function() {
		if(hAjax != null) hAjax.abort();
		hAjax = null;

		initUI();
	});

	function initUI() {
		/* 예문, 사용자입력 레이어 숨기기 */
		$('.step01').fadeIn(300);

		/* 진행중 레이어 숨기기 */
		$('.step02').hide();

		/* 결과 레이어 숨기기 */
		for(var xx = 1; xx <= 4; xx++) {
			$('#layer_result_' + xx).hide();
		}
		$('.step03').hide();

		/* 추가 문장생성 버튼 보이기 */
		$('.btn_back2').css("float", "left");
		$(".btn_add").show();

		$('#input_user').val('');
		$('#input_more').val('');
		g_input_text = '';

		$('#layer_sample').click();

		step = 0;
	}

	/* ============================================================================================================== */
	// 생성 버턴
	/* ============================================================================================================== */

	$("#btn_gen").click(genSentence);
	$(".btn_add").click(genSentence);

	function genSentence() {

		var txt = g_input_text;
		if(step >= 1){
			txt = $('#text_request_'+step).text() + g_input_text;
		}

		var lang = $("input[type=radio][name=radio_lang]:checked").val();

		console.log("text = " + txt);
		if(txt == null || txt.length <= 0) {
			alert("샘플을 선택하거나 문장을 입력해 주세요.");
			return;
		}

		step++;

		if(step == 1) {
			$('.step01').hide();
			$('#layer_progress_1').fadeIn(300);
		}
		else {
			$('#layer_add_btn').hide();
			$('#layer_add_input').hide();
			$('#layer_progress_2').fadeIn(300);
		}

		hAjax = $.ajax({
			type: 'POST',
			url: '${pageContext.request.contextPath}/api/gpt',
			data: {
				"context": txt,
				"lang": lang,
				'${_csrf.parameterName}' : '${_csrf.token}'
			},
			timeout: 20000,
			error: function(xhr, status, error){
				hAjax = null;

				if(error != 'abort') alert('[에러] Internal Server Error');

				console.dir(xhr.responseText);
				console.dir(status);
				console.dir(error);
			},
			success: function(data) {
				hAjax = null;
				var responseData = JSON.parse(data);
				var responseString = JSON.stringify(responseData);

				var status = responseData['message']['status'];
				if(status == 0) {
					$('#house_result').fadeIn(300);
					$('#layer_add_btn').fadeIn(300);
					$('#layer_add_input').fadeIn(300);


					if(step == 01) $('#layer_progress_1').hide();
					else $('#layer_progress_2').hide();

					$('#layer_result_' + step).fadeIn(300);

					// 성공

					//결과 화면
					var resultStr = responseData['result'];
					var reqText = responseData['reqText'];

					$("#text_request_" + step).html(reqText);
					$("#text_result_" + step).html(resultStr);

					$('#layer_progress_1').hide();
					$('#layer_progress_2').hide();

					$('#input_more').val('');
					g_input_text = '';

					if(step >= 3) {
						$('.btn_back2').css("float", "unset");
						$(".btn_add").hide();
						$("#layer_add_input").hide();
					}
				}
			}
		}).done(function(data) {

			console.dir(data);
		});
	}
	
	//API 탭  	
	function openTap(evt, menu) {
	  var i, demobox, tablinks;
	  demobox = document.getElementsByClassName("demobox");
	  for (i = 0; i < demobox.length; i++) {
	    demobox[i].style.display = "none";
	  }
	  tablinks = document.getElementsByClassName("tablinks");
	  for (i = 0; i < tablinks.length; i++) {
	    tablinks[i].className = tablinks[i].className.replace(" active", "");
	  }
	  document.getElementById(menu).style.display = "block";
	  evt.currentTarget.className += " active";
	}

	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();




</script>