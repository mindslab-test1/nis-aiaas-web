<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>파일 업로드 실패</h5>
            <p>파일 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>* 지원가능 파일 확장자: .wav, .mp4<br>
* 동영상파일 용량 50MB 이하만 가능합니다</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">이상행동 감지</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'ardemo')" id="defaultOpen">
                <button type="button">데모</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'arexample')">
                <button type="button">적용사례</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'armenu')">
                <button type="button">매뉴얼</button>
            </li>
<%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="ardemo">
            <p><span>이상행동 감지</span> <small>(Anomaly Detection)</small></p>
            <span class="sub">영상 내에 있는 이상행동을 감지해 주며, 현재 모델은 공항 출국장의 역주행을 감지합니다. </span>
            <!--faceTracking_box-->
            <div class="demo_layout faceTracking_box">
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-video"></em><strong>샘플 파일</strong>로 테스트 하기</p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="anomaly_option" value="1" checked>
                                    <label for="sample1" class="female">
                                        <video controls width="300">
                                            <source src="${pageContext.request.contextPath}/aiaas/kr/video/anomaly.reverse.01.mp4"
                                                    type="video/mp4">
                                            IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.
                                        </video>
                                    </label>
                                </div>
                            </div>
<%--                            <div class="sample_2">--%>
<%--                                <div class="radio">--%>
<%--                                    <input type="radio" id="sample2" name="anomaly_option" value="2">--%>
<%--                                    <label for="sample2" class="male">--%>
<%--                                        <video controls width="300">--%>
<%--                                            <source src="${pageContext.request.contextPath}/aiaas/kr/video/anomaly.reverse.03.mp4"--%>
<%--                                                    type="video/mp4">--%>
<%--                                            IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.--%>
<%--                                        </video>--%>
<%--                                    </label>--%>
<%--                                </div>--%>
<%--                            </div>--%>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-video"></em><strong>내 파일</strong>로 해보기</p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-video hidden"></em>
                                <label for="demoFile" class="demolabel">동영상 업로드</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".mp4">
                            </div>
                            <ul>
                                <li>* 지원가능 파일 확장자: .mp4</li>
                                <li>* 동영상파일 용량 50MB 이하만 가능합니다.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">영상 분석</button>
                    </div>
                </div>
                <!--tr_1-->
                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-video"></em>행동 분석 및 결과 처리중</p>
                    <div class="loding_box ">
                        <!-- ball-scale-rotate -->
                        <div class="lds">
                            <div class="ball-scale-rotate">
                                <span> </span>
                                <span> </span>
                                <span> </span>
                            </div>
                        </div>

                        <p>약간의 시간이 소요 됩니다.</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>
                <!--tr_2-->
                <!--tr_3-->
                <div class="tr_3">
                    <div class="origin_file">
                        <p id="result_title"><em class="far fa-file-video"></em>원본 파일</p>
                        <div class="videoBox">
                            <video controls width="300"
                                   src="${pageContext.request.contextPath}/aiaas/kr/video/anomaly.reverse.03.mp4"
                                   type="video/mp4" id="input_video">
                                IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.
                            </video>
                        </div>
                    </div>
                    <div class="result_file">
                        <p><em class="far fa-file-image"></em>결과 파일</p>
                        <div class="start_point">
                            <ul id="result_ul">
                                <li>
                                    <span>이상행동 시작 시점</span>

                                    <img src="" alt="이상행동 시작 시점" id="snap_left">
                                    <p id="snap_time">-1 초</p>

                                </li>
                                <li>
                                    <span></span>
                                    <img src="" alt="이상행동 0.5초 뒤" id="snap">
                                    <p id="snap_pre"> + 0.5 s</p>
                                </li>
                                <li>
                                    <span></span>
                                    <img src="" alt="이상행동 1초 뒤" id="snap_right">
                                    <p id="snap_next"> + 0.5 s</p>
                                </li>
                            </ul>
                            <p class="fail_noti" style="padding:10px 0 0 0;font-size:13px;color:#2c3f51;margin:0;">감지된 이상행동이 없습니다.</p>
                        </div>

                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.faceTracking_box-->

        </div>
        <!-- //.demobox -->
        <!--.ftmenu-->
        <div class="demobox vision_menu" id="armenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

                </div>
                <div class="guide_group">
                    <div class="title">
                        이상행동 감지 <small>(역주행 감지)</small>
                    </div>
                    <p class="sub_txt">비디오 중 이상행동 (역주행)을 감지하여 알려줍니다.</p>
                    <span class="sub_title">
								준비사항
					</span>
                    <p class="sub_txt">- Input: 비디오 파일 </p>
                    <ul>
                        <li>확장자: .mp4</li>
                        <li>용량: 50MB 이하</li>
                    </ul>
                    <span class="sub_title">
								 실행 가이드
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/smartXLoad/anomalyDetect</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>사용자의 고유 ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>video</td>
                            <td>type:file (.mp4) 비디오 파일 </td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        curl --location --request POST 'http://api.maum.ai/smartXLoad/anomalyDetect' \<br>
                        --header 'Content-Type: multipart/form-data' \<br>
                        --form 'apiId= 발급받은 API ID' \<br>
                        --form 'apiKey= 발급받은 API KEY' \<br>
                        --form 'video= 역주행 비디오 파일'<br>
                    </div>

                    <p class="sub_txt">④ Response 예제 </p>

                    <div class="code_box">
<pre>
{
    "frame_1": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICAg...",

    "frame_2": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICAg...",

    "frame_3": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICAg...",

     "result_text": "0:0:6.000"
}
</pre>
                    </div>


                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.ftmenu-->
        <!--.ftexample-->
        <div class="demobox" id="arexample">
            <p><em style="color:#f7778a;font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>

            <div class="useCasesBox">
                <!-- 이상행동 감지-->
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>역주행 감지</span>
                            </dt>
                            <dd class="txt">공항 이상행동 식별 및 추적 과제에 사용된 엔진으로, 공항 내 출입국 심사대에서의 역주행을 감지합니다.
                                <span><em class="fas fa-book-reader"></em> Reference: A공항</span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_anor"><span>이상 행동</span></li>
                                    <li class="ico_pr"><span>포즈 인식</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>이상 객체 포착</span>
                            </dt>
                            <dd class="txt">설정된 특정 시간이나 장소 내 비정상적인 움직임이 포착되면, 이상 신호를 알려 사고를 미연에 방지해 줍니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_anor"><span>이상 행동</span></li>
                                    <li class="ico_tr"><span>ESR</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>신속한 범죄 대응</span>
                            </dt>
                            <dd class="txt">길거리 내 CCTV 속 포착된 납치 또는 강도 사건을 바로 감지하여 빠르게 대응합니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_anor"><span>이상 행동</span></li>
                                    <li class="ico_tr"><span>ESR</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
                <!--이상행동 감지-->
            </div>
        </div>
        <!--//.ftexample-->


    </div>
    <!-- //.content -->
</div>


<script>
    var sampleImage1;

    var ajaxXHR;

    let movie1_blob;

    //파일명 변경
    document.querySelector("#demoFile").addEventListener('change', function (ev) {

        $('.fl_box').on('click', function () {
            $(this).css("opacity", "1");
            $('.tr_1 .btn_area .disBox').remove();
        });

        //파일 용량 체크
        var demoFileInput = document.getElementById('demoFile');
        var demoFile = demoFileInput.files[0];
        var demoFileSize = demoFile.size;
        var max_demoFileSize = 1024 * 1024 * 50;//1kb는 1024바이트
        var demofile = document.querySelector("#demoFile");

        if (demoFileSize > max_demoFileSize || !demofile.files[0].type.match(/video.mp4/)) {
            $('.pop_simple').show();
            // 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
            $('.fl_box').css("opacity", "0.5");
            $('#demoFile').val('');
            $('.tr_1 .btn_area').append('<span class="disBox"></span>');

        } else {
            document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
            var element = document.getElementById('uploadFile');
            element.classList.remove('btn');
            element.classList.add('btn_change');
            $('.fl_box').css("opacity", "0.5");
            $('.tr_1 .btn_area .disBox').remove();
            $('#sample1').removeAttr('checked');
        }
    });

    jQuery.event.add(window, "load", function () {

        function loadSample1() {
            var blob = null;
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/video/anomaly.reverse.01.mp4");
            xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
            xhr.onload = function () {
                blob = xhr.response;//xhr.response is now a blob object
                //sampleImage1 = new File([blob], "anomaly.reverse.01.mp4");

                var imgSrcURL = URL.createObjectURL(blob);
                var textr_output = document.getElementById('input_video');
                textr_output.setAttribute("src", imgSrcURL);

                movie1_blob = blob;
            }

            xhr.send();
        }

        $(document).ready(function () {
            loadSample1();

            // close button
            $('em.close').on('click', function () {
                $('.fl_box').css("opacity", "1");
                $(this).parent().removeClass("btn_change");
                $(this).parent().addClass("btn");
                $(this).parent().children('.demolabel').text('동영상 업로드');
                $('#demoFile').val('');
                //파일명 변경
                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
                    var element = document.getElementById('uploadFile');
                    element.classList.remove('btn');
                    element.classList.add('btn_change');

                    $('#sample1').prop('checked', false);
                });

            });
            $('.radio input').on('click', function () {
                $('em.close').trigger('click');

            });

            $('#sub').on('click', function (blob) {
                //console.log("영상 분석 시작");
                var $demoFile = $("#demoFile");
                //샘플전송
                var formData = new FormData();

                if ($demoFile.val() === "" || $demoFile.val() === null) {
                    var demoFile;
                    var option = $("input[type=radio][name=anomaly_option]:checked").val();
                    if (option == 1) {
                        loadSample1();
                        //demoFile = sampleImage1;

                        demoFile = movie1_blob;
                    }
                    // console.log("File size : " + demoFile.size);
                    formData.append('file', demoFile);
                    formData.append('${_csrf.parameterName}', '${_csrf.token}');
                } else { // 사용자 파일로 해보기
                    // console.log("Your local file !! ");

                    const blob = document.getElementById('demoFile').files[0];
                    movie1_blob = blob;
                    var filename = Date.now() + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).toString(2, 15);
                    filename += '.' + blob.type.split('/').pop();

                    //var file = new File([blob], filename, {type: blob.type, lastModified: Date.now()});
                    var file = movie1_blob;
                    // console.log("File size : " + file.size);

                    formData.append('file', file);
                    formData.append('${_csrf.parameterName}', '${_csrf.token}');

                    var imgSrcURL = URL.createObjectURL(blob);
                    $('#input_video').attr('src', imgSrcURL);
                }
                ajaxXHR = $.ajax({
                    type: "POST",
                    async: true,
                    url: "${pageContext.request.contextPath}/anomalyReverse/multiAnomaly",
                    data: formData,
                    processData: false,
                    contentType: false,
                    timeout: 20000,
                    success: function (result) {
                        //console.log('result', result);

                        if(result == undefined || result == ""){
                            $('#result_ul').css("display", "none");
                            $('.fail_noti').css("display", "block");

                            $('#snap_left').attr('src', "");
                            $('#snap').attr('src', "");
                            $('#snap_right').attr('src', "");
                            $('#snap_time').text("");
                            return;
                        }

                        let resultData = JSON.parse(result);

                        $('.tr_2').hide();
                        $('.tr_3').fadeIn(300);


                        $('#result_ul').fadeIn(200);
                        $('.fail_noti').css("display", "none");

                        $('#snap_left').attr('src', "data:image/jpeg;base64," + resultData.frame_1);
                        $('#snap').attr('src', "data:image/jpeg;base64," + resultData.frame_2);
                        $('#snap_right').attr('src', "data:image/jpeg;base64," + resultData.frame_3);

                        var second = resultData.result_text;

                        var substring = second.substr(2,3);
                        var number= substring.substring(2);
                        var minute = substring.substr(0,2);
                        var numberto = parseInt(number);

                        if (numberto < 10){
                            numberto = "0" + numberto;
                            $('#snap_time').text(minute+numberto);
                        }else {
                            $('#snap_time').text(substring);
                        }
                    },
                    error: function (jqXHR) {
                        if (jqXHR.status === 0) {
                            return false;
                        }
                        alert("ERROR");
                        console.dir(error);
                        window.location.reload();
                    }
                });

                $('.tr_1').hide();
                $('.tr_2').fadeIn(300);

            });

            // 처음으로 버튼
            $('.btn_back1').on('click', function () {
                ajaxXHR.abort();
                $('.tr_3').hide();
                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);
                $('.fl_box').css("opacity", "1");
                $('#sample1').trigger('click');
                $('#demoFile').val("");
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('.tr_3').hide();
                $('.tr_1').fadeIn(300);
                $('.fl_box').css("opacity", "1");
                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;

                });
            });

            //알림 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
        });
    });

    function sendMultiRequest(anomaly_option, file, url) {
        var formData = new FormData();
        formData.append('file', file);
        formData.append('option', anomaly_option);
        formData.append($("#key").val(), $("#value").val());
        console.log("MultiRequest");
        $.ajax({
            type: "POST",
            async: true,
            url: url,
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {

                $('.tr_2').hide();
                $('.tr_3').fadeIn(300);
                $('#snap_left').attr('src', "data:image/jpeg;base64," + result[0].body);
                $('#snap').attr('src', "data:image/jpeg;base64," + result[1].body);
                $('#snap_right').attr('src', "data:image/jpeg;base64," + result[2].body);
                //$('#snap_time').text(result[3].body);
                var second = result[3].body;

                var substring = second.substr(2,3);
                var number= substring.substring(2);
                var minute = substring.substr(0,2);
                var numberto = parseInt(number);
                //var addtxt = substring.substr(0,2);

                if (numberto<10){
                    var minus = numberto - 1;
                    var plus = numberto + 1;
                    numberto = "0"+numberto;

                    $('#snap_time').text(minute+numberto);

                }else {
                    $('#snap_time').text(substring);
                }


            },
            error: function (error) {
                alert("Error");
                console.dir(error);
                window.location.reload();
            }
        });
    }

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

</script>