<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">텍스트 분류</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'xdcdemo')" id="defaultOpen"><button type="button">데모</button></li>
			<li class="tablinks" onclick="openTap(event, 'xdcexample')"><button type="button">적용사례</button></li>
			<li class="tablinks" onclick="openTap(event, 'xdcmenu')"><button type="button">매뉴얼</button></li>
<%--			<li class="tablinks"><a href="/member/krApiAccount" >API ID, key 발급</a></li>--%>
		</ul>
		<!--.demobox_xdc-->
		<div class="demobox demobox_xdc"  id="xdcdemo">
			<p>정확한 <span>텍스트 분류</span><small style="font-size:14px;">(XDC, eXplainable Document Classifier)</small>를 통한 뉴스 분석</p>
			<span class="sub">내용과 분량에 관계 없이 어떤 문서든 분류하고 분석합니다.</span>
			<!--.xdc_box-->
			<div class="xdc_box">
				<div class="xdc_wrap">
					<div class="demo_top">
						<p>본문넣기</p>
					</div>
					<%-- <div class="selectarea">
						<div class="select_box">
							<label for="lang">언어</label>
							<select id="lang">
								<option value="kor">한국어</option>
								<option value="eng">영어</option>
							</select>
						</div>
					</div> --%>
					<div class="text_area">
						<textarea rows="12" id="id_input_text" placeholder="문서 본문(뉴스기사)을 적거나 붙여 넣어주세요."></textarea>
					</div>
					<!-- Modal -->
					<div id="news_modal" class="modal">
						<!-- Modal content -->
						<div class="modal_content search_modal naver_news_search_div">
							<span class="close close_naver">×</span>
							<div class="search_modal_wrap">
								<span class="naver_search_tit">네이버 기사 검색</span>
								<button type="button" class="btn search_btn" id="search_bu`tton">검색</button>
								<input type="hidden" id="article_content" value=" " name="article_content">
								<div class="loading_wrap" id="loading_gif"><img src="${pageContext.request.contextPath}/aiaas/kr/images/spinner.gif" alt="spinner"></div>

								<button type="button" class="btn cont_btn" id="insert_selected_content">확인</button>
							</div>
						</div>
						<!--//.Modal content -->
					</div>
					<!-- //.Modal -->
					<div class="btn_area">
						<button type="button" class="start_btn" id="classify_content" disabled><em class="fas fa-file-invoice"></em>분석하기</button>
						<%--<span class="disBox"></span>--%>
					</div>
				</div>

				<!--.xdc_result_wrap-->
				<div class="xdc_result_wrap">
					<div class="resultBoxTop">
						<div class="tit"><span>분류 및 XDC 결과</span></div>
						<div class="xdc_recod">
							<!-- recording -->
							<div class="recording">
								<span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
								<span></span><span></span><span></span><span></span><span></span><span></span><span></span>
							</div>
							<!-- //recording -->
						</div>
						<div class="txt" id="xdc_result_classification">
							<ol class="xdc_lst">
<%--								<li>--%>
<%--									<span class="rank">카테고리 :</span>--%>
<%--									<span class="label">IT</span>--%>
<%--									<span class="rank">신뢰도 : </span>--%>
<%--									<span class="probability"><em>0.012341234</em></span>--%>
<%--								</li>--%>
<%--								<li>--%>
<%--									<span class="rank">후보군 :</span>--%>
<%--									<span class="label">모바일</span>--%>
<%--									<span class="probability">--%>
<%--                                        <em>0.012341234</em>--%>
<%--                                    </span>--%>
<%--									<span class="label">소프트웨어</span>--%>
<%--									<span class="probability">--%>
<%--                                        <em>0.012341234</em>--%>
<%--                                    </span>--%>
<%--								</li>--%>
<%--								<li>--%>
<%--									<span class="rank"><b>3</b>위</span>--%>
<%--									<span class="label">소프트웨어</span>--%>
<%--									<span class="probability">--%>
<%--                                        <em>0.012341234</em>--%>
<%--                                    </span>--%>
<%--								</li>--%>
							</ol>
						</div>
					</div>
					<div class="result_desc">
						<span><img src="${pageContext.request.contextPath}/aiaas/kr/images/mouse.png" alt="mouse icon"> 마우스를 표시된 문장이나 단어 위로 이동시켜 보세요. 분석 내용이 표시됩니다.</span>
					</div>
					<div class="resultBoxBottom">
						<div class="balloon" id="xdc_result_weights" style="width:160px;">
							<div id="sentenceWeight"></div>
							<div id="wordWeight"></div>
						</div>
						<!--  <div class="txt" id="xdc_result_xdc" >『국어의 <span>기술</span>외전 독서활동 추천도서: 미래를 바꾼 아홉 가지 알고리즘』 소개
기왕에 <span>독서</span>를 해야 한다면 시험에도 도움이 될 만한 책을 고르는 것이 중요하다. 그런데 학생 입장에서 어떤 책이 수능 국어영역과 학생부 독서활동 모두에 도움이 되는지 판단하기 어렵다. 『국어의 기술 외전 독서활동 추천도서』는 적합한 책을 추천하고, 또 해당 책과 관련된 [한국교육과정평가원 기출지문+‘국어의 기술’식 해설]을 제공한다. 국어시험 점수,
학생부 독서활동이라는 두 마리 토끼를 한꺼번에 잡을 수 있기 바란다.
『미래를 바꾼 아홉 가지 알고리즘』 소개
오늘날 우리는 궁금한 것이 있으면 인터넷 검색엔진에 검색어를 입력하고, 필요한 물건이 있으면 인터넷 쇼핑몰에서 구입한다. 일상생활의 모든 활동을 컴퓨터로 하고 있다고 해도 과언이 아니다. 그렇다면 우리가 컴퓨터를 사용할 때, 컴퓨터 안에서는 무슨 일이 일어나고 있을까? 존 맥코믹은 이를 가능케 한 아홉 가지 위대한 알고리즘 이면의 기본적 아이디어를 다양한 비유와 예를 활용해 쉽고 정확하게 설명한다. 독자들은 이 아홉 가지 알고리즘에 관한 설명에서 검색엔진, 데이터 압축, 암호화, 오류 정정, 패턴 인식 등 우리가 매일 이용하는 컴퓨터 기술의 이론적 아이디어를 흥미진진하게 이해할 수 있다.
기왕에 독서를 해야 한다면 시험에도 도움이 될 만한 책을 고르는 것이 중요하다. 그런데 학생 입장에서 어떤 책이 수능 국어영역과 학생부 독서활동 모두에 도움이 되는지 판단하기 어렵다. 『국어의 기술 외전 독서활동 추천도서』는 적합한 책을 추천하고, 또 해당 책과 관련된 [한국교육과정평가원 기출지문+‘국어의 기술’식 해설]을 제공한다. 국어시험 점수, 학생부 독서활동이라는 두 마리 토끼를 한꺼번에 잡을 수 있기 바란다.

앤서니 브라운
저자 앤서니 브라운
1946년 영국에서 태어났다. LEEDS COLLEGE OF ART에서 그래픽 디자인을 공부한 뒤, 3년 동안 맨체스터 왕립 병원에서 의학 전문 화가로 일했다. LEEDS COLLEGE OF ART에서 파트 타임으로 학생들을 가르쳤고 15년 동안 GORDON FRASER 갤러리에서 연하장을 디자인했다.
아주 우연히 를 그리게 되면서 본격적인 그림책 작가가 되었다. 1983년 고릴라로 영국 도서관 협회에서 그 해 최고의 그림책에 주는 '케이트 그린어웨이' 상과 '커트 매쉴러' 상을 받았고 동물원으로 두 번째 '케이트 그린어웨이' 상을 받았다.
독특하고 뛰어난 작품으로 세계적으로 높은 평가와 주목을 받고 있는 작가로 군더더기 하나 없는 완벽한 구성, 간결하면서도 유머가 넘치는 글
</div> -->
						<div class="txt" id="xdc_result_xdc" ></div>
					</div>
					<!--.btn_area-->
					<div class="btn_area">
						<button type="button" class="btn_goback" id="back_to_article">처음으로</button>
						<!--							<button type="button" class="btn_save" id="">Save</button>	-->
					</div>
				</div>
				<!--//.xdc_result_wrap-->
			</div>
			<!--//.xdc_box-->

		</div>
		<!-- .demobox_xdc -->
		<!--.xdcmenu-->
		<div class="demobox" id="xdcmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API 공통 가이드
					</div>
					<p class="sub_title">개발 환경 세팅</p>
					<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
					<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

				</div>
				<div class="guide_group">
					<div class="title">
						Bert XDC(eXplainable Document Classifier)
					</div>
					<p class="sub_txt">XDC(문서 분류, eXplainable Document Classifier)는 내용과 분량에 관계 없이 어떤 문서든 즉시 분류한 뒤 분류 이유를 설명할 수 있는 혁신적인 인공지능 문서 분류 기술입니다.</p>

					<span class="sub_title">
							준비사항
						</span>
					<p class="sub_txt">① Input: 지문 (텍스트)</p>
					<p class="sub_txt">② 아래 Model 중 택 1  </p>
					<ul>
						<li>뉴스 분류 (default)</li>
					</ul>
					<span class="sub_title">
							 실행 가이드
						</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/bert.xdc/</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>사용자의 고유 key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>context</td>
							<td>텍스트 분류를 실행할 지문 (텍스트) </td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
<pre>
curl -X POST \
https: //api.maum.ai/api/bert.xdc \
-H 'Content-Type: application/json' \
-d '{
	"apiId": "(*ID 요청 필요)",
	"apiKey": "(*key 요청 필요)",
	"context": "지난해 야구 국가대표 사령탑 지휘봉을 스스로 내려놓은 국보급 투수 선동열 전 감독이 새로운 도전에
   나섭니다. 선 전 감독은 오늘(11일) 서울 목동구장에서 기자 회견을 열어 내년 미국프로야구 메이저리그 뉴욕 양키스 구단의
   스프링캠프에 참가해 메이저리그 선진 야구를 배울 생각이라고 밝혔습니다. 기자회견에 동석한 스티브 윌슨 양키스 국제담당 총괄
   스카우트는 양키스 구단이 일본 지도자를 구단에 초청한 적은 있지만, 한국 지도자는 최초로 초청한다며 영광으로 생각한다고
   덧붙였습니다. 자세한 인터뷰 내용은 영상으로 확인하시죠. "
}</pre>
					</div>

					<p class="sub_txt">④ Response 파라미터 설명 </p>
					<span class="table_tit">Response</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>message</td>
							<td>API 동작여부</td>
							<td>list</td>
						</tr>
						<tr>
							<td>labels</td>
							<td>분류 결과의 리스트</td>
							<td>list</td>
						</tr>
						<tr>
							<td>wordIndices</td>
							<td>분류 결과에 영향을 미친 비중이 큰 단어 리스트 </td>
							<td>list</td>
						</tr>
						<tr>
							<td>sentenceIndices</td>
							<td>분류 결과에 영향을 미친 비중이 큰 문장 리스트</td>
							<td>list</td>
						</tr>
					</table>
					<span class="table_tit">message: API 동작여부</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>message</td>
							<td>요청 처리 상태를 설명하는 문자열 (Success/ Fail)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>status</td>
							<td>요청 처리 상태에 대한 status code (0: Success)</td>
							<td>number</td>
						</tr>
					</table>
					<span class="table_tit">labels: 분류 결과의 리스트</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>label</td>
							<td>분류 결과 (신뢰도가 높은 순으로 나열) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>probability</td>
							<td>분류 결과 신뢰도 (0~1) </td>
							<td>number</td>
						</tr>
					</table>
					<span class="table_tit">wordIndices: 분류 결과에 영향을 미친 비중이 큰 단어 리스트</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>startIdx</td>
							<td>단어 위치 시작점 </td>
							<td>int</td>
						</tr>
						<tr>
							<td>endIdx</td>
							<td>단어 위치 종점</td>
							<td>int</td>
						</tr>
						<tr>
							<td>weight</td>
							<td>단어 비중(0~1) </td>
							<td>number</td>
						</tr>
					</table>
					<span class="table_tit">sentenceIndices: 분류 결과에 영향을 미친 비중이 큰 문장 리스트</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>startIdx</td>
							<td>문장 위치 시작점 </td>
							<td>int</td>
						</tr>
						<tr>
							<td>endIdx</td>
							<td>문장 위치 종점 </td>
							<td>int</td>
						</tr>
						<tr>
							<td>weight</td>
							<td>문장 비중(0~1)</td>
							<td>number</td>
						</tr>
					</table>
					<p class="sub_txt">⑤ Response 예제 </p>
					<div class="code_box">
							<pre style="font-size: 13px;font-family: Menlo,Monaco,Consolas,Courier New,monospace;color:#666;">
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"labels": [
		{
			"label": "야구",
			"probability": 0.9995629191398621
		},
		{
			"label": "축구",
			"probability": 0.00013259267143439502
		},
		{
			"label": "스포츠기타",
			"probability": 0.00008373271703021601
		}
	],
	"wordIndices": [
		{
			"startIdx": 4,
			"endIdx": 6,
			"weight": 0.16227564215660095
		},
		{
			"startIdx": 79,
			"endIdx": 83,
			"weight": 0.2021310180425644
		},
		{
			"startIdx": 143,
			"endIdx": 145,
			"weight": 0.10961653292179108
		}
	],
	"sentenceIndices": [
		{
			"startIdx": 0,
			"endIdx": 60,
			"weight": 0.06997623294591904
		},
		{
			"startIdx": 162,
			"endIdx": 266,
			"weight": 0.099941685795784
		}
	]
}
</pre>

					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--//xdcmenu-->
		<!--.xdcexample-->
		<div class="demobox" id="xdcexample">
			<p><em style="color:#27c1c1;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

			<%--텍스트 분류(XDC)--%>
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>장문 텍스트 분류</span>
							</dt>
							<dd class="txt">기존의 분류기로 분류하기 어려웠던 장문의 텍스트에 대해 카테고리나 의도, 감정 등을 분류할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_xdc"><span>XDC</span></li>
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>무제한 <strong>카테고리 분류</strong></span>
							</dt>
							<dd class="txt">분류 카테고리 개수에 제한 없이, 원하는 만큼 자동으로 텍스트를 분류하는 작업에 활용할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 03</em>
								<span>고객 만족도 <strong>정밀 분석</strong></span>
							</dt>
							<dd class="txt">고객 의도를 자동으로 분류할 수 있을 뿐 아니라, 분류 근거까지 추적할 수 있기 때문에 정밀한 고객만족도 분석이 가능합니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_xdc"><span>XDC</span></li>
									<li class="ico_stt"><span>STT</span></li>
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 04</em>
								<span>뉴스 부가서비스 <strong>구현</strong></span>
							</dt>
							<dd class="txt">전 분야에 걸쳐 최신 뉴스를 분류할 수 있고, 이를 활용한 다양한 부가 서비스를 구현하는 데 활용할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_xdc"><span>XDC</span></li>
									<li class="ico_mrc"><span>MRC</span></li>
									<li class="ico_bot"><span>챗봇</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //텍스트 분류(XDC) -->
		</div>



	</div>

</div>
<!-- //.contents -->

<!-- //.page loading -->

<script type="text/javascript">
	$(window).load(function() {
		$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
			$(this).remove();
		});
	});
</script>

<script type="text/javascript">
	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){
			//XDC
			$('.xdc_result_wrap').hide();

			$('.xdc_box .text_area textarea').on('input keyup paste', function() {
				var txtValLth = $(this).val().length;
				if ( txtValLth > 0) {
					$('.xdc_box .btn_area button').removeClass('disable');
					$('.xdc_box .btn_area button').removeAttr('disabled');
					$('.xdc_box .btn_area button').addClass('start_btn');
				} else {
					$('.xdc_box .btn_area button').attr('disabled');
				}
			});

			// product layer popup
			$('.close, .lyr_bg').on('click', function () {
				$('.demobox_xdc .modal').fadeOut(300);
				$('body').css({
					'overflow': '',
				});
			});

//		step 2
			$('#classify_content').on('click', function () {
				$(this).parent().parent().hide();
				$('.xdc_result_wrap').show();
			});


			$('.btn_goback').on('click', function () {
                $('#id_input_text').val("");
				$('.xdc_result_wrap').hide();
				$('.xdc_wrap').show();
			});

			//XDC
			var hideResult = true;
			$("#classify_content").click(function(){

				$("#xdc_article_wrap").hide();
				$("#loading_gif").show();

				if(hideResult){
					var txt = document.getElementById('id_input_text').value;
					$.ajax({
						type: 'POST',
						url: '${pageContext.request.contextPath}/xdc/xdcApiv20/',
						data: {
							"context": txt,
							'${_csrf.parameterName}' : '${_csrf.token}'
						},
						timeout: 20000,
						error: function(xhr, status, error){
							$("#xdc_article_wrap").show();
							$("#loading_gif").hide();
							alert('[에러] 문단을 분석하는 데에 실패했습니다.');
							console.dir(xhr.responseText);
							console.dir(status);
							console.dir(error);
						}, success: function(data) {
							console.log(data);
							if(data['message']['status'] != 0) {
								alert('[에러] 문단을 분석하는 데에 실패했습니다.');
								return;
							} else {
								$('.xdc_result_wrap').fadeIn();
								var labels = data['labels'];
								for(var i=0; i<labels.length; i++) {
									if (i==0){
										$(".xdc_lst").append("<li><span class=\"rank\">카테고리 :</span>"
												+ "<span class='label'>"
												+ (labels[i]["label"]) + "</span>"
												+ "<span class=\"rank\">신뢰도 : </span>"
												+ "<span class='probability'><em>"
												+ parseFloat(labels[i]["probability"]*100).toFixed(1)
												+ "%</em></span>"
												+"</li>");
									}else if(i==1){

										$(".xdc_lst").append("<li><span class=\"rank\">후보군 :</span>"
												+ "<span class='label'>"
												+ (labels[i]["label"]) + "</span><span class='probability'><em>"
												+ parseFloat(labels[i]["probability"]*100).toFixed(1)
												+ "%</em></span>"
												+"</li>");
									}else {
										$(".xdc_lst li:nth-child(2)").append(", <span class='label'>"
												+ (labels[i]["label"]) + "</span><span class='probability'><em>"
												+ parseFloat(labels[i]["probability"]*100).toFixed(1)
												+ "%</em></span>");
									}
								}
								
								var sentenceIndices = data['sentenceIndices'];
								var wordIndices = data['wordIndices'];
								var sentence = txt;
								var appendedSentenceOffset = 0;
								
								// 1. 주요 문장 단위를 기준으로 배열로 나눈다.
								var sentenceArray = [];
								//var weightArray = [];
								var index = [];
								var lastEndSentIdx = 0;
								if(sentenceIndices != null && sentenceIndices.length > 0) {
									for(var s=0; s<sentenceIndices.length; s++) {
										var sent_weight = sentenceIndices[s]['weight'];
										var startSentIdx = sentenceIndices[s]['startIdx'];
										var endSentIdx = sentenceIndices[s]['endIdx'];
										
										// 문장별 인덱스 값을 저장
										// 첫문단 저장
										if(s==0 && startSentIdx > 0) {
											var indexSet = new Map();
											sentenceArray.push(sentence.substring(0, startSentIdx));
											indexSet.set("start", 0);
											indexSet.set("end", startSentIdx);
											index.push(indexSet);
										}
										// 낀 문장 저장
										else if(startSentIdx > lastEndSentIdx) {
											var indexSet = new Map();
											sentenceArray.push(sentence.substring(lastEndSentIdx, startSentIdx));
											indexSet.set("start", lastEndSentIdx);
											indexSet.set("end", startSentIdx);
											index.push(indexSet);
										}
										
										var indexSetModel = new Map();
										// 목록 문장 저장
										sentenceArray.push(sentence.substring(startSentIdx, endSentIdx));
										indexSetModel.set("start", startSentIdx);
										indexSetModel.set("end", endSentIdx);
										indexSetModel.set("weight", sent_weight);
										index.push(indexSetModel);
										
										// 끝 문장 저장
										if(s==sentenceIndices.length-1 && endSentIdx < txt.length) {
											var indexSet = new Map();
											sentenceArray.push(sentence.substring(endSentIdx));
											indexSet.set("start", endSentIdx);
											indexSet.set("end", txt.length);
											index.push(indexSet);
										}
										lastEndSentIdx = endSentIdx;
									}
								} else {
									// 문장 주요도 정보가 없을 때
									var indexSetModel = new Map();
									indexSetModel.set("start", 0);
									indexSetModel.set("end", txt.length);
									index.push(indexSetModel);
									
									sentenceArray.push(txt);
								}
								
								// 4. 태그와 이벤트를 단어에 삽입 한 후
								// 5. 치환한다.
								var spanOpen = "";
								var spanClose = "";
								if(wordIndices != null && wordIndices != 'undefined') {
									for(var i=wordIndices.length-1; i>=0; i--) {
										var word_weight = wordIndices[i]['weight'];
										var startIdx = wordIndices[i]['startIdx'];
										var endIdx = wordIndices[i]['endIdx'];
										var sentString = sentence;
										
										// 2. 단어가 주요 문장 내에 속해 있는지 판단하고 (모든 문장 Loop가 가능)
										for(var x=0; x<index.length; x++) {
											var mapStart = index[x].get('start');//["start"];
											var mapEnd = index[x].get('end');//["end"];
											var sent_weight = index[x].get('weight');
	
											if(sent_weight == null || sent_weight == 'undefined') {
												sent_weight = 0;
											}
											
											if(startIdx >= mapStart && endIdx <= mapEnd) {
												// 3. 해당되는 문장 배열을 찾아서
												sentString = sentenceArray[x];
												startIdx = startIdx - mapStart;
												endIdx = endIdx - mapStart;
	
												spanOpen = "<span class=s_" + x + "_w_" + i + " id=s_" + x + "_w_" + i + " style='color: rgb(255, 96, 0); font-weight: bold;' onmouseover=showWeight(" + x + "," + i + "," + sent_weight + "," + word_weight + ") onmouseout=hideWeight()>";
												spanClose = "</span>";
												sentString = sentString.substring(0,startIdx)+spanOpen+sentString.substring(startIdx, endIdx)+spanClose+sentString.substring(endIdx);
												
												sentenceArray[x] = sentString;
											}
										}
									}
								}
								for(var x=0; x<index.length; x++) {
									var sent_weight = index[x].get('weight');
									if(sent_weight != null) {
										var spanOpen = "<span class=s_" + x + "_w_" + 9999 + " id=s_" + x + "_w_" + 9999 + " style='font-weight: bold;' onmouseover=showWeight(" + x + "," + 9999 + "," + sent_weight + ",null" + ") onmouseout=hideWeight()>";
										var spanClose = "</span>";
										var sentString = sentenceArray[x];
										
										sentString = spanOpen + sentString + spanClose;
										sentenceArray[x] = sentString;
									}
								}
								for(var s=0; s<sentenceArray.length; s++) {
									var resultXdc = $("#xdc_result_xdc").append(sentenceArray[s]);
								}
							}
						}
					}).done(function(data){
						console.dir(data);
					});
				}
			});
			$("#back_to_article").click(function() {
				$("#xdc_result_wrap").css("display", "none");
				$("#xdc_article_wrap").css("display", "block");
				$(".xdc_lst").empty();
				$("#xdc_result_xdc").empty();
				hideResult = true;
			});

			$(".close").click(function(){
				document.getElementById("myModal").style.display = "none";
			});
		});
	});

	var wordFrame = false;
	function showWeight(i, j, sent_w, word_w){
		/* $("#sentenceWeight").html("");
		$("#wordWeight").html(""); */
		//word_w = Number.parseFloat(word_w*1);
		//sent_w = Number.parseFloat(sent_w*1);
		
		if(!wordFrame && sent_w != null) {
			word_left = $("#s_" + i + "_w_" + j).position().left + 5;
			word_top = $("#s_" + i + "_w_" + j).position().top - 50;
			$("#xdc_result_weights").css('visibility', "visible");
			$("#xdc_result_weights").css('left', word_left);
			$("#xdc_result_weights").css('top', word_top);
			
			$("#sentenceWeight").html("문장 영향력: "+sent_w.toFixed(6));
		}
		if(word_w != null) {
			word_left = $("#s_" + i + "_w_" + j).position().left + 5;
			word_top = $("#s_" + i + "_w_" + j).position().top - 55;
			$("#xdc_result_weights").css('visibility', "visible");
			$("#xdc_result_weights").css('left', word_left);
			$("#xdc_result_weights").css('top', word_top);
			
			$("#sentenceWeight").html("");
			$("#wordWeight").html("단어 영향력: "+word_w.toFixed(6));
			wordFrame = true;
		}
	}

	function hideWeight(){
		$("#xdc_result_weights").css('visibility', "hidden");
		$("#sentenceWeight").html("");
		$("#wordWeight").html("");
		wordFrame = false;
	}
</script>


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/xdc.js"></script>

