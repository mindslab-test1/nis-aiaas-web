<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="article_board avr_basic">
    <div class="text_box">
        <dl class="category_page chapter_intro" id="chapter_intro_avr">
            <dt class="scroll_text">학습목표</dt>
            <dd>1) 도로상의 객체 인식을 이해한다.<br>
                2) 창문 검출 (Window Detection)을 학습할 수 있다.<br>
                3) 번호판 검출 (Plate Detection)을 학습할 수 있다.</dd>
        </dl>
        <dl class="category_page">
            <dt>컨텐츠 목차</dt>
            <dd>
                <a class="chapter_link" href="#chapter_1_avr">#1  도로상의 객체 인식이란</a>
                <p>1.1 Intro<br>
                </p>
                <a class="chapter_link" href="#chapter_2_avr">#2  학습 방법 </a>
                <p>2.1 창문 검출 (Window Detection)<br>
                    2.2 번호판 검출 (Plate Detection)<br>
                </p>
            </dd>
        </dl>
        <dl class="article_title chapter_1" id="chapter_1_avr">
            <dt>#1</dt>
            <dd class="scroll_text">도로상의 객체 인식이란</dd>
        </dl>
        <dl>
            <dt>
                1.1<br>
                Intro
            </dt>
            <dd> 도로상의 객체 인식은 도로 위에 있는 CCTV 카메라를 통해 들어온 이미지에 대해 차량의 창문과 번호판의 위치, 그리고 보행자나 운전자의 얼굴을 검출하는 기능을 수행한다.<br>
                - 창문 검출 (Window Detection) : 도로 위 차량의 창문의 위치를 검출하는 기능<br>
                - 번호판 검출 (Plate Detection) : 도로 위 차량의 번호판의 위치를 검출하는 기능<br>
                - 얼굴 검출 (Face Detection) : 사람의 얼굴의 위치를 검출하는 기능</dd>
        </dl>
        <dl class="article_title chapter_2" id="chapter_2_avr">
            <dt>#2</dt>
            <dd class="scroll_text">학습 방법</dd>
        </dl>
        <dl>
            <dt>2.1<br>
                창문 검출 <br>(Window Detection)
            </dt>
            <dd><strong>학습 데이터</strong><br>
                - 2개의 촬영 환경 (도로 위 CCTV 환경, 톨게이트 카메라 환경)<br>
                - 대략 1,000건의 도로 위 차량이 찍힌 이미지<br>
                - 이미지 파일(jpg), 이미지 파일에 매칭되는 레이블링 파일(txt)<br>
                - 레이블링 파일에는 창문 위치 정보가 포함된다.
            </dd>
        </dl>
        <dl>
            <dt>
            </dt>
            <dd><strong>학습 모델</strong><br>
                YOLO v3<br>
                - YOLO(You Only Look Once)는Object Detection 분야에서 가장 유명한 딥러닝 모델 중 하나<br>
                - single convolutional network를 통해 multiple bounding box에 대한 class probability를 계산하는 방식<br>
            </dd>
        </dl>
        <dl>
            <dt>
            </dt>
            <dd><strong>학습 command</strong><br>
                - train: 학습 모드<br>
                - cfg/coco.data: 데이터 설정 파일 경로<br>
                - cfg/yolov3.cfg: 네트워크 설정 파일 경로<br>
                - darknet53.conv.74: 사전 학습 모델 경로<br>
                - gpus: 학습에 사용될 GPU 번호<br>

                <span style="display: inline-block; background:#fffce6;padding:10px;font-family: Console;">
./darknet detector \<br>
train \<br>
cfg/coco.data \<br>
cfg/yolov3.cfg \<br>
darknet53.conv.74 \<br>
-gpus 0,1,2,3
                </span>
            </dd>
        </dl>
        <dl>
            <dt>
            </dt>
            <dd>
                <strong>Inference command</strong><br>
                - test: Inference 모드<br>
                - cfg/coco.data: 데이터 설정 파일 경로<br>
                - cfg/yolov3.cfg: 네트워크 설정 파일 경로<br>
                - yolov3,weights: 학습한 모델 경로<br>
                - data/test1.jpg: 입력 이미지 경로<br>
                <span style="display: inline-block; background:#fffce6;padding:10px;font-family: Console;">
./darknet detector \<br>
test \<br>
cfg/coco.data \<br>
cfg/yolov3.cfg \<br>
yolov3.weights \<br>
data/test1.jpg</span>
            </dd>
        </dl>
        <dl class="chapter_3" id="chapter_3_avr">
            <dt>2.2<br>
                번호판 검출<br> (Plate Detection)
            </dt>
            <dd>
                <strong>학습 데이터</strong><br>
                - 2개의 촬영 환경 (도로 위 CCTV 환경, 톨게이트 카메라 환경)<br>
                - 대략 5,000건의 도로 위 차량이 찍힌 이미지<br>
                - 이미지 파일(jpg), 이미지 파일에 매칭되는 레이블링 파일(txt)<br>
                - 레이블링 파일에는 번호판 위치 정보가 포함된다.<br>
            </dd>
        </dl>
        <dl>
            <dt>
            </dt>
            <dd><strong>학습 모델</strong><br>
                EAST (ResNet)<br>
                - EAST(An Efficient and Accurate Scene Text Detector)는 Scene Text Detection 분야의 딥러닝 모델이며 ResNet 네트워크로 설계되었음.
                <figure style="width: 100%;margin: 10px 0 0;text-align: center;">
                    <img style="width:100%; margin:0;" src="${pageContext.request.contextPath}/aiaas/common/images/img_east.JPG" alt="EAST">
                </figure>

            </dd>
        </dl>
        <dl>
            <dt></dt>
            <dd>
                <strong> Source tree</strong><br>
                - checkpoint: EAST 학습 모델<br>
                - data_util: data generator 관련 유틸<br>
                - eval: inference 기능<br>
                - multigpu_train: training 기능<br>
                - nets: resnet 모델 네트워크 관련<br>
                - training_samples: 학습 데이터셋 샘플<br>
                <figure style="width: 100%;margin: 10px 0 0;text-align: center;">
                    <img style="width:50%; margin:0;" src="${pageContext.request.contextPath}/aiaas/common/images/img_sourcetree.png" alt="sourcetree">
                </figure>
            </dd>
        </dl>
        <dl>
            <dt>
            </dt>
            <dd><strong>학습 command</strong><br>
                - gpu_list: 학습에 사용할 GPU 번호<br>
                - input_size: 입력 이미지 크기<br>
                - batch_size_per_gpu: 한 GPU 당 batch size<br>
                - training_data_path: 학습 데이터셋 경로<br>
                - geometry: 검출 박스 종류 지정<br>
                - learning_rate: 학습률 (폭)<br>
                - pretrained_model_path: 이어서 학습할 사전 모델 경로<br>
                <span style="display: inline-block; background:#fffce6;padding:10px;font-family: Console;">
                    python multigpu_train.py<br>
--gpu_list=0<br>
--input_size=512<br>
--batch_size_per_gpu=14 --checkpoint_path=/tmp/east_icdar2015_resnet_v1_50_rbox/<br>
--text_scale=512 --training_data_path=/data/ocr/icdar2015/<br>
--geometry=RBOX<br>
--learning_rate=0.0001<br>
--num_readers=24<br>
--pretrained_model_path=/tmp/resnet_v1_50.ckpt
                </span>
            </dd>
        </dl>
        <dl>
            <dt></dt>
            <dd><strong>Inference command</strong><br>
                - test_data_path: 테스트 할 데이터셋 경로<br>
                - gpu_list: 테스트에 사용될 GPU 번호<br>
                - checkpoint_path: 학습한 모델의 경로<br>
                - output_dir: 인식 결과 이미지가 저장될 폴더 경로<br>
                <span style="display: inline-block; background:#fffce6;padding:10px;font-family: Console;">
python eval.py --test_data_path=/<br>
tmp/images/ --gpu_list=0 --<br>
checkpoint_path=/tmp/<br>
east_icdar2015_resnet_v1_50_rbox/<br>
--output_dir=/tmp/
                </span>
            </dd>
        </dl>
        <dl class="article_title chapter_4" >
            <dt></dt>
            <dd class="scroll_text"></dd>
        </dl>
        <div class="tag_box">
            <small>#실무교육</small>
        </div>
    </div>
</div>

<nav class="chapter_box chapter_box_avr_basic">
    <a class="chapter_link active intro" href="#chapter_intro_avr">
        #0
        <span>학습목표</span>
    </a>
    <a class="chapter_link chp_1" href="#chapter_1_avr">
        #1
        <span>도로상의 <br>객체 인식이란?</span>
    </a>
    <a class="chapter_link chp_2" href="#chapter_2_avr">
        #2
        <span><small>학습 방법</small><br>- 창문 검출</span>
    </a>
    <a class="chapter_link chp_3" href="#chapter_3_avr">
        #2
        <span><small>학습 방법</small><br>- 번호판 검출</span>
    </a>
</nav>


<script type="text/javascript">


</script>