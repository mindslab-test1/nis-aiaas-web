<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="article_board common_guide">
    <div class="text_box">
        <dl class="category_page chapter_intro" id="engine_chapter_intro">
            <dt>학습목표</dt>
            <dd>엔진을 사용하는 데 있어 API 연동의 필요성을 설명할 수 있다.<br>
                모든 엔진의 API 키를 발급받을 수 있다.</dd>
        </dl>
        <dl class="category_page">
            <dt>컨텐츠 목차</dt>
            <dd>
                <a class="chapter_link" href="#engine_chapter_0">#0 Intro</a>
                <p>
                </p>
                <a class="chapter_link" href="#engine_chapter_1">#1 API란?</a>
                <p>
                    1.1 API 정의<br>
                    1.2 엔진가이드와 API
                </p>
                <a class="chapter_link" href="#engine_chapter_2">#2 Postman 사용하기</a>
                <p>
                    2.1 Postman이란? <br>
                    2.2 http 요청 <br>
                    2.3 http 응답 <br>
                </p>
                <a class="chapter_link" href="#engine_chapter_3">#3 엔진 가이드 예시 </a>
                <p>
                    3.1 엔진 가이드 안내<br>
                    3.2 예시: MRC
                </p>

            </dd>
        </dl>
        <dl class="article_title chapter_0" id="engine_chapter_0">
            <dt>#0</dt>
            <dd class="scroll_text">Intro</dd>
        </dl>
        <dl>
            <dt>
            </dt>
            <dd>인공지능 기술을 클라우드에 엔진과 응용 프로그램의 형태로 구현하여 사용자가 최신의 인공지능 서비스를 자유롭게 이용할 수 있도록 한 것이 특징이죠.
                <figure style="text-align: center;">
                    <img style="width:100%;" src="${pageContext.request.contextPath}/aiaas/kr/images/academy_bitmap3x.png" alt="서비스 구조">
                    <figcaption style="text-align: center;">플랫폼의 서비스 구조</figcaption>
                </figure>
                위의 그림과 같이 maum.ai 플랫폼은 4개의 layer를 통해 다양한 인공지능 서비스를 제공하고 있습니다. 바로 ‘AI 데이터 라벨링 서비스’의 Layer 1, ‘인공지능 모델학습’의 Layer 2, ‘엔진API’의 Layer 3, 그리고 ‘어플리케이션’인 Layer 4로 말이죠. <br>
                <figure style="text-align: center;">
                    <img style="width:100%;" src="${pageContext.request.contextPath}/aiaas/common/images/2.jpg" alt="layer 3">
                    <figcaption style="text-align: center;">플랫폼의 서비스 구조</figcaption>
                </figure>
                이 중 layer 3의 인공지능 엔진은 무려 30개나 있는데요. 2020년을 기준으로 현재 음성 엔진 6개, 시각 엔진 10개, 언어 엔진 6개, 분석 엔진 1개, 대화 엔진 3개, 그리고 영어 교육 엔진 3개를 사용해볼 수 있습니다. 이 엔진들은 API 형식으로 제공되므로, 어느 누구나 API를 통해 지속적으로 업데이트되는 최신 인공지능 제품과 서비스들을 체험해볼 수 있습니다.
            </dd>
        </dl>
        <dl class="article_title chapter_1" id="engine_chapter_1">
            <dt>#1</dt>
            <dd class="scroll_text">API란?</dd>
        </dl>

        <dl>
            <dt>
                1.1<br>
                API 정의
            </dt>
            <dd>API (Application Programming Interface):
                응용 프로그램에서 사용할 수 있도록 운영 체제나 프로그래밍 언어가 제공하는 기능을 제어할 수 있게 만든 인터페이스
                (출처: 위키피디아)
            </dd>
        </dl>
        <dl>
            <dt>1.2<br>
                엔진 가이드와 API
            </dt>
            <dd>위키피디아에서는 API를 위와 같이 정의하는데요. 이를 간단하게 말한다면 API는 운영 체제가 제공하는 기능을 응용 프로그램에서 사용할 수 있도록 만든 인터페이스로, 마치 운영 체제의 프로그램과 응용 프로그램의 프로그램 사이의 다리로 볼 수 있죠. 예를 들어 아이폰의 iOS 운영 체제도 여러 API를 제공하고 있는데요. 이러한 API 덕분에 개발자는 비교적 손쉽게 원하는 어플을 개발할 수 있습니다. 만약 카메라 어플을 개발한다고 하면, 개발자는 카메라 인터페이스를 직접 개발할 필요 없이 카메라 어플만 제작하고 그 안에서 애플이 제공하는 카메라 API를 실행시키면 되는 거죠. 마찬가지로 마인즈랩의 maum.ai가 제공하는 인공지능 엔진 역시 고객들이 응용 프로그램에서 손쉽게 마인즈랩의 인공지능 기술을 활용할 수 있도록 API 연동 기능을 제공하고 있습니다. 그러므로 &lt;엔진 가이드&gt;의 0차시 컨텐츠인 이번 &lt;API 공통 가이드&gt;에서는 API를 통해 maum.ai 내의 모든 엔진을 체험할 수 있는 공통 가이드에 대해서 안내하도록 하겠습니다.
            </dd>
        </dl>

        <dl class="article_title chapter_2" id="engine_chapter_2">
            <dt>#2</dt>
            <dd class="scroll_text">Postman 사용하기 </dd>
        </dl>
        <dl>
            <dt>2.1<br>
                Postman이란?</dt>
            <dd>
                그럼 이제 API 키를 가지고 maum.ai의 엔진을 체험해보겠습니다. 엔진을 체험하기 위해서는 API를 테스트할 수 있는 별도의 소프트웨어가 필요한데요. 본 &lt;엔진 가이드&gt; 시리즈에서는 API를 연동할 수 있는 소프트웨어로 가장 보편적인 POSTMAN을 예시로 보여드리겠습니다.<br>
                Postman이란?<br>
                REST API나 URL 요청 또는 http 프로토콜 등을 테스트할 수 있는 소프트웨어<br>

                <figure style="width: 100%;margin: 0;text-align: center;">
                    <img style="width:100%;" src="${pageContext.request.contextPath}/aiaas/common/images/3.jpg" alt="postman">
                    <figcaption style="text-align: center;">Postman 다운로드 페이지</figcaption>
                </figure>
                주소창에 https://www.getpostman.com/downloads/를 입력하면 위와 같은 화면에서 postman을 설치하여 사용할 수 있습니다.<br><br>
                Postman을 설치하였으니 Postman의 기능을 간단하게 살펴보도록 하겠습니다. Postman을 설치하고 로그인을 하면, 아래와 같은 메인 화면을 보실 수 있습니다.
                <figure style="width: 100%;margin: 0;text-align: center;">
                    <img style="width:100%;" src="${pageContext.request.contextPath}/aiaas/common/images/4.jpg" alt="postman">

                </figure>
                Postman은 크게 http 요청과 관련된 섹션과 http 응답과 관련된 섹션으로 나누어져 있습니다. 먼저 http 요청부터 안내드리겠습니다.
            </dd>
        </dl>
        <dl>
            <dt>2.2<br>
                http 요청
            </dt>
            <dd>
                <figure style="width: 100%;margin: 10px 0 0;text-align: center;">
                    <img style="width:100%;" src="${pageContext.request.contextPath}/aiaas/common/images/5.jpg" alt="http요청">
                </figure>
                요청(Request)이란 클라이언트가 서버로 전달해서 서버의 액션이 일어나게끔 하는 메시지를 의미하여, 여기서 http 요청과 관련한 요소를 설정할 수 있습니다.<br><br>

                ① http 메서드<br>
                <figure style="width: 100%;margin: 10px 0 0;text-align: center;">
                    <img style="width:100%;" src="${pageContext.request.contextPath}/aiaas/common/images/6.jpg" alt="http메서드">
                </figure>
                서버가 수행해야 할 동작을 설정할 수 있습니다. GET이나 POST 등의 API 호출 방식을 설정하고, 체험하고자 하는 엔진의 URL 입력할 수 있습니다.<br><br>
                ② header<br>
                <figure style="width: 100%;margin: 10px 0 0;text-align: center;">
                    <img style="width:100%;" src="${pageContext.request.contextPath}/aiaas/common/images/7.jpg" alt="header">
                </figure>
                http 헤더 정보를 입력하는 칸으로, content-type이나 content-language 등을 설정할 수 있습니다.<br><br>
                ③ body<br>
                <figure style="width: 100%;margin: 10px 0 0;text-align: center;">
                    <img style="width:100%;" src="${pageContext.request.contextPath}/aiaas/common/images/8.jpg" alt="body">
                </figure>
                Body는 요청의 본문이 들어가는 내용으로, 데이터 값을 입력하는 창입니다. 이때 Raw 탭을 누르면 JSON 형식도 입력할 수 있습니다.
            </dd>
        </dl>
        <dl>
            <dt>2.3<br>
                http 응답
            </dt>
            <dd>
                <figure style="width: 100%;margin: 10px 0 0;text-align: center;">
                    <img style="width:100%;" src="${pageContext.request.contextPath}/aiaas/common/images/9.jpg" alt="http 응답">
                </figure>
                응답(Response)는 요청에 대한 서버의 답변을 의미하여, API 연동하여 체험한 엔진의 결과값을 확인할 수 있습니다. 따라서 각 엔진의 API에 맞는 요청에 대한 응답(Response)를 이 곳에서 확인할 수 있는 거죠.
            </dd>
        </dl>안
        <dl class="article_title chapter_3" id="engine_chapter_3">
            <dt>#3</dt>
            <dd class="scroll_text"> 엔진 가이드 예시 </dd>
        </dl>

        <dl>
            <dt>
                3.1<br>
                엔진 가이드 안내
            </dt>
            <dd>지금까지 maum.ai의 엔진을 체험하기 위해 필요한 API 키와 API 연동 소프트웨어를 사용하는 법을 알아보았는데요. &lt;엔진 가이드&gt;의 다음 1차시 컨텐츠부터는 API를 연동하여 각 엔진을 체험하는 방법을 안내드리도록 하겠습니다. 다음 컨텐츠 주제인 MRC 엔진을 예시로 해서,  앞으로의 &lt;엔진 가이드&gt; 내용을 간단하게 먼저 보여드릴게요.
            </dd>
        </dl>
        <dl>
            <dt>3.2<br>
                예시: MRC
            <dd>
            MRC (Machine Reading Comprehension)는 주어진 텍스트의 문맥을 이해하여 질문에 해당하는 답을 정확하게 찾아주는 인공지능 독해 엔진입니다. MRC 엔진의 API 키 발급한 상태라고 가정하고, 텍스트와 질문을 통해 http 요청을 하면 어떤 결과가 나올까요?
            아래는 헝가리 태생의 작가 Capa에 대한 지문입니다. 이 지문에는 Capa가 자신의 이름을 바꾸게 된 배경이 나와있죠.
            <span style="display: block; background: #eee;">Born in Hungary in 1913 as Friedmann Endre Ernő, Capa was forced to leave his native country after his involvement in anti government protests. Capa had originally wanted to become a writer, but after his arrival in Berlin had first found work as a photographer. He later left Germany and moved to France due to the rise in Nazism. He tried to find work as a freelance journalist and it was here that he changed his name to Robert Capa, mainly because he thought it would sound more American.</span>

            그가 이름을 바꾼 이유에 대해 아래와 같이 질문을 하겠습니다
            <span style="display: block; background: #eee;">Why did Capa changed his name?</span>

            위의 지문과 질문을 가지고 postman을 통해 http 요청을 하면 아래와 같은 응답값을 확인할 수 있습니다.<br><br>
            <figure style="width: 100%;margin: 10px 0 0;text-align: center;">
                <img style="width:100%;" src="${pageContext.request.contextPath}/aiaas/common/images/10.jpg" alt="응답값">
            </figure>
            바로 Capa가 이름을 바꾼 이유에 대한 답이지요. 이렇게 MRC의 API 키를 발급받고 API를 연동할 수만 있다면, MRC 엔진을 정확하게 사용할 수 있습니다!<br>
            이처럼 &lt;엔진 가이드&gt;의 다음 컨텐츠부터는 API를 활용하여 maum.ai의 각 엔진을 체험하는 방법을 여러분께 안내드리겠습니다.
            </dd>
        </dl>
        <div class="tag_box">
            <small>#실무교육</small>
        </div>
        <dl class="article_title chapter_4" id="engine_chapter_4" style="height:200px;">
            <dt></dt>
            <dd></dd>
        </dl>
    </div>
</div>

<nav class="chapter_box chapter_box_common_guide">
    <a class="chapter_link active intro" href="#engine_chapter_0">
        #0<br>
        <span>Intro</span>
    </a>
    <a class="chapter_link chp_1" href="#engine_chapter_1">
        #1
        <span>API란?</span>
    </a>
    <a class="chapter_link chp_2" href="#engine_chapter_2">
        #2
        <span>Postman 사용하기</span>
    </a>
    <a class="chapter_link chp_3" href="#engine_chapter_3">
        #3
        <span>엔진 가이드 예시</span>
    </a>
<%--    <a class="chapter_link chp_4" href="#ai_chapter_4">--%>
<%--        #4--%>
<%--        <span>엔진 가이드 예시</span>--%>
<%--    </a>--%>
</nav>

<script type="text/javascript">


</script>
