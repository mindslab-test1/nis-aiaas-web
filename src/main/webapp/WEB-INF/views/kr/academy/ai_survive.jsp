<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="article_board ai_survive">
    <div class="text_box">
        <dl class="category_page chapter_intro" id="ai_chapter_intro">
            <dt>학습목표</dt>
            <dd>1) 언어학 지식이 인공지능 음성기술 개발에 중요한 이유와 음성기술이  실제 비즈니스에서 활용되는 사례를 설명할 수 있다.<br>
                2) 언어학 지식이 챗봇 개발에 중요한 이유와 챗봇이 실제 비즈니스에서 활용되는 사례를 설명할 수 있다.<br>
                3) 어문학 지식이 인공지능 통번역 개발에 중요한 이유와 인공지능 통번역의 전망을 설명할 수 있다.<br>
                4) 인공지능 분야에서 어문계열 전공자가 발휘할 수 있는 잠재력을 설명할 수 있다.</dd>
        </dl>
        <dl class="category_page">
            <dt>컨텐츠 목차</dt>
            <dd>
                <a class="chapter_link" href="#ai_chapter_0">#0 Intro</a>
                <p>

                </p>
                <a class="chapter_link" href="#ai_chapter_1">#1 음성 기술이란?</a>
                <p>
                    1.1 음성기술의 구성<br>
                    1.2 음성기술과 언어학<br>
                    1.3 음성인식의 비즈니스 사례<br>
                    1.4 음성합성의 비즈니스 사례<br>
                </p>
                <a class="chapter_link" href="#ai_chapter_2">#2 챗봇이란?</a>
                <p>
                    2.1 챗봇과 언어학<br>
                    2.2 챗봇의 비즈니스 사례<br>
                </p>
                <a class="chapter_link" href="#ai_chapter_3">#3 미래가 밝은 인공지능 통번역! </a>
                <p>
                    3.1 통번역과 어문학<br>
                    3.2 인공지능 통번역의 전망
                </p>
                <a class="chapter_link" href="#ai_chapter_4">#4 어문계열 전공자가 보는 인공지능이란? </a>
                <p>
                    4.1 마인즈랩 구성원이 말하는 인공지능과 어문계열<br>
                    4.2 인공지능 시대에서의 어문계열 전공자
                </p>

            </dd>
        </dl>
        <dl class="article_title chapter_0" id="ai_chapter_0">
            <dt>#0</dt>
            <dd class="scroll_text">Intro</dd>
        </dl>
        <dl>
            <dt>
            </dt>
            <dd>‘문송합니다!’ 취업난을 겪어본 문과생들이라면, 특히 인문학도에게는 너무나 서러운 신조어죠. 여러분은 인문학이라고 하면 보통 어떤 전공이 생각나나요? 아무래도 영어영문학과, 중어중문학과, 불어불문학과 등 인문학부가 있는 학교라면 없을 수가 없는 어문계열을 흔히들 많이 떠올리실 거예요. 그런 의미에서 &lt;인문학도를 위한 인공지능&gt;의 첫번째 주자로 어문계열에 대해 알아보도록 합시다! 대부분의 어문계열 커리큘럼은 크게 언어학과 어문학으로 나뉩니다. 언어학은 하나의 언어 그 자체를 공부하는 학문으로, 조음을 공부하는 음성학에서부터 음운론, 형태론, 통사론, 화용론 등의 세부 학문을 다룹니다. 반면 어문학은 한 언어로 쓰인 문학 작품을 인문학적 관점에서 비평하면서 그 언어를 이해하는 학문으로, 해당 언어 능력과 함께 그 언어가 쓰이는 문화까지 배우는 학문이죠. 그렇다면 언어학과 어문학으로 세계의 각 언어에 대한 통찰력을 가진 어문계열 사람들은 인공지능 시대에 어떻게 기여할 수 있을까요?</dd>
        </dl>
        <dl class="article_title chapter_1" id="ai_chapter_1">
            <dt>#1</dt>
            <dd class="scroll_text">음성기술이란?</dd>
        </dl>

        <dl>
            <dt>
                1.1<br>
                음성기술의 구성
            </dt>
            <dd>언어학적 소양이 있는 어문계열이 큰 두곽을 보이는 인공지능 분야 중 하나는 바로 음성기술입니다. 음성 기술은 크게 두 가지로 나누어 볼 수 있는데요. 먼저 우리 일상에서 흔히 볼 수 있는 애플의 시리나 아마존의 알렉사처럼 컴퓨터가 사람의 음성 언어를 인식하고 이를 텍스트 데이터로 출력하는 음성인식(Speech Recognition, Speech-to-Text)과 인공지능 뉴스 앵커처럼 텍스트 데이터를 사람의 음성으로 합성하여 출력하는 음성합성(Speech Synthesis, Text-to-Speech)이 있습니다.
                <figure style="text-align: center;">
                    <img style="" src="${pageContext.request.contextPath}/aiaas/common/images/img_article_1.png" alt="음성기술의 구성">
                </figure>
            </dd>
        </dl>
        <dl>
            <dt>1.2<br>
                음성기술과 <br>언어학
            </dt>
            <dd>이때 인공지능 음성기술의 성능을 높이기 위해서는 인공지능 학습용 언어 데이터를 잘 선별하는 것이 중요한데요. 적절한 언어 데이터를 선별하기 위해서는 언어가 어떻게 작용하는지를 이해하고 언어에서 보이는 특징을 발견해낼 수 있는 언어학적 지식과 통찰력이 매우 중요합니다. 예를 들어 인공지능에게 발음은 비슷하지만 다른 철자로 표기되는 음성을 학습시키기 위해서는 음성학 지식이 필요하며, 특정 환경에 따라 발음이 변화하는 동화현상, 구개음화 등의 음운 규칙을 학습시키기 위해서는 음성 데이터를 음절이나 음소로 분할하여 이해할 수 있는 음운론 지식이 필요하죠. 실제로 음성기술을 연구하는 고려대학교 NAMZ의 연구원들 모두 인문대생이라고 하니, 인공지능 음성기술에 있어 언어학이 얼마나 중요한지는 더 말할 필요가 없겠죠? </dd>
        </dl>
        <dl>
            <dt>1.3<br>
                음성인식의 <br>비즈니스 사례
            </dt>
            <dd>
                음성기술 중 음성인식은 외국어 교육 컨텐츠에서 자주 쓰이고 있습니다. 바로 음성인식 기술을 사용해서 외국어 학습자의 발화를 평가하는 거죠. 이번 글에서 소개드릴 영어 회화 학습 컨텐츠인 스피킹맥스에서도 이러한 음성인식 기술을 엿볼 수 있는데요. 스피킹맥스는 학습자의 발음을 평가하기 위하여 음성인식 기술을 통해 학습자의 음성을 개별 소리로 분리한 뒤 원어민의 발음과 비교하여 점수를 부여하는 시스템을 도입하였습니다. 학습자가 원어민의 음성과 자신의 음성을 시각화한 그래프를 보고 스스로 자신의 발음과 억양을 교정할 수 있도록 말이죠.
                <figure style="text-align: center;">
                    <img style="" src="${pageContext.request.contextPath}/aiaas/common/images/img_article_2.png" alt="외국어 교육 컨텐츠에서의 음성인식">
                    <figcaption style="text-align: center;">▲ 외국어 교육 컨텐츠에서의 음성인식 (이미지 출처: 스피킹맥스)</figcaption>
                </figure>
            </dd>
        </dl>
        <dl>
            <dt>1.4<br>
                음성합성의 <br>비즈니스 사례
            </dt>
            <dd>
                WellSaid Labs는 음성합성 기술로 미국의 CB인사이츠의 ‘2020년 글로벌 AI 스타트업 톱 100’에 선정되었습니다. WellSaid Labs는 음성합성 기술을 접목한 음성 라이브러리를 보유하고 있는데요. 이를 통해 고객이 원하는 목소리를 커스터마이징해주거나, 사고 등으로 인해 말을 하지 못하는 사람들에게 인공 목소리(synthesized voice)를 제공한다고 합니다. 구글 홈과 같은 기존의 음성합성 서비스는 주어진 텍스트를 일정하게 발음하여 부자연스러운 반면, WellSaid Labs의 음성은 텍스트 문맥에 따라 음성을 다르게 출력하고 같은 텍스트라도 그때그때 다르게 출력하기 때문에 인공지능 음성인지 눈치채지 못할 정도로 자연스럽다고 해요.
                <figure style="text-align: center;">
                    <img style="" src="${pageContext.request.contextPath}/aiaas/common/images/img_article_3.png" alt="음성합성 서비스">
                    <figcaption style="text-align: center;">▲ 음성합성 서비스 (이미지 출처: WellSaid Labs)</figcaption>
                </figure>
            </dd>
        </dl>
        <dl class="article_title chapter_2" id="ai_chapter_2">
            <dt>#2</dt>
            <dd class="scroll_text">챗봇이란? </dd>
        </dl>
        <dl>
            <dt>2.1<br>
                챗봇과 언어학</dt>
            <dd>
                <figure style="float: left;">
                    <img style="" src="${pageContext.request.contextPath}/aiaas/common/images/img_article_4.png" alt="호텔의 챗봇 서비스">
                    <figcaption style="text-align: center;">▲ 호텔의 챗봇 서비스 (이미지 출처: 벨류호텔부산)</figcaption>
                </figure>
                <span style=" font-family:serif;font-size:13px;">
                                    <em style="display: block;font-size: 37px; color: #b3b3b3;font-family: cursive;text-align: center;line-height: 10px">&#34;</em>야후에서는 챗봇 모델을 개발하기 위해 522,000여개의 실제 네티즌들의 의도와 감정이 담긴 코멘트(Yahoo News An-notated Comments Corpus)를 연구하고 있습니다. 이 프로젝트에 참여하는 선임 연구원 역시 머신러닝과 함께 언어학을 전공했다는데요. 챗봇이 과연 뭐길래 그러죠?
                                     <em style="display: block;font-size: 37px; color: #b3b3b3;font-family: cursive;text-align: center;height: 20px;line-height: 32px;">&#148;</em>
                                </span>
                <p style="clear:both">음성기술 이외에 언어학이 중요한 또 다른 분야는 바로 챗봇입니다. 챗봇이란 대화형 인터페이스에서 컴퓨터와 유저가 자유롭게 대화하는 서비스인데요. 사실 여러분들이 잘 알고 있는 심심이나 카카오톡 챗봇도 바로 챗봇이랍니다. 그리고 챗봇 중 인공지능을 기반으로 하는 챗봇은 상대방의 발화에 담긴 의도를 파악하여 유저와 대화를 하는 법을 배울 수 있는데요. 인공지능에게 대화를 통해 정보를 습득하는 법을 학습시키기 위해서는 인간의 실제 의사소통 방식을 모델로 한 다수의 예시 질문과 답변으로 구성된 학습 데이터가 필요합니다. 이때 질문과 답변을 문장으로 구사하는 법을 학습시키기 위해서는 문장의 구조에 따라 데이터를 학습시킬 수 있는 통사론 지식이 매우 중요하죠. 그리고 만약 인공지능이 잘못된 답변을 한다면, 담화로 구성된 데이터 중 오류의 원인을 파악하여 데이터를 수정할 수 있어야 합니다. 이를 위해서는 상황이나 맥락에 따른 의미에 관한 화용론 지식이 필요한 거죠.</p>
            </dd>
        </dl>
        <dl>
            <dt>2.2<br>
                챗봇의 <br>비즈니스 사례
            </dt>
            <dd>챗봇은 현재 많은 기업에서 활용하고 있는데요. 이 중 챗봇 관련 스타트업 Hugging Face는 뉴욕을 시작으로 미국 전역으로 빠르게 성장하여 CB인사이츠가 선정한 ‘2020년 글로벌 AI 스타트업 톱 100’에 선정되었습니다. 고객 서비스 용도로 쓰이는 대부분의 챗봇과는 달리, Hugging Face 챗봇은 인공지능과 유저가 서로 친구가 되어 채팅을 하고 셀카나 비디오 등을 공유할 수 있는 엔터테인먼트 목적으로 만들어졌다고 해요. Hugging Face의 챗봇 속 인공지능은 유저가 공유한 텍스트와 이미지를 통해 유저의 감정을 파악하고 이를 바탕으로 대화를 이어나갈 수 있다고 합니다. 언제 어디서나 비밀을 공유할 수 있는 가상의 친구를 만들 수 있다는 점에서 Hugging Face는 현재 청소년 유저들 사이에서 선풍적인 인기를 끌고 있습니다.
                <figure style="width: 100%;margin: 10px 0 0;text-align: center;">
                    <img style="display: inline-block; margin: 0 40px 0 0;" src="${pageContext.request.contextPath}/aiaas/common/images/img_article_5_.png" alt="유저와 대화하는 Hugging Face 챗봇의 가상 셀카">
                    <img style="display: inline-block" src="${pageContext.request.contextPath}/aiaas/common/images/img_article_5.png" alt="유저와 대화하는 Hugging Face 챗봇의 가상 채팅">
                    <figcaption style="text-align: center;clear:both;">▲ 유저와 대화하는 Hugging Face 챗봇의 가상 셀카 및 채팅 (이미지 출처: PR Newswire)</figcaption>
                </figure>

            </dd>
        </dl>

        <dl class="article_title chapter_3" id="ai_chapter_3">
            <dt>#3</dt>
            <dd class="scroll_text">미래가 밝은 인공지능 통번역! </dd>
        </dl>

        <dl>
            <dt>
                3.1<br>
                통번역과 어문학
            </dt>
            <dd>지금까지 언어학 지식이 인공지능에 도움이 될 수 있는 분야에 대해 살펴봤는데요. 그럼 어문학은 인공지능 시대에 어떤 이점이 있을까요? 어문학을 공부하면 인문학적 소양과 함께 그 언어의 구사 능력과 그 언어권의 문화에 대한 이해력도 덤으로 얻게 되죠. 이러한 언어 실력과 문화 이해력은 인공지능 통번역의 발달에 큰 기여를 할 수 있습니다. 그 이유는 통번역기의 질은 그 번역기가 학습한 데이터의 양도 중요하지만 무엇보다 맥락과 문화에 따른 해당 언어의 관용적 의미를 얼마나 잘 이해하고 있는지에 크게 좌우되기 때문이예요. 이를 위해선 의미론과 화용론과 함께 해당 언어로 쓰이는 문화적 배경을 이해하는 문학적 소양이 매우 중요한 것이죠. 인공지능에게 이런 문학적 소양을 학습시킬 수 있다면, 인공지능 통번역기는 우리가 흔히 생각하는 부자연스러운 기계식 번역을 하지 않게 되겠죠.
                <figure>
                    <img style="" src="${pageContext.request.contextPath}/aiaas/common/images/img_article_6.png" alt="구글 번역기">
                    <figcaption style="text-align: center;">▲ 구글 번역기 (이미지 출처: 구글)</figcaption>
                </figure>
        </dl>
        <dl>
            <dt>3.2<br>
                인공지능 <br>통번역의 전망
            <dd>
                실제 인공지능 통번역은 우리 일상에서 흔하게 접할 수 있는데요. 음성이던 텍스트던 바로 원하는 언어로 번역해주는 구글 번역기, 네이버 파파고가 그 예이죠. 마찬가지로 2021년 도쿄 올림픽을 개최하는 일본 정부는 이번 올림픽을 “언어 장벽 없는 올림픽”으로 선언하고 자동번역 서비스 개발에 박차를 가하고 있다고 하니, 인공지능 번역의 전망이 아주 밝다고 볼 수 있겠습니다.  </dd>
        </dl>
        <dl class="article_title chapter_4" id="ai_chapter_4">
            <dt>#4</dt>
            <dd class="scroll_text">어문계열 전공자가 보는 인공지능이란? </dd>
        </dl>
        <dl>
            <dt>4.1<br>
                마인즈랩 구성원이 <br>말하는 인공지능과 <br>어문계열
            <dd>
                지금까지 언어학과 어문학적 소양을 겸비한 어문계열 전공자가 활약을 보일 수 있는 인공지능에 대해 살펴보았는데요. 정리하자면 어문계열 전공자의 강점은 언어나 외국어에 대한 지식과 통찰력에 있다고 볼 수 있겠습니다.
                실제로 AI 시장에서도 언어학 전공자를 우대하고 있는데요. 스페인에 본사를 두고 있는 글로벌 인공지능 전문 기업인 Inbenta는 Chief linguistic officer(수석 언어 담당자)라는 직책을 신설하여 언어학 전공자를 집중적으로 채용하고 있습니다. 마찬가지로 국내 대표적인 인공지능 전문기업 마인즈랩에서도 데이터 분석 업무에서부터 사내 교육, 비즈니스 등 많은 부서에서 어문학 전공자들이 활약하고 있다고 하는데요. 그래서 국어국문학을 전공하여 마인즈랩 인재개발부서 팀장으로 근무하고 계시는 L 씨와 인터뷰를 해보았는데요. L 씨는 어문계열 전공자의 강점이 인공지능을 인간에 가깝게 하는 능력에 있다고 합니다. 어딘가 어색하지만 데이터나 수치로 객관화될 수 없는 인공지능의 모습을 어문학을 전공한 사람들이 더 잘 잡아내서 인공지능을 더욱 자연스럽게 만드는 데 기여한다는 거죠. 실제 L 씨는 이러한 강점을 가지고 음성기술 학습 데이터 분석 및 정제, 그리고 고효율 학습 데이터셋 구축을 위한 가이드라인 기획 등 다양한 업무를 통해 회사에 기여했다고 합니다. L 씨는 어문학을 공부하는 후배들에게 다음과 같이 조언해주고 싶다고 합니다.

                <span style=" display: block;font-family:serif;font-size:13px;width:400px;margin:0 auto;">
                                    <em style="display: block;font-size: 37px; color: #b3b3b3;font-family: cursive;text-align: center; height: 10px;line-height: 10px;margin: 20px 0 0;">&#34;</em>
                                문학을 공부하는 것은 단순히 작품이나 언어를 공부하는 것을 넘어 사회와 문화를 공부한다고 생각합니다. 이런 이해를 기반으로 본인 전공에 구애받지 않고 본인이 하고싶은 다양한 분야에 도전해서 시너지를 스스로 만들 수 있으면 좋겠어요.
                                    <em style="display: block;font-size: 37px; color: #b3b3b3;font-family: cursive;text-align: center; height: 10px;line-height: 10px;margin: 10px 0 0;">&#148;</em>
                            </span>
            </dd>

        </dl>
        <dl>
            <dt>4.2<br>
                인공지능 <br>시대에서의 <br>어문계열 전공자
            <dd>
                이처럼 어문계열 전공자들은 언어에 대한 이해를 바탕으로 그 언어를 사용하는 사람들의 사회와 문화까지 이해하는 통찰력을 가지고 있는데요. 여기서 언어는 사람과 사회, 그리고 심지어 사람과 컴퓨터 간의 커뮤니케이션 수단이 될 수 있으므로, 어문계열 전공자들은 인공지능과 인간의 구분이 점점 흐릿해지는 지금과 같은 인공지능 시대에 커뮤니케이터의 역할을 톡톡히 해낼 것으로 기대됩니다. 마치 사람 간의 언어를 이해하고 이를 컴퓨터와 사람 간의 언어로 구현하는 음성기술과 챗봇, 그리고 사람과 사람 간의 소통을 도와주는 통번역기처럼 말이죠. 어문계열 전공자인 여러분들은 우리 사회에서 어떤 시너지를 만들어보고 싶나요?
            </dd>
        </dl>
        <div class="tag_box">
            <small>#소양교육</small>
        </div>
    </div>
</div>

<nav class="chapter_box chapter_box_ai_survive">
    <a class="chapter_link active intro" href="#ai_chapter_0">
        #0<br>
        <span>Intro</span>
    </a>
    <a class="chapter_link chp_1" href="#ai_chapter_1">
        #1
        <span>음성 기술이란?</span>
    </a>
    <a class="chapter_link chp_2" href="#ai_chapter_2">
        #2
        <span>챗봇이란?</span>
    </a>
    <a class="chapter_link chp_3" href="#ai_chapter_3">
        #3
        <span>미래가 밝은 인공지능 통번역!</span>
    </a>
    <a class="chapter_link chp_4" href="#ai_chapter_4">
        #4
        <span>어문계열 전공자가 보는 인공지능이란?</span>
    </a>
</nav>

<script type="text/javascript">

    // var w_height = $(window).innerHeight();
    // $('.chapter_box').css('bottom',w_height-315+'px');

    // $( window ).scroll( function() {
    //     var windowObj = $(window).scrollTop();
    //     var chapter_0 =$('.ai_survive .ai_chapter_0').offset().top - 100;
    //     var chapter_1 =$('.ai_survive .ai_chapter_1').offset().top - 100;
    //     var chapter_2 =$('.ai_survive .ai_chapter_2').offset().top - 100;
    //     var chapter_3 =$('.ai_survive .ai_chapter_3').offset().top - 100;
    //     var chapter_4 =$('.ai_survive .ai_chapter_4').offset().top - 100;
    //
    //     console.log(windowObj);
    //     console.log(chapter_1);
    //     console.log(chapter_2);
    //     console.log(chapter_3);
    //     console.log(chapter_4);
    //     // if( windowObj <= chapter_0){
    //     //     $('.chapter_link').removeClass('active');
    //     //     $( '.intro' ).addClass('active');
    //     // }
    //
    //     if(windowObj>278){
    //         $( '.chapter_box' ).addClass('fixed').css({
    //             'bottom':'auto',
    //         });
    //     }else{
    //         $( '.chapter_box' ).removeClass('fixed').css({
    //             'bottom':w_height-315+'px',
    //         });
    //     }
    // });
</script>
