<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--ddd--%>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%

    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/academy.css?ver=<%=fmt.format(lastModifiedStyle)%>">




<!-- .contents -->
<div class="contents">
    <div class="content academy_cont">
    <%--        <h1>마음 아카데미</h1>--%>
        <div class="academy_demobox">
            <%--academy_top--%>
            <div class="academy_top">
                <div class="academy_top_area">
                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_academy.svg" alt="아카데미 로고" />

                </div>
            </div>
            <%--//academy_top--%>
            <div class="cont_top">
                <div class="tab_button">
                    <ul>
                        <li class="active"><button type="button" class="sorting_btn tab" onclick="all_show(this)">전체 교육</button></li>
                        <li class=""><button type="button" class="sorting_btn tab" onclick="tab_filter(this)">소양교육</button></li>
                        <li class=""><button type="button" class="sorting_btn tab" onclick="tab_filter(this)">실무교육</button></li>
                    </ul>
                    <div class="srchBox">
                        <input type="text" onkeyup="filter()" class="ipt_srch" id="value" placeholder="검색어 입력 ex) #BERT">
                        <button type="button" class="btn_ico" data-tooltip="academy_srch"><em class="fas fa-search"></em></button>
                    </div>
                </div>
                <div class="article_top">
                    <button type="button" class="back_btn"><em class="fas fa-chevron-left"></em> 뒤로</button>
                    <h2 class="article_tit">maum.ai 기초</h2>
                </div>
            </div>
            <div class="cont_body">
                <div class="list_board">
                    <ul>
                        <li class="available item" id="maumai_basic">
                            <a href="#" class="unit_btn tag ">
                                <h5 class="name_tit"><br>maum.ai 기초</h5>
                                <p class=""><small>#maumai</small><small>#소양교육</small></p>
                            </a>
                        </li>
                        <li class="available item" id="humanities_ai">
                            <a href="#" class="tag">
                                <h5 class="name_tit ">인문학도를 위한 <br>인공지능</h5>
                                <p class=""><small>#소양교육</small></p>
                            </a>
                        </li>
                        <li class="item unavailable">
                            <a href="#" class="tag">
                                <h5 class="name_tit "><br>AIaaS 기초</h5>
                                <p class=""><small>#소양교육</small></p>
                            </a>
                        </li>
                        <li class="item unavailable">
                            <a href="#" class="tag">
                                <h5 class="name_tit "><br>인물 포즈인식</h5>
                                <p class=""><small>#엔진가이드</small></p>
                            </a>
                        </li>
                        <li class="item unavailable">
                            <a href="#" class="tag">
                                <h5 class="name_tit "><br>STT</h5>
                                <p class=""><small>#음성인식</small><small>#실무교육</small></p>
                            </a>
                        </li>
                        <li class="item unavailable">
                            <a href="#" class="tag">
                                <h5 class="name_tit "><br>maum회의록</h5>
                                <p class=""><small>#회의록</small><small>#엔진가이드</small></p>
                            </a>
                        </li>
                        <li class="item unavailable">
                            <a href="#" class="tag">
                                <h5 class="name_tit "><br>maumSDS</h5>
                                <p class=""><small>#챗봇</small><small>#실무교육</small></p>
                            </a>
                        </li>
                        <li class="item unavailable">
                            <a href="#" class="tag">
                                <h5 class="name_tit  "><br>AI Builder</h5>
                                <p class=""><small>#엔진가이드</small><small>#실무교육</small></p>
                            </a>
                        </li>
<%--                        <li class="item unavailable">--%>
<%--                            <a href="#" class="tag">--%>
<%--                                <h5 class="name ">비전공자로<br>--%>
<%--                                    인공지능 시대 살아남기</h5>--%>
<%--                                <p class=""><small>#소양교육</small></p>--%>
<%--                            </a>--%>
<%--                        </li>--%>
                        <li class="item unavailable">
                            <a href="#" class="tag">
                                <h5 class="name_tit "><br>AIaaS 심화</h5>
                                <p class=""><small>#소양교육</small></p>
                            </a>
                        </li>

                    </ul>
                </div>
                <%@ include file="../academy/maumai_basic.jsp" %>

                <%@ include file="../academy/ai_survive.jsp" %>

                <div class="ai_list">
                    <ul>
                        <li id="ai_survive" >
                            <p>1. 어문계열로 인공지능 시대 살아남기</p>
                            <em class="fas fa-arrow-right"></em>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- //.content -->
</div>
<!-- //.contents -->



<script type="text/javascript">
    $(window).load(function() {
        $(document).ready(function (){
            $('.academy_link').addClass('active');
            $('.tab_list li').each(function(index){
                $(this).click(function(){
                    $('.tab_list li').removeClass('active');
                    $(this).addClass('active');
                    $('.academy_main ').hide();

                    if ($(this).hasClass('active')){
                        var cont = $(this).children().attr('class');
                        $("#"+cont).fadeIn();
                    }
                });
            });

            var pageClass = "";

            $('.available.item').on('click',function(){
                var this_value = $(this).attr('id');
                var name_tit = $(this).children().children('.name_tit').text();
                $('.list_board').hide();
                $('.tab_button').hide();
                $('.article_top').fadeIn();
                $('.article_tit').text(name_tit);
                $('.'+this_value).fadeIn();
                $('.chapter_box_'+this_value).fadeIn();
                pageClass = this_value;
            });

            //back_btn click function
            $('.back_btn').on('click',function(){
                $('.list_board').css('display','block');
                $('.tab_button').fadeIn();
                $('.article_top').hide();
                $('.article_board').hide();
                $('.chapter_box').hide();
                $('.ai_list').hide();
            });

            //인문학도를 위한 인공지능
            $("#humanities_ai").on('click',function(){
                $('.list_board').hide();
                $('.tab_button').hide();

                $('.ai_list').show();
            });

            $('#ai_survive').on('click',function() {
                var this_value = $(this).attr('id');
                $('.ai_list').hide();
                // $(".chapter_box_maumai_basic").hide();
                $('.'+this_value).fadeIn();
                $('.chapter_box_'+this_value).fadeIn();
                pageClass = this_value;

            });

            var w_height = $(window).innerHeight();
            $('.chapter_box').css('bottom',w_height-315+'px');

            $( window ).scroll( function() {
                console.log(pageClass);
                var windowObj = $(window).scrollTop();
                var chapter_1 =$('.'+pageClass + ' .chapter_1').offset().top - 100;
                var chapter_2 =$('.'+pageClass + ' .chapter_2').offset().top - 100;
                var chapter_3 =$('.'+pageClass + ' .chapter_3').offset().top - 100;

                var z = $('.'+pageClass + ' .chapter_4').offset();
                var chapter_4 = windowObj;
                if(z != undefined) {
                    chapter_4 = z.top - 500;
                }

                console.log(windowObj);
                console.log(chapter_1);
                console.log(chapter_2);
                console.log(chapter_3);
                console.log(chapter_4);

                if( windowObj <= chapter_1){
                    $('.chapter_link').removeClass('active');
                    $( '.intro' ).addClass('active');
                }else if(windowObj > chapter_1 && windowObj < chapter_2){
                    $('.chapter_link').removeClass('active');
                    $( '.chp_1' ).addClass('active');
                }else if(windowObj > chapter_2 && windowObj < chapter_3){
                    $('.chapter_link').removeClass('active');
                    $( '.chp_2' ).addClass('active');
                }else if(windowObj > chapter_3 && windowObj <= chapter_4){
                    $('.chapter_link').removeClass('active');
                    $( '.chp_3' ).addClass('active');
                }else if(windowObj > chapter_4){
                    $('.chapter_link').removeClass('active');
                    $( '.chp_4' ).addClass('active');
                }

                if(windowObj>278){
                    $( '.chapter_box' ).addClass('fixed').css({
                        'bottom':'auto',
                    });
                }else{
                    $( '.chapter_box' ).removeClass('fixed').css({
                        'bottom':w_height-315+'px',
                    });
                }
            });

            $('.chapter_link').on('click',function(e){
                e.preventDefault();
                var this_link = $(this).attr('href');
                $('html, body').animate({scrollTop : $(this_link).offset().top-70}, 500);

            });



            //팝업 창
            $('#seminar').on('click', function(){
                $('.seminar').fadeIn();
            });
            $('#membership').on('click', function(){
                $('.membership').fadeIn();
            });
            $('#curriculum').on('click', function(){
                $('.curriculum').fadeIn();
            });
            // 공통 팝업창 닫기
            $('.ico_close, .pop_bg').on('click', function () {
                $('.membership').fadeOut(300);
                $('.seminar').fadeOut(300);
                $('.curriculum').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
        });
    });
    $(function(){
        $('#container').css({'height':'auto'});

    });


    //검색기능
    function filter(){
        var value, tag, item, i;

        value = document.getElementById("value").value.toUpperCase();
        item = document.getElementsByClassName("item");

        for(i=0;i<item.length;i++){
            tag = item[i].getElementsByClassName("tag");
            if(tag[0].innerHTML.toUpperCase().indexOf(value) > -1){
                item[i].style.display = "block";
            }else{
                item[i].style.display = "none";
            }
        }
    }

    function tab_filter(tabName){


        var tab, tag, item, tabclass, button, click_button, i;
        tab = tabName.innerText;
        item = document.getElementsByClassName("item");

        tabclass = document.getElementsByClassName("tab");


        for(i=0;i<item.length;i++){
            tag = item[i].getElementsByClassName("tag")[0];
            value = tag.innerText;

            if(value.indexOf(tab) > -1){
                item[i].style.display = "block";
            }else{
                item[i].style.display = "none";
            }
        }

        for(i=0;i<tabclass.length;i++){
            button = tabclass[i].parentNode;
            button.classList.toggle('active',false);
        }

        tabName.parentElement.classList.toggle('active',true);
    }

    function all_show(tabName){
        var item;
        item = document.getElementsByClassName("item");
        tabclass = document.getElementsByClassName("tab");

        for(i=0;i<item.length;i++){
            item[i].style.display = "block";
        }

        for(i=0;i<tabclass.length;i++){
            button = tabclass[i].parentNode;
            button.classList.toggle('active',false);
        }
        tabName.parentElement.classList.toggle('active',true);
    }
</script>
