<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-ui-1.10.3.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/pop_common.css">


<div class="contents">
	<div class="content main_content">
		<div class="stn_landing">
			<div class="stn_landing_cont">
				<div class="item_set">
					<h4 class=""><em class="fas fa-microphone"></em> 음성</h4>
					<ul class="item_lst bg_purple">
						<li>
							<dl class="ico_tts">
								<dt>
									<a href="${pageContext.request.contextPath}/tts/krTtsMain" target="_self">
										<span>음성생성 (TTS)</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>실제 그 사람의 목소리 그대로 자연스럽게, 세계 최고 수준의 음질과 실시간 합성 속도를 제공합니다.</dd>
								<dd class="ytb_link">
									<a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a>
								</dd>
							</dl>
						</li>
						<li>

							<dl class="ico_stt">
								<dt>
									<a href="${pageContext.request.contextPath}/stt/krSttMain" target="_self">
										<span>음성 인식 (STT)</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>음성을 텍스트로 변환하는 엔진으로, 다양한 학습모델을 활용할 수 있고 높은 인식률과 빠른 처리 속도를 제공합니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>

						</li>
						<li>
							<dl class="ico_den">
								<dt>
									<a href="${pageContext.request.contextPath}/denoise/krDenoiseMain" target="_self">
										<span>음성 정제</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>음성에 섞여있는 배경음과 같이, 음성 내의 다양한 잡음을 제거합니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
					   	<li>
							<dl class="ico_dia">
								<dt>
									<a href="${pageContext.request.contextPath}/diarize/krDiarizeMain" target="_self">
										<span>화자 분리</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>회의 음성 등 여러 사람의 음성이 포함된 음성을 입력하면 어떤 시간에 어떤 화자가 말한 음성인지 분리해 냅니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
					    </li>
						<li>
							<dl class="ico_vf">
								<dt>
									<a href="${pageContext.request.contextPath}/voicefilter/krVoicefilterMain" target="_self">
										<span>Voice Filter</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>내 목소리와 다른 사람의 목소리가 겹쳐 있는 파일을 입력하면 내 목소리를 분리해냅니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_vr">
								<dt>
									<a href="${pageContext.request.contextPath}/vr/krVoiceRecogMain" target="_self">
										<span>Voice Recognition</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>사람의 음성 데이터를 Vector화하고 그 값을 대조하여 목소리를 인식합니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
					</ul>
				</div>
				<div class="item_set">
					<h4 class=""><em class="fas fa-eye"></em> 시각</h4>
					<ul class="item_lst bg_pink">
						<li>
							<dl class="ico_fr">
								<dt>
									<a href="${pageContext.request.contextPath}/fr/krFaceRecogMain" target="_self">
										<span>Face Recognition</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>사람의 얼굴 데이터를 Vector화하고 그 값을 대조하여 얼굴을 인식합니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_esr" >
								<dt>
									<a href="${pageContext.request.contextPath}/superResolution/krSuperResolutionMain" target="_self">
										<span>Enhanced Super Resolution</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>작은 크기의 이미지를 손실률을 최소화하여 확대해 줍니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_tti">
								<dt>
									<a href="${pageContext.request.contextPath}/tti/krTtiMain" target="_self">
										<span>AI 스타일링</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd class="txt">패션에 대한 설명 텍스트를 입력하면 이를 이미지로 생성해 냅니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_tr">
								<dt>
									<a href="${pageContext.request.contextPath}/textRemoval/krTextRemovalMain" target="_self">
										<span>텍스트 제거</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd class="txt">이미지에 있는 텍스트를 찾아 내어 제거해줍니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_avr">
								<dt>
									<a href="${pageContext.request.contextPath}/avr/krAvrMain" target="_self">
										<span>도로상의 객체 인식</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>도로 상에서 달리는 차량의 이미지를 입력하면 창문의 위치, 차안에 있는 사람의 위치 그리고 번호판의 위치를 표시해줍니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_pr">
								<dt>
									<a href="${pageContext.request.contextPath}/poseRecog/krPoseRecogMain" target="_self">
										<span>인물 포즈 인식</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>이미지 내에 있는 사람의 포즈를 인식하여 추출하고 시각화하는 엔진입니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_ft">
								<dt>
									<a href="${pageContext.request.contextPath}/faceTracking/krFaceTrackingMain" target="_self">
										<span>얼굴 추적</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>동영상 내에 있는 얼굴을 인식하여 추출하는 인공지능 엔진입니다. </dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_an">
								<dt>
									<a href="${pageContext.request.contextPath}/anomalyReverse/krAnomalyReverseMain" target="_self">
										<span>이상행동 감지</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>영상 내에 있는 이상행동을 감지해주며, 현재 모델은 공항 출국장의 역주행을 감지합니다. </dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_subrecog">
								<dt>
									<a href="${pageContext.request.contextPath}/subtitleRecog/krSubtitleRecogMain" target="_self">
										<span>이미지 자막 인식</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>이미지 내에 있는 자막을 인식하여 추출하는 인공지능 엔진입니다. </dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
					   	<li>
							<dl class="ico_dir">
								<dt>
									<a href="${pageContext.request.contextPath}/idr/krIdrMain" target="_self">
										<span>문서 이미지 분석</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>각 나라 별 화폐, 고지서 또는 병원 진료비 계산서·영수증의 이미지를 활용하여 정보를 분석해줍니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
					  </li>
					</ul>
				</div>
				<div class="item_set">
					<h4 class=""><em class="fas fa-file-alt"></em> 언어</h4>
					<ul class="item_lst bg_bGreen">
						<li>
							<dl class="ico_nlu">
								<dt>
									<a href="${pageContext.request.contextPath}/nlu/krNluMain" target="_self">
										<span>자연어 이해</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>문장을 입력하면 형태소 분석과 개체명 인식 결과를 제공해 줍니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_mrc">
								<dt>
									<a href="${pageContext.request.contextPath}/mrc/krMrcMain" target="_self">
										<span>AI 독해</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>주어진 텍스트를 독해하여 문맥을 이해하고, 질문에 맞는 정답의 위치를 찾아내서 정답을 제공합니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_xdc">
								<dt>
									<a href="${pageContext.request.contextPath}/xdc/krXdcMain" target="_self">
										<span>텍스트 분류</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>뉴스 기사를 입력하면 기사의 주제를 정확하게 분류해 냅니다. 더불어 분류의 근거를 문장 단위와 단어 단위로 제공하는 '설명 가능한 AI'입니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_nlg">
								<dt>
									<a href="${pageContext.request.contextPath}/gpt/krGptMain" target="_self">
										<span>문장 생성</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>문장을 입력하면, 언어 모델 기반의 새로운 문장을 생성합니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_hmd">
								<dt>
									<a href="${pageContext.request.contextPath}/hmd/krHmdMain" target="_self">
										<span>패턴 분류</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>정규 표현식으로 표현된 긍정, 부정 문장 패턴을 통해서 문장의 감정을 파악하고 분류합니다.</dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
						<li>
							<dl class="ico_itf">
								<dt>
									<a href="${pageContext.request.contextPath}/itf/krItfMain" target="_self">
										<span>의도 분류</span>
										<em class="fas fa-angle-right"></em>
									</a>
								</dt>
								<dd>입력된 질문의 의도를 파악하여 알려줍니다. </dd>
								<dd class="ytb_link"><a href="https://youtu.be/pj9_kLzUa2k" target="_blank" title="유튜브 데모영상 새창으로 이동"></a></dd>
							</dl>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
	<div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/main.js"></script>

<script type="text/javascript">   
$(window).load(function() {    
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() { 
		$(this).remove(); 
	});
}); 
</script>
<script type="text/javascript">
jQuery(function(){
	jQuery("a.btn_movLayer").movLayer();
});	
	
jQuery.event.add(window,"load",function(){
	$(document).ready(function (){
		$('.list1').trigger('click');
		$('.list2').trigger('click');
		$('.list3').trigger('click');

		$('.snb ul .sublist .twodepth').removeClass("active");

		$('.path').remove();

	});	
});	
	

</script>
	
