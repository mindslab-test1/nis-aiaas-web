﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">자연어 이해</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'nludemo')" id="defaultOpen"><button type="button">데모</button></li>
			<li class="tablinks" onclick="openTap(event, 'nluexample')"><button type="button">적용사례</button></li>
			<li class="tablinks" onclick="openTap(event, 'nlumenu')"><button type="button">매뉴얼</button></li>
<%--			<li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
		</ul>
		<!--.demobox_nlu-->
		<div class="demobox demobox_nlu" id="nludemo">
			<p><span>자연어 이해</span> <small>(NLU, Natural Language Understanding)</small></p>
			<span class="sub">사람의 언어를 컴퓨터가 이해할 수 있도록 바꿔줍니다.</span>

			<!--.nlu_box-->
			<div class="nlu_box">
				<div class="" style ="width: 328px; height: 40px; border-radius: 5px; border: 1px solid #dce2f2; margin: 0 auto 25px; line-height: 40px;">
					<span style="font-size:13px;margin-right:28px; ">언어선택</span>
					<div class="radio">
						<input type="radio" id="1" name="lang" checked="checked" value="kor">
						<label for="1"><span style=" color: #576068;">한국어</span></label>
					</div>
					<div class="radio">
						<input type="radio" id="2" name="lang" value="eng">
						<label for="2"><span style=" color: #576068;">English</span></label>
					</div>
				</div>


				<!--.step01-->
				<div class="step01">
					<div class="demo_top">
						<!--							<img src="${pageContext.request.contextPath}/aiaas/en/images/ico_xdc.png" alt="nlu text">-->
					</div>
					<div class="text_area">
						<textarea rows="7" placeholder="" id="text-contents-nlp">사막이 아름다운 건 어디엔가 우물이 숨어있기 때문이야</textarea>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_search disable" id="nlpbtn"><em class="fas fa-file-invoice"></em>분석하기</button>
					</div>
				</div>
				<!--//.step01-->

				<!--.step02-->
				<div class="step02">
					<ul class="radio_area">
						<li class="type_radio type_01"><input type="checkbox" name="syntax" id="part" value="part" checked /><label for="part">Part of Speech</label></li>
						<li class="type_radio type_02"><input type="checkbox" name="syntax" id="lemma" value="lemma" checked /><label for="lemma">Lemma</label></li>

						<!--
						<li class="type_radio type_03"><input type="checkbox" name="syntax" id="entity" value="entity" checked /><label for="entity">Named entity</label></li>
						-->
					</ul>
					<table>
						<tbody>
						<tr class="text_word">
							<%--<td>Google</td>
                            <td>,</td>
                            <td>headquartered</td>
                            <td>in</td>
                            <td>Mountain</td>
                            <td>View</td>
                            <td>,</td>
                            <td>unveiled</td>
                            <td>the</td>
                            <td>new</td>
                            <td>Android</td>
                            <td>phone</td>
                            <td>at</td>
                            <td>the</td>
                            <td>Consumer</td>
                            <td>Electronic</td>
                            <td>Show</td>
                            <td>.</td>--%>
						</tr>
						<tr class="result_text text_lemma">
							<%--<td></td>
                            <td></td>
                            <td>headquarter</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>unveil</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>--%>
						</tr>
						<tr class="result_text text_part">
							<%--td>NOUN</td>
										<td>PUNCT</td>
										<td>VERB</td>
										<td>ADP</td>
										<td>NOUN</td>
										<td>NOUN</td>
										<td>PUNCT</td>
										<td>VERB</td>
										<td>DET</td>
										<td>ADJ</td>
										<td>NOUN</td>
										<td>NOUN</td>
										<td>ADP</td>
										<td>DET</td>
										<td>NOUN</td>
										<td>NOUN</td>
										<td>NOUN</td>
										<td>PUNCT</td>--%>
						</tr>
						<tr class="result_text text_entity" hidden="hidden" style="...">
							<%--<td></td>
                            <td></td>
                            <td class="named_entity" colspan="3">PT_FRUIT</td>
                            <td></td>
                            <td></td>
                            <td class="named_entity" colspan="3">PT_FRUIT</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>--%>
						</tr>
						</tbody>
					</table>
					<%--<table>
                        <tbody>
                            <tr class="text_word">
                                &lt;%&ndash;<td>Google</td>
                                <td>,</td>
                                <td>headquartered</td>
                                <td>in</td>
                                <td>Mountain</td>
                                <td>View</td>
                                <td>,</td>
                                <td>unveiled</td>
                                <td>the</td>
                                <td>new</td>
                                <td>Android</td>
                                <td>phone</td>
                                <td>at</td>
                                <td>the</td>
                                <td>Consumer</td>
                                <td>Electronic</td>
                                <td>Show</td>
                                <td>.</td>&ndash;%&gt;
                            </tr>
                            <tr class="text_lemma">
                                &lt;%&ndash;<td></td>
                                <td></td>
                                <td>headquarter</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>unveil</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>&ndash;%&gt;
                            </tr>
                            <tr class="text_part">
                                &lt;%&ndash;<td>NOUN</td>
                                <td>PUNCT</td>
                                <td>VERB</td>
                                <td>ADP</td>
                                <td>NOUN</td>
                                <td>NOUN</td>
                                <td>PUNCT</td>
                                <td>VERB</td>
                                <td>DET</td>
                                <td>ADJ</td>
                                <td>NOUN</td>
                                <td>NOUN</td>
                                <td>ADP</td>
                                <td>DET</td>
                                <td>NOUN</td>
                                <td>NOUN</td>
                                <td>NOUN</td>
                                <td>PUNCT</td>&ndash;%&gt;
                            </tr>
                            <tr class="text_entity">
                                &lt;%&ndash;<td></td>
                                <td></td>
                                <td class="named_entity" colspan="2" >PT_FRUIT</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="named_entity" colspan="2">PT_FRUIT</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>&ndash;%&gt;
                            </tr>
                        </tbody>
                    </table>--%>
				</div>
				<!--//.step02-->
			</div>
			<!--//.nlu_box-->
		</div>
		<!-- .demobox_nlu -->

		<!--.nlumenu-->
		<div class="demobox" id="nlumenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API 공통 가이드
					</div>
					<p class="sub_title">개발 환경 세팅</p>
					<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
					<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

				</div>
				<div class="guide_group">
					<div class="title">
						NLU <small>(Natural Language Understanding)</small>
					</div>
					<p class="sub_txt"> 텍스트에서 의미있는 정보를 분석 및 추출하고 이해하는 기술로, 자연어를 활용한 모든 서비스에 활용할 수 있습니다.</p>
					<span class="sub_title">
							준비사항
						</span>
					<p class="sub_txt">① Input: 문자열 (텍스트) </p>
					<p class="sub_txt">② 아래 언어 중 택 1  </p>
					<ul>
						<li>한글 (kor)</li>
						<li>영어 (eng)</li>
					</ul>
					<span class="sub_title">
							 실행 가이드
						</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/nlu/</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>사용자의 고유 key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>lang</td>
							<td>사용할 모델의 언어 (kor / eng) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>reqText </td>
							<td>자연어 이해 처리할 문자열  </td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
						{<br>
						<span>"apiId": "(*ID 요청 필요)",</span><br>
						<span>"apiKey": "(*Key 요청 필요)",</span><br>
						<span>"lang": "eng",</span><br>
						<span>"reqText": "I'm having a bad day."</span><br>
						}
					</div>

					<p class="sub_txt">④ Response 파라미터 설명 </p>
					<span class="table_tit">Response</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>message </td>
							<td>API 동작여부  </td>
							<td>list</td>
						</tr>
						<tr>
							<td>document</td>
							<td>형태소 분석 결과 </td>
							<td>list</td>
						</tr>
					</table>
					<span class="table_tit">message: API 동작여부</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>message </td>
							<td>요청 처리 상태를 설명하는 문자열 (Success/ Fail)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>status</td>
							<td>요청 처리 상태에 대한 status code (0: Success)</td>
							<td>number</td>
						</tr>
					</table>
					<span class="table_tit">document: 형태소 분석 결과</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>category Weight</td>
							<td>Unused </td>
							<td>-</td>
						</tr>
						<tr>
							<td>sentences </td>
							<td>문장별 형태소 분석 결과</td>
							<td>array</td>
						</tr>
					</table>
					<span class="table_tit">sentences: 문장별 형태소 분석 결과</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>sequence</td>
							<td>분석한 문장 번호 (n) </td>
							<td>int</td>
						</tr>
						<tr>
							<td>text </td>
							<td>분석한 원문장</td>
							<td>string</td>
						</tr>
						<tr>
							<td>words </td>
							<td>어절 정보</td>
							<td>list</td>
						</tr>
						<tr>
							<td>morps  </td>
							<td>형태소분석 정보 </td>
							<td>list</td>
						</tr>
						<tr>
							<td>namedEntities  </td>
							<td>개체명 정보 </td>
							<td>list</td>
						</tr>
					</table>
					<span class="table_tit">words: 어절 정보</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>seq </td>
							<td>어절 ID </td>
							<td>int</td>
						</tr>
						<tr>
							<td>text </td>
							<td>어절</td>
							<td>string</td>
						</tr>
						<tr>
							<td>begin </td>
							<td>어절을 구성하는 첫 형태소의 ID</td>
							<td>int</td>
						</tr>
						<tr>
							<td>end</td>
							<td>어절을 구성하는 끝 형태소의 ID </td>
							<td>int</td>
						</tr>
						<tr>
							<td>beginSid </td>
							<td>Unused </td>
							<td>-</td>
						</tr>
						<tr>
							<td>endSid </td>
							<td>Unused</td>
							<td>-</td>
						</tr>
						<tr>
							<td>position </td>
							<td>어절 ID (영어만 사용)</td>
							<td>int</td>
						</tr>
					</table>
					<span class="table_tit">morps: 형태소 분석 정보</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>seq </td>
							<td>형태소 ID (0 부터 시작하여 문장에서의 출현 순서대로 값 부여)</td>
							<td>int</td>
						</tr>
						<tr>
							<td>lemma </td>
							<td>형태소 단어 </td>
							<td>string</td>
						</tr>
						<tr>
							<td>type </td>
							<td>형태소 태그</td>
							<td>string</td>
						</tr>
						<tr>
							<td>position</td>
							<td>문장에서의 byte position </td>
							<td>int</td>
						</tr>
						<tr>
							<td>wid  </td>
							<td>Unused </td>
							<td>-</td>
						</tr>
						<tr>
							<td>weight  </td>
							<td>형태소 분석 결과 신뢰도</td>
							<td>long</td>
						</tr>
					</table>
					<span class="table_tit">namedEntities: 개체명 정보</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>seq </td>
							<td>개체명의 ID (0 부터 시작하여 문장에서의 출현 순서대로 값 부여)</td>
							<td>int</td>
						</tr>
						<tr>
							<td>text  </td>
							<td>개체명 단어  </td>
							<td>string</td>
						</tr>
						<tr>
							<td>type </td>
							<td>개체명 타입 </td>
							<td>string</td>
						</tr>
						<tr>
							<td>begin </td>
							<td>개체명을 구성하는 첫 형태소의 ID </td>
							<td>int</td>
						</tr>
						<tr>
							<td>end </td>
							<td>개체명을 구성하는 끝 형태소의 ID </td>
							<td>int</td>
						</tr>
						<tr>
							<td>weight  </td>
							<td>개체명 분석 결과의 신뢰도 (0~1) </td>
							<td>long</td>
						</tr>
						<tr>
							<td>commonNoun </td>
							<td>고유명사일 경우 0, 일반명사일 경우 1  </td>
							<td>int</td>
						</tr>
						<tr>
							<td>beginSid  </td>
							<td>Unused </td>
							<td>-</td>
						</tr>
						<tr>
							<td>endSid </td>
							<td>Unused </td>
							<td>-</td>
						</tr>
					</table>
					<p class="sub_txt">⑤ Response 예제 </p>
					<div class="code_box">
						{ <br>
						<span> "message": {</span><br>
						<span class="twoD">"message": "Success",</span><br>
						<span class="twoD">"status": 0</span><br>
						<span> },</span><br>
						<span>"document": {</span><br>
						<span class="twoD">"categoryWeight": 0,	</span><br>
						<span class="twoD">"sentences": [</span><br>
						<span class="thirD">{</span><br>
						<span class="fourD">"sequence": 0,</span><br>
						<span class="fourD">"text": "I 'm having a bad day .",</span><br>
						<span class="fourD">"words": [</span><br>
						<span class="fivD">{</span><br>
						<span class="sixD">"seq": 0,</span><br>
						<span class="sixD">"text": "I",</span><br>
						<span class="sixD">"type": "PRP",</span><br>
						<span class="sixD">"begin": 0,</span><br>
						<span class="sixD">"end": 0,</span><br>
						<span class="sixD">"beginSid": 0,</span><br>
						<span class="sixD">"endSid": 0,</span><br>
						<span class="sixD">"position": 0</span><br>
						<span class="fivD">},</span><br>
						<span class="fivD">...</span><br>
						<span class="fivD">{</span><br>
						<span class="sixD">"seq": 6,</span><br>
						<span class="sixD">"text": ".",</span><br>
						<span class="sixD">"type": ".",</span><br>
						<span class="sixD">"begin": 0,</span><br>
						<span class="sixD">"end": 0,</span><br>
						<span class="sixD">"beginSid": 0,</span><br>
						<span class="sixD">"endSid": 0,</span><br>
						<span class="sixD">"position": 6</span><br>
						<span class="fivD">}</span><br>
						<span class="fourD">],</span><br>
						<span class="fourD">"morps": [</span><br>
						<span class="fivD">{</span><br>
						<span class="sixD">"seq": 0,</span><br>
						<span class="sixD">"lemma": "i",</span><br>
						<span class="sixD">"type": "PRP",</span><br>
						<span class="sixD">"position": 0,</span><br>
						<span class="sixD">"wid": 0,</span><br>
						<span class="sixD">"weight": 0</span><br>
						<span class="fivD">},</span><br>
						<span class="fivD">...</span><br>
						<span class="fivD">{</span><br>
						<span class="sixD">"seq": 6,</span><br>
						<span class="sixD">"lemma": ".",</span><br>
						<span class="sixD">"type": ".",</span><br>
						<span class="sixD">"position": 6,</span><br>
						<span class="sixD">"wid": 0,</span><br>
						<span class="sixD">"weight": 0</span><br>
						<span class="fivD">}</span><br>
						<span class="fourD">],</span><br>
						<span class="fourD">"namedEntities": [</span><br>
						<span class="fivD">{</span><br>
						<span class="sixD">"seq": 0,</span><br>
						<span class="sixD">"text": "a bad day",</span><br>
						<span class="sixD">"type": "DATE",</span><br>
						<span class="sixD">"begin": 3,</span><br>
						<span class="sixD">"end": 5,</span><br>
						<span class="sixD">"weight": 0,</span><br>
						<span class="sixD">"commonNoun": 0,</span><br>
						<span class="sixD">"beginSid": 0,</span><br>
						<span class="sixD">"endSid": 0</span><br>
						<span class="fivD">}</span><br>
						<span class="fourD">]</span><br>
						<span class="thirD">}</span><br>
						<span class="twoD">]</span><br>
						<span>}</span><br>
						}
					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--//nlumenu-->
		<!--.nluexample-->
		<div class="demobox" id="nluexample">
			<p><em style="color:#27c1c1;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

			<%--자연어 이해(NLU)--%>
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>대화 분석</span>
							</dt>
							<dd class="txt">챗봇이나 전화에서 진행된 다양한 형태의 대화를 분석할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_bqa"><span>NQA</span></li>
									<li class="ico_sds"><span>SDS</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>텍스트 분석</span>
							</dt>
							<dd class="txt">다양한 분야의 비정형 문서 및 텍스트를 분석할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
									<li class="ico_if"><span>ITF</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 03</em>
								<span>자동 분류</span>
							</dt>
							<dd class="txt">카테고리별로 텍스트를 자동으로 분류하고 분석할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
									<li class="ico_if"><span>ITF</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //자연어 이해(NLU) -->
		</div>

	</div>
</div>
<!-- //.contents -->


<script type="text/javascript">
	$(window).load(function() {
		$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
			$(this).remove();
		});
	});
</script>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/nlu.js"></script>

<script type="text/javascript">
	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){
			//nlu

			$('.nlu_box .step01 textarea').on('input keyup paste', function() {
				var txtValLth = $(this).val().length;

				if ( txtValLth > 0) {
					$('.nlu_box .step01 .btn_area button').removeClass('disable');
					$('.nlu_box .step01 .btn_area button').removeAttr('disabled');
					$('.nlu_box .step01 .btn_area .disBox').remove();
				} else {
					$('.nlu_box .step02').fadeOut();
				}
			});

            $('.nlu_box .step01 .btn_search').on('click',function(){
				var language = $("input[type=radio][name=lang]:checked").val();

                $.ajax({
                    type:'POST',
                    url:'${pageContext.request.contextPath}/nlu/nluApi',
                    data: {
                        "lang": language,
                        "reqText": $('#text-contents-nlp').val(),
                        "${_csrf.parameterName}" : "${_csrf.token}"
                    },
					timeout: 20000,
                    error: function(error){
                        console.log(error);
                    },
                    success: function(data){
                        var decodedData = decodeURIComponent(data);

                        console.log("result:"+decodedData);
                        $('.nlu_box .step02').fadeIn();

                        procResult(decodedData);
                    }
                });
            });

		});

	});

	/*
    ** 수신된 결과 메세지 처리
    */
	function procResult(data) {
		initUI();

		var language = $("input[type=radio][name=lang]:checked").val();
		if(language == "kor") procResult_Korean(data);
		else procResult_English(data);
	}

	function procResult_Korean(data) {

		var jsonObj = JSON.parse(data);
		var sentences = jsonObj.document.sentences;

		for(var zz = 0; zz < sentences.length; zz++) {
			var morphEvals = jsonObj.document.sentences[zz].morphEvals;

			for (var xx = 0; xx < morphEvals.length; xx++) {
				var part = "";
				var lemma = "";
				var target = morphEvals[xx].target;
				var results = morphEvals[xx].result.split("+");
				for (var yy = 0; yy < results.length; yy++) {
					var tokens = results[yy].split("/");
					lemma += (yy == 0 ? tokens[0] : "+" + tokens[0]);
					part += (yy == 0 ? tokens[1] : "+" + tokens[1]);
				}

				addResultUI(target, part, lemma);
			}
		}
	}

	function procResult_English(data) {

		var jsonObj = JSON.parse(data);
		var sentences = jsonObj.document.sentences;

		for(var zz = 0; zz < sentences.length; zz++) {
			var morps = jsonObj.document.sentences[zz].morps;
			var words = jsonObj.document.sentences[zz].words;

			for (var xx = 0; xx < morps.length; xx++) {
				var word = words[xx].text;
				var lemma = morps[xx].lemma;
				var part = morps[xx].type;

				addResultUI(word, part, lemma);
			}
		}
	}

	function addResultUI(target, part, lemma) {
		$('.text_word').append("<td>" + target + "</td>");
		$('.text_lemma').append("<td>" + lemma + "</td>");
		$('.text_part').append("<td>" + part + "</td>");
	}

	function initUI() {
		$('.text_word').empty();
		$('.text_lemma').empty();
		$('.text_part').empty();
	}

	$('#part').on('click',function(){
		var chk = $(this).is(":checked");
		if(chk) $(".text_part").fadeIn(300);
		else  $(".text_part").fadeOut(400);

	});

	$('#lemma').on('click',function(){
		var chk = $(this).is(":checked");
		if(chk) $(".text_lemma").fadeIn(300);
		else  $(".text_lemma").fadeOut(400);
	});
	
	$('input[type=radio][name=lang]').on('click',function(){
		var chk = $("input[type=radio][name=lang]:checked").val();
		if(chk == 'kor'){
			$('#text-contents-nlp').val('사막이 아름다운 건 어디엔가 우물이 숨어있기 때문이야');
		}else{
			$('#text-contents-nlp').val('The true beauty of the desert is the oasis that is hidden.');
		}
	});


	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();

</script>
