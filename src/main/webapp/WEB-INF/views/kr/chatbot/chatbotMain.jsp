﻿		<!-- .contents -->
		<div class="contents">
			<div class="content api_content">
				<h1 class="api_tit ">날씨봇·위키봇</h1>
				<ul class="menu_lst bot_lst">
					<li class="tablink" onclick="openTap(event, 'chatdemo')" id="default"><button type="button">데모</button></li>
					<li class="tablink" onclick="openTap(event, 'chatexample')"><button type="button">적용사례</button></li>
					<li class="tablink" onclick="openTap(event, 'chatmenu')"><button type="button">매뉴얼</button></li>
<%--					<li class="tablink"><a href="/member/krApiAccount" >API ID, key 발급</a></li>--%>
				</ul>
				<div class="demobox" id="chatdemo">
					<p><span style="color:#2cace5;">챗봇</span>, 필요한 곳에 바로 가져다 쓰세요!</p>
						<span class="sub">제공  가능: 날씨를 알려주는 날씨봇, 위키 백과 지식을 알려주는 위키봇 (다양한 주제의 봇 확장 중) </span>
					<div class="chatbot_tab">
					  	<button class="tablinks weather" onclick="openCity(event, 'weather')" id="defaultOpen">
							<img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_chabot.png" alt="weatherbot">
							<span>날씨봇</span>
						</button>
					  	<button class="tablinks wiki" onclick="openCity(event, 'wiki')">
							<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_wiki.png" alt="wikibot">
							<span>위키봇</span>
						</button> 
					</div>
					
					<!-- chatbot_box -->
					<div class="chatbot_box" id="weather">						
						<!-- .chat_mid -->
						<div class="chat_mid">
							<div class="bot_infoBox">
								<div class="thumb"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_chabot.png" alt="chatbot"></div>
								<div class="txt">잠깐!! 우산 챙기셨나요?  <br>
오늘의 날씨가 궁금하시면 저에게 물어보세요. </div>
								<ul class="info_btnBox">
									<li><button type="button">오늘 날씨 알려줘</button></li>
									<li><button type="button">오늘 성남 날씨는?</button></li>
									<li><button type="button">내일 비 와?</button></li>
								</ul>
							</div>
							<ul class="talkLst">								
								<li class="newDate">
									<span><!-- 날짜는 스크립트가 정의--></span>
								</li> 
							</ul>
						</div>
						<!-- //.chat_mid -->
						<!-- .chat_btm -->
						<div class="chat_btm">
							<form method="post" action="" id="formChat" name="formChat">
								<textarea class="textArea" placeholder="메세지를 입력해 주세요"></textarea>
								<input type="button" name="btn_chat" id="btn_chat" class="btn_chat" title="전송" value="전송">
							</form>
						</div>
						<!-- //.chat_btm -->
						<div class="remark">발표: 기상청 &nbsp;&nbsp;&nbsp;제공: 공공데이터포털</div>
					</div>
					<!-- //.chatbot_box -->
					
					
					<!-- chatbot_box -->
					<div class="chatbot_box" id="wiki">						
						<!-- .chat_mid -->
						<div class="chat_mid">
							<div class="bot_infoBox">
								<div class="thumb"><img src="${pageContext.request.contextPath}/aiaas/kr/images/img_wiki.png" alt="chatbot"></div>
								<div class="txt">지식을 모두 섭렵한 챗봇 계의 석학, 위키봇 입니다.<br>
제 상식을 한번 테스트해 보시겠어요?</div>
								<ul class="info_btnBox">
									<li><button type="button">프랑스 혁명은 언제 일어났어?</button></li>
									<li><button type="button">김연아 선수는 키가 얼마나 크지?</button></li>
									<li><button type="button">데미안의 저자는 누구야? </button></li>
								</ul>
							</div>
							<ul class="talkLst">	
								<li class="newDate">
									<span><!-- 날짜는 스크립트가 정의--></span>
								</li> 
							</ul>
						</div>
						<!-- //.chat_mid -->
						<!-- .chat_btm -->
						<div class="chat_btm">
							<form method="post" action="" id="formChat2" name="formChat2">
								<textarea class="textArea" placeholder="메세지를 입력해 주세요"></textarea>
								<input type="button" name="btn_chat2" id="btn_chat2" class="btn_chat" title="전송" value="전송">
							</form>
						</div>
						<!-- //.chat_btm -->
						<div class="remark">발표: 기상청 &nbsp;&nbsp;&nbsp;제공: 공공데이터포털</div>
					</div>
					<!-- //.chatbot_box -->


					
				</div>
				<!-- .demobox -->




				<!--.chatmenu-->
				<div class="demobox bot_menu" id="chatmenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API 공통 가이드
							</div>
							<p class="sub_title">개발 환경 세팅</p>
							<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
							<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

						</div>
						<div class="guide_group">
							<div class="title">
								IRQA (Information Retrieval Question Answering)
							</div>
							<p class="sub_txt">IR QA는 다양한 분야의 지식 소통이 가능한 인공지능 소프트웨어입니다.</p>

						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--//chatmenu-->
				<!--.chatexample-->
				<div class="demobox" id="chatexample">
					<p><em style="color:#2cace5;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

					<!-- 날씨봇n위키봇 -->
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>인공지능 비서</span>
									</dt>
									<dd class="txt">날씨를 알려주는 챗봇을 바로 갖다 붙이면, 인공지능 비서가 날씨를 알려줍니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_weatherBot"><span>날씨봇</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>AI Tutor</span>
									</dt>
									<dd class="txt">다양한 지식 정보를 대화 형식으로 제공받을 수 있는 현존하는 가장 간편한 지식 소통 인공지능 소프트웨어로, 다양한 분야의 콘텐츠에 접목될 수 있습니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_wikiBot"><span>위키봇</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 03</em>
										<span>장학 퀴즈</span>
									</dt>
									<dd class="txt">1,300만 건 이상의 비정형 지식베이스와 3,800만 건의 학습 데이터를 통해 정확도 높은 답변을 제공합니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_wikiBot"><span>위키봇</span></li>
										</ul>
									</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //날씨봇n위키봇 -->
				</div>
			</div>

		</div>
		<!-- //.contents -->

<input type="hidden" id="AUTH_ID">
<input type="hidden" id="SESSION_ID">
<input type="hidden" id="thumb">

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/common.js"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/chatbot/app.js?v=20190902"></script>
<script type="text/javascript">   

chatSingIn();
function chatSingIn() {
    $.ajax({
        url: 'https://aicc-prd1.maum.ai:9980/api/v3/auth/signIn',
        async: false,
        type: 'POST',
        headers: {
            "Content-Type": "application/json",
            "m2u-auth-internal": "m2u-auth-internal"
        },
        data: JSON.stringify({
            "userKey": "admin",
            "passphrase": "1234"
        }),
        dataType: 'json',
        success: function(data) {
            // console.log(data.directive.payload.authSuccess.authToken);

            document.getElementById('AUTH_ID').value = data.directive.payload.authSuccess.authToken;
            // console.log(document.getElementById('AUTH_ID').value);
            console.log('login success!')
           
        }, error: function(err) {
            console.log("chatbot Login error! ", err);
        }
    });
}
	
	
//	챗봇 탭 메뉴
	function openCity(evt, cityName) {
		var i, tabcontent, tablinks;
		chatbot_box = document.getElementsByClassName("chatbot_box");
		for (i = 0; i < chatbot_box.length; i++) {
			chatbot_box[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className
					.replace(" active", "");
		}
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
		chatNm = cityName;

		if($('#'+chatNm+' .chat_mid .talkLst li').length===1){
			$('#'+chatNm+' .bot_infoBox').css({
				'display':'block'
			});
			$('#'+chatNm+' .talkLst').css({
				'display':'none'
			});
		}
		if(cityName==='weather'){
			document.getElementById('thumb').value='${pageContext.request.contextPath}/aiaas/kr/images/ico_chabot.png';
			 chatOpen("Weather");
		} else {
			document.getElementById('thumb').value='${pageContext.request.contextPath}/aiaas/kr/images/img_wiki.png';
			chatOpen("Noah");
		}
	}

	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();


//API 탭
function openTap(evt, menu) {
	var i, demobox, tablinks;
	demobox = document.getElementsByClassName("demobox");
	for (i = 0; i < demobox.length; i++) {
		demobox[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablink");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(menu).style.display = "block";
	evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("default").click();

</script>


