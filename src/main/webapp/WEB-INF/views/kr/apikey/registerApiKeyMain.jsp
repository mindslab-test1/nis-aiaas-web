<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ include file="../common/common_header.jsp" %>

<div class="contents">
    <!-- .api_content -->
    <div class="content ai_content">

        <div style="padding-left:64px;padding-top:64px;">

            <div style="height:64px;font-size:24px; font-weight: bolder;">
                API ID/Key 발급
            </div>

            <!--.ttsdemo-->
            <div class="demobox ttsdemo" id="ttsdemo" >
                <div style="height:32px;">
                    <input type="text" id="name" readonly="readonly" value="이름 : ${user.name}" style="background-color:transparent;border:0 solid black; width:300px;">
                </div>
                <div style="height:32px;">
                    <input type="text" id="phone" readonly="readonly" value="전화번호 : ${user.phone}" style="background-color:transparent;border:0 solid black; width:300px;">
                </div>
                <div style="height:32px;">
                    <input type="text" id="company" readonly="readonly" value="회사 : ${user.company}" style="background-color:transparent;border:0 solid black; width:300px;">
                </div>
                <div style="height:32px;">
                    <input type="text" id="email" readonly="readonly" value="메일 : ${user.email}" style="background-color:transparent;border:0 solid black; width:300px;">
                </div>
                <div style="height:32px;margin-top:24px;">
                    <input type="text" id="apiId" name="apiId" style="width:300px;" placeholder="API ID를 입력하세요.">
                    <button type="button" id="btn_api" name="btn_api"> 발급 요청 </button>
                </div>
                <div style="height:32px;">
                    <input type="text" id="apiKey" name="apiKey" readonly="readonly" style="background-color:transparent;border:0 solid black; width:300px;">
                </div>
            </div>

        </div>

    </div>
</div>



<script type="text/javascript">
    $(document).ready(function(){
        var userEmail;
        var apiId;
        var apiKey;

        $('#btn_api').on('click', function(){
            apiId = document.getElementById("apiId").value;
            apiId = apiId.trim();
            if(apiId.length == 0) {
                alert("API ID를 입력해 주세요.");
                return;
            }

            var param = {
                '${_csrf.parameterName}' : '${_csrf.token}',
                'apiId' : apiId,
                'email' : "${user.email}",
                'name' : "${user.name}"
            }

            $.ajax({
               url : '${pageContext.request.contextPath}/admin/apikey/register',
               async : false,
               type : 'POST',
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                data: param,
                error: function(request, status, error){
                    alert("서버 연동 오류");
                },
                success: function(data){
                   console.log("서버에 연결")
                   console.log(data);
                   var deserialize = JSON.parse(data);

                   if(deserialize.message.toString().match("Success")){
                       apiId = deserialize.id;
                       apiKey = deserialize.key;
                       inputApiInfo(true, apiKey);
                   }else{
                       inputApiInfo(false, deserialize.message);
                   }
                }
            });
        });
    });

    function inputApiInfo(success, message){
        if(success == false){
            $('#apiKey').val(message);
            var apiKeyText = document.getElementById("apiKey");
            apiKeyText.style.color = "red";
        }else{
            $('#apiKey').val("apiKey : "+message);
            document.getElementById('test').style.visibility = 'hidden';
        }
    }
</script>