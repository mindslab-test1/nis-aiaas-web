<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/avr/croppie.css" />


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/avr/avr.js?ver=20200202"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/avr/croppie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/avr/filesaver.js"></script>
<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">


<!--사용자 가이드 -->
<div class="pop_simple" id="user_guide">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap">
		<div class="pop_hd">
			도로상의 객체 인식 모듈 사용자 가이드
			<em class="fas fa-times close_layer"></em>
		</div>
		<!-- .pop_bd -->
		<div class="pop_bd">
			<div class="pop_body">
				<h5>1. 테스트 이미지 환경</h5>
				<p>번호판 검출 및 인식 모듈은 도로 위 달리고 있는 차량을 찍고있는 CCTV 환경에 맞게 학습 및 특화되어 있으므로,<br>
					기본 제공되는 테스트 이미지와 같이 촬영된 이미지가 가장 인식율이높습니다.<br>
					아래 조건을 확인하여 테스트하시길 권고드립니다.<br><br>
					- 최소한 차량 전체가 잘리지 않고 온전해야 하며, 도로 위에 존재해야 합니다.<br>
					- 이미지가 잘리지 않도록 전체를 넣어주는 것이 좋습니다.<br>
					- 만약 차량만 crop된 이미지라면 줌 아웃하는 것이 적절합니다<br>
					- 차량이 너무 정면이거나 가까운 이미지는 부적절합니다.<br>
				</p>
				<img class="img1" src="${pageContext.request.contextPath}/aiaas/common/images/img_smartX01.png" alt="도로 차량 이미지">
				<h5>2. 차량 번호판 검출 및 인식</h5>
				<p>위 번호판 인식 모델은 실제 도로 위 CCTV 상황에 적용되어 있으며, 아래와 같은 사항을주의하여 이용해주시길 바랍니다.<br>
					- 인식에 해당되는 번호판은 아래 5가지 번호판입니다. (외교나 건설 등 특수 번호판은제외)<br>
					- 대한민국 번호판 규칙에 따른 번호판만 해당됩니다. 즉, 임의의 가짜 번호판은부적절합니다.<br>
					- 폰트 및 간격의 임의적인 변화에 따라 인식이 어려워지게 됩니다.<br>
				</p>
				<figure>
					<img class="img2" src="${pageContext.request.contextPath}/aiaas/common/images/img_smartX02.png" alt="인식 가능한 5가지 번호판 예시">
					<figcaption>&lt;인식 가능한 5가지 번호판 예시&gt;</figcaption>
				</figure>
				<figure>
					<img class="img3" src="${pageContext.request.contextPath}/aiaas/common/images/img_smartX03.png" alt="대한민국 번호판 규칙">
					<figcaption>&lt;대한민국 번호판 규칙&gt;</figcaption>
				</figure>

				<img class="img4" src="${pageContext.request.contextPath}/aiaas/common/images/img_smartX04.png" alt="대한민국 번호판 규칙">

				<figure>
					<img class="img5" src="${pageContext.request.contextPath}/aiaas/common/images/img_smartX05.png" alt="잘못된 번호판의 예">
					<figcaption>&lt;잘못된 번호판의 예&gt;</figcaption>
				</figure>

			</div>

		</div>
		<!-- //.pop_bd -->

	</div>
	<!-- //.popWrap -->
</div>
<!-- // \user_guide -->

<!-- .contents -->
<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">도로상의 객체 인식</h1>
		<ul class="menu_lst vision_lst">
			<li class="tablinks" onclick="openTap(event, 'avrdemo')" id="defaultOpen"><button type="button">데모</button></li>
			<li class="tablinks" onclick="openTap(event, 'avrexample')"><button type="button">적용사례</button></li>
			<li class="tablinks" onclick="openTap(event, 'avrmenu')"><button type="button">매뉴얼</button></li>
<%--			<li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
		</ul>
		<div class="demobox" id="avrdemo">
			<p>AVR <small>(AI Vehicle Recognition)</small> 을 이용한 <span>도로상의 객체 인식</span> </p>
			<span class="sub">차량 이미지에서 번호판을 검출하는 엔진입니다.</span>
			<!--textremoval_box-->
			<div class="demo_layout avr_box">
				<!--avr_1-->
				<div class="avr_1">
					<div class="fl_box">
						<p><em class="far fa-file-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
						<div class="sample_box">
							<div class="sample_1">
								<div class="radio">
									<input type="radio" id="sample1" name="option" value="샘플1" checked>
									<label for="sample1" class="female">
										<div class="img_area">
											<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_avr.png" alt="sample img 1" />
										</div>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="fr_box">
						<p><em class="far fa-file-image"></em><strong>내 파일</strong>로 해보기</p>
						<div class="uplode_box">
							<div class="btn" id="uploadFile">
								<em class="fas fa-times hidden close"></em>
								<em class="far fa-file-image hidden"></em>
								<label for="demoFile" class="demolabel">이미지 업로드</label>
								<input type="file" id="demoFile" class="demoFile" accept=".jpg, .png" >
							</div>
							<div class="user_guide"><em class="fas fa-file-alt"></em>&nbsp;&nbsp;사용자 가이드 보기&nbsp;&nbsp;<em class="fas fa-chevron-right"></em></div>
							<ul>
								<li>* 지원가능 파일 확장자: .jpg, .png</li>
								<li>* 이미지 용량 2MB 이하만 가능합니다.</li>
							</ul>
						</div>
					</div>
					<div class="bottom_box">
						<p><em class="fas fa-expand"></em><strong>검출 조건 선택</strong></p>
						<div class="range_box">
							<div class="radio">
								<input type="radio" id="radio3" name="avr_option" value='plate' checked="checked">
								<label for="radio3">차량 번호판 검출 및 인식</label>
							</div>
						</div>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_start" id="change_txt">이미지 편집</button>
					</div>
				</div>
				<!--avr_1-->
				<!--edit_box-->
				<div class="edit_box pattern_2">
					<p><em class="far fa-file-image"></em>이미지 편집</p>
					<p class="desc">이미지를 '여백 없이' 맞춰주세요.</p>
					<div class="img_box">
						<em class="fas fa-minus minus"></em>
						<em class="fas fa-plus plus"></em>
						<!--불러온 이미지 들어갈 곳-->
						<img src="" alt="불러온 이미지" id="previewImg">
					</div>
					<%--<p class="desc">사진의 하단 바를 조절하여 크기를, 마우스를 움직여서 상하좌우 위치를 '여백없이' 편집해주세요.</p>--%>
					<div class="btn_area">
						<a class="btn_start btn_cancel" id="cancelCrop_avr">취소</a>
						<a class="btn_start" id="recogButton">인식하기</a>
					</div>
				</div>
				<!--//edit_box-->
				<!--avr_2-->
				<div class="avr_2">
					<p><em class="far fa-file-image"></em>객체 검출 및 이미지 처리중</p>
					<div class="loding_box ">
						<!-- ball-scale-rotate -->
						<div class="lds">
							<div class="ball-scale-rotate">
								<span> </span>
								<span> </span>
								<span> </span>
							</div>
						</div>
						<p>약간의 시간이 소요 됩니다. (약 3초 내외)</p>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
					</div>
				</div>
				<!--avr_2-->
				<!--avr_3-->
				<div class="avr_3" >
					<p><em class="far fa-file-image"></em>결과 파일</p>
					<div class="result_file" >
						<div class="imgBox">
							<img src="" id="resultImg" alt="결과 이미지" />
						</div>
						<%--<div class="recogbox" id="testRecog" style="display:block;">--%>	<!-- 차량 번호판 인식일 경우에만 display : blcok  -->
						<div class="recogbox" id="recogbox" style="display:none">
							<span>검출 및 인식 결과</span>
							<div class="carnumber"><img src="" id="carnumber" alt="번호판 이미지"/></div>
							<div class="carnumber_txt">33호 5598</div>
							<div class="errortxt"><em class="fas fa-exclamation-triangle"></em>&nbsp; 사진의 크기와 위치를 다시 맞춰주세요. </div>
						</div>
					</div>
					<%--<a id="save" onclick="downloadResultImg();" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> 결과 파일 다운로드</a>--%>
					<div class="btn_area">
						<button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
					</div>
				</div>
				<!--avr_3-->
			</div>
			<!--// textremoval_box-->
		</div>
		<!-- .demobox -->

		<!--.avrmenu-->
		<div class="demobox vision_menu" id="avrmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API 공통 가이드
					</div>
					<p class="sub_title">개발 환경 세팅</p>
					<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
					<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

				</div>
				<div class="guide_group">
					<div class="title">
						AVR <small>(AI Vehicle Recognition)</small>
					</div>
					<p class="sub_txt">도로상의 객체 (차량) 를 인식하여 특정 위치를 검출하여 나타내줍니다.</p>
					<span class="sub_title">
								준비사항
					</span>
					<p class="sub_txt">- Input: 차량 이미지 파일 </p>
					<ul>
						<li>확장자: .jpg, .png.</li>
						<li>용량: 2MB 이하 </li>
					</ul>
					<span class="sub_title">
								 실행 가이드
							</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/avr</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId </td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>사용자의 고유 key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>file</td>
							<td>type:file (.jpg,.png) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>option</td>
							<td>검출할 대상 (plate)</td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
						curl -X POST \<br>
						https://api.maum.ai/api/avr \<br>
						-H 'Accept: */*' \<br>
						-H 'Accept-Encoding: gzip, deflate' \<br>
						-H 'Cache-Control: no-cache' \<br>
						-H 'Connection: keep-alive' \<br>
						-H 'Host: api.maum.ai' \<br>
						-H 'content-type: multipart/form-data; boundary=----<br>
						WebKitFormBoundary7MA4YWxkTrZu0gW' \<br>
						-F apiId={발급 받은 API ID} \<br>
						-F apiKey={발급 받은 API Key} \<br>
						-F file=@{file 경로}<br>
						-F option={상단 참조}
					</div>

					<p class="sub_txt">④ Response 예제 </p>

					<div class="code_box">
						<img style="width:50%;" src="${pageContext.request.contextPath}/aiaas/common/images/img_avr_guide.png" alt="도로상의 객체 인식 결과 예제">
					</div>
				</div>
			</div>

		</div>
		<!--//avrmenu-->
		<!--.avrexample-->
		<div class="demobox" id="avrexample">
			<p><em style="color:#f7778a;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

			<!--도로상의 객체 인식(AVR) -->
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>CCTV 이상검출</span>
							</dt>
							<dd class="txt">비디오 인식을 통해 실시간으로 특정 물체 또는 사람을 인지하여 모니터링할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_avr"><span>AVR</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>노후 경유차량 <strong>인식</strong></span>
							</dt>
							<dd class="txt">차량 번호판 인식을 통해 노후 경유차량을 감지하고 벌금을 부과할 수 있는 자동 모니터링 시스템을 구축할 수 있습니다.
								<span><em class="fas fa-book-reader"></em> Reference: 서울시</span>
							</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_avr"><span>AVR</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 03</em>
								<span>동영상 이미지 <strong>비식별화</strong></span>
							</dt>
							<dd class="txt">과속 차량 적발 시 촬영되는 얼굴 이미지를 자동으로 비식별화해서 개인 정보를 보호합니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_avr"><span>AVR</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //도로상의 객체 인식(AVR) -->
		</div>
		<!--//.avrexample-->

	</div>
</div>
<!-- //.contents -->
<script>

var sample1SrcUrl;
var data;

let image1_blob;

//파일명 변경
document.querySelector("#demoFile").addEventListener('change', function (ev) {
	document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
	var element = document.getElementById( 'uploadFile' );
	element.classList.remove( 'btn' );
	element.classList.add( 'btn_change' );
	$('.fl_box').css("opacity", "0.5");
});

jQuery.event.add(window,"load",function(){
	$(document).ready(function (){
		loadSample1();
		$('.carnumber_txt').text("");
		// step1->step2  (이미지 선택)
		$('.btn').on('click', function () {
//			$('.fl_box').css("opacity", "0.5");
			$('#change_txt').text('이미지 편집');

		});
		// step1->step2  (close button)
		$('em.close').on('click', function () {
			console.log(3);
			$(this).parent().removeClass("btn_change");
			$(this).parent().addClass("btn");
			$(this).parent().children('.demolabel').text('이미지 업로드');
			$('.fl_box').css('opacity', '1');
			//파일명 변경
			document.querySelector("#demoFile").addEventListener('change', function (ev) {
				document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
				var element = document.getElementById( 'uploadFile' );
				element.classList.remove( 'btn' );
				element.classList.add( 'btn_change' );
			});
		});

		// step1->step2
		$('#change_txt').on('click', function () {
			var $checked = $('input[name="avr_option"]:checked');
			var uploadfile = $('#demoFile');
			var demoFileTxt = uploadfile.val();

			if (demoFileTxt === "") {
				loadSample1();

				if( $checked.val() === "plate")
					console.log("plate 샘플로");

				activateCroppie_sample();
			} else {

				console.log("이미지 업로드");
				var url = URL.createObjectURL(uploadfile.get(0).files[0]);

				$('#previewImg').attr('src', url);

				image1_blob = uploadfile.get(0).files[0];

				activateCroppie_upload();
			}
			$('.avr_1').hide();
			$('.edit_box').fadeIn(300);
		});

		// step2->step3
		$('.btn_back1').on('click', function () {
			document.getElementById('recogbox').style.display = "block";
			window.location.reload();
		});

		// step3->step1
		$('.btn_back2').on('click', function () {
			window.location.reload();
		});

		//사용자 가이드 보기
		$('.user_guide').on('click', function(){
			$('#user_guide').fadeIn();
		});
		$('.close_layer').on('click', function(){
			$('#user_guide').fadeOut();
		});

		$('#recogButton').on('click', function (){

			$cropper.croppie('result', {
				type: 'blob',
				size: 'original'
			}).then(function (blob) {

				var url = blob;
				console.log(url);
				$('#resultImg').attr('src', url);

				var avr_option = document.querySelector('input[name="avr_option"]:checked').value;
				var file = blob;

				$('.edit_box').hide();
				$('.avr_2').fadeIn(300);

				console.log('avr',avr_option);

				if(avr_option == 'plate') {
					sendMultiRequest(avr_option, file, "${pageContext.request.contextPath}/api/avr/multiAvr");
				}
			});
		});
	});
});


function sendMultiRequest(avr_option, file, url){
	var formData = new FormData();
	formData.append('file', file);
	formData.append('option', avr_option);
	formData.append($("#key").val(), $("#value").val());

	$.ajax({
		type: "POST",
		async: true,
		url: url,
		data: formData,
		processData: false,
		contentType: false,
		timeout: 20000,
		success: function(result){

			document.getElementById('recogbox').style.display = "block";

			let resultData = JSON.parse(result);
			let PltImg = resultData.plt_img;
			let BoxedPltImg = resultData.plt_boxed_img;
			let pltNum = resultData.plt_num;
			console.log(pltNum);

			$('.avr_2').hide();
			$('.avr_3').fadeIn(300);
			if (PltImg == ""){
				console.log(PltImg);
				$('.carnumber').hide();
				$('.carnumber_txt').hide();
				$('#save').hide();
				$(".errortxt").fadeIn();
			}else if (pltNum == "" || pltNum.length < 7){
				$('.carnumber').hide();
				$('.carnumber_txt').hide();
				$('#save').hide();
				$(".errortxt").fadeIn();
			}
			else{
				$('#resultImg').attr('src', "data:image/jpeg;base64," + BoxedPltImg);
				$('#carnumber').attr('src', "data:image/jpeg;base64," + PltImg);
				$('.carnumber_txt').text(pltNum);
			}

		},
		error: function(error){
			alert("Error");
			console.dir(error);
			window.location.reload();
		}
	});
}

// 크롭 취소
$('#cancelCrop_avr').on('click', function(){
	$('input[id="opt1"]').trigger("click");
	$('.avr_1').css('display', 'block');
	$('.edit_box').css('display', 'none');
	$('.pattern_2').hide();
	$('#previewImg').removeAttr('src');

	reset();
});

function reset(){
	$cropper.croppie('destroy');
	$cropper = null;
	$('.pattern_1').fadeIn(300);
	$('#demoFile').val('');
}

function loadSample1() {
	var blob = null;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/images/img_avr.png");
	xhr.onloadstart = function(ev) {
		xhr.responseType = "blob";
	}
	xhr.onload = function()
	{
		blob = xhr.response;//xhr.response is now a blob object
		//sampleImage1 = new File([blob], "img_avr.jpg");

		sample1SrcUrl = URL.createObjectURL(blob);
		var avr_preview=document.getElementById('previewImg');
		avr_preview.setAttribute("src",sample1SrcUrl);

		image1_blob = blob;
	};

	xhr.send();
}

/*function downloadResultImg(){
	var img = document.getElementById('resultImg');
	var link = document.getElementById("save");
	link.href = img.src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
	link.download = "AVR_IMAGE.JPG";
}*/

//API 탭
function openTap(evt, menu) {
	var i, demobox, tablinks;
	demobox = document.getElementsByClassName("demobox");
	for (i = 0; i < demobox.length; i++) {
		demobox[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(menu).style.display = "block";
	evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();

</script>