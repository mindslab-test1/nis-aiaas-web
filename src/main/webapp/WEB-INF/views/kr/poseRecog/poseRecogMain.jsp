<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/download.js"></script>

		<!-- .contents -->
		<div class="contents api_content">
			<!-- .content -->
			<div class="content">
				<h1 class="api_tit">텍스트 제거</h1>
				<ul class="menu_lst vision_lst">
					<li class="tablinks" onclick="openTap(event, 'iprdemo')" id="defaultOpen"><button type="button">데모</button></li>
					<li class="tablinks" onclick="openTap(event, 'iprexample')"><button type="button">적용사례</button></li>
					<li class="tablinks" onclick="openTap(event, 'iprmenu')"><button type="button">매뉴얼</button></li>
<%--					<li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
				</ul>
				<!-- .demobox -->
				<div class="demobox" id="iprdemo">
					<p><span>인물 포즈 인식 </span><small>(Recognition of Poses)</small></p>
					<span class="sub">이미지 내에 있는 사람의 포즈를 인식하여 추출하고 시각화하는 엔진입니다. </span>
					<!--textremoval_box-->
					<div class="demo_layout poserecog_box">
						<!--tr_1-->
						<div class="tr_1">
							<div class="fl_box">
								<p><em class="far fa-file-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
								<div class="sample_box">
									<div class="sample_1">
										<div class="radio">
											<input type="radio" id="sample1" name="option" value="1" checked>
											<label for="sample1" class="female">
												<div class="img_area">
													<img src="${pageContext.request.contextPath}/aiaas/common/images/pose01.jpg" alt="sample img 1" />
												</div>
											</label>
										</div>
									</div>
									<div class="sample_2">
										<div class="radio">
											<input type="radio" id="sample2" name="option" value="2" >
											<label for="sample2"  class="male">
												<div class="img_area">
													<img src="${pageContext.request.contextPath}/aiaas/common/images/pose02_.png" alt="sample img 2" />
												</div>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="fr_box">
								<p><em class="far fa-file-image"></em><strong>내 파일</strong>로 해보기</p>
								<div class="uplode_box">
									<div class="btn" id="uploadFile">
										<em class="fas fa-times hidden close"></em>
										<em class="far fa-file-image hidden"></em>
										<label for="demoFile" class="demolabel">이미지 업로드</label>
										<input type="file" id="demoFile" class="demoFile" accept=".jpg, .png">
									</div>
									<ul>
										<li>* 지원가능 파일 확장자: .jpg, .png</li>
										<li>* 이미지 용량 2MB 이하만 가능합니다.</li>
										<li>* 사람의 전신 사진이 100 pixel 이상인 이미지를 사용해 주세요.</li>
										<li>* 이미지 해상도가 높을수록 인식의 정확도가 높습니다.</li>
									</ul>
								</div>
							</div>
							<div class="bottom_box" style="display:none">
								<p><em class="fas fa-sort-amount-up"></em><strong>지우기 강도 선택</strong></p>
								<div class="range_box">
									<div class="range_num"><span>1</span><span>2</span><span>3</span><span>4</span><span class="lstnum">5</span></div>
									<div class="slidecontainer">
										<input type="range" min="1" max="5" value="2" step="1" class="slider" id="myRange">
									</div>
								</div>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_start" id="sub">결과보기</button>
							</div>
						</div>
						<!--tr_1-->
						<!--tr_2-->
						<div class="tr_2">
							<p><em class="far fa-file-image"></em>객체 검출 및 이미지 처리중</p>
							<div class="loding_box ">
								<!-- ball-scale-rotate -->
								<div class="lds">
									<div class="ball-scale-rotate">
										<span> </span>
										<span> </span>
										<span> </span>
									</div>
								</div>
								<p>약간의 시간이 소요 됩니다.</p>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
							</div>

						</div>
						<!--tr_2-->
						<!--tr_3-->
						<div class="tr_3" >
							<div class="origin_file">
								<p><em class="far fa-file-image"></em>입력 파일</p>
								<div class="imgBox">
									<img id="input_img" src="" alt="원본 이미지" />
								</div>
							</div>
							<div class="result_file" >
								<p><em class="far fa-file-image"></em>결과 파일</p>
								<div class="imgBox">
									<img id="output_img" src="" alt="결과 이미지" />
								</div>
								<a id="save" onclick="downloadResultImg();" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> 결과 파일 다운로드</a>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
							</div>
						</div>
						<!--tr_3-->
					</div>
					<!--//.poserecog_box-->

				</div>
				<!-- //.demobox -->
				<!--.iprmenu-->
				<div class="demobox vision_menu" id="iprmenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API 공통 가이드
							</div>
							<p class="sub_title">개발 환경 세팅</p>
							<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
							<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

						</div>
						<div class="guide_group">
							<div class="title">
								인물 포즈 인식 <small></small>
							</div>
							<p class="sub_txt">이미지 중 인물의 포즈를 인식하여 스켈레톤 형식으로 추출해줍니다. </p>
							<span class="sub_title">
								준비사항
					</span>
							<p class="sub_txt">- Input: 이미지 파일 </p>
							<ul>
								<li>확장자: .jpg, .png.</li>
								<li>용량: 2MB 이하 </li>
							</ul>
							<span class="sub_title">
								 실행 가이드
							</span>
							<p class="sub_txt">① Request  </p>
							<ul>
								<li>Method : POST</li>
								<li>URL : https://api.maum.ai/smartXLoad/poseExtract</li>
							</ul>
							<p class="sub_txt">② Request 파라미터 설명 </p>
							<table>
								<tr>
									<th>키</th>
									<th>설명</th>
									<th>type</th>
								</tr>
								<tr>
									<td>apiId </td>
									<td>사용자의 고유 ID. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>apiKey </td>
									<td>사용자의 고유 key. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>pose_img</td>
									<td>type:file (.jpg, .png) 이미지 파일 </td>
									<td>string</td>
								</tr>
							</table>
							<p class="sub_txt">③ Request 예제 </p>
							<div class="code_box">
								curl --location --request POST 'http://api.maum.ai/smartXLoad/poseExtract' \<br>
								--header 'Content-Type: multipart/form-data' \<br>
								--form 'apiId= 발급받은 API ID' \<br>
								--form 'apiKey= 발급받은 API KEY' \<br>
								--form 'pose_img= 포즈 이미지 파일'<br>
							</div>

							<p class="sub_txt">④ Response 예제 </p>

							<div class="code_box">
<pre>
{
    "result_img": "iVBORw0KGgoAAAANSUhEUgAAAwIAAAIBCAYAAAD6XABHNCSV...",
}
</pre>
							</div>
						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--.iprmenu-->
				<!--.iprexample-->
				<div class="demobox" id="iprexample">
					<p><em style="color:#f7778a;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

					<!-- 이미지 포즈 인식 -->
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>이상행동 식별</span>
									</dt>
									<dd class="txt">특정 인물의 행동을 정확히 감지하기 위해 사용합니다.
										<span><em class="fas fa-book-reader"></em> Reference: A공항, 수원시 CCTV 이상행동 감지 </span>
									</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_pr"><span>포즈인식</span></li>
											<li class="ico_tr"><span>ESR</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>모션 인식</span>
									</dt>
									<dd class="txt">스포츠, 자세 교정 등 관절의 움직임 파악이 필요한 분야에서 활용할 수 있습니다.<dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_pr"><span>포즈인식</span></li>
										</ul>
									</dd>
								</dl>
							</li>

						</ul>
					</div>
					<!--이미지 포즈 인식  -->
				</div>
				<!--//.iprexample-->


			</div>
			<!-- //.content -->
		</div>
		
<script type="text/javascript">
var sampleImage1;
var sampleImage2;
var data;
var xhr;

let image1_blob;
let image2_blob;

jQuery.event.add(window,"load",function(){
	function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/images/pose01.jpg");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            //sampleImage1 = new File([blob], "pose01.jpg");

            var imgSrcURL = URL.createObjectURL(blob);
    		var textr_output=document.getElementById('input_img');
    		textr_output.setAttribute("src",imgSrcURL);

    		image1_blob = blob;
        };
		
        xhr.send();
	}

	function loadSample2() {
	    var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/images/pose02_.png");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            //sampleImage2 = new File([blob], "pose02_.png");
            
            var imgSrcURL = URL.createObjectURL(blob);
    		var textr_output=document.getElementById('input_img');
    		textr_output.setAttribute("src",imgSrcURL);

    		image2_blob = blob;
        };
		
        xhr.send();
	}
	
	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#input_img').attr('src', e.target.result);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}

	$(document).ready(function (){
		loadSample2();
		loadSample1();

 		$("#demoFile").change(function(){
			readURL(this);
		});

 		$('.radio label').on('click',function(){
			$('.fl_box').attr('opacity',1);
			$('em.close').click();
		});

		$('#sub').on('click',function (){

			var demoFileTxt = $('#demoFile').val(); //파일을 추가한 input 박스의 값
			var formData = new FormData();
			var demoFile;

			if(demoFileTxt === ""){
				var option = $("input[type=radio][name=option]:checked").val();
				if(option == 1){
					loadSample1();
					//demoFile = sampleImage1;

					demoFile = image1_blob;
				}else{
					loadSample2();
					//demoFile = sampleImage2;

					demoFile = image2_blob;
				}
			}
			else {				
				var demoFileInput = document.getElementById('demoFile');
				// console.log(demoFileInput);
				demoFile = demoFileInput.files[0];
				// console.log(demoFile);
			}

			formData.append('file',demoFile);
			formData.append('${_csrf.parameterName}', '${_csrf.token}');

			$('.tr_1').hide();
			$('.tr_2').fadeIn(300);

			xhr = $.ajax({
				type: "POST",
				async: true,
				url: '${pageContext.request.contextPath}/poseRecog/poseApi',
				data: formData,
				processData: false,
				contentType: false,
				timeout: 20000,
				success: function(result){
					let resultData = JSON.parse(result);
					$('.tr_1').hide();
					$('.tr_2').hide();
					$('.tr_3').fadeIn(300);

					data = "data:image/jpeg;base64," + resultData.result_img;
					$('#output_img').attr('src', data);
				},
				error: function(jqXHR, error){
					if(jqXHR.status === 0){
						return false;
					}

					alert("Error");
					console.dir(error);
					window.location.reload();
				}
			});


		});

	});

});

function downloadResultImg(){
    let fileName = "PoseRecognition.jpg";
	download(data,fileName,"image/jpeg");
}
</script>


<script>
	//파일명 변경
	document.querySelector("#demoFile").addEventListener('change', function (ev) {
		document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
		var element = document.getElementById( 'uploadFile' );
		element.classList.remove( 'btn' );
		element.classList.add( 'btn_change' );
		$('.fl_box').css("opacity", "0.5");
	});


	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){

			// step1->step2  (close button)
			$('em.close').on('click', function () {
				$('.fl_box').css("opacity", "1");
				$('#uploadFile label').text('이미지 업로드');
				$('#uploadFile').removeClass("btn_change");
				$('#uploadFile').addClass("btn");
				$('#demoFile').val("");

				//파일명 변경
				document.querySelector("#demoFile").addEventListener('change', function (ev) {
					document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
					var element = document.getElementById( 'uploadFile' );
					element.classList.remove( 'btn' );
					element.classList.add( 'btn_change' );
				});
			});


			// 처음으로 버튼
			$('.btn_back1').on('click', function () {
				xhr.abort();
				$('.tr_2').hide();
				$('.tr_1').fadeIn(300);
			});

			// step3->step1
			$('.btn_back2').on('click', function () {
				$('.tr_3').hide();
				$('.tr_1').fadeIn(300);

				// $('em.close').parent().removeClass("btn_change");
				// $('em.close').parent().addClass("btn");
				// $('.fl_box').css("opacity", "1");
				// $('em.close').parent().children('.demolabel').text('이미지 업로드');

				document.querySelector("#demoFile").addEventListener('change', function (ev) {
					document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
					
				});
			});
			
		});
	});
	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();

</script>