﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">패턴 분류</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'hmddemo')" id="defaultOpen"><button type="button">데모</button></li>
			<li class="tablinks" onclick="openTap(event, 'hmdexample')"><button type="button">적용사례</button></li>
			<li class="tablinks" onclick="openTap(event, 'hmdmenu')"><button type="button">매뉴얼</button></li>
<%--			<li class="tablinks"><a href="/member/krApiAccount" >API ID, key 발급</a></li>--%>
		</ul>
		<div class="demobox demobox_hmd" id="hmddemo">
			<p>계층적 다중 탐지 사전<small>(HMD, Hierarchical Multiple Dictionary)</small>을 이용한 <span>패턴(감정) 분류</span></p>
			<!--				<span class="sub">문장에 들어 있는 감정표현을 분석합니다</span>-->
			<span class="sub">기존에 구축한 사전을 이용하여 문장 패턴을 탐지하고 문장을 분류합니다.</span>
			<form>
				<!--.hmd_box-->
				<div class="hmd_box">
					<div class="" style ="width: 328px; height: 40px; border-radius: 5px; border: 1px solid #dce2f2; margin: 0 auto 25px; line-height: 40px;">
						<span style="font-size:13px;margin-right:28px; ">언어선택</span>
						<div class="radio">
							<input type="radio" name="language" id="language_ko" checked value="kor"/>
							<label for="language_ko">한국어</label>
						</div>
						<div class="radio">
							<input type="radio" name="language" id="language_en" value="eng"/>
							<label for="language_en">English</label>
						</div>
					</div>

					<div class="text_area">
						<textarea rows="12" id="text-contents" maxlength="1000" placeholder="텍스트를 직접 입력하거나 텍스트 파일을 업로드 해 주세요.">:: 긍정 표현 예시::
훌륭하네요. 덕분에 기분이 매우 좋습니다.
문제를 해결해주셔서 감사합니다. 당신은 제 생명의 은인입니다.

:: 부정 표현 예시 ::
너무 불친절하고 무례하네요. 뻔뻔하고 최악이예요.
실망스럽습니다. 저는 이 최악의 제안을 받아들일 수가 없어요.</textarea>
					</div>
					<div class="text_info">
						<p>* 텍스트 1000자 이내, 파일 바이트 30KB 이내 [파일확장자:  .txt ]</p>
						<span class=""><strong id="count">0</strong>/1000</span>
					</div>
				</div>
				<!--//.hmd_box-->
				<!--.btn_area-->
				<div class="btn_area btn_area_815 step_1btn">
					<div class="btn_float_l">
						<input type="file" id="file-down" class="demoFile" accept=".txt"/><label for="file-down" class="btn_type btn_gray">파일 업로드 (euc-kr)</label>
					</div>
					<button type="button" id="hmdbtn" class="btn_type btn_blue"><em class="fas fa-file-invoice"></em>분석하기</button>
				</div>
				<!--//.btn_area-->
			</form>

			<!--posi_nega_area-->
			<div class="posi_nega_area">
				<!--posi_box-->
				<div class="posi_box">
					<div class="tit_posi"><span>긍정 표현</span></div>
					<div class="posiNega_area">
						<ul class="tab_posi_nega">
							<li><span>낮음</span></li>
							<li><span></span></li>
							<li><span></span></li>
							<li><span>높음</span></li>
						</ul>
						<ul class="tab_posi_nega2">
							<li id='Positive_Cnt_1'></li>
							<li id='Positive_Cnt_2'></li>
							<li id='Positive_Cnt_3'></li>
							<li id='Positive_Cnt_4'></li>
						</ul>

						<div class="posi_nega_conts ">
							<div class="posi_nega_list">
								<ul id='Positive_1'>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul id='Positive_2'>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul id='Positive_3'>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul id='Positive_4'>
								</ul>
							</div>
<%--							<a href="#none" class="btn_more">더보기</a>--%>
						</div>
					</div>
				</div>
				<!--//.posi_box-->
				<!--.nega_box-->
				<div class="nega_box">
					<div class="tit_posi tit_nega"><span>부정 표현</span></div>
					<div class="posiNega_area">
						<ul class="tab_posi_nega tab_nega">
							<li><span>낮음</span></li>
							<li><span></span></li>
							<li><span></span></li>
							<li><span>높음</span></li>
						</ul>
						<ul class="tab_posi_nega2">
							<li id='Negative_Cnt_1'></li>
							<li id='Negative_Cnt_2'></li>
							<li id='Negative_Cnt_3'></li>
							<li id='Negative_Cnt_4'></li>
						</ul>

						<div class="posi_nega_conts01 ">
							<div class="posi_nega_list">
								<ul id='Negative_1'>
								</ul>

							</div>
							<div class="posi_nega_list">
								<ul id='Negative_2'>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul id='Negative_3'>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul id='Negative_4'>
								</ul>
							</div>
<%--							<a href="#none" class="btn_more">더보기</a>--%>
						</div>
					</div>
				</div>
				<!--//.nega_box-->

				<!--.btn_area-->
				<div class="btn_area btn_area_815">
					<button type="button" class="btn_type btn_blue btn_goback">다시하기</button>
				</div>
				<!--//.btn_area-->

			</div>
			<!--//posi_nega_area-->
		</div>
		<!-- .demobox -->


		<!--.hmdmenu-->
		<div class="demobox" id="hmdmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API 공통 가이드
					</div>
					<p class="sub_title">개발 환경 세팅</p>
					<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
					<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

				</div>
				<div class="guide_group">
					<div class="title">
						Hierarchical-Multiple-Dictionary (HMD)
					</div>
					<p class="sub_txt">계층적 다중 탐지 사전을 이용하여 패턴을 분류 합니다. 현재는 감정 사전을 이용하여 감정 패턴을 분류합니다.</p>

					<span class="sub_title">
							준비사항
						</span>
					<p class="sub_txt">① Input: 문자열 (텍스트)</p>
					<p class="sub_txt">② 아래 언어 중 택 1 </p>
					<ul>
						<li>한글 (kor)</li>
						<li>영어 (eng)</li>
					</ul>
					<span class="sub_title">
							 실행 가이드
						</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/hmd/</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>사용자의 고유 key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>lang</td>
							<td>사용할 모델의 언어 (kor / eng) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>reqText</td>
							<td>패턴 분류를 실행할 문자열 (텍스트) </td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
<pre>
curl -X POST \
 https: //dev-api.maum.ai/api/hmd \
 -H 'Content-Type: application/json' \
 -d '{
    "apiId": "(*ID 요청필요",
    "apiKey": "(*key 요청필요)",
    "lang": "kor",
    "reqText": "오늘 난 기분이 좋아"
}'
</pre>
					</div>

					<p class="sub_txt">④ Response 파라미터 설명 </p>
					<span class="table_tit">Response</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>message</td>
							<td>API 동작여부</td>
							<td>list</td>
						</tr>
						<tr>
							<td>document</td>
							<td>형태소 분석 결과</td>
							<td>list</td>
						</tr>
						<tr>
							<td>cls </td>
							<td>HMD 분류 결과 </td>
							<td>list</td>
						</tr>
					</table>
					<span class="table_tit">message: API 동작여부</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>message</td>
							<td>요청 처리 상태를 설명하는 문자열 (Success/ Fail)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>status</td>
							<td>요청 처리 상태에 대한 status code (0: Success)</td>
							<td>number</td>
						</tr>
					</table>

					<span class="table_tit">document: 형태소 분석 결과</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>category Weight</td>
							<td>Unused</td>
							<td>-</td>
						</tr>
						<tr>
							<td>sentences</td>
							<td>문장 별 형태소 분석 결과 </td>
							<td>array</td>
						</tr>
					</table>

					<span class="table_tit">sentences: 문장 별 형태소 분석 결과</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>sequence</td>
							<td>분석한 문장 번호 (n) </td>
							<td>int</td>
						</tr>
						<tr>
							<td>text</td>
							<td>분석한 원문장</td>
							<td>string</td>
						</tr>
						<tr>
							<td>words</td>
							<td>어절 정보 </td>
							<td>list</td>
						</tr>
						<tr>
							<td>morps</td>
							<td>형태소분석 정보 </td>
							<td>list</td>
						</tr>
						<tr>
							<td>morphEvals</td>
							<td>어절 단위 형태소 분석 결과 정보</td>
							<td>list</td>
						</tr>
					</table>

					<span class="table_tit">words: 어절 정보</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>seq</td>
							<td>어절 ID</td>
							<td>int</td>
						</tr>
						<tr>
							<td>text</td>
							<td>어절</td>
							<td>string</td>
						</tr>
						<tr>
							<td>begin </td>
							<td>어절을 구성하는 첫 형태소의 ID </td>
							<td>int</td>
						</tr>
						<tr>
							<td>end </td>
							<td>어절을 구성하는 끝 형태소의 ID </td>
							<td>int</td>
						</tr>
						<tr>
							<td>beginSid</td>
							<td>Unused</td>
							<td>-</td>
						</tr>
						<tr>
							<td>endSid</td>
							<td>Unused </td>
							<td>-</td>
						</tr>
						<tr>
							<td>position </td>
							<td>어절 ID (영어만 사용)</td>
							<td>int</td>
						</tr>
					</table>
					<span class="table_tit">morps: 형태소 분석 정보</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>seq</td>
							<td>형태소 ID (0 부터 시작하여 문장에서의 출현 순서대로 값 부여)</td>
							<td>int</td>
						</tr>
						<tr>
							<td>lemma </td>
							<td>형태소 단어</td>
							<td>string</td>
						</tr>
						<tr>
							<td>type</td>
							<td>형태소 태그 </td>
							<td>string</td>
						</tr>
						<tr>
							<td>position </td>
							<td>문장에서의 byte position</td>
							<td>int</td>
						</tr>
						<tr>
							<td>wid  </td>
							<td>Unused</td>
							<td>-</td>
						</tr>
						<tr>
							<td>weight  </td>
							<td>형태소 분석 결과 신뢰도 (0~1)</td>
							<td>long</td>
						</tr>
					</table>
					<span class="table_tit">morphEvals: 어절 단위 형태소 분석 결과 정보</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>seq</td>
							<td>어절 ID </td>
							<td>int</td>
						</tr>
						<tr>
							<td>target</td>
							<td>어절</td>
							<td>string</td>
						</tr>
						<tr>
							<td>result </td>
							<td>형태소 분석 결과 </td>
							<td>string</td>
						</tr>
						<tr>
							<td>wordId </td>
							<td>Unused (seq 와 동일)</td>
							<td>int</td>
						</tr>
						<tr>
							<td>mBegin </td>
							<td>어절을 구성하는 첫 형태소의 id </td>
							<td>int</td>
						</tr>
						<tr>
							<td>mEnd</td>
							<td>어절을 구성하는 끝 형태소의 id </td>
							<td>int</td>
						</tr>
					</table>
					<span class="table_tit">cls: HMD 분류 결과</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>sentenceSequence</td>
							<td>분류된 문장 ID</td>
							<td>int</td>
						</tr>
						<tr>
							<td>category</td>
							<td>분류 카테고리 명 </td>
							<td>string</td>
						</tr>
						<tr>
							<td>pattern</td>
							<td>분류 패턴 </td>
							<td>string</td>
						</tr>
						<tr>
							<td>sentence</td>
							<td>분류 기준 문장</td>
							<td>string</td>
						</tr>
					</table>

					<p class="sub_txt">⑤ Response 예제 </p>
					<div class="code_box">
<pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "document": {
        "categoryWeight": 0.0,
        "sentences": [
            {
                "sequence": 0,
                "text": "오늘 난 기분이 좋아",
                "words": [
                    {
                        "seq": 0,
                        "text": "오늘",
                        "begin": 0,
                        "end": 0,
                        "beginSid": 0,
                        "endSid": 0,
                        "position": 0
                    },
                    {
                        "seq": 1,
                        "text": "난",
                        "begin": 1,
                        "end": 2,
                        "beginSid": 0,
                        "endSid": 0,
                        "position": 0
                    },
                    {
                        "seq": 2,
                        "text": "기분이",
                        "begin": 3,
                        "end": 4,
                        "beginSid": 0,
                        "endSid": 0,
                        "position": 0
                    },
                    {
                        "seq": 3,
                        "text": "좋아",
                        "begin": 5,
                        "end": 6,
                        "beginSid": 0,
                        "endSid": 0,
                        "position": 0
                    }
                ],
                "morps": [
                    {
                        "seq": 0,
                        "lemma": "오늘",
                        "type": "NNG",
                        "position": 0,
                        "wid": 0,
                        "weight": 0.3608239127814169
                    },
                    {
                        "seq": 1,
                        "lemma": "나",
                        "type": "NP",
                        "position": 7,
                        "wid": 0,
                        "weight": 8.88242304732498E-4
                    },
                    {
                        "seq": 2,
                        "lemma": "ㄴ",
                        "type": "JX",
                        "position": 7,
                        "wid": 0,
                        "weight": 0.012165351778778002
                    },
                    {
                        "seq": 3,
                        "lemma": "기분",
                        "type": "NNG",
                        "position": 11,
                        "wid": 0,
                        "weight": 0.9
                    },
                    {
                        "seq": 4,
                        "lemma": "이",
                        "type": "JKS",
                        "position": 17,
                        "wid": 0,
                        "weight": 0.03607228016640132
                    },
                    {
                        "seq": 5,
                        "lemma": "좋",
                        "type": "VA",
                        "position": 21,
                        "wid": 0,
                        "weight": 0.24929561958347904
                    },
                    {
                        "seq": 6,
                        "lemma": "아",
                        "type": "EC",
                        "position": 24,
                        "wid": 0,
                        "weight": 0.2970646275702992
                    }
                ],
                "morphEvals": [
                    {
                        "seq": 0,
                        "target": "오늘",
                        "result": "오늘/NNG",
                        "wordId": 0,
                        "mBegin": 0,
                        "mEnd": 0
                    },
                    {
                        "seq": 1,
                        "target": "난",
                        "result": "나/NP+ㄴ/JX",
                        "wordId": 1,
                        "mBegin": 1,
                        "mEnd": 2
                    },
                    {
                        "seq": 2,
                        "target": "기분이",
                        "result": "기분/NNG+이/JKS",
                        "wordId": 2,
                        "mBegin": 3,
                        "mEnd": 4
                    },
                    {
                        "seq": 3,
                        "target": "좋아",
                        "result": "좋/VA+어/EC",
                        "wordId": 3,
                        "mBegin": 5,
                        "mEnd": 6
                    }
                ]
            }
        ]
    },
    "cls": [
        {
            "sentenceSequence": 0,
            "category": "SENTIMENT_Positive_2",
            "pattern": "좋다$@!아니다$@!안 하다$@!못하다$@!없다$@!%않",
            "sentence": "오늘 난 기분이 좋아"
        }
    ]
}
</pre>

					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--//hmdmenu-->
		<!--.hmdexample-->
		<div class="demobox" id="hmdexample">
			<p><em style="color:#27c1c1;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

			<%--패턴 분류(HMD)--%>
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>텍스트 분석</span>
							</dt>
							<dd class="txt">다양한 분야의 비정형 문서 및 텍스트를 분석할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
									<li class="ico_if"><span>ITF</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>자동 분류</span>
							</dt>
							<dd class="txt">카테고리별로 텍스트를 자동으로 분류하고 분석할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
									<li class="ico_if"><span>ITF</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //패턴 분류(HMD) -->
		</div>
	</div>
</div>
<!-- //.contents -->
<script type="text/javascript">
	document.getElementById('file-down').addEventListener('change', readFile, false);

	var content = $('#text-contents').val();
	$('#count').html(content.length);

	$('#text-contents').keyup(function (e){
		var content = $(this).val();
		$('#count').html(content.length);
	});

	$('.btn_goback').on('click', function () {
		$(this).parent().parent().hide();
		$('form').fadeIn(300);

	});

	function readFile(e) {
		var file = e.target.files[0];

		if (!file) {
			return;
		}
		var reader = new FileReader();
		reader.onload = function(e) {
			fileData = e.target.result;
			$('#text-contents').val(fileData);

			// var txtValLth = $('#text-contents').val().length;
			// console.log("this is file upload");
			// if ( txtValLth > 0) {
			// 	$('.demobox_hmd .step_1btn .btn_type').removeClass('disable');
			// 	$('.demobox_hmd .step_1btn .btn_type').removeAttr('disabled');
			// 	$('.demobox_hmd .btn_area .disable .disBox').remove();
			// 	$('.demobox_hmd .step_1btn .disBox').remove();
			// }
		};

		reader.readAsText(file, "utf-8");
	}


	$('#hmdbtn').on('click',function(){
		$(this).parent().parent().hide();
		$('.posi_nega_area').fadeIn(300);
		var language = $("input[type=radio][name=language]:checked").val();

		$.ajax({
			type:'POST',
			url:'${pageContext.request.contextPath}/hmd/hmdApi',
			data: {
				"lang": language,
				"reqText": $('#text-contents').val(),
				"${_csrf.parameterName}" : "${_csrf.token}"
			},
			timeout: 20000,
			error: function(error){
				console.log(error);
			},
			success: function(data){
                var decodedData = decodeURIComponent(data);
				console.log("result:"+decodedData);

                procResult(decodedData);

				// $('.hmd_box .step02').fadeIn();

				var pnHeight = $(".posi_nega_conts").height();
				var pnHeight01 = $(".posi_nega_conts01").height();

				console.log(pnHeight);
				if(pnHeight>205){
					$(".posi_nega_conts").addClass('posi_nega_close');
					$('.btn_more').text("접기");
				}
				if(pnHeight01>205){
					$(".posi_nega_conts01").addClass('posi_nega_close');
					$('.btn_more').text("접기");
				}

				// $('.hidden').css('display','none');
				var span_ = $('.patternTxt');
				for(var ss = 0; ss < span_.length; ss++){
					var span_txt = span_[ss].innerText;
					if(span_txt == ''){
						$(span_[ss]).css('cursor','default');
						$(span_[ss]).next().remove();
					}
				}

			}
		});
	});


	$('.btn_more').click(function() {
		// $(this).toggleClass('on');
		$(this).parent().toggleClass('on');

		if($(this).text() == "더보기" ){
			$(this).addClass('on');
			$(this).text("접기");
			$('.hidden').fadeIn();
		}else{
			$(this).removeClass('on');
			$(this).text("더보기");
			$('.hidden').css('display','none');


		}
	});
    /*
    ** 수신된 결과 메세지 처리
    */
    function procResult(data) {
        var jsonObj = JSON.parse(data);
        var cls = jsonObj.cls;

        var positiveCnts = new Array(0, 0, 0, 0);
        var negativeCnts = new Array(0, 0, 0, 0);

        initUI();

        for(var xx = 0; xx < cls.length; xx++) {
			var categories = cls[xx].category.split("_");
			var pattern = cls[xx].pattern;
			var level = parseInt(categories[2]);

			if (categories[1] == 'Positive') positiveCnts[level - 1]++;
			else negativeCnts[level - 1]++;

			// 표기 변경 추가
			var replaceDallarSymbol = "(" + pattern.toString() + ")";

			replaceDallarSymbol = replaceDallarSymbol.split("$").join(")(");
			console.log(replaceDallarSymbol);

			addResultUI(categories[1], level, replaceDallarSymbol, xx);
            // alert("<" + xx + "> <" + categories[1] + "> <" + level + "> <" + pattern + ">");
        }
        // alert("pCnt=" + positiveCnt + ", nCnt=" + negativeCnt);

        for(var xx = 0; xx < 4; xx++) {
            $('#Positive_Cnt_' + (xx+1)).append("" + positiveCnts[xx]);
            $('#Negative_Cnt_' + (xx+1)).append("" + negativeCnts[xx]);
        }

        //0916 추가
		// $('.patternTxt').each(function(){
		// 	var text_re = $(this).text();
		// 	if (text_re === ""){
		// 		$(this).parent().remove();
		// 	}
		// });
    }

    /*
    **
    */
    function addResultUI(type, level, pattern, count) {
		for (var xx = 0; xx < 4; xx++) {
			$('#' + type + '_' + (xx + 1)).append("<li>" + "<span class=\"patternTxt\">" + (level == xx + 1 ? pattern : "") + "</span>" + "<em>" + (level == xx + 1 ? pattern : "") + "</em>" + "</li>");
		}
    	console.log(type);
		// for (var xx = 0; xx < 4; xx++) {
		// 	if(count<5){
		// 		$('#' + type + '_' + (xx + 1)).append("<li>" + "<span class=\"patternTxt\">" + (level == xx + 1 ? pattern : "") + "</span>" + "<em>" + (level == xx + 1 ? pattern : "") + "</em>" + "</li>");
		//
		// 	}else{
		// 		$('#' + type + '_' + (xx + 1)).append("<li class=\"hidden\">" + "<span class=\"patternTxt\">" + (level == xx + 1 ? pattern : "") + "</span>" + "<em>" + (level == xx + 1 ? pattern : "") + "</em>" + "</li>");
		//
		// 	}
		//

		// }

    }

    function initUI() {
        for(var xx = 0; xx < 4; xx++) {
            $('#Positive_' + (xx+1)).empty();
            $('#Negative_' + (xx+1)).empty();
        }


        for(var xx = 0; xx < 4; xx++) {
            $('#Positive_Cnt_' + (xx+1)).empty();
            $('#Negative_Cnt_' + (xx+1)).empty();
        }
    }

	$('input[type=radio][name=language]').on('click',function(){
		var chk = $("input[type=radio][name=language]:checked").val();
		if(chk == 'kor'){
			$('#text-contents').val(':: 긍정 표현 예시::\n' +
					'훌륭하네요. 덕분에 기분이 매우 좋습니다.\n' +
					'문제를 해결해주셔서 감사합니다. 당신은 제 생명의 은인입니다.\n' +
					'\n' +
					':: 부정 표현 예시 ::\n' +
					'너무 불친절하고 무례하네요. 뻔뻔하고 최악이예요.\n' +
					'실망스럽습니다. 저는 이 최악의 제안을 받아들일 수가 없어요.');
		}else{
			$('#text-contents').val(':: Example of Positive expression ::\n' +
					'Awesome! Thank you for your good behavior.\n' +
					'I am very happy and astonished because of you.\n' +
					'\n' +
					':: Example of Negative expression ::\n' +
					'It was a horrible, unpleasant memory.\n' +
					'You disappointed me and made me miserable... I\'ll never forgive you again.');
		}
	});

	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>


<%--<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/hmd.js"></script>--%>
