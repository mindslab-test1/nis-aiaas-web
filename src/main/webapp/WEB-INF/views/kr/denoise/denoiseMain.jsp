<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/download.js"></script>

<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">


<!-- 5 .pop_simple -->
<div class="pop_simple" id="api_upload_fail_popup">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>파일 업로드 실패</h5>
            <p>파일 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>* 지원가능 파일 확장자: .wav<br>
* sample rate 16000 / channels mono<br>
* 2MB 이하의 음성 파일을 이용해주세요.</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="#">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- 5 .pop_simple -->
<div class="pop_simple " id="api_fail_popup">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap">
        <button class="pop_close" type="button">팝업 닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-exclamation-triangle"></em>
            <p id="fail_msg">Timeout error</p>
            <p style="font-size: small"><strong id="fail_msg_strong">업로드한 파일을 확인해주세요</strong></p>
        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents">
    <!-- .content -->
    <div class="content api_content">
        <h1 class="api_tit">음성정제</h1>
        <ul class="menu_lst voice_menulst">
            <li class="tablinks" onclick="openTap(event, 'dndemo')" id="defaultOpen">
                <button type="button">데모</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'dnexample')">
                <button type="button">적용사례</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'dnmenu')">
                <button type="button">매뉴얼</button>
            </li>
<%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="dndemo">
            <p>어떤 환경에서도 또렷한 <span>음성 정제</span> <small>(Denoise)</small></p>
            <span class="sub">배경 음악 소리가 크던, 목소리의 울림이 심하던, 목소리가 가장 잘 들리게끔 음성을 정제해줍니다.</span>
            <!--dn_box-->
            <div class="demo_layout dn_box">
                <!--dn_1-->
                <div class="dn_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-audio"></em><strong>샘플 파일</strong>로 테스트 하기</p>
                        <div class="sample_box">
                            <div class="sample first_sample">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="샘플1" checked>
                                    <label for="sample1" class="">샘플1</label>
                                </div>
                                <!--player-->
                                <div class="player">
                                    <div class="button-items">
                                        <audio id="music" class="music" preload="auto" onended="audioEnded($(this))">
                                            <source src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise/msl-seo.mp3"
                                                    type="audio/mp3">
                                            <p>Alas, your browser doesn't support html5 audio.</p>
                                        </audio>
                                        <div id="slider" class="slider">
                                            <div id="elapsed" class="elapsed"></div>
                                        </div>
                                        <p id="timer" class="timer">0:00</p>
                                        <p class="timer_fr">0:00</p>
                                        <div class="controls">
                                            <div id="play" class="play sample_play">
                                            </div>
                                            <div id="pause" class="pause">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sample">
                                <div class="radio">
                                    <input type="radio" id="sample2" name="option" value="샘플2">
                                    <label for="sample2" class="">샘플2</label>
                                </div>
                                <!--player-->
                                <div class="player">
                                    <div class="button-items">
                                        <audio id="music2" class="music" preload="auto" onended="audioEnded($(this))">
                                            <source src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise/trump-cut.mp3"
                                                    type="audio/mp3">
                                            <p>Alas, your browser doesn't support html5 audio.</p>
                                        </audio>
                                        <div class="slider">
                                            <div class="elapsed"></div>
                                        </div>
                                        <p class="timer">0:00</p>
                                        <p class="timer_fr">0:00</p>
                                        <div class="controls">
                                            <div class="play sample_play">
                                            </div>
                                            <div class="pause">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--player-->
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-audio"></em><strong>내 파일</strong>로 해보기</p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-image hidden"></em>
                                <label for="demoFile" class="demolabel">.wav 파일 업로드</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".wav">
                            </div>
                            <ul>
                                <li>* 지원가능 파일 확장자: .wav</li>
                                <li>* sample rate 16000 / channels mono</li>
<%--                                <li>* 파일명에 특수문자나 공백은 없애주세요.</li>--%>
                                <li>* 2MB 이하의 음성 파일을 이용해주세요.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_start" id="start_denoise">음성 <span>정제하기</span></button>
                    </div>

                </div>
                <!--dn_1-->
                <!--dn_2-->
                <div class="dn_2">
                    <p><em class="far fa-file-audio"></em>음성 정제중</p>
                    <div class="loding_box ">
                        <!-- ball-scale-rotate -->
                        <div class="lds">
                            <div class="ball-scale-rotate">
                                <span> </span>
                                <span> </span>
                                <span> </span>
                            </div>
                        </div>

                        <p>약간의 시간이 소요 됩니다.</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo" id="reload_denoise"></em>처음으로</button>
                    </div>

                </div>
                <!--dn_2-->
                <!--dn_3-->
                <div class="dn_3">
                    <div class="origin_file">
                        <p><em class="far fa-file-audio"></em> 원본 파일</p>
                        <div class="origin">
                            <!--player-->
                            <div class="player">
                                <div class="button-items">
                                    <audio id="music3" class="music" preload="auto" onended="audioEnded($(this))">
                                        <source src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise.wav"
                                                type="audio/wav">
                                        <p>Alas, your browser doesn't support html5 audio.</p>
                                    </audio>
                                    <div class="slider">
                                        <div class="elapsed"></div>
                                    </div>
                                    <p class="timer">0:00</p>
                                    <p class="timer_fr">0:00</p>
                                    <div class="controls">
                                        <div class="play">
                                        </div>
                                        <div class="pause">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--player-->
                        </div>
                    </div>
                    <div class="result_file">
                        <p><em class="far fa-file-audio"></em> 음성 정제 파일</p>
                        <div class="result">
                            <!--player-->
                            <div class="player">
                                <div class="button-items">
                                    <audio id="music4" class="music" preload="auto" onended="audioEnded($(this))">
                                        <source src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise.wav"
                                                type="audio/wav">
                                        <p>Alas, your browser doesn't support html5 audio.</p>
                                    </audio>
                                    <div class="slider">
                                        <div class="elapsed"></div>
                                    </div>
                                    <p class="timer">0:00</p>
                                    <p class="timer_fr">0:00</p>
                                    <div class="controls">
                                        <div class="play">
                                        </div>
                                        <div class="pause">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--player-->
                            <a id="btn_dwn" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> 결과 파일
                                다운로드</a>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>
                <!--dn_3-->
            </div>
            <!--// dn_box-->
            <span class="remark">* 배경음에 가사가 섞여 있을 경우, 소리가 너무 큰 경우에는 성능이 다소 낮게 나올 수 있습니다.</span>
        </div>
        <!-- //.demobox -->


        <!--.dnmenu-->
        <div class="demobox voice_menu" id="dnmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

                </div>
                <div class="guide_group">
                    <div class="title">
                        음성정제 <small>(Denoise)</small>
                    </div>
                    <p class="sub_txt">음성정제 API는 잡음 뿐만 아니라, 배경 음악 소리, 울림소리 등 화자 목소리를 제외한 소리들을 정제하여 음성의 질을
                        높여줍니다.</p>

                    <span class="sub_title">
								준비사항
							</span>
                    <p class="sub_txt">Input: 음성파일 </p>
                    <ul>
                        <li>확장자: .wav</li>
                        <li>Sample rate: 16000</li>
                        <li>Channels: mono</li>
                    </ul>
                    <span class="sub_title">
								 실행 가이드
							</span>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/api/dap/denoise/</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>사용자의 고유 ID.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey</td>
                            <td>사용자의 고유 key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>type:file (wav) 음성파일 (5분 길이 이하)</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
<pre>
curl -X POST \
   'https://api.maum.ai/api/dap/denoise/ \
   -H 'content-type: multipart/form-data;
boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
   -F apiId=(*ID 요청 필요) \
   -F apiKey=(*key 요청 필요) \
   -F 'file=@sample.wav'
</pre>
                    </div>

                    <p class="sub_txt">④ Response 예제 </p>

                    <div class="code_box">
                        <audio src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise.wav" preload="auto"
                               controls></audio>
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.dnmenu-->
        <!--.dnexample-->
        <div class="demobox" id="dnexample">
            <p><em style="font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>

            <!-- //화폐, 고지서 인식(DIARL) -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>음성파일 보관</span>
                            </dt>
                            <dd class="txt">기존에 가지고 있는 잡음이 많은 음성 파일을 필요한 음성만 깨끗하게 보관할 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_den"><span>음성 정제</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>음성인식 성능 <strong>향상</strong></span>
                            </dt>
                            <dd class="txt">다양한 음성인식 서비스 기기들의 인식률 향상을 위해 실제 서비스 전에 음성을 먼저 정제할 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_den"><span>음성 정제</span></li>
                                    <li class="ico_stt"><span>STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //음성정제(Denoise) -->
        </div>
        <!--//.dnexample-->

    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->


<script type="text/javascript">

    var request;
    var dwnAudio = null;
    var timelineWidth = $('#slider').get(0).offsetWidth;

    var sample1wavFile;
    var sample2wavFile;
    var sample1SrcUrl;
    var sample2SrcUrl;

    // Set audio duration
    $('.music').each(function () {
        $(this).on("canplay", function () {
            var $parent = $(this).parent();
            var dur = this.duration;
            var fl_dur = Math.floor(dur);
            var endTime = toTimeFormat(fl_dur + "");
            $parent.children('.timer_fr').text(endTime);
        });
    });

    $(document).ready(function () {
        loadSample1();
        loadSample2();


        // Time update event
        $('.music').on("timeupdate", function () {
            var curIdx = $('.music').index($(this));
            timeUpdate($(this), curIdx);
        });

        // Audio play
        $('.play').on('click', function (me) {
            var curIdx = $('.play').index(this);

            $('.play').each(function (idx, e) {
                if (e !== me.currentTarget) {
                    $('.pause').eq(idx).css("display", "none");
                    $('.play').eq(idx).css("display", "block");
                    $('.music').eq(idx).trigger("pause");
                }
            });

            $('.music').eq(curIdx).trigger("play");
            $(this).css("display", "none");
            $('.pause').eq(curIdx).css("display", "block");
        });


        // Audio pause
        $('.pause').on('click', function () {
            var curIdx = $('.pause').index(this);

            $('.music').eq(curIdx).trigger("pause");
            $(this).css("display", "none");
            $('.play').eq(curIdx).css("display", "block");
        });


        // 재생중인 오디오 중지 시키기
        $('#start_denoise, .btn_back2, .demolabel, .tablinks').on('click', function () {
            $('.music').each(function () {
                $('.pause').trigger('click');
            });
        });


        $('.sample .sample_play').on('click', function () {
            var sampleNum = $('.sample .sample_play').index(this);
            $('input[type="radio"]:eq(' + sampleNum + ')').trigger('click');
        });


        $('input[type="radio"]').on('click', function () {
            $('em.close').trigger('click');
        });

        $('.radio').on('click', function () {
            if ($('.fl_box').css("opacity") === "0.5") {
                $('.fl_box').css("opacity", "1");
            }
        });




        // 팝업 닫기
        $('.pop_close, .pop_bg, .btn a').on('click', function () {
            $('.pop_simple').fadeOut(300);
            $('body').css({
                'overflow': '',
            });
        });


        // 파일 업로드 후 이벤트
        document.querySelector("#demoFile").addEventListener('change', function (ev) {

            var file = this.files[0];

            if (file === undefined || file === null) {
                return;
            }

            var name = file.name;
            var size = (file.size / 1048576).toFixed(3); //size in mb
            var fileSize = file.size;
            var maxSize = 1024 * 1024 * 2; //2MB

            if (file.type.match(/audio\/wav/) && fileSize < maxSize) {

                $('input[type="radio"]:checked').prop("checked", false);
                $('.demolabel').text(name + ' (' + size + 'MB)');
                $('#uploadFile').removeClass('btn');
                $('#uploadFile').addClass('btn_change');
                $('.fl_box').css("opacity", "0.5");
            } else {
                this.value = null;
                $('#api_upload_fail_popup').fadeIn(300);
            }
        });

        // Remove uploaded file
        $('em.close').on('click', function () {
            $('#demoFile').val(null);
            $('.demolabel').text('.wav 파일 업로드');
            $(this).parent().removeClass("btn_change");
            $(this).parent().addClass("btn");
            $('.fl_box').css('opacity', '1');
        });


        // 음성 정제하기
        $('#start_denoise').on('click', function () {
            var $checked = $('input[type="radio"]:checked');
            var $demoFile = $("#demoFile");
            var inputFile;
            var url;

            //내 파일로
            if ($checked.length === 0) {
                if ($demoFile.val() === "" || $demoFile.val() === null) {
                    alert("샘플을 선택하거나 파일을 업로드해 주세요.");
                    return 0;
                }
                inputFile = $demoFile.get(0).files[0];
                // url = URL.createObjectURL(inputFile);
            }
            //샘플로
            else {
                if ($checked.val() === "샘플1") {
                    // url = sample1SrcUrl;
                    inputFile = sample1wavFile;
                } else if ($checked.val() === "샘플2") {
                    // url = sample2SrcUrl;
                    inputFile = sample2wavFile;
                }
            }

            $('.dn_1').hide();
            $('.dn_2').fadeIn(300);

            startRequest(inputFile);

        });


        // step2->step1
        $('.btn_back1').on('click', function () {

            if(request){ request.abort(); }

            initPlayerUI();
            $('.dn_2').hide();
            $('.dn_1').fadeIn(300);

        });


        // Result -> 처음으로
        $('.btn_back2').on('click', function () {

            initPlayerUI();
            $('.dn_3').hide();
            $('.dn_1').fadeIn(300);

        });


        $('#api_fail_popup .pop_close, #api_fail_popup .btn').on('click', function(){
            if($('.dn_2').is(":visible")) $('.dn_2').hide();
            $('.btn_back2').click();
        });


        $('#btn_dwn').on('click', function(){
            let fileName = "Denoise.wav";
            download(dwnAudio, fileName, "audio/wav");
        });

    });


    //------------------ Functions --------------------------

    // Update current play time and player bar
    //      obj : current .music jquery object
    //      idx : currnet .music index (= audio player index)
    function timeUpdate(obj, idx) {
        var cur_music = obj.get(0);
        var playHead = $('.elapsed').eq(idx).get(0);
        var timer = $('.timer').eq(idx).get(0);

        var playPercent = timelineWidth * (cur_music.currentTime / cur_music.duration);
        playHead.style.width = playPercent + "px";

        var secondsIn = Math.floor(cur_music.currentTime);
        var curTime = toTimeFormat(secondsIn + "");
        timer.innerHTML = curTime;
    }


    function toTimeFormat(text) {
        var sec_num = parseInt(text, 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        // if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {
            minutes = minutes;
        }
        if (seconds < 10) {
            seconds = "0" + seconds;
        }

        return minutes + ':' + seconds;
    }


    function audioEnded(audio) {
        var idx = $('.music').index(audio);
        // audio UI setting
        $('.pause').eq(idx).trigger('click');
        $('.music').eq(idx).get(0).currentTime = 0;
        $('.elapsed').eq(idx).css("width", "0px");
        $('.timer').eq(idx).text("0:00");
    }

    function initPlayerUI(){
        $('.music').each(function(){
            if (!isNaN(this.duration)) {
                this.currentTime = 0;
            }
        });
        $('.elapsed').css("width", "0px");
        $('.timer').text("0:00");
    }


    function showApiFailPopup(mainErrMsg, detailErrMsg){
        $('#fail_msg').html(mainErrMsg);
        $('#fail_msg_strong').html(detailErrMsg);
        $('#api_fail_popup').show();
    }


    function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/audio/denoise/msl-seo.wav");
        xhr.responseType = "blob";
        xhr.onload = function () {
            blob = xhr.response;
            sample1wavFile = new Blob([blob], {type:"audio/wav"});
            sample1SrcUrl = URL.createObjectURL(blob);

            sound1_blob = blob;
        };

        xhr.send();
    }

    function loadSample2() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/audio/denoise/trump-cut.wav");
        xhr.responseType = "blob";
        xhr.onload = function () {
            blob = xhr.response;
            sample2wavFile = new Blob([blob], {type:"audio/wav"});
            sample2SrcUrl = URL.createObjectURL(blob);
        };

        xhr.send();
    }

    function startRequest(inputFile){
        var formData = new FormData();
        formData.append('noiseFile',inputFile);
        formData.append($("#key").val(), $("#value").val());

        request = new XMLHttpRequest();

        request.onloadstart = function(ev) {
            // request.responseType = "blob";
        }
        request.onreadystatechange = function(){
            if (request.readyState === 4){

                if(request.status === 200){
                    if(request.response == null || request.response == ""){
                        showApiFailPopup("Response error", "서버에서 응답을 받지 못했습니다.<br> 다시 시도해 주세요.");
                        dwnAudio = null;
                        return;
                    }

                    let responseJson = JSON.parse(request.response);
                    let orgMp3Audio = "data:audio/mp3;base64," + responseJson['orgMp3File'].body;
                    let changedMp3Audio = "data:audio/mp3;base64," + responseJson['changedMp3File'].body;
                    dwnAudio = "data:audio/wav;base64," + responseJson['wavFile'].body;

                    $('#music3').attr('src', orgMp3Audio);
                    $('#music4').attr('src', changedMp3Audio);

                    $('.dn_2').hide();
                    $('.dn_3').fadeIn(300);
                    $('.play').eq(3).trigger('click');
                }

                else{

                    if(request.status === 0){ return; } // abort

                    showApiFailPopup("Response error", "서버에서 응답을 받지 못했습니다.<br> 다시 시도해 주세요.");
                    dwnAudio = null;
                }
            }

        };

        request.open('POST', '${pageContext.request.contextPath}/api/dap/denoise');
        request.timeout = 20000;
        request.send(formData);

        request.ontimeout = function() {
            showApiFailPopup("Timeout error", "응답 시간이 초과되었습니다.<br>업로드한 파일을 확인하고 다시 시도해 주세요.");
            dwnAudio = null;
        };

        request.addEventListener("abort", function(){
            // dwnAudio = null;
        });

    }

</script>


<script>
    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>
