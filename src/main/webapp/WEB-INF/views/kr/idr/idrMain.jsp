﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%

    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/idr/croppie.css">
<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">

<!-- 1 .pop_common -->
<div class="pop_common sample_pop">
    <!-- .pop_bg 팝업 바탕 어두운 색 -->
    <div class="pop_bg"></div>
    <!-- //.pop_bg 팝업 바탕 어두운 색 -->

    <!-- .popWrap -->
    <div class="popWrap">
        <!-- 4 .pop_2btn 버튼 두개 팝업 -->
        <div class="pop_2btn">
            <div class="pop_bg2"></div>
            <!-- .popWrap -->
            <div class="popWrap">
                <em class="fas fa-times minipop_close"></em>
                <!-- .pop_bd -->
                <div class="pop_bd">
                    <em class="fas fa-money-bill"></em>
                    <p>화폐 선택</p>
                    <span>선택하신 <strong>샘플 파일</strong>로 테스트를<br>진행하시겠습니까?
			            </span>
                </div>
                <!-- //.pop_bd -->
                <div class="btn">
                    <button class="btn_gray">취소</button>
                    <button class="btn_blue" id="">확인</button>
                </div>
            </div>
            <!-- //.popWrap -->
        </div>
        <!-- //.pop_2btn -->

        <!-- .pop_hd -->
        <div class="pop_hd">
            <h3><em class="far fa-file-image"></em>&nbsp; 화폐 샘플 파일</h3>
            <em class="fas fa-times pop_sample_close"></em>
        </div>
        <!-- //.pop_hd -->
        <!-- .pop_bd -->
        <div class="pop_bd">

            <div class="country">
                <p class="p_kr"><img src="${pageContext.request.contextPath}/aiaas/common/images/img_kr.svg"  alt="한국 국기"/> 한국 (KRW)</p>
                <div class="option_box">
                    <div class="radio">
                        <input type="radio" id="sample_kr1" name="option" value="sample_1" checked>
                        <label for="sample_kr1" class="">
                            <em class="img_area">
                                <img id="sample_1" src="${pageContext.request.contextPath}/aiaas/common/images/img_ko1.png" alt="천원" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_kr2" name="option" value="sample_2" >
                        <label for="sample_kr2" class="">
                            <em class="img_area">
                                <img id="sample_2" src="${pageContext.request.contextPath}/aiaas/common/images/img_ko2.png" alt="오천원" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_kr3" name="option" value="sample_3" >
                        <label for="sample_kr3" class="">
                            <em class="img_area">
                                <img id="sample_3" src="${pageContext.request.contextPath}/aiaas/common/images/img_ko3.png" alt="만원" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_kr4" name="option" value="sample_4" >
                        <label for="sample_kr4" class="">
                            <em class="img_area">
                                <img id="sample_4" src="${pageContext.request.contextPath}/aiaas/common/images/img_ko4.png" alt="오만원" />
                            </em>
                        </label>
                    </div>
                </div>
            </div>
            <div class="country">
                <p><img src="${pageContext.request.contextPath}/aiaas/common/images/img_usd.svg"  alt="미국 국기"/>  미국 (USD)</p>
                <div class="option_box">
                    <div class="radio">
                        <input type="radio" id="sample_us5" name="option" value="sample_5" checked>
                        <label for="sample_us5" class="">
                            <em class="img_area">
                                <img id="sample_5" src="${pageContext.request.contextPath}/aiaas/common/images/img_usd1.png" alt="1달러" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_us6" name="option" value="sample_6" >
                        <label for="sample_us6" class="">
                            <em class="img_area">
                                <img id="sample_6" src="${pageContext.request.contextPath}/aiaas/common/images/img_usd2.png" alt="2달러" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_us7" name="option" value="sample_7" >
                        <label for="sample_us7" class="">
                            <em class="img_area">
                                <img id="sample_7" src="${pageContext.request.contextPath}/aiaas/common/images/img_usd3.png" alt="5달러" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_us8" name="option" value="sample_8" >
                        <label for="sample_us8" class="">
                            <em class="img_area">
                                <img id="sample_8" src="${pageContext.request.contextPath}/aiaas/common/images/img_usd4.png" alt="20달러" />
                            </em>
                        </label>
                    </div>
                </div>
            </div>
            <div class="country">
                <p><img src="${pageContext.request.contextPath}/aiaas/common/images/img_eu.svg"  alt="유럽 국기"/>  유럽 (EUR)</p>
                <div class="option_box">
                    <div class="radio">
                        <input type="radio" id="sample_eu9" name="option" value="sample_9" checked>
                        <label for="sample_eu9" class="">
                            <em class="img_area">
                                <img id="sample_9" src="${pageContext.request.contextPath}/aiaas/common/images/img_eur1.png" alt="20유로" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_eu10" name="option" value="sample_10" >
                        <label for="sample_eu10" class="">
                            <em class="img_area">
                                <img id="sample_10" src="${pageContext.request.contextPath}/aiaas/common/images/img_eur2.png" alt="100유로" />
                            </em>
                        </label>
                    </div>
                </div>
            </div>
            <div class="country">
                <p><img src="${pageContext.request.contextPath}/aiaas/common/images/img_cn.svg"  alt="중국 국기"/>  중국 (CNY)</p>
                <div class="option_box">
                    <div class="radio">
                        <input type="radio" id="sample_cn11" name="option" value="sample_11" checked>
                        <label for="sample_cn11" class="">
                            <em class="img_area">
                                <img id="sample_11" src="${pageContext.request.contextPath}/aiaas/common/images/img_cny1.png" alt="10위안" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_cn12" name="option" value="sample_12" >
                        <label for="sample_cn12" class="">
                            <em class="img_area">
                                <img id="sample_12" src="${pageContext.request.contextPath}/aiaas/common/images/img_cny2.png" alt="20위안" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_cn13" name="option" value="sample_13" >
                        <label for="sample_cn13" class="">
                            <em class="img_area">
                                <img id="sample_13" src="${pageContext.request.contextPath}/aiaas/common/images/img_cny3.png" alt="50위안" />
                            </em>
                        </label>
                    </div>

                </div>
            </div>
            <div class="country">
                <p><img src="${pageContext.request.contextPath}/aiaas/common/images/img_jp.svg" alt="일본 국기"/>  일본 (JPY)</p>
                <div class="option_box">
                    <div class="radio">
                        <input type="radio" id="sample_jp14" name="option" value="sample_14" checked>
                        <label for="sample_jp14" class="">
                            <em class="img_area">
                                <img id="sample_14" src="${pageContext.request.contextPath}/aiaas/common/images/img_jp1.png" alt="5000엔" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_jp15" name="option" value="sample_15" >
                        <label for="sample_jp15" class="">
                            <em class="img_area">
                                <img id="sample_15" src="${pageContext.request.contextPath}/aiaas/common/images/img_jp2.png" alt="10000엔" />
                            </em>
                        </label>
                    </div>


                </div>
            </div>
        </div>
        <!-- //.pop_bd -->
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_common -->


<!-- .contents -->
<div class="contents">
    <div class="content api_content">
        <h1 class="api_tit">문서 이미지 분석</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'recogdemo')" id="defaultOpen"><button type="button">데모</button></li>
            <li class="tablinks" onclick="openTap(event, 'recogexample')"><button type="button">적용사례</button></li>
            <li class="tablinks" onclick="openTap(event, 'recogmenu')"><button type="button">매뉴얼</button></li>
            <%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <div class="demobox" id="recogdemo">
            <p>딥러닝을 통한 문서 이미지 분석 및 인식 플랫폼 <span>DIARL</span><small>(Document Image Analysis, Recognition and Learning)</small></p>
            <span class="sub">각 나라 별 화폐 이미지를 활용하여 정보를 분석해줍니다.</span>
            <!--이미지인식box-->
            <div class="demo_layout pattern_box">
                <div class="pattern_1" >
                    <div class="fl_box" id="opacity">
                        <p><em class="far fa-file-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="sample" value="화폐" checked>
                                    <label for="sample1" class="">
                                        <em class="img_area">
                                            <img id="sampleImg_1" src="${pageContext.request.contextPath}/aiaas/kr/images/img_diarl_sample1.jpg" alt="화폐 샘플 이미지" />
                                        </em>
                                        <span>화폐</span>
                                    </label>
                                    <button class="sample_more"><em class="fas fa-plus-circle"></em>&nbsp;샘플 더보기</button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-image"></em><strong>내 파일</strong>로 해보기</p>
                        <div class="uplode_box">
                            <%--                            <button type="button" class="upload_pop">이미지 업로드</button>--%>
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-image hidden"></em>
                                <label for="demoFile" class="demolabel">이미지 업로드</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".jpg, .png" >
                            </div>
                            <p class="img_desc"><em class="fas fa-exclamation"></em> 이미지 업로드 시 유의사항</p>
                            <div>
                                <span><em>화폐</em> : 5개국 (한국, 중국, 일본, 미국, 유로)화폐<br>직접 찍은 사진 권고 </span>
<%--                                <span><em>고지서</em> : 일반지로요금(OCR), KT 통신요금,전기요금,<br>4대보험요금, 각종 서울특별시 지방세 고지서에 한함 </span>
                                <span><em>병원 진료비 계산서 ·영수증 </em> : 상급 종합병원에서 보편적으로 쓰이는 영수증 형식 권고</span>--%>
                            </div>


                            <ul>
                                <li>* 지원가능 파일 확장자: .jpg, .png</li>
                                <li>* 이미지 용량 2MB 이하만 가능합니다.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_start editImgBtn" id="editImgBtn">분석하기</button>
                    </div>
                </div>
                <!--pattern_2-->
                <div class="pattern_2" >
                    <p><em class="far fa-file-image"></em>이미지 편집</p>
                    <p class="desc">이미지를 '여백 없이' 맞춰주세요.</p>
                    <div class="img_box">
                        <em class="fas fa-minus minus"></em>
                        <em class="fas fa-plus plus"></em>
                        <!--불러온 이미지 들어갈 곳-->
                        <img src="" alt="불러온 이미지" id="previewImg">
                        <div id="rotate_left" class="redobox">
                            <em class="fas fa-undo-alt"></em>
                            <span>회전</span>
                        </div>
                    </div>
                    <p class="desctxt">확대, 축소, 회전 버튼과 마우스 움직임을 통해 이미지를 편집할 수 있습니다.</p>
                    <span>예시) 아래 이미지와 동일한 위치에 맞춰주세요.</span>
                    <div class="bill_sample">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_cropsample_big.png" alt="고지서 위치 샘플 이미지" />
                    </div>
                    <div class="hospital_sample">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_cropsample_medical.png" alt="진료비 영수증 위치 샘플 이미지" />
                    </div>
                    <div class="money_sample">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_cropsample_money.png" alt="화폐 위치 샘플 이미지" />
                    </div>
                    <div class="btn">
                        <a id="editCancel">취소</a>
                        <a class="active" id="recogButton">결과보기</a>
                        <img id="loading_img" class="loading_img" src="${pageContext.request.contextPath}/aiaas/kr/images/loading.gif" alt="loading">
                    </div>
                </div>
                <!--pattern_2-->
                <!--loding_area-->
                <div class="loding_area">
                    <p><em class="far fa-file-image"></em>이미지 인식 및 분석 처리중</p>
                    <div class="loding_box ">
                        <!-- ball-scale-rotate -->
                        <div class="lds">
                            <div class="ball-scale-rotate">
                                <span> </span>
                                <span> </span>
                                <span> </span>
                            </div>
                        </div>
                        <p>약간의 시간이 소요 됩니다. (약 30초 내외)</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1" id="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--loding_area-->
                <!--pattern_3-->
                <div class="pattern_3">
                    <div class="img_box">
                        <p><em class="far fa-file-image"></em>이미지 인식 및 분석 처리 결과</p>
                        <!--결과 이미지 들어갈 곳-->
                        <img id="resultImg" src="" alt="결과 이미지" >
                    </div>
                    <div class="resultBox" >
                        <div class="txtBox" id="note_table">
                            <dl class="dltype02">
                                <dt>분류</dt>
                                <dd>화폐</dd>
                            </dl>
                            <dl class="dltype02">
                                <dt>통화명</dt>
                                <dd id="currency"></dd>
                            </dl>
                            <dl class="dltype02">
                                <dt>국가명</dt>
                                <dd id="country"></dd>
                            </dl>
                            <dl class="dltype02">
                                <dt>금액</dt>
                                <dd id="amount"></dd>
                            </dl>
                        </div>

                        <div class="btn_area">
                            <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                        </div>
                    </div>



                    <!--						인식하지 못했을 때 style="display: block;" 해두세용-->
                    <div class="resultBox_fail" style="display: none;">
                        <div class="no_recog">인식할 수 없는 이미지 입니다.</div>
                        <div class="btn_area">
                            <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                        </div>
                    </div>
                </div>
                <!--pattern_3-->
            </div>

        </div>
        <!-- .demobox -->

        <!--.recogmenu-->
        <div class="demobox vision_menu" id="recogmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

                </div>
                <div class="guide_group">
                    <div class="title">
                        DIAR <small>(Document Image Analysis, Recognition and Learning)</small>
                    </div>
                    <p class="sub_txt">이미지 내 텍스트를 인식합니다. 현재 API는 병원 영수증 인식만 제공합니다.</p>

                    <span class="sub_title">
								준비사항
					</span>
                    <p class="sub_txt">- Input: 병원 영수증 이미지 파일 </p>
                    <ul>
                        <li>확장자: .jpg, .png.</li>
                        <li>용량: 2MB 이하 </li>
                    </ul>
                    <span class="sub_title">
								 실행 가이드
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/ocr/hospitalReceipt</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>사용자의 고유 ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>receipt_img</td>
                            <td>type:file (.jpg,.png)  </td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        url --location --request POST 'http://api.maum.ai/ocr/hospitalReceipt' \<br>
                        --header 'Content-Type: application/x-www-form-urlencoded' \<br>
                        --form 'apiId= 발급받은 API ID' \<br>
                        --form 'apiKey= 발급받은 API KEY' \<br>
                        --form 'receipt_img= 영수증 이미지 파일'<br>
                    </div>

                    <p class="sub_txt">④ Response 예제 </p>

                    <div class="code_box">
<pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "result_img": {
        "name": "ori_4_rst.jpg",
        "data": "MzAzMjAs7JW97ZKI67mELCwsLC...LOy5mOujjOyerOujjOuMgCwsLCwsLA0K"
    },
    "result_csv": {
        "name": "ori_4_output.csv",
        "data": "MzAzMjAs7JW97ZKI67mELCwsLC...LOy5mOujjOyerOujjOuMgCwsLCwsLA0K"
    }
}
</pre>
                    </div>


                </div>
            </div>

        </div>
        <!--//recogmenu-->
        <!--.recogexample-->
        <div class="demobox" id="recogexample">
            <p><em style="color:#f7778a;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

            <!-- //화폐, 고지서 인식(DIARL) -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>화폐 인식</span>
                            </dt>
                            <dd class="txt">전세계 각국의 화폐를 자동으로 인식하고, 환율 조회까지 제공 가능한 부가서비스를 구현하는 데 활용할 수 있습니다.
                                <span><em class="fas fa-book-reader"></em> Reference: A은행 </span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_dir"><span>DIARL</span></li>
                                    <li class="ico_bot"><span>챗봇</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>고지서 인식</span>
                            </dt>
                            <dd class="txt">고지서, 영수증 등을 자동으로 인식하고 자동 납부까지 이뤄지는 서비스를 만드는 데 활용할 수 있습니다.
                                <span><em class="fas fa-book-reader"></em> Reference: A은행</span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_dir"><span>DIARL</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>병원영수증 인식</span>
                            </dt>
                            <dd class="txt">보험금 청구를 위해 첨부하는 병원 영수증을 자동으로 인식하여 금액들을 추출해주는 시스템입니다.
                                <span><em class="fas fa-book-reader"></em> Reference: A보험사</span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_dir"><span>DIARL</span></li>
                                    <li class="ico_dataAnaly"><span>데이터 분석</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //화폐 인식(DIARL) -->
        </div>
        <!--//.recogexample-->

    </div>
</div>
<!-- //.contents -->

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/idr/croppie.js?ver=<%=fmt.format(lastModifiedStyle)%>"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/idr/recog.js?ver=20200203"></script>

<script>

    var sample1File;
    var sample1SrcUrl;
    let image1_blob;

    jQuery.event.add(window,"load",function(){

        $(document).ready(function (){
            loadSample1();

            $('.sample_more').on('click', function () {
                $('.sample_pop').fadeIn(300);
            });

            $('.sample_pop .pop_bd .country .option_box .radio').on('click', function () {
                $('.pop_2btn').fadeIn(300);
            });


            // 공통 팝업창 닫기
            $('.pop_sample_close, .pop_bg').on('click', function () {
                $('.pop_common').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });

            $('.btn_gray, .minipop_close').on('click', function () {
                $('.pop_2btn').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });

            //샘플 화폐 선택하기 버튼
            $('.btn_blue').on('click', function () {
                var option = $("input[type=radio]:checked").val();
                var imgget = $('#'+option).attr('src');
                $('.pop_common').fadeOut(300);
                $('.pop_2btn').fadeOut(300);
                $('#sampleImg_1').trigger("click");
                $('#sampleImg_1').attr('src', imgget);
                loadSample1();
                $('body').css({
                    'overflow': '',
                });
            });

            // Remove uploaded image
            $('em.close').on('click', function () {
                $('#demoFile').val(null);
                $(this).parent().removeClass("btn_change");
                $(this).parent().addClass("btn");
                $('.fl_box').css('opacity', '1');
                $('.upload_popup .btn_area').hide();
            });

            $('em.pop_sample_close').on('click', function () {
                console.log('팝업창 닫기')
                // $('#demoFile').val(null);
                $(this).parent().removeClass("btn_change");
                $(this).parent().addClass("btn");
                $('.fl_box').css('opacity', '1');
                $('.upload_popup .btn_area').hide();

            });

            // 파일 업로드 후 이벤트
            document.querySelector("#demoFile").addEventListener('change', function (ev) {

                let uploadedFile = document.getElementById("demoFile").files[0];

                console.log(uploadedFile);

                if(uploadedFile != null){
                    if (uploadedFile.type.match(/image.*/)){
                        $('input[type="radio"]:checked').prop("checked", true);

                        document.querySelector(".demolabel").innerHTML = uploadedFile.name;
                        // var url = URL.createObjectURL(this.files[0]);

                        var element = document.getElementById( 'uploadFile' );
                        element.classList.remove( 'btn' );
                        element.classList.add( 'btn_change' );

                        $('.fl_box').css("opacity", "0.5");
                        var btn = $('.upload_popup .pop_bd .btn_area');
                        btn.fadeIn(300);

                    }
                    else{
                        this.value = null;
                        alert('이미지 파일을 업로드하세요.');
                    }
                }
            });

            // 샘플 선택 후 인식하기 누를 때
            $('#editImgBtn').on('click', function () {

                let $demoFile = $('#demoFile').get(0).files[0];

                if($demoFile != null && $demoFile != ""){
                    sample1SrcUrl  = URL.createObjectURL($demoFile);
                    $('input[name="sample"]').prop('checked',false);

                    $('#previewImg').attr('src', $demoFile);

                    $('em.close').trigger('click');
                    $('.upload_popup .btn_area').hide();

                }

                activateCroppie();

                $('.desc').removeClass('desc_3');
                $('.money_sample').fadeIn();

                $('.pattern_1').hide();
                $('.pattern_2').fadeIn(300);
            });
        });


        function loadSample1() {
            let blob = null;
            let xhr = new XMLHttpRequest();
            let imgsrc = $('#sampleImg_1').attr('src');

            xhr.open("GET", imgsrc);
            xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
            xhr.onload = function()
            {
                blob = xhr.response;//xhr.response is now a blob object

                sample1SrcUrl = URL.createObjectURL(blob);
                image1_blob = blob;
            };

            xhr.send();
        }
        $('#recogButton').on('click', function () {
            $("html").scrollTop(0);
            $cropper.croppie('result',{
                type: 'blob',
                size: 'original' //original or viewport
            }).then(function(blob){
                console.log(blob);

                var url = URL.createObjectURL(blob);
                $('#resultImg').attr('src', url);

                var file = new Blob([blob], { type: blob.type });

                sendRequest(file);

                $('.pattern_2').hide();
                $('.loding_area').fadeIn(300);
            });
        });
    });

    function sendRequest(file){
        console.dir(file);

        var formData = new FormData();
        formData.append('file', file);
        formData.append($("#key").val(), $("#value").val());

        console.dir(formData);
        req = $.ajax({
            type: "POST",
            async: true,
            url: "${pageContext.request.contextPath}/api/idr/callRecog",
            data: formData,
            processData: false,
            contentType: false,
            timeout: 20000,
            success: function(stringData){

                console.dir(stringData);

                let resultData = JSON.parse(stringData);
                let status = resultData.message.message;
                let currency = resultData.data.currency;
                let amount = resultData.data.amount;

                if(status == "Success") {

                    showNote(currency, amount);

                    $('.demo_layout ').css("border", "none");
                    $('.loding_area').hide();
                    $('.pattern_3').fadeIn(300);
                }
            },
            error: function(error){
                if(error.statusText === "abort"){
                    return;
                }
                $('.demo_layout ').css("border", "none");
                $('.loding_area').hide();
                $('.pattern_3').fadeIn(300);
                $('.pattern_3 .resultBox_fail').fadeIn(300);
                $('.resultBox').hide();
                $('#bill_table').hide();
                $('#note_table').hide();

                alert("Error");
                console.error(error);
                window.location.reload();
            }

        });

    }

    function showNote(currency, amount){
        $('#country').text(currency);
        $('#currency').text(currency);
        $('#amount').text(amount);
    }

</script>