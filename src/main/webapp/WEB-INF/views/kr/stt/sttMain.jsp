<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<!--5MB 이상의 파일 업로드 시 안내 팝업-->
<!-- .lyr_file -->
<div class="lyr_lack" id="lyr_file">
	<div class="lyr_bg_notice"></div>
	<!-- .lyrWrap -->
	<div class="lyrWrap">
		<button class="btn_lyrWrap_close" type="button" onclick="popup_cancel();">제품 리스트 닫기</button>
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<h4><em class="fas fa-file-audio"></em></h4>
			<p class="notice_desc"><strong>5MB 이하의 파일만 지원합니다.</strong>
				다른 파일을 업로드 해주세요. </p>
			<a class="btn_cancel" onclick="popup_cancel();">닫기</a>
<%--			<a href="#" class="btn_upgrade">확인</a>--%>
		</div>
		<!-- //.lyr_bd -->
	</div>
	<!-- //.lyrWrap -->
</div>
<!-- //.lyr_file -->

<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">음성인식</h1>
		<ul class="menu_lst voice_menulst">
			<li class="tablinks" onclick="openTap(event, 'sttdemo')" id="defaultOpen"><button type="button">데모</button></li>
			<li class="tablinks" onclick="openTap(event, 'sttexample')"><button type="button">적용사례</button></li>
			<li class="tablinks" onclick="openTap(event, 'sttmenu')"><button type="button">매뉴얼</button></li>
<%--			<li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
		</ul>
		<div class="demobox sttdemo apisttdemo" id="sttdemo">
			<p>정확하고 강력한 <em>음성인식</em> <small>(Speech Recognition)</small></p>
			<span class="sub">음성을 텍스트로 빠르고 정확하게 변환합니다. </span>

			<div class="stt_demo">
				<!-- #demo04 -->
				<div id="demo04" class="tab_contents">

					<!-- .rightBox -->
					<div class="rightBox">
						<div class="selectarea">

							<div class="radio" name="level" id="level">
								<input type="radio" id="1" value="kor_baseline" name="voice" checked="checked">
								<label for="1"><span>한국어</span></label>
							</div>
<%--							<div class="radio">
								<input type="radio" id="2" value="kor_address" name="voice">
								<label for="2"><span>한국어 주소</span></label>
							</div>
							<div class="radio">
								<input type="radio" id="3" value="eng_baseline" name="voice">
								<label for="3"><span>영어</span></label>
							</div>
							<div class="radio minipopbtn">
								<em class="fas fa-plus-circle"></em>
								<span>more</span>
                                <strong class="minipop">한국어, 영어 콜센터 전용, 영어 교육용 음성인식 api는 별도 문의 바랍니다. <br>(hello@mindslab.ai)</strong>
							</div>--%>

						</div>
						<!-- .step01 -->
						<div class="demo_intro">
							<div class="demo_fileTxt">인식하고자 하는 음성파일을 업로드 해 주세요.</div>
							<div class="demo_recordTxt">음성을 듣고 있습니다.<br><strong>완료 버튼</strong>을 클릭하시면 변환을 시작합니다.</div>
							<div class="btnupload">
								<label for="demoFile"><span><em class="fas fa-file-upload"></em>파일 업로드</span></label>
								<input type="file" id="demoFile" class="demoFile" onchange="checkSize(this)">
							</div>
<%--							<div class="btnBox">
								<span><em>녹음</em></span>
							</div>--%>
							<div class="btnreset">
								<button class="demoReset"><span>다시하기</span></button>
								<button class="demoStop"><span>완료</span></button>
							</div>
<%--							<div class="holeBox">--%>
<%--								<div class="hole">--%>
<%--									<em></em><em></em><em></em><em></em><em></em>--%>
<%--								</div>--%>
<%--							</div>--%>
						</div>
						<!-- //.step01 -->
						<!-- .step03 -->
						<div class="demo_recording">
							<div class="demo_infoTxt">파일을 변환 중입니다. <br>잠시만 기다려 주세요.</div>
							<!-- recording -->
							<div class="recording">
								<span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
								<span></span><span></span><span></span><span></span><span></span><span></span><span></span>
							</div>
							<!-- //recording -->
						</div>
						<!-- //.step03 -->
						<!-- .step04 -->
						<div class="demo_result">
							<div class="resultTxt" id="demo_result_text">
							</div>
							<div class="btnBox btnReset">
								<button class="demoReset"><span>다시하기</span></button>
							</div>
<%--							<div class="btnBox btndwn">
								<button class="dwn"><a id="save"><span>다운로드</span></a></button>
							</div>--%>
						</div>
						<!-- //.step04 -->

					</div>
					<!-- //.rightBox -->

				</div>
				<!-- //#demo04 -->


				<div class="remark">
					*지원가능한 파일 확장자 : .wav, .pcm <br>
					*5MB 이하의 음성 파일을 이용해 주세요.<br>
					*한국어, 영어 외 다른 언어가 필요하시면, 고객지원센터로 연락 주세요.
				</div>
			</div>
		</div>

		<!--.sttmenu-->
		<div class="demobox voice_menu" id="sttmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API 공통 가이드
					</div>
					<p class="sub_title">개발 환경 세팅</p>
					<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
					<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

				</div>
				<div class="guide_group">
					<div class="title">
						STT <small>(Speech-To-Text)</small>
					</div>
					<p class="sub_txt">STT API는 다양한 환경의 음성을 텍스트로 변환시켜주는 API입니다.</p>

					<span class="sub_title">
							준비사항
						</span>
					<p class="sub_txt">① Input: 음성파일 (.wav/.pcm/.mp3)  </p>
					<p class="sub_txt">② 아래 Model 중 택 1  </p>
					<ul>
						<li>한글 고객센터용 8K (baseline, kor, 8000)</li>
						<li>한글 일반용 16K (baseline, kor, 16000)</li>
						<li>영어 고객센터용 8K (baseline, eng, 8000)</li>
						<li>영어 일반용 16K (baseline, eng, 16000)</li>
						<li>한글 주소인식 (address, kor, 8000)</li>
					</ul>
					<span class="sub_title">
							 실행 가이드
						</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/stt/</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey</td>
							<td>사용자의 고유 key. </td>
							<td>string</td>
						</tr>
						<%--<tr>
							<td>cmd</td>
							<td>어떤 API 를 사용할지에 대한 간단한 키워드 (runFileStt) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>lang</td>
							<td>사용할 모델의 언어 ( kor / eng ) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>sampling</td>
							<td>사용할 모델의 sampling rate (8000 / 16000) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>level</td>
							<td>사용할 모델의 level (baseline / address / kor_accent /  mindsedu_t / mindsedu_s )</td>
							<td>string</td>
						</tr>--%>
						<tr>
							<td>file</td>
							<td>type:file (wav, pcm, mp3) 음성파일 </td>
							<td>string</td>
						</tr>

					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
						curl -X POST \<br>
						https://api.maum.ai/api//stt/cnnStt \<br>
						-H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \<br>
						-F apiId= (*ID 요청 필요) \<br>
						-F apiKey= (*key 요청 필요) \<br>
						<%---F lang=eng \<br>
						-F sampling=8000 \<br>
						-F level=baseline \<br>
						-F cmd=runFileStt<br>--%>
						-F 'file=@eng_8k_1.wav' \<br>

					</div>

					<p class="sub_txt">④ Response 파라미터 설명 </p>
					<span class="table_tit">Response</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>status</td>
							<td>API 동작여부 (Success / Fail) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>result</td>
							<td>인식한 text 결과</td>
							<td>string</td>
						</tr>
					</table>
					<%--<span class="table_tit">extra_data: 음성파일에 대한 데이터</span>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>타입</th>
						</tr>
						<tr>
							<td>stt_length</td>
							<td>입력된 글자 수  </td>
							<td>int</td>
						</tr>
						<tr>
							<td>stt_data</td>
							<td>STT 변환 결과</td>
							<td>string</td>
						</tr>
						<tr>
							<td>stt_duration</td>
							<td>음성파일 길이(초) </td>
							<td>number</td>
						</tr>
					</table>--%>
					<p class="sub_txt">⑤ Response 예제 </p>
					<div class="code_box">
						{ <br>
						<span> "status": "Success", </span><br>
						<span>"extra_data": { </span><br>
						<span class="twoD">"stt_data": "Let's play badminton.\n",</span> <br>
						<span class="twoD">"stt_duration": "2.387" </span><br>
						<span>}, </span><br>
						<span> "data": "Let's play badminton.\n" </span><br>
						}

					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--//sttmenu-->
		<!--.sttexample-->
		<div class="demobox" id="sttexample">
			<p><em style="font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>콜센터 분석</span>
							</dt>
							<dd class="txt">콜센터에서 진행된 고객과의 상담 전화를 텍스트로 옮기고 상담 내용을 분석할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_stt"><span>STT</span></li>
<%--									<li class="ico_dia"><span>화자 분리</span></li>--%>
									<li class="ico_nlu"><span>자연어 이해</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>고객 주소 관리</span>
							</dt>
							<dd class="txt">음성으로 수집한 고객이나 거래처 등의 주소 정보를 텍스트로 옮겨 체계적으로 관리할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_stt"><span>STT</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 03</em>
								<span>인터뷰/면담</span>
							</dt>
							<dd class="txt">녹음파일로 보유하고 있는 인터뷰나 면담, 대화 내용 등을 텍스트로 변환하여 보관할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_stt"><span>STT</span></li>
<%--									<li class="ico_dia"><span>화자 분리</span></li>--%>
									<li class="ico_vf"><span>Voice Filter</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 04</em>
								<span>강의/강연</span>
							</dt>
							<dd class="txt">녹음파일로 보유하고 있는 강의나 강연 등의 내용을 텍스트로 변환하여 보관할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_stt"><span>STT</span></li>
									<li class="ico_den"><span>음성 정제</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //음성 생성(STT) -->
		</div>

	</div>

</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/api_stt.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/app.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/particles.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/recorder.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/bootstrap-datepicker.js"></script>


<script type="text/javascript">

	var timeLimit_trigger=1;
	var trigger_demostop;

	//Expose globally your audio_context, the recorder instance and audio_stream
	var audio_context;
	var recorder;
	var audio_stream;
	var modelStr = '';

	function replaceAll(strTemp, strValue1, strValue2){
		while (1) {
			if (strTemp.indexOf(strValue1) != -1)
				strTemp = strTemp.replace(strValue1, strValue2);
			else
				break;
		}
		return strTemp;
	}

	function downloadText(){
		var test = $('#demo_result_text').text();
		var data = new Blob([test]);
		var link = document.getElementById("save");
		link.href = URL.createObjectURL(data);
		link.download = "STT.txt" ;
	}

	function upload_file_submit(){
		var formData = new FormData();
		console.log("==file_submit==\nmodel: " + $('input:radio[name="voice"]:checked').val());
		var model = $('input:radio[name="voice"]:checked').val().split('_')
		var txtlang = model[0];
		var txtlevel = model[1];
		var txtsampleRate = "8000";
		if(txtlevel=="baseline" && txtlang=="kor"){
			txtsampleRate = "16000";
		}
		formData.append('file', document.getElementById('demoFile').files[0]);
		formData.append('${_csrf.parameterName}', '${_csrf.token}');

		var request = new XMLHttpRequest();
		request.onreadystatechange = function(){
			if (request.readyState === 4){
				console.log(request.response);

				var jsonResult = JSON.parse(request.response)['result'];
				$('.demo_result .resultTxt').text(jsonResult);

				//downloadText();

				$('.rightBox .demo_recording').hide();
				$('.rightBox .demo_result').show();
				$('.demo_result div').show();
			}
		};
		request.timeout = 20000;
		request.ontimeout = function() {
			alert('Timeout');
			window.location.reload();
		}
		request.open('POST', '${pageContext.request.contextPath}/api/stt');
		request.send(formData);
	}

	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){
			$('.demo_recording').hide();
			$('.demo_result').hide();
			$('.holeBox').hide();

			$('.minipopbtn').on('click', function(){
				if($('.minipop').is(':visible')){
					$('.minipop').hide();
				}else{
					$('.minipop').show();
				}
			});

			// 리셋 버튼 누를때
			$('.demo_result button.demoReset').on('click', function(){
				timeLimit_trigger = 1;
				$('#demoFile').val("");

				$('#demo04 .demo_fileTxt').removeClass('errorTxt').html('인식하고자 하는 음성파일을 업로드 해 주세요.');
				$('.rightBox .demo_result').hide();
                $('.demo_intro .demo_recordTxt').hide();
                
				$('.stt_demo .rightBox .selectarea').show();
				$('.rightBox .demo_intro').show();
				$('.rightBox .demo_intro div').show();
				$('.rightBox .demo_intro .demo_recordTxt').hide();
				$('.rightBox .demo_intro .btnreset').hide();
				$('.rightBox .demo_intro .holeBox').hide();
			});

			// Demo 파일업로드
			$('#demoFile').on('change', function() {
				console.log("model: " + $('input:radio[name="voice"]:checked').val());
				var fileTxt = $('.demoFile').val(); //파일을 추가한 input 박스의 값
				var fneTxt = fileTxt.split(".").pop(-1).toLowerCase();

				if ( fneTxt != "mp3" && fneTxt != "wav" && fneTxt != "pcm") {// 음성파일 체크
					// 실패
					$('#demo04 .demo_fileTxt').addClass('errorTxt').html('잘못된 파일입니다. mp3, wav, pcm 파일을 넣어주세요.');
					$('.rightBox .demo_intro .demo_recordTxt').hide();
					$('.rightBox .demo_intro .btnreset').hide();
					$('.rightBox .demo_intro .holeBox').hide();

				} else {
					// 성공
					upload_file_submit();

					$('.stt_demo .rightBox .demo_intro').hide();
					$('.stt_demo .rightBox .selectarea').hide();
					$('.stt_demo .rightBox .demo_recording').show();
				}
			});

			// Demo 음성
			// step01 > step02
			$('.demo_intro .btnBox').on('click', function(){

				audio_context = new AudioContext;
				// Access the Microphone using the navigator.getUserMedia method to obtain a stream
				var promise = navigator.mediaDevices.getUserMedia({ audio: true })
						.then(function (stream) {
// 				console.log("진입");
							// Expose the stream to be accessible globally
							audio_stream = stream;
// 				console.log("스트림객체");
							// Create the MediaStreamSource for the Recorder library
							var input = audio_context.createMediaStreamSource(stream);
// 				console.log('Media stream succesfully created');
							var txtmodel = $("input:radio[name='voice']:checked").val();

							var txtsampleRate = 8000;
							if(txtmodel == "kor_baseline"){
								txtsampleRate = 16000;
							}

							// Initialize the Recorder Library
							//recorder = new Recorder(input, {sampleRate: modelStr.split('-')[2]});
							recorder = new Recorder(input, {sampleRate: txtsampleRate});
// 				console.log('Recorder initialised');

							// Start recording !
							recorder && recorder.record();
// 					console.log('Recording...');

							$('.rightBox .selectarea').hide();
							$('.rightBox .demo_intro').show();
							$('.rightBox .demo_intro div').hide();
							$('.rightBox .demo_intro .demo_recordTxt').fadeIn(200);
							$('.rightBox .demo_intro .btnreset').show();
							$('.rightBox .demo_intro .btnreset .demoReset').hide();
							$('.rightBox .demo_intro .btnreset .demoStop').show();
							$('.rightBox .demo_intro .holeBox').fadeIn(200);

							//녹음 시간 제한
							trigger_demostop = setTimeout(function (){
								if(timeLimit_trigger==1) {
									$('.demoStop').trigger('click');
								}
							}, 9000);


						}).catch(function (e) {
							// 사용자 마이크 연결 안 됐을 때
							alert('마이크가 연결 되어있지 않습니다. 마이크를 연결하시고 다시 시도하여 주시기 바랍니다.');
							// $(this).parent().parent().find('.demo_infoTxt').addClass('errorTxt').text('마이크가 연결 되어있지 않습니다. 마이크를 연결하시고 다시 시도하여 주시기 바랍니다.');
// 				console.error('No live audio input: ' + e);
						});
			});

			// step02 > step03
			$('.demoStop').on('click', function(){
				timeLimit_trigger = 0;
				clearTimeout(trigger_demostop);		//완료 누르면 timeout 중지

				var _AudioFormat = "audio/pcm";
				stopRecording(function(AudioBLOB){
					// Note:
					// Use the AudioBLOB for whatever you need, to download
					// directly in the browser, to upload to the server, you name it !

					// In this case we are going to add an Audio item to the list so you
					// can play every stored Audio
					var url = URL.createObjectURL(AudioBLOB);
					var li = document.createElement('li');
					var au = document.createElement('audio');
					var hf = document.createElement('a');

					au.controls = true;
					au.src = url;
					hf.href = url;
					// Important:
					// Change the format of the file according to the mimetype
					// e.g for audio/wav the extension is .wav
					//     for audio/mpeg (mp3) the extension is .mp3

					// hf.download = new Date().toISOString() + '.pcm';
					// hf.innerHTML = hf.download;
					// li.appendChild(au);
					// li.appendChild(hf);
					// recordingslist.appendChild(li);


					// stop 버튼을 누르는 순간 submit 하도록 바꾸자.....
					post_submit(AudioBLOB, modelStr);

				}, _AudioFormat);

				$('.rightBox .demo_intro').hide();
				$('.rightBox .demo_recording').show();
				$('.progress li:nth-child(2)').addClass('active');
				$('.rightBox .demo_infoTxt').removeClass('errorTxt');
			});

			(function() {
				// strict mode
				"use strict";
				// configurable options that determine particle behavior and appearance
				var options = {
							// connector line options
							connector: {
								// color of line
								color: "white",
								// base line width of each connector
								lineWidth: 0.5
							},
							// individual particle options
							particle: {
								// base color
								color: "white",
								// number of particles to render onto stage
								count: 75,
								// size of particle influence when drawing connectors (in pixels)
								influence: 75,
								// a range of sizes for an individual particle (falls within max-min)
								sizeRange: {
									min: 1,
									max: 5
								},
								// a range of velocities for each particle to inherit (falls within max-min)
								velocityRange: {
									min: 10,
									max: 60
								}
							}
						},
						particles,
						stage;
				// sets the render loop state based on page's visibility
				function handleDocumentVisibilityChange() {
					document.hidden ?
							stage.render.pause() :
							stage.render.resume();
				}

				function initPage() {
					initObjects();
					initListeners();
				}
				// invoked when page has loaded
				// registers window and document level event listeners
				// required post object instantiation
				function initListeners() {
					// resize canvas on window resize
					window.addEventListener(
							"resize",
							stage.resize);
					// pause and resume rendering loop when visibility state is changed
					document.addEventListener(
							"visibilitychange",
							handleDocumentVisibilityChange);
				}
				// invoked when page has loaded
				// performs setup of stage and particles
				// triggers the rendering loop
				function initObjects() {
					particles = new ParticleGroup();
					// create our stage instance
					stage = new Stage(document.querySelector("canvas"));
					// init the canvas bounds and fidelity
					stage.resize();
					// populate particle group collection
					particles.populate();
					// begin stage rendering with the renderAll draw routine on each tick
					stage.render.begin(
							particles.render.bind(particles));
				}
				// renders an FPS value to the canvas top-left corner
				function renderFPS(value) {
					context.save();
					context.font = 10 * stage.pixelRatio + "px sans-serif";
					context.fillStyle = "white";
					context.textBaseline = "top";
					context.fillText(value, 5 * stage.pixelRatio, 5 * stage.pixelRatio);
					context.restore();
				}
				// Paricle object construct:
				// stores critical particle information regarding trajectory and location
				// 		v: distance delta coefficient
				// 		t: angular theta in degrees
				//		r: particle radius
				//		p: position on stage, x:y coordinate properties
				// 		influence: radial influence of connectivity to other particles
				// defines methods relating to particle rendering and comparison to other particle locations
				function Particle(props) {
					// private particle properties
					var x = props.x || 0,
							y = props.y || 0,
							v = props.speed,
							t = props.theta,
							r = props.radius,
							influence = props.influence,
							color = props.color;
					// public properties and methods
					return {
						// position property getter: read-only
						get x() {
							return x;
						},
						get y() {
							return y;
						},
						// compares this particle location to another
						// returns a coefficient describing the ratio of closeness to particle influence
						influencedBy: function(a, b) {
							var hyp = Math.sqrt(Math.pow(a - x, 2) + Math.pow(b - y, 2));
							return Math.abs(hyp) <= influence ?
									hyp / influence : 0;
						},
						// render this particle to a given context
						render: function(ctx) {
							ctx.strokeStyle = color;
							ctx.lineWidth = 1 / r * r * stage.pixelRatio;
							ctx.beginPath();
							ctx.arc(x || 10, y || 10, r, 0, 8);
							ctx.stroke();
						},
						// sets the position of this particle compared to its previous position
						// in relation to a total time delta since last positioning
						// positions infinitely loop within canvas bounds
						setPosition: function(timeDelta) {
							x += timeDelta / v * Math.cos(t);
							x = x > stage.width + r ? 0 - r : x;
							x = x < 0 - r ? stage.width + r : x;
							y += timeDelta / v / 2 * Math.sin(t);
							y = y > stage.height + r ? 0 - r : y;
							y = y < 0 - r ? stage.height + r : y;
						}
					}
				}
				// ParticleGroup object construct
				// an object which controls higher level methods to create, destroy and compare
				// a grouping of Particle instances. Holds a private collection of all existing particles
				function ParticleGroup() {
					// particle instance array
					var _collection = [],
							// cache connector option property values
							_connectColor = options.connector.color,
							_connectWidth = options.connector.lineWidth,
							_particleCount = options.particle.count;
					// generates and returns a new particle instance with
					// randomized properties within ranges defined in the options object
					function _generateNewParticle() {
						return new Particle({
							x: stage.width * Math.random(),
							y: stage.height * Math.random(),
							speed: getRandomWithinRange(options.particle.velocityRange) / stage.pixelRatio,
							radius: getRandomWithinRange(options.particle.sizeRange),
							theta: Math.round(Math.random() * 360),
							influence: options.particle.influence * stage.pixelRatio,
							color: options.particle.color
						});
						// random number generator within a given range object defined by a max and min property
						function getRandomWithinRange(range) {
							return ((range.max - range.min) * Math.random() + range.min) * stage.pixelRatio;
						}
					}
					// loops through particle collection
					// sets the new particle location given a time delta
					// queries other particles to see if a connection between particles should be rendered
					function _checkForNeighboringParticles(ctx, p1) {
						// cache particle influence method
						var getInfluenceCoeff = p1.influencedBy;
						// particle collection iterator
						for (var i = 0, p2, d; i < _particleCount; i++) {
							p2 = _collection[i];
							// conditional checks - ignore if p2 is the same as p1
							// or if p2 has already been iterated through to check for neighbors
							if (p1 !== p2 && !p2.checked) {
								// compare the distance delta between the two particles
								d = getInfluenceCoeff(p2.x, p2.y);
								// render the connector if coefficient is non-zero
								if (d) {
									_connectParticles(ctx, p1.x, p1.y, p2.x, p2.y, d);
								}
							}
						}
					}
					// given two particles and an influence coefficient between them,
					// renders a connecting line between the two particles
					// the coefficient determines the opacity (strength) of the connection
					function _connectParticles(ctx, x1, y1, x2, y2, d) {
						ctx.save();
						ctx.globalAlpha = 1 - d;
						ctx.strokeStyle = _connectColor;
						ctx.lineWidth = _connectWidth * (1 - d) * stage.pixelRatio;
						ctx.beginPath();
						ctx.moveTo(x1, y1);
						ctx.lineTo(x2, y2);
						ctx.stroke();
						ctx.restore();
					}
					// public object
					return {
						// returns the size of the collection
						get length() {
							return _collection.length;
						},
						// adds a particle object instance to the collection
						add: function(p) {
							_collection.push(p || _generateNewParticle());
						},
						// initial population of bar collection
						populate: function() {
							for (var i = 0; i < _particleCount; i++) {
								this.add();
							}
						},
						// loops through all particle instances within collection and
						// invokes instance rendering method on a given context at tick coefficient t
						render: function(ctx, t) {
							// loop through each particle, position it and render
							for (var i = 0, p; i < _particleCount; i++) {
								p = _collection[i];
								p.checked = false;
								p.setPosition(t);
								p.render(ctx);
							}
							// loop through each particle, check for connectors to be rendered with neighboring particles
							for (var i = 0, p; i < _particleCount; i++) {
								p = _collection[i];
								_checkForNeighboringParticles(ctx, p);
								p.checked = true;
							}
						}
					};
				}
				// Stage object construct
				// provides a structure to store a canvas and its context upon isntantiation
				// provides methods to interact with and adjust the canvas
				function Stage(canvasEl) {
					// canvas element reference
					var canvas = canvasEl instanceof Node ?
							canvasEl :
							document.querySelector(canvasEl),
							// 2d context reference
							context = canvas.getContext("2d"),
							// cache the device pixel ratio
							pixelRatio = window.devicePixelRatio;
					// resize the canvas initially to fit its containing element
					function _adjustCanvasBounds() {
						var parentSize = canvas.parentNode.getBoundingClientRect();
						canvas.width = parentSize.width;
						canvas.height = parentSize.height;
					}
					// updates the canvas dimensions relative to the device's pixel ratio for highest fidelity and accuracy
					function _adjustCanvasFidelity() {
						canvas.style.width = canvas.width + "px";
						canvas.style.height = canvas.height + "px";
						canvas.width *= pixelRatio;
						canvas.height *= pixelRatio;
					}
					// public object returned
					return {
						// dimension getters
						get height() {
							return canvas.height;
						},
						get width() {
							return canvas.width;
						},
						get pixelRatio() {
							return pixelRatio;
						},
						init: function(el) {
							canvas = el;
							context = canvas.getContext("2d");
						},
						resize: function() {
							_adjustCanvasBounds();
							_adjustCanvasFidelity();
						},
						render: (function() {
							// a flag indicating state of animation loop
							var paused = false,
									// animation request ID reported during loop
									requestID = null,
									// reference to a function detailing all draw operations per frame of animation
									renderMethod;
							// public methods
							return {
								// once invoked, creates a rendering loop which in turn invokes
								// drawMethod argument, passing along the delta (in milliseconds) since the last frame draw
								begin: function(drawMethod) {
									// cache the draw method to invoke per frame
									renderMethod = drawMethod;
									// a reference to requestAnimationFrame object
									// this should also utilize some vendor prefixing and a setTimeout fallback
									var requestFrame = window.requestAnimationFrame,
											// get initial start time
											latestTime, startTime = Date.now(),
											// during each interval, clear the canvas and invoke renderMethod
											intervalMethod = function(tick) {
												this.clear();
												renderMethod(
														context,
														tick);
											}.bind(this);
									// start animation loop
									(function loop() {
										// calculate tick time between frames
										var now = Date.now(),
												tick = now - latestTime || 1;
										// update latest time stamp
										latestTime = now;
										// report tick value to intervalCallback
										intervalMethod(tick);
										// loop iteration if no pause state is set
										requestID = paused ?
												null :
												requestAnimationFrame(loop);
									})();
								},
								// clears the stage's canvas
								clear: function() {
									context.clearRect(0, 0, canvas.width, canvas.height);
								},
								// pause the canvas rendering
								pause: function() {
									paused = true;
									cancelAnimationFrame(requestID);
								},
								// resumes the animation with a given rendering method
								resume: function() {
									paused = false;
									this.begin(renderMethod);
								}
							}
						}())
					};
				}

				// our init is triggered when the window is loaded/ready
				window.addEventListener(
						"load",
						initPage);
			}());



		});
	});


	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();


	//파일 크기 체크
	function checkSize(file_check){
		console.log(file_check.files[0].size);

		if(file_check.files && file_check.files[0].size > (5 *1024 * 1024)){	//5 *1024 * 1024 = 5mb
			$('#lyr_file').fadeIn(300);
			file_check.value = null;
		}
	}

	function popup_cancel(){
		$('#lyr_file').fadeOut(300);
	}

</script>
