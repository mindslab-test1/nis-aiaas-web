<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/subtitleRecog.css?ver=<%=fmt.format(lastModifiedStyle)%>" />

<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>이미지 업로드 실패</h5>
            <p>이미지 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>* 지원하는 언어: 한글 (특수문자 제외)<br>
                * 지원가능 파일 확장자: .jpg, .png<br>
* 이미지 용량 5MB 이하만 가능합니다.<br>
            </span>
        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">이미지 자막 인식</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'iprdemo')" id="defaultOpen"><button type="button">데모</button></li>
            <li class="tablinks" onclick="openTap(event, 'iprexample')"><button type="button">적용사례</button></li>
            <li class="tablinks" onclick="openTap(event, 'iprmenu')"><button type="button">매뉴얼</button></li>
<%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="iprdemo">
            <p><span>이미지 자막 인식</span> <small>(Subtitles Recognition)</small></p>
            <span class="sub">이미지 내에 있는 자막을 인식하여 추출하는 인공지능 엔진입니다. </span>
            <!--textremoval_box-->
            <div class="demo_layout poserecog_box subtitleRecog_box">
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="1" checked>
                                    <label for="sample1" class="female">
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_subtitle_sample.jpg" alt="sample img 1" />
                                        </div>
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-image"></em><strong>내 파일</strong>로 해보기</p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-image hidden"></em>
                                <label for="demoFile" class="demolabel">이미지 업로드</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".jpg, .png">
                            </div>
                            <ul>
                                <li>* 지원하는 언어: 한글 (특수문자 제외)</li>
                                <li>* 지원가능 파일 확장자: .jpg, .png</li>
                                <li>* 이미지 파일 용량 5MB 이하만 가능합니다.</li>

                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">결과보기</button>
                    </div>
                </div>
                <!--tr_1-->
                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-image"></em>자막 검출 및 텍스트 처리중</p>
                    <div class="loding_box ">
                        <!-- ball-scale-rotate -->
                        <div class="lds">
                            <div class="ball-scale-rotate">
                                <span> </span>
                                <span> </span>
                                <span> </span>
                            </div>
                        </div>
                        <p>약간의 시간이 소요 됩니다.</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1" id="reload_subtitleRecog"><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>
                <!--tr_2-->
                <!--tr_3-->
                <div class="tr_3" >
                    <div class="origin_file">
                        <p><em class="far fa-file-image"></em>입력 파일</p>
                        <div class="imgBox">
                            <img id="input_img" src="" alt="원본 이미지" />
                        </div>
                    </div>
                    <div class="result_file" >
                        <p><em class="far fa-file-text"></em>추출 자막</p>
                        <div class="subtitle_box">
                            <div class="subtitle_text">
                                <div id="resultTxt">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2" id=""><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.poserecog_box-->

        </div>
        <!-- //.demobox -->
        <!--.iprmenu-->
        <div class="demobox vision_menu" id="iprmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

                </div>
                <div class="guide_group">
                    <div class="title">
                        이미지 자막 인식 <small></small>
                    </div>
                    <p class="sub_txt">이미지 중 자막을 인식하여 추출해줍니다. 현재는 한국어만 인식합니다.</p>

                    <span class="sub_title">
								준비사항
					</span>
                    <p class="sub_txt">- Input: 이미지 파일</p>
                    <ul>
                        <li>확장자: .jpg, .png.</li>
                        <li>용량: 5MB 이하</li>
                    </ul>
                    <span class="sub_title">
								 실행 가이드
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/Vca/VCASubtitleExtract</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>사용자의 고유 ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>scene_img </td>
                            <td>type:file (.jpg, .png) 이미지 파일</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        curl --location --request POST 'http://api.maum.ai/Vca/VCASubtitleExtract' \<br>
                        --header 'Content-Type: multipart/form-data' \<br>
                        --form 'apiId= 발급받은 API ID' \<br>
                        --form 'apiKey= 발급받은 API KEY' \<br>
                        --form 'scene_img= 자막 추출할 이미지 파일'<br>
                    </div>

                    <p class="sub_txt">④ Response 예제 </p>

                    <div class="code_box">
<pre>
{
    "result_img": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",
    "result_text": "별로 볼 거 없네"
}
</pre>
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.iprmenu-->
        <!--.iprexample-->
        <div class="demobox" id="iprexample">
            <p><em style="color:#f7778a;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

            <!-- 이미지 자막 인식 -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>불법 영상 탐지</span>
                            </dt>
                            <dd class="txt">원본 영상의 프레임 속 자막을 추출하여 DB 및 검색 시스템에 등록하고, 불법으로 의심되는 영상의 자막을 읽고 검색하여 영상 일치 여부를 판단합니다.
                                <span><em class="fas fa-book-reader"></em> Reference: 한국저작권보호원</span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_sr"><span>자막 인식</span></li>
                                    <li class="ico_ftr"><span>얼굴 추적</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>영상 자동 번역</span>
                            </dt>
                            <dd class="txt">영상 내에 있는 자막을 검출하여 자동 번역 시스템에 활용합니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_sr"><span>자막 인식</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!--이미지 자막 인식  -->
        </div>
        <!--//.iprexample-->


    </div>
    <!-- //.content -->
</div>

<script type="text/javascript">
    var sampleImage1;

    let image1_blob;

    //파일명 변경
    document.querySelector("#demoFile").addEventListener('change', function (ev) {
        var demoFileInput = document.getElementById('demoFile');
        var demoFile = demoFileInput.files[0];
        var demoFileSize = demoFile.size;
        var max_demoFileSize = 1024 * 5000;//1kb는 1024바이트
        var demofile = document.querySelector("#demoFile");
        if(demoFileSize > max_demoFileSize || !demofile.files[0].type.match(/image.*/)){
            $('.pop_simple').show();
            // 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
            $('.fl_box').css("opacity", "0.5");
            $('#demoFile').val('');
            $('.tr_1 .btn_area').append('<span class="disBox"></span>');

            return;
        }


        document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
        var element = document.getElementById( 'uploadFile' );
        element.classList.remove( 'btn' );
        element.classList.add( 'btn_change' );
        $('.fl_box').css("opacity", "0.5");
    });


    jQuery.event.add(window,"load",function(){

        //샘플
        function loadSample1() {
            var blob = null;
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/images/img_subtitle_sample.jpg");
            xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
            xhr.onload = function()
            {
                blob = xhr.response;//xhr.response is now a blob object
                //sampleImage1 = new File([blob], "img_subtitle_sample.jpg");

                var imgSrcURL = URL.createObjectURL(blob);
                var subtitle_output=document.getElementById('input_img');
                subtitle_output.setAttribute("src",imgSrcURL);

                image1_blob = blob;
            };

            xhr.send();
        }


        $(document).ready(function (){
            loadSample1();

            // $("#demoFile").change(function(){
            //     readURL(this);
            // });

            $('#reload_subtitleRecog').on('click', function () {
                window.location.reload();
            });

            $('.radio label').on('click',function(){
                $('.fl_box').attr('opacity',1);
                $('em.close').click();
            });


            // step1->step2  (close button)
            $('em.close').on('click', function () {
                $('.fl_box').css("opacity", "1");
                $('#uploadFile label').text('이미지 업로드');
                $('#uploadFile').removeClass("btn_change");
                $('#uploadFile').addClass("btn");
                $('#demoFile').val("");

                //파일명 변경
                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
                    var element = document.getElementById( 'uploadFile' );
                    element.classList.remove( 'btn' );
                    element.classList.add( 'btn_change' );
                });
            });


            // 처음으로 버튼
            $('.btn_back1').on('click', function () {

                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('.tr_3').hide();
                $('.tr_1').fadeIn(300);

                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;

                });
            });

            //결과보기 버튼
            $('#sub').on('click',function (){

                var demoFileTxt = $('#demoFile').val(); //파일을 추가한 input 박스의 값
                var formData = new FormData();
                var demoFile;

                //파일을 추가한 input 값 없을 때
                if(demoFileTxt === ""){
                    var option = $("input[type=radio][name=option]:checked").val();
                    loadSample1();
                    //demoFile = sampleImage1;
                    demoFile = image1_blob;
                }

                //파일을 추가한 input 값 있을 때
                else {
                    var demoFileInput = document.getElementById('demoFile');
                    // console.log(demoFileInput);
                    demoFile = demoFileInput.files[0];
                    // console.log(demoFile);


                }

                formData.append('file',demoFile);
                formData.append('${_csrf.parameterName}', '${_csrf.token}');

                $('.tr_1').hide();
                $('.tr_2').fadeIn(300);

                xhr = $.ajax({
                    type: "POST",
                    async: true,
                    url: '${pageContext.request.contextPath}/api/subtitleRecog',
                    data: formData,
                    processData: false,
                    contentType: false,
                    timeout: 20000,
                    success: function(result){

                        let resultData = JSON.parse(result);
                        $('#resultTxt').text(resultData.result_text);

                        var imgSrcURL = URL.createObjectURL(demoFile);
                        var esr_output=document.getElementById('input_img');
                        esr_output.setAttribute("src",imgSrcURL);

                        $('.tr_1').hide();
                        $('.tr_2').hide();
                        $('.tr_3').fadeIn(300);

                    },
                    error: function(jqXHR, error){
                        if(jqXHR.status === 0){
                            return false;
                        }

                        alert("서버와 연결이 되지 않습니다.\n잠시 후에 다시 이용해주세요.");
                        console.dir(error);
                        window.location.reload();
                    }
                });


            });

        });
    });


//API 탭
function openTap(evt, menu) {
    var i, demobox, tablinks;
    demobox = document.getElementsByClassName("demobox");
    for (i = 0; i < demobox.length; i++) {
        demobox[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(menu).style.display = "block";
    evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();


</script>
