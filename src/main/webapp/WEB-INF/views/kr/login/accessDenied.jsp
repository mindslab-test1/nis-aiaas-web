<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%
String lang = request.getLocale().toString();
String paramLang = (String)request.getAttribute("lang");
//System.out.println("footer paramLang : " + paramLang);

if(paramLang != null && !"".equals(paramLang)) {
	lang = paramLang;
}	
%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Access Denied</title>
</head>

<body>

<h1>Access Denied!</h1>

<h3>[<a href="<c:url value="${pageContext.request.contextPath}/main/krMainHome" />">홈</a>]</h3>


</body>
</html>
