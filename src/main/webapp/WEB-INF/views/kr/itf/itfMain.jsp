﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
	<div class="content api_content">
		<h1  class="api_tit">의도 분류</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'itfdemo')" id="defaultOpen"><button type="button">데모</button></li>
			<li class="tablinks" onclick="openTap(event, 'itfexample')"><button type="button">적용사례</button></li>
			<li class="tablinks" onclick="openTap(event, 'itfmenu')"><button type="button">매뉴얼</button></li>
<%--			<li class="tablinks"><a href="/member/krApiAccount" >API ID, key 발급</a></li>--%>
		</ul>
		<!--.itfdemo-->
		<div class="demobox demobox_nlu" id="itfdemo">
			<p><span>의도 분류</span> ITF(Intent Finder) </p>
			<span class="sub">금융 상담 및 업무 중 발생되는 질문의 의도를 찾아보세요.<br>
금융 업무/상담 의도 분류기는 ITF을 이용한 예시입니다. ITF는 다양한 의도를 파악할 수 있습니다.</span>

			<!--.itf_box-->
			<div class="itf_box">
				<!--.step01-->
				<div class="step01">
					<div class="demo_top">
						<em class="far fa-list-alt"></em><span>예문 선택</span>
					</div>
					<div class="ift_topbox">
						<p>의도를 분류하고 싶은 예문을 선택 해주세요.</p>
						<ul class="ift_lst">
							<li>
								<div class="radio">
									<input type="radio" id="radio" name="radio_article" value="최근 이체했던 계좌 목록" checked="checked">
									<label for="radio">최근 이체했던 계좌 목록</label>
								</div>
							</li>
							<li>
								<div class="radio">
									<input type="radio" id="radio2" name="radio_article" value="지로요금 납부할래">
									<label for="radio2">지로요금 납부할래</label>
								</div>
							</li>
							<li>
								<div class="radio">
									<input type="radio" id="radio3" name="radio_article" value="환전하고 싶어">
									<label for="radio3">환전하고 싶어</label>
								</div>
							</li>
						</ul>
					</div>
					<p class="txt_or">또는</p>
					<div class="demo_top">
						<em class="far fa-keyboard"></em><span>텍스트 입력</span>
					</div>
					<div class="text_area">
						<textarea rows="10" placeholder="의도를 분류하고 싶은 내용을 직접 넣어주세요 "></textarea>
					</div>

					<div class="btn_area">
						<button type="button" class="btn_search" id="find_btn"><em class="fas fa-search"></em>의도 찾기</button>
					</div>
				</div>
				<!--//.step01-->

				<!--.step02-->
				<div class="step02">
					<div class="demo_top">
						<em class="far fa-list-alt"></em><span>결과</span>
					</div>
					<div class="ift_topbox">
						<p>의도 분류</p>
						<div class="ift_result">
							<em class="fas fa-chevron-right"></em>
							<p>보험상품 문의</p>
						</div>

<%--						<p class="second_txt">의도 구분</p>--%>
<%--						<div class="ift_result">--%>
<%--							<em class="fas fa-chevron-right"></em>--%>
<%--							<span>상품</span><span>보험</span><span>문의</span>--%>
<%--						</div>--%>
					</div>
					<div class="demo_top">
						<em class="fas fa-code"></em><span>의도 분류 프로세스</span>
					</div>
					<div class="text_area">
						<textarea id="result_textArea"  rows="7" placeholder=" "></textarea>
					</div>

					<div class="btn_area">
						<button type="button" class="btn_reset" id="btn_reset"><em class="fas fa-redo-alt"></em>처음으로</button>
					</div>
				</div>
				<!--//.step02-->
			</div>
			<!--//.itf_box-->

		</div>
		<!-- .itfdemo -->

		<!--.itfmenu-->
		<div class="demobox" id="itfmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API 공통 가이드
					</div>
					<p class="sub_title">개발 환경 세팅</p>
					<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
					<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

				</div>
				<div class="guide_group">
					<div class="title">
						Intend Finder <small>(ITF)</small>
					</div>
					<p class="sub_txt">사용자의 의도를 이해하고 분류할 수 있는 엔진으로, 가장 적절한 답변을 찾아 챗봇이 대답할 수 있도록 합니다.</p>

					<span class="sub_title">
							준비사항
						</span>
					<p class="sub_txt">① Input: 문자열 (텍스트)</p>
					<p class="sub_txt">② 아래 언어 중 택 1 </p>
					<ul>
						<li>국내 금융 업무 및 상담 의도 분류</li>
					</ul>
					<span class="sub_title">
							 실행 가이드
						</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/itf/</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>사용자의 고유 key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>utter</td>
							<td>의도 분류를 시행할 문장 </td>
							<td>string</td>
						</tr>
						<tr>
							<td>lang</td>
							<td>사용할 모델의 언어 (ko_KR)</td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
							<pre>
curl -X POST \
  http://api.maum.ai/api/itf \
  -H 'Content-Type: application/json' \
  -d '{
	"apiId": "(*ID 요청필요)",
	"apiKey": "(*key 요청필요)"",
	"utter": "통장 잔액이 얼마야",
	"lang": "ko_KR"
}'
</pre>
					</div>

					<p class="sub_txt">④ Response 예제 </p>

					<div class="code_box">
<pre>
{
    "testFindIntentCase_": 1,
    "testFindIntent_": {
        "bitField0_": 0,
        "skill_": "잔액조회",
        "probability_": 1.0,
        "isDefaultSkill_": false,
        "history_": {
            "runResults_": [
                {
                    "type_": 11,
                    "name_": "itf_cca_sc",
                    "hasResult_": false,
                    "isHint_": false,
                    "skill_": "",
                    "hint_": "",
                    "probability_": 0.0,
                    "detail_": "",
                    "memoizedIsInitialized": -1,
                    "unknownFields": {
                        "fields": {}
                    },
                    "memoizedSize": -1,
                    "memoizedHashCode": 0
                },
                {
                    "type_": 12,
                    "name_": "itf_eca_hmd",
                    "hasResult_": true,
                    "isHint_": false,
                    "skill_": "잔액조회",
                    "hint_": "",
                    "probability_": 1.0,
                    "detail_": "%잔액$!계좌 별$!통장 별$!전체 계좌$!전 계좌$!계좌 전체$!계좌 전부$!전체 통장$!통장 전체$!통장 전부$!모든 계좌$!적금$!펀드$!대출$!예금$!증권$!이체$!외화",
                    "memoizedIsInitialized": -1,
                    "unknownFields": {
                        "fields": {}
                    },
                    "memoizedSize": -1,
                    "memoizedHashCode": 0
                }
            ],
            "memoizedIsInitialized": -1,
            "unknownFields": {
                "fields": {}
            },
            "memoizedSize": -1,
            "memoizedHashCode": 0
        },
        "nluResult_": {
            "bitField0_": 0,
            "category_": "",
            "categoryWeight_": 0.0,
            "lang_": 0,
            "sentences_": [
                {
                    "bitField0_": 0,
                    "seq_": 0,
                    "text_": "통장 잔액이 얼마야",
                    "words_": [
                        {
                            "seq_": 0,
                            "text_": "통장",
                            "taggedText_": "",
                            "type_": "",
                            "begin_": 0,
                            "end_": 0,
                            "beginSid_": 0,
                            "endSid_": 0,
                            "position_": 0,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 1,
                            "text_": "잔액이",
                            "taggedText_": "",
                            "type_": "",
                            "begin_": 1,
                            "end_": 2,
                            "beginSid_": 0,
                            "endSid_": 0,
                            "position_": 0,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 2,
                            "text_": "얼마야",
                            "taggedText_": "",
                            "type_": "",
                            "begin_": 3,
                            "end_": 3,
                            "beginSid_": 0,
                            "endSid_": 0,
                            "position_": 0,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        }
                    ],
                    "morps_": [
                        {
                            "seq_": 0,
                            "lemma_": "통장",
                            "type_": "NNG",
                            "position_": 0,
                            "wid_": 0,
                            "weight_": 0.9,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 1,
                            "lemma_": "잔액",
                            "type_": "NNG",
                            "position_": 7,
                            "wid_": 0,
                            "weight_": 0.9,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 2,
                            "lemma_": "이",
                            "type_": "JKS",
                            "position_": 13,
                            "wid_": 0,
                            "weight_": 0.03607228016640132,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 3,
                            "lemma_": "얼마야",
                            "type_": "NNG",
                            "position_": 17,
                            "wid_": 0,
                            "weight_": 0.0,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        }
                    ],
                    "morphEvals_": [
                        {
                            "seq_": 0,
                            "target_": "통장",
                            "result_": "통장/NNG",
                            "wordId_": 0,
                            "mBegin_": 0,
                            "mEnd_": 0,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 1,
                            "target_": "잔액이",
                            "result_": "잔액/NNG+이/JKS",
                            "wordId_": 1,
                            "mBegin_": 1,
                            "mEnd_": 2,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 2,
                            "target_": "얼마야",
                            "result_": "얼마야/NNG",
                            "wordId_": 2,
                            "mBegin_": 3,
                            "mEnd_": 3,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        }
                    ],
                    "nes_": [],
                    "wsds_": [],
                    "chunks_": [],
                    "dependencyParsers_": [],
                    "phraseClauses_": [],
                    "srls_": [],
                    "srlInfos_": [],
                    "relations_": [],
                    "sas_": [],
                    "zas_": [],
                    "memoizedIsInitialized": -1,
                    "unknownFields": {
                        "fields": {}
                    },
                    "memoizedSize": -1,
                    "memoizedHashCode": 0
                }
            ],
            "keywordFrequencies_": [],
            "memoizedIsInitialized": -1,
            "unknownFields": {
                "fields": {}
            },
            "memoizedSize": -1,
            "memoizedHashCode": 0
        },
        "memoizedIsInitialized": -1,
        "unknownFields": {
            "fields": {}
        },
        "memoizedSize": -1,
        "memoizedHashCode": 0
    },
    "replace_": {
        "bitField0_": 0,
        "replacedUtter_": "",
        "memoizedIsInitialized": -1,
        "unknownFields": {
            "fields": {}
        },
        "memoizedSize": -1,
        "memoizedHashCode": 0
    },
    "memoizedIsInitialized": 1,
    "unknownFields": {
        "fields": {}
    },
    "memoizedSize": -1,
    "memoizedHashCode": 0
}

</pre>

					</div>
				</div>
			</div>

		</div>
		<!--//itfmenu-->
		<!--.itfexample-->
		<div class="demobox" id="itfexample">
			<p><em style="color:#27c1c1;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

			<%--itf--%>
			<div class="useCasesBox">
				<ul class="lst_useCases">

					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>텍스트 분석</span>
							</dt>
							<dd class="txt">다양한 분야의 비정형 문서 및 텍스트를 분석할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_if"><span>ITF</span></li>
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>자동 분류</span>
							</dt>
							<dd class="txt">카테고리별로 텍스트를 자동으로 분류하고 분석할 수 있습니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_if"><span>ITF</span></li>
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //itf -->
		</div>
		<!--//.itfexample-->


	</div>
</div>
<!-- //.contents -->


<script type="text/javascript">
	$(window).load(function() {
		$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
			$(this).remove();
		});
	});
</script>

<script type="text/javascript">
	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){
			var $textArea = $('.itf_box .step01 textarea');

			//===== radioBox event =====
			$('input[type=radio]').on('click', function(){
				$textArea.text("");
				$textArea.val("");
			});

			//===== textArea event =====
			$textArea.on('change paste input keyup', function(){
				$('input[type=radio]:checked').prop("checked", false);
			});

			//===== 의도 찾기 =====
			$('#find_btn').on('click',function(){
				var $checkedRadio = $('input[type=radio]:checked');
				var utter = "";
				var lang = "";

				if( $textArea.val().trim() === ""){
					if($checkedRadio.length === 0){
						alert("의도를 분류하고 싶은 예문을 선택하거나 텍스트를 입력해주세요.");
					    return;
					}
                    utter = $checkedRadio.val();
				}else{
					utter = $textArea.val();
				}

				lang = "ko";
				console.log("utter: "+utter + " lang: "+lang);

				var param = {
					'utter' : utter,
					'lang' : lang,
					'${_csrf.parameterName}' : '${_csrf.token}'
				};

				$.ajax({
					url: '${pageContext.request.contextPath}/api/itf/',
					async: false,
					type: 'POST',
					headers: {
						"Content-Type": "application/x-www-form-urlencoded"
					},
					data: param,
					timeout: 20000,
					error: function(request, status, error){
						if (request.statusText === ""){
							alert("서버에 연결할 수 없습니다.");
						}else if (request.statusText === "INTERNAL SERVER ERROR"){
							alert("서버에서 답을 불러올 수 없습니다.");
						}else{
							alert(request.statusText);
						}
					},
					success: function(data){
						var responseData = JSON.parse(data);
						var responseString = JSON.stringify(responseData,undefined,8);
						var textAreaObj = document.getElementById('result_textArea');
						var skill_ = "";

						textAreaObj.scrollTop = textAreaObj.offsetHeight- 300;
						textAreaObj.value = responseString;
						skill_ = responseData.testFindIntent_.skill_;

						if(skill_ === "9999"){
							skill_ = "의도를 알 수 없습니다.";
						}
						$('.ift_result p').text(skill_);

						$('.itf_box .step01').hide();
						$('.itf_box .step02').fadeIn();
					}
				});
			});


			$('#btn_reset').on('click',function(){
				var textAreaObj = document.getElementById('result_textArea');
				textAreaObj.scrollTop = textAreaObj.offsetHeight - 300;
				document.getElementById('result_textArea').innerHTML = "";

				$('.itf_box .step02').hide();
				$('.itf_box .step01').fadeIn();
			});

		});

	});

	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();



</script>
