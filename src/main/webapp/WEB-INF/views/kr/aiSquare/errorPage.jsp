<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>

<!DOCTYPE html>
<html lang="ko">
<head>
    <%@ include file="../aiSquare/head.jsp" %>
</head>


<body>
    <!-- #wrap -->
    <div id="wrap">
       <div class="errorwrap">
           <div>
               <h1>Error <span>Something went wrong..</span></h1>
               <a href="<spring:eval expression="@property['login.url']" />" title="돌아가기">Back to Login</a>
           </div>
       </div>

    </div>
    <!-- //#wrap -->
</body>
</html>

<script>

    $(document).ready(function() {
        console.log("${errMsg}");
        console.log("${redirectErrMsg}");
    });

</script>