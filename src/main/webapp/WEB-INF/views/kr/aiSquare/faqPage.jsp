<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>

<!DOCTYPE html>
<html lang="ko">
<head>
    <%@ include file="../aiSquare/head.jsp" %>
</head>


<body>
<!-- #wrap -->
<div id="wrap">
    <%@ include file="../aiSquare/header.jsp" %>
    <!-- #container -->
    <div id="container">
        <!-- #contents -->
        <div id="contents">
            <!-- .content -->
            <div class="content">
                <!-- .sta -->
                <div class="stn" id="faq_top">
                    <div class="box_area stn_top">
                        <div class="title">
                            <h2>FAQ</h2>
                            <h5>자주하는 질문  ㅣ  문의하기</h5>
                        </div>
                    </div>
                </div>
                <!-- //.sta -->

                <!-- .section -->
                <div class="stn" id="faqBox">
                    <div class="box_area faqBox">
                        <div class="tab_menu">
                            <ul class="tab_list">
                                <li class="active" id="faq_cont">자주하는 질문</li>
                                <li id="req_cont">문의 하기</li>
                            </ul>
                        </div>
                        <%--자주하는 질문--%>
                        <div class="stn_cont faq_cont on" id="faq_board">
<%--                          <ul>
                              <li>
                                  <p class="faq_tit">
                                     1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?
                                      <em class="fas fa-angle-down"></em>
                                      <em class="fas fa-angle-up"></em>
                                  </p>
                                  <div class="dropdown-box">
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.  사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                  </div>
                              </li>
                              <li>
                                  <p class="faq_tit">
                                      2. AI Platform을 사용하려면 어떻게 해야 하나요?
                                      <em class="fas fa-angle-down"></em>
                                      <em class="fas fa-angle-up"></em>
                                  </p>
                                  <div class="dropdown-box">
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.  사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                  </div>
                              </li>
                              <li>
                                  <p class="faq_tit">
                                      3. AI 산업에 주는 Value는 무엇인가요?
                                      <em class="fas fa-angle-down"></em>
                                      <em class="fas fa-angle-up"></em>
                                  </p>
                                  <div class="dropdown-box">
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.  사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                  </div>
                              </li>
                              <li>
                                  <p class="faq_tit">
                                      2. AI Platform을 사용하려면 어떻게 해야 하나요?
                                      <em class="fas fa-angle-down"></em>
                                      <em class="fas fa-angle-up"></em>
                                  </p>
                                  <div class="dropdown-box">
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.  사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                  </div>
                              </li>
                              <li>
                                  <p class="faq_tit">
                                      2. AI Platform을 사용하려면 어떻게 해야 하나요?
                                      <em class="fas fa-angle-down"></em>
                                      <em class="fas fa-angle-up"></em>
                                  </p>
                                  <div class="dropdown-box">
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                      사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.  사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.
                                  </div>
                              </li>
                          </ul>--%>
                        </div>

                        <%--문의하기--%>
                        <div class="stn_cont req_cont">
                            <div class="req_box">
                                <textarea id="req_board" placeholder="문의 사항을 입력해 주세요."></textarea>
                                <button type="button" id="req_submit">문의하기</button>
                                <sec:authorize access="hasRole('ADMIN')">
                                    <button type="button" id="req_list">문의내용 리스트</button>
                                </sec:authorize>
                            </div>
                            <div class="stn_cont faq_list" id="faq_list">
                                <div class="tit">문의내용 리스트</div>
                                <div class="questionList" id="faq_question_board">
<%--                                <ul>--%>
<%--                                    <li>--%>
<%--                                      <p class="faq_tit">--%>
<%--                                         1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?1. 인공지능이란 무엇인가요?--%>
<%--                                          <span class="user_num">사번 : 299248</span>--%>
<%--                                          <span class="date_txt">2020-02-02</span>--%>
<%--                                          <em class="fas fa-angle-down"></em>--%>
<%--                                          <em class="fas fa-angle-up"></em>--%>
<%--                                      </p>--%>
<%--                                      <div class="dropdown-box">--%>
<%--                                          사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.--%>
<%--                                          사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.--%>
<%--                                          사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.  사업은 플랫폼 및 솔루션을 활용하여 직접 구축한 서비스를 기업 고객 대상으로 제공하는 사업과, 기업 및 개인 개발자들이 자체적으로 서비스를 구축할 수 있도록 플랫폼 상에서 API를 제공하는 Eco사업 유형으로 구분됩니다.--%>
<%--                                      </div>--%>
<%--                                    </li>--%>

<%--                                </ul>--%>
                                </div>
                                <%--paging--%>
                                <%--<div class="paging">
                                    <a class="btn_paging_prev" href="#none" ><em class="fas fa-angle-left"></em></a>
                                    <span class="list">
                                        <a href="#none" class="on">1</a>
                                        <a href="#none">2</a>
                                        <a href="#none">3</a>
                                        <a href="#none">4</a>
                                        <a href="#none">5</a>
                                    </span>
                                    <a class="btn_paging_next" href="#none"><em class="fas fa-angle-right"></em></a>
                                </div>--%>
                                <%--//paging--%>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- //.section-->
            </div>
            <!-- //.content -->
        </div>
        <!-- //#contents -->
    </div>
    <!-- //#container -->

</div>
<!-- //#wrap -->
</body>
</html>

<script>

    var current_page;
    var numberOfContents = 10;

    $(document).ready(function() {

        $('.tab_list li').each(function(index){

            $(this).click(function(){
                $('.tab_list li').removeClass('active');
                $(this).addClass('active');
                $('.stn_cont').hide();

                if ($(this).hasClass('active')){
                    var cont = $(this).attr('id');
                    $("."+cont).fadeIn();

                }
            });
        });

        loadBoard();

        // 문의하기 게시판 '문의하기' 버튼 클릭 시,
        $('#req_submit').on('click', function() {
            if( $('#req_board').val() == '' ) {
                alert('문의 내용을 확인해 주세요.');
            } else {
                submitReq();
            }
        });


        $('#req_list').on('click', function(){
            $('#faq_question_board').html("");
            $('.faq_list').fadeIn();

            current_page = 1;

            q_list_Board(current_page);
        });
    });

    // 문의하기 기능
    function submitReq() {
        var formData = new FormData();

        formData.append('question', $('#req_board').val());
        formData.append('userNo', ${fn:escapeXml(sessionScope.accessUser.userNo)});
        formData.append('userId', ${fn:escapeXml(sessionScope.accessUser.userId)});

        hAjax = $.ajax({
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            url: '${pageContext.request.contextPath}/faq/insertQuestion',
            success: function(result) {
                console.log(result);
                alert("문의 내용이 등록 되었습니다.");
                $('#req_board').val(''); // 초기화
            },
            error: function(xhr, status, error){
                hAjax = null;

                console.log(xhr);
                console.log(status);
                console.log(error);
            }
        });
    }


    // DB table(FAQ_t)을 통해 정보 얻어 오기
    function loadBoard() {
        hAjax = $.ajax({
            type: 'POST',
            processData: false,
            contentType: false,
            url: '${pageContext.request.contextPath}/faq/getFaqContents',
            success: function(faqArr) {
                console.dir(faqArr);
                console.log(faqArr.length);

                drawBoard(faqArr);
            },
            error: function(xhr, status, error){
                hAjax = null;

                console.log(xhr);
                console.log(status);
                console.log(error);
            }
        });
    }

    // 얻어 온 정보로 '자주하는 질문' 게시판 그리기
    function drawBoard(faqArr) {
        var numQ = faqArr.length;
        var brdContents = "<ul>";

        for(let i=0 ; i<numQ ; i++) {
            brdContents = brdContents +
                '<li> ' +
                '   <p class="faq_tit">' +
                '       Q. ' + faqArr[i].question +
                '      <em class="fas fa-angle-down"></em>' +
                '      <em class="fas fa-angle-up"></em>' +
                '   </p>' +
                '   <div class="dropdown-box">'
                    + faqArr[i].answer + '' +
                '   </div>' +
                '</li>'
        }

        brdContents = brdContents + "</ul>";
        $('#faq_board').append(brdContents);

        $('.faq_tit').on('click', function() {
            $(this).toggleClass('active');
            $(this).next(".dropdown-box").slideToggle(200);
        });
    }

    /*************************************************************************************/
    // 문의내용 리스트 가져오기
    /*************************************************************************************/

    // DB table(QUESTION_t)을 통해 정보 얻어 오기
    function q_list_Board(page_number) {

        var startContent = page_number * numberOfContents;
        var lastContent = page_number * numberOfContents + numberOfContents;

        console.log(startContent);
        console.log(lastContent);

        var formDate = new FormData();
        formDate.append('start', startContent);
        formDate.append('last', lastContent);


        hAjax = $.ajax({
            type: 'POST',
            processData: false,
            contentType: false,
            url: '${pageContext.request.contextPath}/faq/getQuestion',
            success: function(qaArr) {
                console.dir(qaArr);
                console.log(qaArr.length);

                draw_q_Board(qaArr);
            },
            error: function(xhr, status, error){
                hAjax = null;

                console.log(xhr);
                console.log(status);
                console.log(error);
            }
        });
    }

    // 얻어 온 정보로 'QUESTION_t' 게시판 그리기
    function draw_q_Board(qaArr) {
        var numQ = qaArr.length;
        var brdContents = "<ul>";

        console.log(qaArr)
        for(let i=0 ; i<numQ ; i++) {
            var date = qaArr[i].createDateTime;


            brdContents = brdContents +
                '<li> ' +
                '   <p class="faq_tit">' +
                '       Q. ' + qaArr[i].question +
                '<span class="user_num">사번 : '+ qaArr[i].userId + '</span>'+
                '<span class="date_txt">'+ date.substring(0,10) + '</span>' +
                '      <em class="fas fa-angle-down"></em>' +
                '      <em class="fas fa-angle-up"></em>' +
                '   </p>' +
                '   <div class="dropdown-box">'
                + qaArr[i].answer + '' +
                '   </div>' +
                '</li>'
        }

        brdContents = brdContents + "</ul>";
        $('.questionList').append(brdContents);

        $('.faq_tit').on('click', function() {
            $(this).toggleClass('active');
            $(this).next(".dropdown-box").slideToggle(200);
        });
    }
</script>