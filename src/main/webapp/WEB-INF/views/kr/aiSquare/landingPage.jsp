<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!DOCTYPE html>
<html lang="ko">
<head>
    <%@ include file="../aiSquare/head.jsp" %>
    <link rel="stylesheet" href="http://163.8.81.53:10020/css?host=host"/>
</head>


<body>
    <script src="163.8.81.53:10020/js/test"></script>
<!-- #wrap -->
<div id="wrap">
    <%@ include file="../aiSquare/header.jsp" %>

    <!-- #container -->
    <div id="container">
        <!-- #contents -->
        <div id="contents">
            <!-- .content -->
            <div class="content">
                <!-- .sta -->
                <div class="stn" id="stn_top">
                    <div class="box_area stn_top">
                        <div class="title">
                            <h2>AI Square</h2>
                            <h5>인공지능을 도입하는 가장 쉬운 방법</h5>
                        </div>
                    </div>
                </div>
                <!-- //.sta -->
                <!-- .section -->
                <div class="stn" id="serviceBox">
                    <div class="box_area serviceBox">
                        <div class="stn_tit">
                            <h4>인공지능 서비스</h4>
                        </div>
                        <div class="stn_cont">
                            <ul>
                                <li>
                                    <h5>음성</h5>
                                    <p>음성 정보를 이해하고, 합성하는<br>
                                        음성생성, 음성인식, 음성정제, 화자분리, Voice Filter<br>
                                        엔진 등을 제공합니다.
                                    </p>
                                </li>
                                <li>
                                    <h5>시각</h5>
                                    <p>이미지 정보를 이해하고, 활용하는<br>
                                        얼굴 인증, 도로 상의 객체 인식, 화폐/고지서 인식,<br>
                                        텍스트 지우기 엔진 등을 제공합니다.
                                    </p>
                                </li>
                                <li>
                                    <h5>언어</h5>
                                    <p>사람의 언어를 이해하고, 활용하는<br>
                                        자연어 이해, 패턴분류, 문장생성,의도분류, 텍스트 분류<br>
                                        엔진 등을 제공합니다.
                                    </p>
                                </li>
                            </ul>
                            <a href="${pageContext.request.contextPath}/main/krMainHome" target="_self" class="btn_go">인공지능 서비스 바로가기</a>
                        </div>
                    </div>
                </div>
                <!-- //.section-->
                <!-- .section -->
                <div class="stn" id="videoBox">
                    <div class="box_area videoBox">
                        <div class="stn_tit">
                            <h4>인공지능 엔진 활용법 영상</h4>
                        </div>
                        <div class="stn_cont swiper-container video ">
                            <ul class="swiper-wrapper ">
                                <c:forEach items="${list}" var="list">
                                    <li class="swiper-slide ">
                                        <div class="img_box">
                                            <a class="btn_movLayer" href="<c:out value="${list.videoPath}" />" target="_blank">
                                                <img src="<c:out value="${list.imgPath}" />" target="_blank"/>
                                            </a>
<%--                                            <video class="btn_movLayer" controls>
                                                <source src="<c:out value="${list.videoPath}" />" type="video/mp4">
                                                Your browser does not support the video tag.
                                            </video>--%>
    <%--                                        <span class="ico_mov">동영상</span>--%>
                                        </div>
                                        <div class="txt_box">
                                            <p><c:out value="${list.title}" /> </p>
                                            <span style="color: <c:out value="${list.fontColor}" />" >
                                                <c:out value="${list.category}" />
                                            </span>
                                        </div>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="swiper-button-prev swipter_btn"><em class="fas fa-chevron-left"></em></div>
                        <div class="swiper-button-next swipter_btn"><em class="fas fa-chevron-right"></em></div>
                    </div>
                </div>
                <!-- //.section-->
                <!-- .section -->
                <div class="stn" id="trendBox">
                    <div class="box_area trendBox">
                        <div class="stn_tit">
                            <h4>최신 AI 기술 트렌드</h4>
                            <p>인공지능의 최신 동향을 파악할 수 있습니다.</p>
                            <a href="${pageContext.request.contextPath}/trendPage" target="_self" class="btn_go">AI 트랜드 바로가기</a>
                        </div>
                        <div class="stn_cont">
                            <ul>
                                <li><em class="fas fa-caret-right"></em> maum.ai 기초 </li>
                                <li><em class="fas fa-caret-right"></em> 인문학도를 위한 인공지능</li>
                                <li><em class="fas fa-caret-right"></em> AIaaS 기초</li>
                                <li><em class="fas fa-caret-right"></em> 인물 포즈인식</li>
                                <li><em class="fas fa-caret-right"></em> STT</li>
                                <li><em class="fas fa-caret-right"></em> maum 회의록</li>
                            </ul>
                            <ul>
                                <li><em class="fas fa-caret-right"></em> maum SDS</li>
                                <li><em class="fas fa-caret-right"></em> AI Builder</li>
                                <li><em class="fas fa-caret-right"></em> AIaaS 심화</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- //.section-->
                <!-- .section -->
                <div class="stn" id="contactBox">
                    <div class="box_area contactBox">
                        <div class="stn_tit">
                            <h4>문의하기</h4>
                            <p>궁금한 점이 있으신가요? 자주묻는 질문이나 문의하기 게시판을 이용해 주세요.</p>
                            <a href="${pageContext.request.contextPath}/faqPage" target="_self" class="btn_go">문의하기 바로가기</a>
                        </div>
                    </div>
                </div>
                <!-- //.section-->
            </div>
            <!-- //.content -->
        </div>
        <!-- //#contents -->
    </div>
    <!-- //#container -->
</div>
<!-- //#wrap -->
</body>
</html>



<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/swiper.min.js"></script>

<script type="text/javascript">
    $(window).load(function() {
        window.moveTo(0,0);
        window.resizeTo(screen.availWidth, screen.availHeight);
        $('#pageldg').addClass('pageldg_hide').delay(500).queue(function() { $(this).remove(); });
    });
</script>

<script>
    $(document).ready(function() {

        applySwiper('.video', {slidesPerView: 3});
        function applySwiper(selector, option) {
            var defaultOption = {
                speed: 200,
                slidesPerView: 3,
                spaceBetween: 10,
                slidesPerGroup : 3,
                centeredSlides: false,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true
                }
            };
            return new Swiper(selector, $.extend(defaultOption, option))
        }
    });
    $('a.dwnlink').on('click',function(e){
        e.preventDefault();
        var this_link = $(this).attr('href');
        $('html, body').animate({scrollTop : $(this_link).offset().top-70}, 500);

    });


    $('.chat_icon').on('click',function(){
        $('.lyr_chat').fadeIn();
    });

    $('.btn_close').on('click', function () {
        $('.lyr_chat').fadeOut();
        $('.chatbot_contents .chat_btm .textArea').val('');
        $('.ipt_srch').val('');
    });

    // Hide Header on on scroll down
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;

    $(window).scroll(function(event){
        didScroll = true;
    });
    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);
    function hasScrolled() {
        var scrollTop = $(window).scrollTop();
        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - scrollTop) <= delta)
            return;
        if (scrollTop > 20){
            // Scroll Down
            $('#header').addClass('fixed');

        } else {
            // Scroll Up
            if(scrollTop + $(window).height() < $(document).height()) {
                $('#header').removeClass('fixed');
            }
        }
        lastScrollTop = scrollTop;
    }

</script>