<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!-- loading -->
<div id="pageldg">
    <div class="ld_item">
        <span></span>
        <span></span>
    </div>
</div>
<!-- //loading -->

<!-- #header -->
<div id="header">
    <div class="head_area">
        <h1><a target="_self" href="${pageContext.request.contextPath}/">AI Square</a></h1>
        <!-- .gnb -->
        <div id="gnb" class="gnb">
            <ul class="nav_gnb">
                <li><a class="" href="${pageContext.request.contextPath}/main/krMainHome" target="_self">AI 서비스</a></li>
                <li><a href="${pageContext.request.contextPath}/trendPage" target="_self">AI 기술 트렌드</a></li>
                <li><a href="${pageContext.request.contextPath}/faqPage" target="_self">FAQ</a></li>
                <li><a class="" href="<spring:eval expression="@property['dataEditTool.url']" />" target="_self">Data edit tool</a></li>
<%--                <li class="user_num"><span>사번 ${fn:escapeXml(sessionScope.accessUser.userId)}</span></li>--%>
                <li class="user_key">
                    <span id="key_btn"><em class="fas fa-key"></em><strong>API<br>KEY</strong></span>
                </li>
            </ul>
        </div>
        <!-- //.gnb -->

        <div class="idkey_box">
            <ul class="idpw">
                <li><strong>API ID </strong><span>${fn:escapeXml(sessionScope.accessUser.apiId)}</span></li>
                <li><strong>API KEY </strong><span>${fn:escapeXml(sessionScope.accessUser.apiKey)}</span></li>
            </ul>
            <em class="fas fa-times"></em>
        </div>
    </div>
</div>
<!-- //#header -->
<script type="text/javascript">
    $(window).load(function() {
        $('#pageldg').addClass('pageldg_hide').delay(500).queue(function() { $(this).remove(); });
    });
</script>
<script>
    // Hide Header on on scroll down
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;

    $(window).scroll(function(event){
        didScroll = true;
    });
    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);
    function hasScrolled() {
        var scrollTop = $(window).scrollTop();
        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - scrollTop) <= delta)
            return;
        if (scrollTop > 20){
            // Scroll Down
            $('#header').addClass('fixed');

        } else {
            // Scroll Up
            if(scrollTop + $(window).height() < $(document).height()) {
                $('#header').removeClass('fixed');
            }
        }
        lastScrollTop = scrollTop;
    }

    $(document).ready(function() {
        $('#key_btn').on('click', function(){
            $(this).parent().parent().parent().toggleClass('active');
        });
        $('.head_area .fa-times, #container ').on('click', function(){
            $('.gnb').removeClass('active');
        })
    })

</script>