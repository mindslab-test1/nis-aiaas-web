<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%
	Date lastModifiedStyle = new Date();
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/superresoution.css?ver=<%=fmt.format(lastModifiedStyle)%>" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/pop_common.css?ver=<%=fmt.format(lastModifiedStyle)%>" />
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/download.js"></script>

<!-- 5 .pop_simple -->
<div class="pop_simple">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap pop_sr_noti">
		<button class="pop_close" type="button">닫기</button>
		<!-- .pop_bd -->
		<div class="pop_bd">
			<em class="fas fa-sad-cry"></em>
			<h5>이미지 업로드 실패</h5>
			<p>이미지 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
				업로드 되지 않았습니다.</p>
			<span>* 지원가능 파일 확장자: .jpg, .png<br>
* 이미지 용량 30KB 이하만 가능합니다.<br>
* 사이즈 200 x 200 pixel 이하만 가능합니다.</span>

		</div>
		<!-- //.pop_bd -->
		<div class="btn">
			<a class="">확인</a>
		</div>
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
		<div class="contents api_content">
			<!-- .content -->
			<div class="content">
				<h1 class="api_tit">Enhanced Super Resolution</h1>
				<ul class="menu_lst vision_lst">
					<li class="tablinks" onclick="openTap(event, 'srdemo')" id="defaultOpen"><button type="button">데모</button></li>
					<li class="tablinks" onclick="openTap(event, 'srexample')"><button type="button">적용사례</button></li>
					<li class="tablinks" onclick="openTap(event, 'srmenu')"><button type="button">매뉴얼</button></li>
<%--					<li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
				</ul>
				<!-- .demobox -->

				<div class="demobox" id="srdemo">
					<p>해상도를 높이는 <span>Enhanced Super Resolution</span> </p>
					<span class="sub">크기가 작은 사진을 손실률이 적게 확대해 줍니다.</span>
					<!--superR_box-->
					<div class="demo_layout superR_box">
						<!-- 2 .pop_confirm -->
						<div class="pop_confirm">
							<div class="pop_bg"></div>
							<!-- .popWrap -->
							<div class="popWrap">
								<em class="fas fa-times close"></em>
								<!-- .pop_bd -->
								<div class="pop_bd">
									<div class="img">
										<p><em class="far fa-file-image"></em> 원본 확대 파일</p>
										<div class="origin_img">
											<img src="" id="origin_img" alt="원본 확대 파일"/>
										</div>
									</div>
									<div class="img">
										<p><em class="far fa-file-image"></em> 결과 파일</p>
										<div class="result_img">
											<img src="" id="result_img" alt="결과 확대 파일"/>
										</div>
										<a id="save" onclick="downloadResultImg();" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> 결과 파일 다운로드</a>
									</div>
								</div>
								<!-- //.pop_bd -->

							</div>
							<!-- //.popWrap -->
						</div>
						<!-- //.pop_confirm -->

						<!--tr_1-->
						<div class="tr_1">
							<div class="fl_box">
								<p><em class="far fa-file-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
								<div class="sample_box">
									<div class="sample_1">
										<div class="radio">
											<input type="radio" id="sample1" name="option" value="1" checked>
											<label for="sample1" class="female">
												<div class="img_area">
													<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_sr_sample1.png" alt="sample img 1" />
												</div>
												<span>샘플 1</span>
											</label>
										</div>
									</div>
									<div class="sample_2">
										<div class="radio">
											<input type="radio" id="sample2" name="option" value="2" >
											<label for="sample2"  class="male">
												<div class="img_area">
													<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_sr_sample2.png" alt="sample img 2" />
												</div>
												<span>샘플 2</span>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="fr_box">
								<p><em class="far fa-file-image"></em><strong>내 파일</strong>로 해보기</p>
								<div class="uplode_box">
									<div class="btn" id="uploadFile">
										<em class="fas fa-times hidden close"></em>
										<em class="far fa-file-image hidden"></em>
										<label for="demoFile" class="demolabel">이미지 업로드</label>
										<input type="file" id="demoFile" class="demoFile" accept=".jpg, .png">
									</div>
									<ul>
										<li>* 지원가능 파일 확장자: .jpg, .png</li>
										<li>* 이미지 용량 30KB 이하만 가능합니다.</li>
										<li>* 사이즈 200 x 200 pixel 이하만 가능합니다.</li>
									</ul>
								</div>
							</div>

							<div class="btn_area">
								<button type="button" class="btn_start" id="sub">결과보기</button>
							</div>
						</div>
						<!--tr_1-->
						<!--tr_2-->
						<div class="tr_2">
							<p><em class="far fa-file-image"></em>해상도 높이는 중</p>
							<div class="loding_box ">
								<!-- ball-scale-rotate -->
								<div class="lds">
									<div class="ball-scale-rotate">
										<span> </span>
										<span> </span>
										<span> </span>
									</div>
								</div>
								<p>약간의 시간이 소요 됩니다.</p>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_back1 reset_btn" id="reload_esr"><em class="fas fa-redo"></em>처음으로</button>
							</div>

						</div>
						<!--tr_2-->
						<!--tr_3-->
						<div class="tr_3" >
							<p><em class="far fa-file-image"></em>결과 미리보기</p>
							<div class="origin_file">
								<div class="imgBox">
									<img id="input_img" src="" alt="원본 이미지" />
								</div>
							</div>
							<div class="result_file" >
								<div class="imgBox">
									<img id="output_img" src="" alt="결과 이미지" />
								</div>
							</div>
							<span class="noti">위 사진은 미리보기입니다. 아래 버튼을 눌러 결과파일을 확인하세요.</span>
							<a class="result_check"><em class="far fa-eye"></em> 결과파일 확인 및 다운로드</a>
							<div class="btn_area">
								<button type="button" class="btn_back2 reset_btn"><em class="fas fa-redo"></em>처음으로</button>
							</div>
						</div>
						<!--tr_3-->
					</div>
					<!--// superR_box-->

				</div>
				<!-- //.srmobox -->
				<!--.srmenu-->
				<div class="demobox vision_menu" id="srmenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API 공통 가이드
							</div>
							<p class="sub_title">개발 환경 세팅</p>
							<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
							<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

						</div>
						<div class="guide_group">
							<div class="title">
								ESR <small>(Enhanced Super Resolution)</small>
							</div>
							<p class="sub_txt">  작은 크기의 이미지 사진의 손실률을 줄이면서 크기를 확대해 줍니다.</p>

							<span class="sub_title">
								준비사항
							</span>
							<p class="sub_txt">- Input: 이미지 파일 </p>
							<ul>
								<li>확장자: .jpg, .png.</li>
								<li>용량: 30KB 이하</li>
								<li>크기: 200 x 200 픽셀 이하</li>
							</ul>
							<span class="sub_title">
								 실행 가이드
							</span>
							<p class="sub_txt">① Request  </p>
							<ul>
								<li>Method : POST</li>
								<li>URL : https://api.maum.ai/api/esr</li>
							</ul>
							<p class="sub_txt">② Request 파라미터 설명 </p>
							<table>
								<tr>
									<th>키</th>
									<th>설명</th>
									<th>type</th>
								</tr>
								<tr>
									<td>apiId </td>
									<td>사용자의 고유 ID. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>apiKey </td>
									<td>사용자의 고유 key. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>file</td>
									<td>type:file (.jpg,.png) 이미지 파일(200x200 픽셀 이하) </td>
									<td>string</td>
								</tr>
							</table>
							<p class="sub_txt">③ Request 예제 </p>
							<div class="code_box">
								curl -X POST \<br>
								&nbsp; https://api.maum.ai/api/esr \<br>
								&nbsp; -H 'content-type: multipart/form-data;<br>
								boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \<br>
								&nbsp; -F apiId=(*ID 요청 필요) \<br>
								&nbsp; -F apiKey=(*key 요청 필요) \<br>
								&nbsp; -F 'file=@sample.jpg'
							</div>

							<p class="sub_txt">④ Response 예제 </p>

							<div class="code_box">
								<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_sr_response.png" alt="super resolution image" style="width:20%;">
							</div>
						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--.srmenu-->
				<!--.srexample-->
				<div class="demobox" id="srexample">
					<p><em style="color:#f7778a;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

					<!-- super resolution(super resolution) -->
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>CCTV 속 인물 탐지</span>
									</dt>
									<dd class="txt">CCTV 영상 속 인물의 작은 사진을 손실률을 줄이면서 확대하여 Identify합니다. CCTV 뿐만 아니라, 비디오 또는 사진 내 작은 인물의 이미지를 확대에 활용할 수 있습니다. </dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_tr"><span>ESR</span></li>
											<li class="ico_avr"><span>AVR</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>옛날 사진 복원</span>
									</dt>
									<dd class="txt">옛날 사진 또는 저화질 이미지의 복원에 활용될 수 있습니다. 딥러닝 기술을 이용하여 저해상도의 사진의 해상도를 늘려서 복원해보세요.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_tr"><span>ESR</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>ID 사진</span>
									</dt>
									<dd class="txt">주민등록증, 운전면허증 등 ID에 나와있는 작은 크기의 사진을 확대하여 보여줍니다. 확대 시에 손실되는 이미지를 해상도를 늘려 복원해 줍니다. </dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_tr"><span>ESR</span></li>
										</ul>
									</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //super resolution -->
				</div>
				<!--//.srexample-->


			</div>
			<!-- //.content -->
		</div>
		
<script type="text/javascript">


//파일명 변경
document.querySelector("#demoFile").addEventListener('change', function (ev) {

	$('.fl_box').on('click', function(){
		$(this).css("opacity", "1");
		$('.tr_1 .btn_area .disBox').remove();
	});

	//이미지 용량 체크
	var demoFileInput = document.getElementById('demoFile');
	var demoFile = demoFileInput.files[0];
	var demoFileSize = demoFile.size;
	var max_demoFileSize = 1024 * 30;//1kb는 1024바이트
	var demofile = document.querySelector("#demoFile");

	if(demoFileSize > max_demoFileSize || !demofile.files[0].type.match(/image.*/)){
		$('.pop_simple').show();
		// 팝업창 닫기
		$('.pop_close, .pop_bg, .btn a').on('click', function () {
			$('.pop_simple').fadeOut(300);
			$('body').css({
				'overflow': '',
			});
		});
		$('.fl_box').css("opacity", "0.5");
		$('#demoFile').val('');
		$('.tr_1 .btn_area').append('<span class="disBox"></span>');

	}else{
		document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
		var element = document.getElementById( 'uploadFile' );
		element.classList.remove( 'btn' );
		element.classList.add( 'btn_change' );
		$('.fl_box').css("opacity", "0.5");
		$('.tr_1 .btn_area .disBox').remove();
	}

});


var sampleImage1;
var sampleImage2;
var data;

let image1_blob;
let image2_blob;

jQuery.event.add(window,"load",function(){
	function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/images/img_sr_sample1.png");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            //sampleImage1 = new File([blob], "img_sr_sample1.png");
            
            var imgSrcURL = URL.createObjectURL(blob);
    		var esr_output=document.getElementById('input_img');
    		esr_output.setAttribute("src",imgSrcURL);

			var output=document.getElementById('origin_img');
			output.setAttribute("src",imgSrcURL);

			image1_blob = blob;
		};
		
        xhr.send();
    }

	function loadSample2() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/images/img_sr_sample2.png");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            //sampleImage2 = new File([blob], "img_sr_sample2.png");
            
            var imgSrcURL = URL.createObjectURL(blob);
    		var esr_output=document.getElementById('input_img');
    		esr_output.setAttribute("src",imgSrcURL);

			var output=document.getElementById('origin_img');
			output.setAttribute("src",imgSrcURL);

			image2_blob = blob;
        };
		
        xhr.send();
    }	
	
	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#input_img').attr('src', e.target.result);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
	$(document).ready(function (){
		loadSample1();
		loadSample2();

 		$("#demoFile").change(function(){
			readURL(this);
		});

 		$('#reload_esr').on('click', function () {
			window.location.reload();
		});

 		//결과보기 버튼 누를 때
		$('#sub').on('click',function (){
			var demoFileTxt = $('#demoFile').val(); //파일을 추가한 input 박스의 값

			if(demoFileTxt == ""){
				var demoFile;
				var option = $("input[type=radio][name=option]:checked").val();
				if(option == 1){
					loadSample1();
					//demoFile = sampleImage1;
					demoFile = image1_blob;
				}else{
					loadSample2();
					//demoFile = sampleImage2;
					demoFile = image2_blob;
				}
				//샘플전송
				var formData = new FormData();

				formData.append('file',demoFile);
				formData.append('${_csrf.parameterName}', '${_csrf.token}');

				var request = new XMLHttpRequest();

				request.onloadstart = function(ev) {
					request.responseType = "blob";
				}
				request.onreadystatechange = function(){
					if (request.readyState === 4){

						if(request.status === 200){

							if(request.response == null || request.response == ""){
								alert("서버에서 응답을 받지 못했습니다. 다시 시도해 주세요");
								window.location.reload();
							}

							console.log(request.response);
							var blob = request.response;
							var imgSrcURL = URL.createObjectURL(blob);

							var esr_output=document.getElementById('output_img');
							esr_output.setAttribute("src",imgSrcURL);
							$("#result_img").attr("src", imgSrcURL);

							data = new Blob([request.response], {type:'image/jpeg'});

							$('.tr_1').hide();
							$('.tr_2').hide();
							$('.tr_3').fadeIn(300);

						}else{
							alert("서버에서 응답을 받지 못했습니다. 다시 시도해 주세요");
							window.location.reload();
						}

					}
				};

				request.open('POST', '${pageContext.request.contextPath}/api/getSuperResolution');
				request.timeout = 20000;
				request.send(formData);

				request.ontimeout=function() {
					alert('서버 응답 시간이 초과되었습니다. 다시 시도해 주세요');
					window.location.reload();
				};

				$('.tr_1').hide();
				$('.tr_2').fadeIn(300);
			}
			else {
				var demoFileInput = document.getElementById('demoFile');
				var demoFile = demoFileInput.files[0];
				var formData = new FormData();

				formData.append('file',demoFile);
				formData.append('${_csrf.parameterName}', '${_csrf.token}');

				var request = new XMLHttpRequest();
				request.onloadstart = function(ev) {
					request.responseType = "blob";
				}
				request.onreadystatechange = function(){
					if (request.readyState === 4){

						if(request.status === 200){

							if(request.response == null || request.response == ""){
								alert("서버에서 응답을 받지 못했습니다. 다시 시도해 주세요");
								window.location.reload();
							}

							console.log(request.response);
							var blob = request.response;
							var imgSrcURL = URL.createObjectURL(blob);

							//팝업 원본
							var origin_img= $('#input_img').attr('src');
							$("#origin_img").attr("src", origin_img);

							//결과
							var esr_output=document.getElementById('output_img')
							esr_output.setAttribute("src",imgSrcURL);
							//결과 (팝업)
							$("#result_img").attr("src", imgSrcURL);
							data = new Blob([blob], {type:'image/jpeg'});

							$('.tr_1').hide();
							$('.tr_2').hide();
							$('.tr_3').fadeIn(300);

						}else{
							alert("서버에서 응답을 받지 못했습니다. 다시 시도해 주세요");
							window.location.reload();
						}
					}
				};

				request.open('POST', '${pageContext.request.contextPath}/api/getSuperResolution');
				request.timeout = 20000;
				request.send(formData);

				request.ontimeout=function() {
					alert('서버 응답 시간이 초과되었습니다. 다시 시도해 주세요');
					window.location.reload();
				};

				$('.tr_1').hide();
				$('.tr_2').fadeIn(300);
			}
		});


		// step1 (close button)
		$('em.close').on('click', function () {
			$('.fl_box').css("opacity", "1");
			$(this).parent().removeClass("btn_change");
			$(this).parent().addClass("btn");
			$(this).parent().children('.demolabel').text('이미지 업로드');
			$('#demoFile').val('');
			//파일명 변경
			document.querySelector("#demoFile").addEventListener('change', function (ev) {
				document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
				var element = document.getElementById( 'uploadFile' );
				element.classList.remove( 'btn' );
				element.classList.add( 'btn_change' );
			});
		});

		// step2->step3
		$('.btn_back1').on('click', function () {
			$('.tr_2').hide();
			$('.tr_1').fadeIn(300);
			$('.fl_box').css("opacity", "1");
		});

		// step3->step1
		$('.btn_back2').on('click', function () {
			$('.tr_3').hide();
			$('.tr_1').fadeIn(300);
			$('.fl_box').css("opacity", "1");
			var label_change = $('.demolabel');
			label_change.text('이미지 업로드');
			label_change.parent().removeClass('btn_change');
			label_change.parent().addClass('btn');
			$('#demoFile').val('');
		});

		// 결과파일 팝업 (새창)
		$('.result_check').on('click', function () {
			$('.pop_confirm').fadeIn(300);

		});

		// 결과 팝업창 닫기
		$('.close, .pop_bg').on('click', function () {
			$('.pop_confirm').fadeOut(300);
			$('body').css({
				'overflow': '',
			});
		});
	});
});

function downloadResultImg(){
	let fileName = "SuperResolution.jpg";
    download(data, fileName, "image/jpg");
}

//API 탭
function openTap(evt, menu) {
	var i, demobox, tablinks;
	demobox = document.getElementsByClassName("demobox");
	for (i = 0; i < demobox.length; i++) {
		demobox[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(menu).style.display = "block";
	evt.currentTarget.className += " active";
}
document.getElementById("defaultOpen").click();

</script>
