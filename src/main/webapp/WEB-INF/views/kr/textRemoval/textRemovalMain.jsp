<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/download.js"></script>

		<!-- .contents -->
		<div class="contents api_content">
			<!-- .content -->
			<div class="content">
				<h1 class="api_tit">텍스트 제거</h1>
				<ul class="menu_lst vision_lst">
					<li class="tablinks" onclick="openTap(event, 'trdemo')" id="defaultOpen"><button type="button">데모</button></li>
					<li class="tablinks" onclick="openTap(event, 'trexample')"><button type="button">적용사례</button></li>
					<li class="tablinks" onclick="openTap(event, 'trmenu')"><button type="button">매뉴얼</button></li>
<%--					<li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
				</ul>
				<!-- .demobox -->

				<div class="demobox" id="trdemo">
					<p><span>텍스트 제거</span> <small>(Text Removal)</small> </p>
					<span class="sub">이미지에 있는 텍스트만 지우고 inpainting 해줍니다.</span>
					<!--textremoval_box-->
					<div class="demo_layout textremoval_box">
						<!--tr_1-->
						<div class="tr_1">
							<div class="fl_box">
								<p><em class="far fa-file-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
								<div class="sample_box">
									<div class="sample_1">
										<div class="radio">
											<input type="radio" id="sample1" name="option" value="1" checked>
											<label for="sample1" class="female">
												<div class="img_area">
													<img src="${pageContext.request.contextPath}/aiaas/kr/images/sample_1_b4.jpg" alt="sample img 1" />
												</div>
												<span>샘플 1</span>
											</label>
										</div>
									</div>
									<div class="sample_2">
										<div class="radio">
											<input type="radio" id="sample2" name="option" value="2" >
											<label for="sample2"  class="male">
												<div class="img_area">
													<img src="${pageContext.request.contextPath}/aiaas/kr/images/sample_2_b4.png" alt="sample img 2" />
												</div>
												<span>샘플 2</span>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="fr_box">
								<p><em class="far fa-file-image"></em><strong>내 파일</strong>로 해보기</p>
								<div class="uplode_box">
									<div class="btn" id="uploadFile">
										<em class="fas fa-times hidden close"></em>
										<em class="far fa-file-image hidden"></em>
										<label for="demoFile" class="demolabel">이미지 업로드</label>
										<input type="file" id="demoFile" class="demoFile" accept=".jpg, .png">
									</div>
									<ul>
										<li>* 지원가능 파일 확장자: .jpg, .png</li>
										<li>* 이미지 용량 2MB 이하만 가능합니다.</li>
									</ul>
								</div>
							</div>
							<div class="bottom_box" style="display:none">
								<p><em class="fas fa-sort-amount-up"></em><strong>지우기 강도 선택</strong></p>
								<div class="range_box">
									<div class="range_num"><span>1</span><span>2</span><span>3</span><span>4</span><span class="lstnum">5</span></div>
									<div class="slidecontainer">
										<input type="range" min="1" max="5" value="2" step="1" class="slider" id="myRange">
									</div>
								</div>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_start" id="sub">결과보기</button>
							</div>
						</div>
						<!--tr_1-->
						<!--tr_2-->
						<div class="tr_2">
							<p><em class="far fa-file-image"></em>텍스트 지우는 중</p>
							<div class="loding_box ">
								<!-- ball-scale-rotate -->
								<div class="lds">
									<div class="ball-scale-rotate">
										<span> </span>
										<span> </span>
										<span> </span>
									</div>
								</div>
								<p>약간의 시간이 소요 됩니다.</p>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_back1" id="reload_textRemoval"><em class="fas fa-redo"></em>처음으로</button>
							</div>

						</div>
						<!--tr_2-->
						<!--tr_3-->
						<div class="tr_3" >
							<div class="origin_file">
								<p><em class="far fa-file-image"></em>원본 파일</p>
								<div class="imgBox">
									<img id="input_img" src="" alt="원본 이미지" />
								</div>
							</div>
							<div class="result_file" >
								<p><em class="far fa-file-image"></em>결과 파일</p>
								<div class="imgBox">
									<img id="output_img" src="" alt="결과 이미지" />
								</div>
								<a id="save" onclick="downloadResultImg();" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> 결과 파일 다운로드</a>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
							</div>
						</div>
						<!--tr_3-->
					</div>
					<!--// textremoval_box-->
					<span class="remark">* 비디오 작업을 원할 시에는, 고객센터로 문의하세요.</span>
				</div>
				<!-- //.demobox -->
				<!--.trmenu-->
				<div class="demobox vision_menu" id="trmenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API 공통 가이드
							</div>
							<p class="sub_title">개발 환경 세팅</p>
							<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
							<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

						</div>
						<div class="guide_group">
							<div class="title">
								텍스트 제거 <small>(Text Removal)</small>
							</div>
							<p class="sub_txt"> 이미지 내 텍스트를 인식하여 제거해줍니다.</p>

							<span class="sub_title">
								준비사항
							</span>
							<p class="sub_txt">- Input: 이미지 파일 </p>
							<ul>
								<li>확장자: .jpg, .png.</li>
								<li>특징: 지우고자 하는 텍스트가 있는 이미지 파일</li>
							</ul>
							<span class="sub_title">
								 실행 가이드
							</span>
							<p class="sub_txt">① Request  </p>
							<ul>
								<li>Method : POST</li>
								<li> URL : https://api.maum.ai/api/txtr/</li>
							</ul>
							<p class="sub_txt">② Request 파라미터 설명 </p>
							<table>
								<tr>
									<th>키</th>
									<th>설명</th>
									<th>type</th>
								</tr>
								<tr>
									<td>apiId </td>
									<td>사용자의 고유 ID. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>apiKey </td>
									<td>사용자의 고유 key. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>file</td>
									<td>type:file (.jpg,.png) 이미지 파일  </td>
									<td>string</td>
								</tr>
							</table>
							<p class="sub_txt">③ Request 예제 </p>
							<div class="code_box">
								curl -X POST \ <br>
								https://api.maum.ai/api/txtr/ \<br>
								-H 'content-type: multipart/form-data;<br>
								boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \<br>
								-F apiId=(*ID 요청 필요) \<br>
								-F apiKey=(*key 요청 필요) \<br>
								-F 'file=@sample.jpg'<br>
							</div>

							<p class="sub_txt">④ Response 예제 </p>

							<div class="code_box">
								<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_textremoval.png" alt="text removal image" style="width:20%;">
							</div>
						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--.trmenu-->
				<!--.trexample-->
				<div class="demobox" id="trexample">
					<p><em style="color:#f7778a;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

					<!-- 텍스트 제거(Text Removal) -->
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>상표 제거</span>
									</dt>
									<dd class="txt">상표권 침해 우려가 있는 이미지의 경우 문제가 될 수 있는 텍스트만 찾아내서 지울 수 있습니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_tr"><span>텍스트 제거</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>이미지 내 <strong>텍스트 수정</strong></span>
									</dt>
									<dd class="txt">이미지 안에 있는 텍스트를 수정하고 싶을 때, 일단 텍스트 제거 엔진을 통해 텍스트만 제거하여 이미지만 남길 수 있습니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_tr"><span>텍스트 제거</span></li>
										</ul>
									</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //텍스트 제거(Text Removal) -->
				</div>
				<!--//.trexample-->


			</div>
			<!-- //.content -->
		</div>

<script type="text/javascript">
var sampleImage1;
var sampleImage2;
var data;

let image1_blob;
let image2_blob;

jQuery.event.add(window,"load",function(){
	function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/images/sample_1_b4.jpg");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            //sampleImage1 = new File([blob], "sample_1_b4.jpg");

            var imgSrcURL = URL.createObjectURL(blob);
    		var textr_output=document.getElementById('input_img')
    		textr_output.setAttribute("src",imgSrcURL);

			image1_blob = blob;
        }

        xhr.send();
    }

	function loadSample2() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/images/sample_2_b4.png");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            //sampleImage2 = new File([blob], "sample_2_b4.png");

            var imgSrcURL = URL.createObjectURL(blob);
    		var textr_output=document.getElementById('input_img')
    		textr_output.setAttribute("src",imgSrcURL);

			image2_blob = blob;
        }

        xhr.send();
    }

	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#input_img').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}
	$(document).ready(function (){

		$('#reload_textRemoval').on('click', function () {
			window.location.reload();
		});


		loadSample1();
		loadSample2();
 		$("#demoFile").change(function(){
			readURL(this);
		});
		$('#sub').on('click',function (){
			//지우기 강도 임시 주석처리
			//var eraseDegree = $('#myRange').val();

			var demoFileTxt = $('#demoFile').val(); //파일을 추가한 input 박스의 값

			if(demoFileTxt == ""){
				var demoFile;
				var option = $("input[type=radio][name=option]:checked").val();
				if(option == 1){
					loadSample1();
					//demoFile = sampleImage1;
					demoFile = image1_blob;
				}else{
					loadSample2();
					//demoFile = sampleImage2;
					demoFile = image2_blob;
				}
				//샘플전송
				var formData = new FormData();

				formData.append('file',demoFile);
				formData.append('${_csrf.parameterName}', '${_csrf.token}');

				var request = new XMLHttpRequest();
				//request.responseType ="blob";
				request.onloadstart = function(ev) {
					request.responseType = "blob";
				}
				request.onreadystatechange = function(){
					if (request.readyState === 4){
						console.log(request.response);
						var blob = request.response;
						var imgSrcURL = URL.createObjectURL(blob);

						var textr_output = document.getElementById('output_img');
						textr_output.setAttribute("src",imgSrcURL);

						data = new Blob([request.response], {type:'image/jpeg'});
						// downloadResultImg();

						$('.tr_1').hide();
						$('.tr_2').hide();
						$('.tr_3').fadeIn(300);
					}
				};
				request.timeout = 20000;
				request.ontimeout = function() {
					alert('timeout');
					window.location.reload();
				};
				request.open('POST', '${pageContext.request.contextPath}/api/getTextRemoval');
				request.send(formData);

				$('.tr_1').hide();
				$('.tr_2').fadeIn(300);
			}
			else {
				var demoFileInput = document.getElementById('demoFile');
				console.log(demoFileInput);
				var demoFile = demoFileInput.files[0];
				console.log(demoFile);

				var formData = new FormData();

				formData.append('file',demoFile);
				formData.append('${_csrf.parameterName}', '${_csrf.token}');

				var request = new XMLHttpRequest();
				request.onloadstart = function(ev) {
					request.responseType = "blob";
				}
				request.onreadystatechange = function(){
					if (request.readyState === 4){
						console.log(request.response);
						var blob = request.response;
						var imgSrcURL = URL.createObjectURL(blob);

						var textr_output=document.getElementById('output_img')
						textr_output.setAttribute("src",imgSrcURL);

						data = new Blob([blob], {type:'image/jpeg'});
						// downloadResultImg();

						$('.tr_1').hide();
						$('.tr_2').hide();
						$('.tr_3').fadeIn(300);
					}
				};
				request.open('POST', '${pageContext.request.contextPath}/api/getTextRemoval');
				request.send(formData);

				$('.tr_1').hide();
				$('.tr_2').fadeIn(300);
			}
		});

	});
});

function downloadResultImg(){
	let fileName = "TextRemoval.jpg";
	download(data, fileName, "image/jpg");
}

</script>
<script>
	//파일명 변경
	document.querySelector("#demoFile").addEventListener('change', function (ev) {
		document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
		var element = document.getElementById( 'uploadFile' );
		element.classList.remove( 'btn' );
		element.classList.add( 'btn_change' );
		$('.fl_box').css("opacity", "0.5");
	});



	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){

			// step1->step2  (이미지 선택)
			// $('.btn').on('click', function () {
			// 	$('.fl_box').css("opacity", "0.5");
			//
			// });
			// step1->step2  (close button)
			$('em.close').on('click', function () {
				console.log(3);
				$('.fl_box').css("opacity", "1");
				$(this).parent().removeClass("btn_change");
				$(this).parent().addClass("btn");
				$('.fl_box').css('opacity', '1');
				$(this).parent().children('.demolabel').text('이미지 업로드');

				//파일명 변경
				document.querySelector("#demoFile").addEventListener('change', function (ev) {
					document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
					var element = document.getElementById( 'uploadFile' );
					element.classList.remove( 'btn' );
					element.classList.add( 'btn_change' );
				});
			});
			// step1->step2
			$('.btn_start').on('click', function () {
				//$('.tr_1').hide();
				//$('.tr_2').fadeIn(300);

			});

			// step2->step3
			$('.btn_back1').on('click', function () {
				$('.tr_2').hide();
				$('.tr_1').fadeIn(300);
				$('.fl_box').css("opacity", "1");
			});

			// step3->step1
			$('.btn_back2').on('click', function () {
				$('.tr_3').hide();
				$('.tr_1').fadeIn(300);
				$('.fl_box').css("opacity", "1");
				document.querySelector("#demoFile").addEventListener('change', function (ev) {
					document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;

				});
			});

		});
	});
	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();

</script>