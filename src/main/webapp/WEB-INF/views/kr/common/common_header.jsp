<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
	String styleCss = application.getRealPath("${pageContext.request.contextPath}/aiaas/kr/css/login_renew.css");
	File style = new File(styleCss);
	Date lastModifiedStyle = new Date(); 
 
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">

<!-- Open Graph Tag -->
<meta property="og:title"            content="maum.ai"/>
<meta property="og:type"             content="website"/><!-- 웹 페이지 타입 -->
<meta property="og:url"              content="https://maum.ai"/>
<meta property="og:image"            content="${pageContext.request.contextPath}/aiaas/common/images/maum.ai_web.png"/>
<meta property="og:description"      content="웹인공지능이 필요할땐 마음AI"/>
<!-- icon_favicon -->
<link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
<link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
<title>maum.ai platform</title>
<!-- resources -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css">
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/common.css"> --%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/pop_common.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/login_renew.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/pricing.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/academy.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/user.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/voicealbum.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<!-- //resources -->

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.0.min.js"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/common.js"></script>


	<!-- floating chat : gitple -->
<%--	<script>--%>
<%--		window.GitpleConfig = {appCode: '617StyiVdqik5b3GXRbaRv9tXs16w3'};--%>
<%--		!function () {--%>
<%--			function e() {--%>
<%--				function e() {--%>
<%--					var e = t.contentDocument, a = e.createElement("script");--%>
<%--					a.type = "text/javascript", a.async = !0, a.src = window[n] && window[n].url ? window[n].url + "/inapp-web/gitple-loader.js" : "https://app.gitple.io/inapp-web/gitple-loader.js", a.charset = "UTF-8", e.head && e.head.appendChild(a)--%>
<%--				}--%>
<%--				var t = document.getElementById(a);--%>
<%--				t || ((t = document.createElement("iframe")).id = a, t.style.display = "none", t.style.width = "0", t.style.height = "0", t.addEventListener ? t.addEventListener("load", e, !1) : t.attachEvent ? t.attachEvent("onload", e) : t.onload = e, document.body.appendChild(t))--%>
<%--			}--%>
<%--			var t = window, n = "GitpleConfig", a = "gitple-loader-frame";--%>
<%--			if (!window.Gitple) {--%>
<%--				document;--%>
<%--				var i = function () {--%>
<%--					i.ex && i.ex(arguments)--%>
<%--				};--%>
<%--				i.q = [], i.ex = function (e) {--%>
<%--					i.processApi ? i.processApi.apply(void 0, e) : i.q && i.q.push(e)--%>
<%--				}, window.Gitple = i, t.attachEvent ? t.attachEvent("onload", e) : t.addEventListener("load", e, !1)--%>
<%--			}--%>
<%--		}();--%>
<%--		Gitple('boot');--%>
<%--	</script>--%>

<%--	<script>--%>
<%--		window.GitpleConfig = {"appCode":"ubFQIWPWwu4xGxC4kMsPhe2mei2533"};--%>
<%--		!function(){function e(){function e(){var e=t.contentDocument,a=e.createElement("script");a.type="text/javascript",a.async=!0,a.src=window[n]&&window[n].url?window[n].url+"/inapp-web/gitple-loader.js":"https://app.gitple.io/inapp-web/gitple-loader.js",a.charset="UTF-8",e.head&&e.head.appendChild(a)}var t=document.getElementById(a);t||((t=document.createElement("iframe")).id=a,t.style.display="none",t.style.width="0",t.style.height="0",t.addEventListener?t.addEventListener("load",e,!1):t.attachEvent?t.attachEvent("onload",e):t.onload=e,document.body.appendChild(t))}var t=window,n="GitpleConfig",a="gitple-loader-frame";if(!window.Gitple){document;var i=function(){i.ex&&i.ex(arguments)};i.q=[],i.ex=function(e){i.processApi?i.processApi.apply(void 0,e):i.q&&i.q.push(e)},window.Gitple=i,t.attachEvent?t.attachEvent("onload",e):t.addEventListener("load",e,!1)}}();--%>
<%--		Gitple('boot');--%>
<%--	</script>--%>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<%--	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122649087-1"></script>--%>
<%--	<script>--%>
<%--		window.dataLayer = window.dataLayer || [];--%>
<%--		function gtag(){dataLayer.push(arguments);}--%>
<%--		gtag('js', new Date());--%>
<%--		gtag('config', 'UA-122649087-1');--%>
<%--	</script>--%>

</head>