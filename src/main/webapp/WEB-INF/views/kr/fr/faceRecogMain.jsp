<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
	Date lastModifiedStyle = new Date();
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/fr.css?ver=<%=fmt.format(lastModifiedStyle)%>" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/pop_common.css?ver=<%=fmt.format(lastModifiedStyle)%>" />
<!-- 5 .pop_simple -->
<div class="pop_simple">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap pop_sr_noti">
		<button class="pop_close" type="button">닫기</button>
		<!-- .pop_bd -->
		<div class="pop_bd">
			<em class="fas fa-sad-cry"></em>
			<h5>이미지 업로드 실패</h5>
			<p>이미지 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
				업로드 되지 않았습니다.</p>
			<span>* 지원가능 파일 확장자: .jpg, .png<br>
* 이미지 용량 3MB 이하만 가능합니다.<br>
* 사이즈 200 x 200 pixel 이하만 가능합니다.</span>

		</div>
		<!-- //.pop_bd -->
		<div class="btn">
			<a class="">확인</a>
		</div>
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">Face Recognition</h1>
		<ul class="menu_lst vision_lst">
			<li class="tablinks" onclick="openTap(event, 'frdemo')" id="defaultOpen"><button type="button">데모</button></li>
			<li class="tablinks" onclick="openTap(event, 'frexample')"><button type="button">적용사례</button></li>
			<li class="tablinks" onclick="openTap(event, 'frmenu')"><button type="button">매뉴얼</button></li>
<%--			<li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
		</ul>
		<!--demobox-->
		<div class="demobox" id="frdemo">
			<p><span>Face Recognition</span> </p>
			<span class="sub">사람의 얼굴 이미지를 512차원 Vector화하고 그 값을 대조하여 얼굴을 인식합니다.<br>
				<small>* 등록하는 얼굴 이미지는 저장하지 않습니다. 등록 이미지를 벡터화한 값만 저장하며, 해당 값은 보안상의 이유로 결과 인식 후 즉시 파기합니다.</small></span>
			<!--faceR_box-->
			<div class="demo_layout faceR_box">
				<div class="sampletest_box">
					<p><em class="far fa-file-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
					<div class="demo_layout">
							<div class="small_layout fl-data">
								<p><em class="fas fa-database"></em>준비된 데이터 베이스</p>
								<ul>
									<li>
										<div class="img_area">
											<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample1_1.png" alt="sample img"/>
										</div>
										<P>FaceID :<span>Anne</span></P>
									</li>
									<li>
										<div class="img_area">
											<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample2_1.png" alt="sample img"/>
										</div>
										<P>FaceID :<span>Edward</span></P></li>
									<li>
										<div class="img_area">
											<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample3_1.png" alt="sample img"/>
										</div>
										<P>FaceID :<span>Nancy</span></P>
									</li>
									<li>
										<div class="img_area">
											<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample4_1.png" alt="sample img"/>
										</div>
										<P>FaceID :<span>Marquis</span></P>
									</li>
								</ul>
							</div>
							<div class="small_layout fr_face">
								<p><em class="fas fa-user-check"></em>인식할 얼굴 선택</p>
								<ul>
									<li class="radio">
										<input type="radio" id="sample1" name="option" value="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample1_2.png" checked>
										<label for="sample1" class="sample">
											<span class="img_area">
												<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample1_2.png" alt="recognition img"/>
											</span>
										</label>
									</li>
									<li class="radio">
										<input type="radio" id="sample2" name="option" value="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample2_2.png">
										<label for="sample2" class="sample">
											<span class="img_area">
												<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample2_2.png" alt="recognition img"/>
											</span>
										</label>
									</li>
									<li class="radio">
										<input type="radio" id="sample3" name="option" value="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample3_2.png">
										<label for="sample3" class="sample">
											<span class="img_area">
												<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample3_2.png" alt="recognition img"/>
											</span>
										</label>
									</li>
									<li class="radio">
										<input type="radio" id="sample4" name="option" value="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample4_2.png">
										<label for="sample4" class="sample">
											<span class="img_area">
												<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample4_2.png" alt="recognition img"/>
											</span>
										</label>
									</li>
								</ul>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_start" id="sampleTest">결과보기</button>
							</div>
					</div>
				</div>
				<div class="myfile_box">
					<p><em class="far fa-file-image"></em><strong>내 파일</strong>로 테스트 하기</p>
					<div class="demo_layout">
						<div class="small_layout">
							<p><em class="fas fa-database"></em>데이터 베이스 등록</p>
							<div class="data_regist">
								<div class="example_box">
									<div class="img_area">
										<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample1_1.png" alt="sample img"/>
									</div>
									<P>FaceID :<span>Anne</span></P>
								</div>
								<div class="example_desc">
									<p>사진 업로드</p>
									<p class="faceid_input">FaceID 입력</p>
								</div>
								<div class="data_upload">
									<div class="uplode_box">
										<div id="uploadFile1" class="btn uploadFile1">
											<em class="fas fa-times hidden close" id = "level1"></em>
											<img class="outputImg" alt="업로드 이미지 미리보기" />
											<input type="file" id="demoFile1" class="demoFile" accept=".jpg, .png">
											<label for="demoFile1" class="demolabel">이미지 업로드</label>
										</div>
										<div class="input_id">
											<p>FaceID :</p>
											<div class="input_level1" id="level1_back">
												<input type="text" placeholder="10자 이내" id = "demoName1"  title="Face Id 입력란"/>
												<button type="submit" id="save1">저장</button>
											</div>
											<div class="input_level2" id="level1_text">
												<span>이름 입력 완료</span>
											</div>
										</div>
									</div>
									<div class="uplode_box">
										<div id="uploadFile2" class="btn uploadFile1">
											<em class="fas fa-times hidden close" id = "level2"></em>
											<img class="outputImg" alt="업로드 이미지 미리보기" />
											<input type="file" id="demoFile2" class="demoFile" accept=".jpg, .png">
											<label for="demoFile2" class="demolabel">이미지 업로드</label>
										</div>
										<div class="input_id">
											<p>FaceID :</p>
											<div class="input_level1" id="level2_back">
												<input type="text" placeholder="10자 이내" id = "demoName2" title="Face Id 입력란" />
												<button type="submit" id="save2">저장</button>
											</div>
											<div class="input_level2" id="level2_text">
												<span>이름 입력 완료</span>
											</div>
										</div>
									</div>
									<div class="uplode_box">
										<div id="uploadFile3" class="btn uploadFile1">
											<em class="fas fa-times hidden close" id = "level3"></em>
											<img class="outputImg" alt="업로드 이미지 미리보기" />
											<input type="file" id="demoFile3" class="demoFile" accept=".jpg, .png">
											<label for="demoFile3" class="demolabel">이미지 업로드</label>
										</div>
										<div class="input_id">
											<p>FaceID :</p>
											<div class="input_level1" id="level3_back">
												<input type="text" placeholder="10자 이내" id = "demoName3"  title="Face Id 입력란"/>
												<button type="submit" id="save3">저장</button>
											</div>
											<div class="input_level2" id="level3_text">
												<span>이름 입력 완료</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="small_layout fr_face">
							<p><em class="fas fa-user-check"></em>인식할 얼굴 선택</p>
							<div class="uplode_box">
								<div id="uploadFile" class="btn uploadFile1">
									<em class="fas fa-times hidden close"></em>
									<img class="outputImg" alt="업로드 이미지 미리보기" />

									<input type="file" id="demoFile" class="demoFile" accept=".jpg, .png" >
									<label for="demoFile" class="input_label">이미지 업로드</label>
								</div>
								<p class="upload_desc">* 지원가능 파일 확장자: .jpg, .png<br>
									* 이미지 용량 3MB 이하 </p>
							</div>
						</div>
						<div class="fr_desc">
							<em class="fas fa-exclamation-circle"></em>
							<ul>
								<li>* 한 사람의 눈, 코, 입이 모두 보이는 [얼굴 사진]을 등록해 주세요.</li>
								<li>* 일부가 잘리거나 손상된 사진은 피해주세요.</li>
								<li>* 깨끗하고 선명한 사진의 경우 인식률이 높아집니다. </li>
							</ul>
						</div>
						<div class="btn_area">
							<button type="button" class="btn_start" id="demoResult">결과보기</button>
						</div>

					</div>
				</div>
			</div>
			<!--//faceR_box-->
			<!-- .fr_2 -->
			<div class="fr_2">
				<p><em class="far fa-file-image"></em>안면 인식 중</p>
				<div class="loding_box ">

					<!-- ball-scale-rotate -->
					<div class="lds">
						<div class="ball-scale-rotate">
							<span> </span>
							<span> </span>
							<span> </span>
						</div>
					</div>

					<p>약간의 시간이 소요 됩니다.</p>
				</div>
			</div>
			<!-- //.fr_2 -->
			<!-- .fr_3 -->
			<div class="demo_layout fr_3">
				<div class="">
					<div class="result_box">
						<p><em class="fas fa-file-image"></em>입력 파일</p>
						<div class="img_file">
							<div class="input_img">
								<%--<img id="resultImg" src="${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample1_1.png" alt="입력 파일"/>--%>
									<img id="resultImg" alt="입력 파일"/>
							</div>
						</div>
					</div>
					<div class="result_box">
						<p><em class="fas fa-code"></em>결과값</p>
						<div class="code_value">
							<p class="result_t">데이터 베이스에 일치되는 값이 있습니다.</p>
							<p class="face_id"><span id="faceId">FaceID:</span> <span id="facd_name"></span></p>
<textarea id="result_txt" name="결과값 코드">
<%--{
	"result": {
		"width": 900,
		"height": 780,
		"faces": [
			{
				"facial_attributes": {
					"gender": {
						"male": 0.0001852084242273122,
						"female": 0.9998148083686829
					},--%>
</textarea>
						</div>
					</div>
					<div class="btn_area">
						<button type="button" class="reset_btn" id=""><em class="fas fa-redo"></em>처음으로</button>
					</div>
				</div>
			</div>
			<!-- .fr_3 -->
		</div>
		<!-- .demobox -->

		<!--.frmenu-->
		<div class="demobox vision_menu" id="frmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API 공통 가이드
					</div>
					<p class="sub_title">개발 환경 세팅</p>
					<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
					<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

				</div>
				<div class="guide_group">
					<div class="title">
						얼굴 인식(Face Recognition)
					</div>
					<p class="sub_txt">얼굴 이미지를 512차원 벡터화하여 그 값을 저장하고, 대조하여 인식해줍니다.</p>
					<span class="sub_title">
							준비사항
					</span>
					<p class="sub_txt">① Input: 얼굴 이미지 파일 </p>
					<ul>
						<li>확장자: .jpg, .png.</li>
						<li>용량: 3MB 이하</li>
					</ul>
					<span class="sub_title">
							 실행 가이드
					</span>
					<em>​setFace : 사람의 얼굴 이미지를 512차원 벡터화하여 지정한 DB에 저장합니다. (API 서버 내에 Json 타입으로 저장)</em>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : PUT</li>
						<li>URL : ​https://api.maum.ai/insight/app/setFace</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>사용자의 고유 key.</td>
							<td>string</td>
						</tr>
						<tr>
							<td>faceId</td>
							<td>해당 얼굴 이미지에 부여할 Face ID </td>
							<td>string</td>
						</tr>
						<tr>
							<td>file </td>
							<td>type:file (.jpg,.png) 등록할 얼굴 이미지 파일 </td>
							<td>string</td>
						</tr>
						<tr>
							<td>dbId </td>
							<td>해당 Vector를 저장 할 DB의 ID (생략 시 ‘default’로 자동 저장) </td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
<pre>
curl -X PUT \
   https://api.maum.ai/insight/app/setFace \
   -H 'content-type: multipart/form-data;
 boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
   -F apiId=(*ID 요청 필요) \
   -F apiKey=(*key 요청 필요) \
   -F faceId=Anne \
   -F 'file=@/Anne.JPG' \
   -F dbId=default
</pre>
					</div>
					<p class="sub_txt">④ Response 예제 </p>
					<div class="code_box">
<pre>
{
   "message": {
      "message": "success",
      "status": 0
   }
}
</pre>
					</div>

					<em>​getFaceList :지정한 DB 내 등록한 Face ID 값과 벡터값을 조회합니다.</em>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : ​https://api.maum.ai/insight/app/getFaceList</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>사용자의 고유 key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>dbId </td>
							<td>조회할 DB의 ID (생략 시, ‘default’로 자동 조회) </td>
							<td>string</td>
						</tr>

					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
<pre>
curl -X POST \
   https://api.maum.ai/insight/app/getFaceList \
   -H 'content-type: multipart/form-data;
boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
   -F apiId=(*ID 요청 필요) \
   -F apiKey=(*key 요청 필요) \
   -F dbId=default
</pre>
					</div>
					<p class="sub_txt">④ Response 예제 </p>
					<div class="code_box" style="height: 400px; overflow: auto;">
<pre>
{
   "message": {
       "message": "Success",
       "status": 0
   },
   "payload": [
       {
           "id": "Anne",
           "faceVector": [
               0.023167092,
               -0.115254715,
               0.08202046,
               0.001044756,
               -0.02246941,
               -0.019141976,
               0.011437614,
               0.007085758,
               -0.014657473,
               0.04635392,
               0.020367756,
               0.043749213,
               -0.022320237,
               0.019732887,
               0.002116495,
               0.03075867,
               -0.024357634,
               0.056279372,
               -0.03210935,
               0.053253915,
               -0.09179587,
               -0.086102426,
               0.06759939,
               -0.07893096,
               0.03427303,
               -1.3085711E-4,
               0.05103928,
               0.009508202,
               0.04113041,
               0.040643256,
               0.029497173,
               -0.0126817655,
               -0.041648213,
               -0.061418727,
               -0.019942183,
               -0.023390496,
               0.08268916,
               0.0039425725,
               0.06694857,
               0.1072623,
               -0.036651153,
               -0.008873752,
               0.06416556,
               -0.057824623,
               -4.618484E-5,
               0.09921391,
               0.09543958,
               0.03410048,
               -8.2083757E-4,
               0.076666825,
               -0.010248741,
               8.977514E-4,
               -0.014961588,
               -0.031114716,
               -0.13343535,
               -0.004074133,
               0.06964459,
               0.05354242,
               0.095347285,
               -0.065555945,
               -0.013562023,
               0.03169847,
               -0.0302061,
               -0.06787956,
               -0.03990423,
               0.053340875,
               -0.0043947264,
               -0.03660058,
               -0.047947466,
               0.06309527,
               0.015022342,
               -0.010542474,
               -0.0035227893,
               -0.038298097,
               -0.05221859,
               -0.0060932767,
               -0.10106471,
               -0.06309463,
               0.04153377,
               -0.004335563,
               0.012127833,
               -0.050130602,
               0.0266069,
               0.006579191,
               -0.030298294,
               0.016160946,
               -0.017390853,
               0.058132935,
               0.0336268,
               -0.029526541,
               -0.07501074,
               -0.0070177643,
               0.02737674,
               0.043103673,
               0.007905191,
               -0.09930914,
               -0.050705567,
               -5.4536684E-4,
               0.033203866,
               0.02715944,
               0.049818713,
               -0.06374101,
               0.032529052,
               -0.039107732,
               -0.0029863722,
               0.053573973,
               -0.010010016,
               -0.026318658,
               -0.025683483,
               0.05171802,
               -0.037741978,
               0.029352238,
               0.029053757,
               0.019211093,
               0.022695784,
               -0.010631512,
               -0.028454328,
               1.6133417E-4,
               0.0077320337,
               0.09075321,
               -0.007603046,
               -0.015462602,
               0.059029024,
               -8.1657973E-4,
               0.022252427,
               0.0086037265,
               0.059548505,
               0.01513373,
               -0.05137191,
               0.054786835,
               -0.05088807,
               0.05706986,
               0.007890585,
               -0.027286263,
               0.024344597,
               0.047551345,
               0.057653513,
               -0.02318205,
               0.055194803,
               0.073232055,
               -0.062192373,
               -0.0135084605,
               0.022299895,
               0.06176941,
               0.049360126,
               -0.06282301,
               -0.009969664,
               -0.03374739,
               0.07846086,
               -0.012234006,
               -0.03874076,
               0.0014987268,
               -0.10988321,
               -0.057132393,
               -0.10998137,
               -0.015612805,
               0.027333993,
               0.007387177,
               -0.044020943,
               -0.0612183,
               0.031655826,
               0.023451053,
               0.0072870012,
               -0.042236336,
               -0.030708138,
               -0.056328133,
               -0.055621564,
               -0.01066452,
               0.027662158,
               0.033985976,
               -0.0042240885,
               -0.021312179,
               0.020406447,
               -0.0025303902,
               -0.009251404,
               0.024459984,
               0.03507456,
               -0.022096686,
               0.061165474,
               0.052819803,
               0.056497,
               0.051449325,
               -0.0505657,
               -0.013971086,
               -0.0055697765,
               -0.06400767,
               -0.05389608,
               -0.015635466,
               -0.023459248,
               6.953899E-4,
               -0.010743064,
               -0.07353776,
               0.0055057774,
               -0.0029300796,
               -0.017097294,
               0.0033097242,
               -0.060925283,
               -0.026520684,
               0.0016056226,
               -0.048362628,
               0.057096463,
               0.021302301,
               0.010100532,
               0.0014430038,
               0.056857705,
               0.02452753,
               0.0035000066,
               -0.020710006,
               0.0073646987,
               -0.060540404,
               0.025239352,
               -0.049564417,
               -0.007160646,
               0.018419117,
               0.05861748,
               0.0070769503,
               0.047355503,
               -0.055157274,
               -0.007050639,
               0.06620086,
               0.04056888,
               0.04328784,
               -0.018623149,
               0.0317184,
               0.005828237,
               -0.063460834,
               0.06030988,
               0.031550404,
               0.013154197,
               -0.043500323,
               0.07499053,
               0.052237794,
               -0.05870544,
               0.025030969,
               -0.01023555,
               -0.07074299,
               9.707069E-4,
               0.010643247,
               -0.045120846,
               -0.04881968,
               -0.048648953,
               0.011253874,
               0.036477037,
               0.09364169,
               -0.033800356,
               0.09829834,
               0.028485795,
               0.0149018355,
               -0.0552856,
               0.047994602,
               -0.03082233,
               0.009615533,
               -0.053342357,
               -0.08532182,
               0.03521669,
               0.0011604187,
               0.07526233,
               0.026803147,
               -0.028000494,
               -0.07601295,
               0.041947804,
               0.04664299,
               0.027810346,
               0.07543727,
               -0.017133687,
               -0.016625557,
               0.06483988,
               -0.017907854,
               0.006202642,
               0.044200517,
               -0.031476688,
               0.07696336,
               -0.021167722,
               0.05962276,
               -0.041302953,
               0.0076100626,
               0.04798449,
               0.074620165,
               0.058078982,
               0.059685495,
               0.0756498,
               0.07335814,
               0.034310453,
               -0.015010034,
               -0.011788979,
               -0.0066372557,
               0.051395796,
               0.067961864,
               -0.042972974,
               0.0745315,
               -0.06603147,
               -0.020393997,
               0.026484333,
               -0.005974925,
               -0.020143542,
               -0.03268443,
               -0.059448782,
               -0.0027974371,
               -0.055005513,
               1.9769161E-4,
               -0.009609283,
               0.026451744,
               0.05365843,
               0.038251735,
               -0.04787063,
               -0.017397715,
               0.021315709,
               -0.058446266,
               -0.0831932,
               0.005557264,
               0.038382147,
               0.0072101397,
               0.051127546,
               -0.04582469,
               -0.07705488,
               -0.042968422,
               0.035531335,
               0.020826882,
               0.008471408,
               -0.033835053,
               -0.007820141,
               -0.05387035,
               -0.014328999,
               0.021965396,
               0.033354662,
               -0.05487349,
               -0.059148032,
               0.096668296,
               -0.07204274,
               0.015318253,
               -0.003845828,
               0.075987354,
               -0.023312097,
               -0.047881868,
               0.054666176,
               -0.075121485,
               -0.037285052,
               -0.033664625,
               -0.0023204042,
               0.014487357,
               -0.056785483,
               0.033835776,
               0.026353411,
               -0.005145729,
               -0.061521288,
               -0.02851029,
               -0.035496064,
               0.027655624,
               -0.024589835,
               0.022662628,
               0.043995842,
               -0.0781266,
               0.005838281,
               0.08220852,
               0.029178862,
               -0.0033926058,
               0.07003067,
               -0.07525622,
               0.012243703,
               0.028661752,
               -0.011349861,
               -0.049175836,
               -0.013229356,
               0.002191643,
               -0.035274815,
               0.01973845,
               -0.065804236,
               0.05360816,
               -0.018776115,
               -0.08475356,
               0.11633688,
               -0.020687552,
               0.059917502,
               0.0502454,
               -0.032284718,
               -0.038167767,
               0.034840316,
               -0.008403738,
               -0.007186281,
               -0.07328257,
               -0.051551566,
               0.029770255,
               -0.025993172,
               0.06453481,
               -0.006645028,
               -0.022681227,
               0.04500692,
               -0.0241867,
               -0.00295331,
               0.0013727389,
               -0.060668368,
               -0.036308262,
               0.012066404,
               -0.021471677,
               0.0022921723,
               -0.010132039,
               0.102131836,
               0.019166553,
               -0.05383503,
               0.0055506295,
               0.040335733,
               -0.04532261,
               0.020013854,
               0.035218444,
               0.022464646,
               -0.037503123,
               -0.081479624,
               0.017131379,
               0.0018825663,
               0.01021113,
               -0.0062930086,
               -0.03639585,
               0.011250401,
               0.023327418,
               -0.01616823,
               -0.056673683,
               0.08645165,
               0.044445906,
               0.043543063,
               0.028504493,
               -0.0024755727,
               -0.015981259,
               0.0021801814,
               0.014207466,
               0.012415407,
               0.029083926,
               0.0025190383,
               -0.045329027,
               0.031095749,
               -0.07973883,
               4.309128E-4,
               -0.0013110137,
               -0.00538488,
               -0.018520819,
               -0.03623377,
               -0.048993178,
               -0.064825974,
               0.06767473,
               0.010198464,
               -0.010286229,
               -0.027689995,
               0.07593468,
               0.026554974,
               -0.09786561,
               0.060423937,
               0.057520885,
               -0.015027954,
               0.03333951,
               -3.580078E-4,
               -0.025554705,
               0.027524233,
               -0.024604607,
               0.055392444,
               -0.0074907043,
               -0.034262516,
               -0.021061435,
               -0.027852945,
               0.045237016,
               -0.09501191,
               0.020249689,
               -0.026050106,
               -0.04334567,
               -0.045527212,
               -0.0065695904,
               0.003966235,
               9.931349E-4,
               7.60192E-4,
               0.053473186,
               0.04921206,
               0.013119134,
               0.0024566038,
               -0.010320417,
               0.063494146,
               -0.034440786,
               -0.04512646,
               0.057532318,
               0.06361217,
               0.013200267,
               0.005639555,
               0.025194317,
               -0.027318474,
               -0.016809192,
               0.0596016,
               -0.020202404,
               0.039479893,
               0.08869942,
               -0.013668791,
               0.09906185,
               0.010461471,
               -0.0059573967,
               0.0029956137,
               0.023400621,
               -0.026552832,
               -0.025335154,
               -0.018261647,
               0.020680927,
               0.02366516,
               -0.008152542,
               0.04288542,
               -0.008806061,
               0.062963985,
               0.01937117,
               0.019644747,
               -0.017688153,
               0.016495755,
               0.03978591,
               0.001936359,
               -0.043246184,
               0.07139622,
               0.0102169495,
               0.024172332,
               -0.012250714
           ],
           "metaData": {
               "createTime": "2019-10-14 11:50:27",
               "updateTime": "2019-10-14 11:50:27"
           }
       }
   ]
}
</pre>
					</div>


					<em>​recogFace : 사람의 얼굴 이미지를 512차원 벡터화하여 DB 내의 데이터와 대조합니다.</em>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : ​https://api.maum.ai/insight/app/recogFace</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>사용자의 고유 key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>dbId </td>
							<td>조회할 DB의 ID (생략 시, ‘default’로 자동 조회) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>file </td>
							<td>type:file (.jpg,.png) DB내 벡터값과와 대조할 얼굴 이미지
								파일</td>
							<td>string</td>
						</tr>

					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
<pre>
curl -X POST \
   https://api.maum.ai/insight/app/recogFace \
   -H 'content-type: multipart/form-data;
boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
   -F apiId=(*ID 요청 필요) \
   -F apiKey=(*key 요청 필요) \
   -F dbId=default
   -F 'file=@/sample.JPG' \
</pre>
					</div>
					<p class="sub_txt">④ Response 예제 </p>
					<div class="code_box">
<pre>
{
   "message": {
      "message": "success",
      "status": 0 },
   "result": {
      "id": "__no__match__",
      "metaData": {
         "createTime": "1970-01-01 00:00:00",
         "updateTime": "1970-01-01 00:00:00"
      }
   }
}
</pre>
					</div>


					<em>​deleteFace : 특정 DB내의 Face ID 벡터값을 삭제합니다.</em>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : ​https://api.maum.ai/insight/app/deleteFace</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>사용자의 고유 ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey</td>
							<td>사용자의 고유 key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>dbId </td>
							<td>삭제할 값이 있는 DB의 ID (생략 시, ‘default’로 자동 조회) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>faceId </td>
							<td>삭제할 벡터값의 Face ID</td>
							<td>string</td>
						</tr>

					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
<pre>
curl -X POST \
   https://api.maum.ai/insight/app/deleteFace \
   -H 'content-type: multipart/form-data;
boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
   -F apiId=(*ID 요청 필요) \
   -F apiKey=(*key 요청 필요) \
   -F dbId=default
   -F 'faceId=Anne'
</pre>
					</div>
					<p class="sub_txt">④ Response 예제 </p>
					<div class="code_box">
<pre>
{
   "message": {
      "message": "success",
      "status": 0
   }
}
</pre>
					</div>

				</div>
			</div>

		</div>
		<!--//frmenu-->
		<!--.frexample-->
		<div class="demobox" id="frexample">
			<p><em style="color:#f7778a;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>

			<!--얼굴인식 -->
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>보안시스템 강화</span>
							</dt>
							<dd class="txt">보안 구역에 카메라를 설치하여 얼굴 인식을 통해 출입을 통제합니다. 보안 통제가 엄격히 필요한 사무실, 연구실, 금고 등에 적용하여 권한이 있는 자들만 얼굴을 등록하여 출입을 허가하여 보안을 강화합니다. </dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_fr"><span>Face Recognition</span></li>
									<li class="ico_vr"><span>Voice Recognition</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>2차 범죄 예방</span>
							</dt>
							<dd class="txt">범죄자들의 얼굴을 등록하여 그들의 2차 범죄를 예방합니다. 특히 강도와 같은 소매 범죄자들을 상점 보안 카메라에 등록하여 일반 상점의 소매 범죄율을 줄일 수가 있습니다.  </dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_fr"><span>Face Recognition</span></li>
									<li class="ico_tr"><span>ESR</span></li>
									<li class="ico_avr"><span>AVR</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 03</em>
								<span>Identification</span>
							</dt>
							<dd class="txt">DB에 얼굴을 등록하여 신분 증명에 활용합니다. 출입 통제가 필요한 경우 사전에 등록하여 얼굴 인식 만으로 편리하게 출입합니다. </dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_fr"><span>Face Recognition</span></li>
									<li class="ico_tr"><span>ESR</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //얼굴인식 -->
		</div>
		<!--//.frexample-->

	</div>
</div>
<!-- //.contents -->

<script type="text/javascript">
	$(document).ready(function () {
		//이미지 파일 업로드
		$('.demoFile').each(function () {
			var $input = $(this);
			var $inputId= $input.attr('id');
			var $label = $input.next('.demolabel');
			var $label_tit = $input.next('.input_label');

			$input.on('change', function(element){
				// var files = element.target.files[0];
				//console.log(files);

				if($input[0].files[0] == null || $input[0].files[0] == ""){
					return;
				}

				var $demoFileSize = $input[0].files[0].size;
				var max_demoFileSize = 1024 * 1024 * 3;//1kb는 1024바이트

				if($demoFileSize > max_demoFileSize || (!$input[0].files[0].type.match(/image.jp*/) && !$input[0].files[0].type.match(/image.png/)) ){
					$('.pop_simple').show();
					// 팝업창 닫기
					$('.pop_close, .pop_bg, .btn a').on('click', function () {
						$('.pop_simple').fadeOut(300);
						$('body').css({
							'overflow': ''
						});
					});
					$('#'+$inputId).val('');

				}else{

					$label.hide();

					var labeltxt = element.target.files[0].name;
					$label_tit.html(labeltxt);

					var $img = $input.parent().children('img');
					// console.log($img);
					readURL(this, $img);

					$(this).parent().removeClass('btn');
					$(this).parent().addClass( 'btn_change' );
				}
			});

		});
	});



	var sampleImage1;
	var sampleImage2;
	var sampleImage3;
	var sampleImage4;

	var level1 = false;
	var level2 = false;
	var level3 = false;

	let image1_blob;
	let image2_blob;
	let image3_blob;
	let image4_blob;
	let imageSelect_blob;

	/* 샘플 이미지를 로드함 */
	function loadSample(url, img) {
		var blob = null;
		var xhr = new XMLHttpRequest();
		xhr.open("GET", url);
		xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
		xhr.onload = function () {

			blob = xhr.response;//xhr.response is now a blob object

			if (img === 1)
			{
				//sampleImage1 = new File([blob], "sampleImage1.jpg");
				image1_blob = blob;
			}
			else if (img === 2)
			{
				//sampleImage2 = new File([blob], "sampleImage2.jpg");
				image2_blob = blob;
			}
			else if (img === 3)
			{
				//sampleImage3 = new File([blob], "sampleImage3.jpg");
				image3_blob = blob;
			}
			else if (img === 4)
			{
				//sampleImage4 = new File([blob], "sampleImage3.jpg");
				image4_blob = blob;
			}
		};

		xhr.send();
	}

	function readURL(input, imgObj) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				imgObj.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

	$(document).ready(function () {
		loadSample("${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample1_1.png", 1);
		loadSample("${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample2_1.png", 2);
		loadSample("${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample3_1.png", 3);
		loadSample("${pageContext.request.contextPath}/aiaas/kr/images/img_fr_sample4_1.png", 4);

		/* 샘플 테스트를 실행할 경우 */
		$('#sampleTest').on('click', function(){
			console.log("this is sampleTest");
			var formData = new FormData();
			var sampleImage;

			var sampleUrl = $('input[name="option"]:checked').val();
            var blob = null;
			var xhr = new XMLHttpRequest();

			xhr.open("GET", sampleUrl);
			xhr.responseType = "blob";

			/* 사용자가 선택한 sample 이미지를 로드 */
			xhr.onload = function () {
			    blob = xhr.response;
			    //var sample = new File([blob], "sample.jpg");
				imageSelect_blob = blob;

			    formData.append('sampleImage1', image1_blob);
			    formData.append('sampleImage2', image2_blob);
			    formData.append('sampleImage3', image3_blob);
			    formData.append('sampleImage4', image4_blob);
			    formData.append('sample', imageSelect_blob);

                formData.append('${_csrf.parameterName}', '${_csrf.token}');

                var request = new XMLHttpRequest();
                request.enctype = "multipart/form-data";
                request.processData = false;
                request.contentType = false;
                /* 받은 이미지를 통해 face Recognition */
                request.onreadystatechange = function () {
                    if(request.readyState === 4){

                    	if(request.status === 200){

                    		if(request.response == null || request.response == ""){
								alert("서버에서 응답을 받지 못했습니다. 다시 시도해 주세요");
								window.location.reload();
							}

							var name = JSON.parse(request.response)['result']['id']; //faceId
							var txtarea =  document.getElementById("result_txt");


							/* 인식한 얼굴이 데이터베이스에 없는 경우 */
							if(name === "__no__match__")
							{
								$(".result_t").html('데이터 베이스에 일치되는 값이 없습니다.');
								$("#faceId").html('');
								$("#facd_name").html('No match');
								txtarea.value =  JSON.stringify(JSON.parse(request.response)['result']);
							}
							else /* 데이터베이스에 있는 경우 */
							{
								$(".result_t").html('데이터 베이스에 일치되는 값이 있습니다.');
								$("#faceId").html('FaceID: ');
								$('#facd_name').html(name);
								txtarea.value = "faceVector : \r\n" + JSON.stringify(JSON.parse(request.response)['result']['faceVector']).replace(/,/g, ', \r\n');	// ,부분에서 줄바꿈
							}

							document.querySelector('#resultImg').src = sampleUrl;	//입력 된 파일의 이미지를 띄어줌

							$('.fr_2').hide();
							$('.fr_3').fadeIn(300);
						}else{
                    		alert("서버에서 응답을 받지 못했습니다. 다시 시도해 주세요");
                    		window.location.reload();
						}


                    }
                };
                request.open('POST', '${pageContext.request.contextPath}/api/sampleFaceRecog');
				request.timeout = 20000;
                request.send(formData);

				request.ontimeout=function() {
					alert('서버 응답 시간이 초과되었습니다. 다시 시도해 주세요');
					window.location.reload();
				};
            };
            xhr.send();
		});


		var formData = new FormData();
		var nameArray = [];
		var fileArray = [];

		$('#demoResult').on('click', function(){

			if($('#demoFile').val() == "") //인식할 얼굴을 선택하지 않은 경우
			{
				alert("인식할 얼굴을 선택해 주세요.");
			}
			else if((level1 || level2 || level3) == false){ //데이터 베이스에 등록 할 이미지를 선택하지 않은 경우
				alert("데이터 베이스에 등록한 이미지를 저장해 주세요.");
			}
			else
			{
				formData.append('demoFile', document.getElementById('demoFile').files[0]);

				setFormData('demoFile1', 'demoName1', level1);
				setFormData('demoFile2', 'demoName2', level2);
				setFormData('demoFile3', 'demoName3', level3);

				formData.append("nameArray", nameArray);
				formData.append('${_csrf.parameterName}', '${_csrf.token}');

				var request = new XMLHttpRequest();
				request.enctype = "multipart/form-data";
				request.processData = false;
				request.contentType = false;
				request.onreadystatechange = function () {
					if(request.readyState === 4){

						if(request.status === 200) {

							if (request.response == null || request.response == "") {
								alert("서버에서 응답을 받지 못했습니다. 다시 시도해 주세요");
								window.location.reload();
							}

							//console.log(request.response);
							var name = JSON.parse(request.response)['result']['id']; //faceId
							var txtarea =  document.getElementById("result_txt");

							/* 인식한 얼굴이 데이터베이스에 없는 경우 */
							if(name === "__no__match__")
							{
								$(".result_t").html('데이터 베이스에 일치되는 값이 없습니다.');
								$("#faceId").html('');
								$("#facd_name").html('No match');

								txtarea.value =  JSON.stringify(JSON.parse(request.response)['result']);
							}
							else /* 인식한 얼굴이 데이터베이스에 있는 경우 */
							{
								$(".result_t").html('데이터 베이스에 일치되는 값이 있습니다.');
								$("#faceId").html('FaceID: ');
								$('#facd_name').html(name);
								txtarea.value = "faceVector : \r\n" + JSON.stringify(JSON.parse(request.response)['result']['faceVector']).replace(/,/g, ', \r\n');	// ,부분에서 줄바꿈
							}

							/* 입력된 파일의 이미지를 띄어줌 */
							var fileInput = document.getElementById('demoFile');
							var resultFile = fileInput.files[0];

							var reader = new FileReader();
							reader.readAsDataURL(resultFile);

							reader.onload = function () {

								var fileSrc = reader.result;
								document.querySelector('#resultImg').src = fileSrc;

								//이미지 크기가 작을 경우 리 사이징
								var img = new Image();
								img.src = fileSrc;
								img.onload = function () {
									var w = this.width;
									var maxWidth = 250;

									if (w < maxWidth) {
										// console.log(w);
										$('#resultImg').css({
											"width": "100%",
											"height": "auto",
										});
									}
								};
							};


							$('.fr_2').hide();
							$('.fr_3').fadeIn(300);

							/* 값 초기화 */
							formData = new FormData();
							nameArray = [];
							fileArray = [];

						}

					}
				};
				request.open('POST', '${pageContext.request.contextPath}/api/getFaceRecog');
				request.timeout = 20000;
				request.send(formData);

				request.ontimeout=function() {
					alert('서버 응답 시간이 초과되었습니다. 다시 시도해 주세요');
					window.location.reload();
				};
			}

		});

		function setFormData(demoF, demoN, flag){
			if(($('#'+demoF).val() != "") && (flag == true)) {
				var data = $('#'+demoN).val();
				nameArray.push(data);
				formData.append(demoF, document.getElementById(demoF).files[0]);
			}else{
				var data = "default";
				nameArray.push(data);
				formData.append(demoF, new Blob([]));
			}
		}

		// close button
		$('em.close').on('click', function () {
			$(this).parent().removeClass("btn_change");
			$(this).parent().addClass("btn");
			$(this).parent().children('.demolabel').fadeIn().text('이미지 업로드');
			$(this).parent().children('.input_label').fadeIn().text('이미지 업로드');

			$(this).parent().children('input').val('');
			$(this).parent().next().find('input').val(""); // faceId 비우기

		});
		//파일명 변경
		document.querySelector(".demoFile").addEventListener('change', function (ev) {
			if(ev.target.files[0] == null) return;

			document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
			var $element = $('.uploadFile');
			$element.removeClass( 'btn' );
			$element.addClass( 'btn_change' );
		});

		/* 사용자가 저장하기 버튼을 통해 이미지를 저장했을 경우  */
		$('#save1').on('click', function(){
			var labelTxt = $('#demoName1').val();

			if(($('#demoFile1').val() == "") || ($('#demoName1').val() == "")){
				alert("이미지를 업로드 해 주세요.");
			}else{
				$("#level1_back").css('display','none');
				$("#level1_text").css('display','block');
				$('#level1_text span').text(labelTxt);
				level1 = true;
			}
		});
		$('#save2').on('click', function(){
			var labelTxt = $('#demoName2').val();
			if(($('#demoFile2').val() == "") || ($('#demoName2').val() == "")){
				alert("이미지를 업로드 해 주세요.");
			}else{
				$("#level2_back").css('display','none');
				$("#level2_text").css('display','block');
				$('#level2_text span').text(labelTxt);
				level2 = true;
			}
		});
		$('#save3').on('click', function(){
			var labelTxt = $('#demoName3').val();
			if(($('#demoFile3').val() == "") || ($('#demoName3').val() == "")){
				alert("이미지를 업로드 해 주세요.");
			}else{
				$("#level3_back").css('display','none');
				$("#level3_text").css('display','block');
				$('#level3_text span').text(labelTxt);
				level3 = true;
			}
		});

		$('.hidden').on('click', function(){
			var id = this.id;
			if((id == "level1") || (id == "level2") || (id == "level3")){
				$("#"+id+"_back").css('display','block');
				$("#"+id+"_text").css('display','none');

				if(id == "level1") {
					level1 = false;
				}else if(id == "level2") {
					level2 = false;
				}else if(id == "level3") {
					level3 = false;
				}
			}
		});

		// step1->step2
		$('.btn_start').on('click', function () {
			if(($('#demoFile').val() != "") && ((level1 || level2 || level3) == true)) {
				$('.faceR_box').hide();
				$('.fr_2').fadeIn(300);
				$("html").scrollTop(0);
			}
		});

		$('#sampleTest').on('click', function () {
			$('.faceR_box').hide();
			$('.fr_2').fadeIn(300);
			$("html").scrollTop(0);
		});

		// step3->step1
		$('.reset_btn').on('click', function () {
			$('.fr_3').hide();
			$('.faceR_box').fadeIn(300);
			$('.fl_box').css("opacity", "1");
			/*		var label_change = $('.demolabel');
                    label_change.text('이미지 업로드');
                    label_change.parent().removeClass('btn_change');
                    label_change.parent().addClass('btn');*/
			/*		$('.demoFile').val('');*/

			/*		$('.input_level1').css('display','block');
                    $('.input_level2').css('display','none');
                    $('.input_level1 input').val('');*/

			/*		imgSave1 = false;
                    imgSave2 = false;
                    imgSave3 = false;*/
			/*		$('.em.close').trigger();*/

		});

	});


	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}

	document.getElementById("defaultOpen").click();

</script>