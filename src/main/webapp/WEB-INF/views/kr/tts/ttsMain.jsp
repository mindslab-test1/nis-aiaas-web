<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/download.js"></script>

	<!-- .contents -->
	<div class="contents">        	
		<div class="content api_content">
			<h1 class="api_tit">음성생성</h1>
            <ul class="menu_lst voice_menulst">
                <li class="tablinks" onclick="openTap(event, 'ttsdemo')" id="defaultOpen"><button type="button">데모</button></li>
				<li class="tablinks" onclick="openTap(event, 'ttsexample')"><button type="button">적용사례</button></li>
                <li class="tablinks" onclick="openTap(event, 'ttsmenu')"><button type="button">매뉴얼</button></li>
<%--                <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
            </ul>
			<div class="demobox ttsdemo apittsdemo" id="ttsdemo">
				<p>자연스러운 <em>음성생성</em>  <small>(Speech Generation)</small></p>
				<span class="sub">실제 목소리처럼 자연스러운 음성을 만듭니다.</span>
				<div class="selectarea">
					<div class="radio">
						<input type="hidden" id="voice1" value="음성생성은 텍스트 문장 또는 파일을 음성으로 변환시켜주는 API 입니다.">
						<input type="radio" id="1" name="voice" data-lang="ko_KR" data-speakerid="baseline_kor" checked="checked">
						<label for="1"><img src="${pageContext.request.contextPath}/aiaas/kr/images/img_tts1.png" alt="여성성우"><span>한국어 <br><strong>여성 상담원</strong></span></label>
					</div>
					<%--<div class="radio">
						<input type="hidden" id="voice4" value="Minds Lab's Voice Generation is not a voice synthesis. It is a state of the art future technology.">
						<input type="radio" id="4" name="voice" data-lang="en_US" data-speakerid="baseline_eng">
						<label for="4"><img src="${pageContext.request.contextPath}/aiaas/kr/images/img_tts4.png" alt="외국인 여성"><span>영어 <br><strong>외국인 여성</strong></span></label>
					</div>
					<div class="radio">
						<input type="hidden" id="voice2" value="마인즈랩의 음성생성 기술은 아무도 따라할 수 없어, 세계 최고인것 같아.">
						<input type="radio" id="2" name="voice" data-lang="kor_kids" data-speakerid="kor_kids_m">
						<label for="2"><img src="${pageContext.request.contextPath}/aiaas/kr/images/img_tts2.png" alt="남자아이"><span>한국어 <br>남자아이</span></label>
					</div>
					<div class="radio">
						<input type="hidden" id="voice3" value="마인즈랩의 음성생성 기술은 아무도 따라할 수 없어, 세계 최고인것 같아.">
						<input type="radio" id="3" name="voice" data-lang="kor_kids" data-speakerid="kor_kids_f">
						<label for="3"><img src="${pageContext.request.contextPath}/aiaas/kr/images/img_tts3.png" alt="여자아이"><span>한국어 <br>여자아이</span></label>
					</div>--%>
				</div>

				<div class="tts_demo">
					<!-- .tabUi -->
					<div class="tabUi demoTab">
						<!-- #demo01 -->
						<div id="demo01" class="tab_contents">
							<div class="demoBox">								
                                <div class="desc_tts">
                                    <p><em class="fas fa-lightbulb"></em> 아래 문장은 예시 문장입니다. 청취하고 싶은 문장을 직접 입력해주세요.</p>
<%--                                    <p><em class="fas fa-lightbulb"></em> 무료 Trial은 25자까지만 지원됩니다. 필요 시 AI Service를 이용해주세요.</p>--%>
                                </div>
								<!-- .step01 -->
								<div class="demo_intro">
									<div class="demo_infoTxt">
                                        <textarea id="text-contents" class="textArea apitextArea" rows="8" maxlength="1000" placeholder="음성을 생성할 텍스트를 1000자 이내로 입력하세요.">음성생성은 텍스트 문장 또는 파일을 음성으로 변환시켜주는 API 입니다.</textarea>
                                        <div class="text_info">												
                                            <span class=""><strong id="count">0</strong>/1000자</span>
                                        </div>
                                    </div>
									<div class="btnBox" id="btnBox_init">
										<div class="holeBox">
											<div class="hole">
												<em></em><em></em><em></em><em></em><em></em>
											</div>
										</div>
										<button class="demoRecord" >
                                            <span>
                                                <em class="fas fa-play-circle"></em><strong>음성 생성</strong>
                                                <span class="tooltiptext">버튼 클릭</span>
                                            </span>
                                        </button>
									</div>
								</div>
								<!-- //.step01 -->
								
								<!-- .step02 -->
								<div class="demo_recording">
									<div class="demo_infoTxt">음성을 변환 중입니다. <br>잠시만 기다려 주세요.</div>	
									<!-- recordingBox -->
									<div class="recordingBox">
										<div class="recording1 loader loader2">	
										</div>
									</div>
									<!-- //recordingBox -->
								</div>
								<!-- //.step02 -->
			
								<!-- .step03 -->
								<div class="demo_recording2">
									<div class="demo_infoTxt">재생 되고 있습니다.</div>
									
									<!-- recording -->
									<div class="recording">
										<span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
										<span></span><span></span><span></span><span></span><span></span><span></span><span></span>
									</div>
									<!-- //recording -->
									
									<div class="btnBox">
										<button id="resetBtn" class="demoReset btnbtn"><span>처음으로</span></button>
										<button class="dwn dwn_tts" onclick="fileDownload()"><span>다운로드</span></button>
									</div>
								</div>
								<!-- //.step03 -->

								<!-- .step04 -->
								<div class="demo_result" hidden>
									<div class="demo_player">
										<audio id="tts_result_audio" src="" type="audio/mpeg" onended="audioEnded_tts($(this))" controls></audio>
									</div>
								</div>
							</div>
						</div>
						<!-- //#demo01 -->
					</div>
					<!-- //.tabUi -->
					<div class="remark">

					</div>
				</div>
			</div>
            <!--//.ttsdemo-->
			<!--.ttsmenu-->
			<div class="demobox voice_menu" id="ttsmenu">
				<!--guide_box-->
				<div class="guide_box">
					<div class="guide_common">
						<div class="title">
							API 공통 가이드
						</div>
						<p class="sub_title">개발 환경 세팅</p>
						<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
						<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>

					</div>
					<!--guide_group-->
					<div class="guide_group">

						<div class="title">
							TTS <small>(Text-To-Speech)</small>
						</div>
						<p class="sub_txt">TTS API는 텍스트 문장 또는 파일을 음성으로 변환시켜주는 API입니다.<br>
							20분 학습만으로도 다양한 모델로 자연스럽게 음성을 합성해주는 API를 경험해보세요.</p>

						<span class="sub_title">
								준비사항
							</span>
						<p class="sub_txt">① Input: 텍스트(문장)
							<span><small>*제약사항: 1000자 이내의 문장(한글/영어 기준)</small></span>
						</p>
						<p class="sub_txt">② 아래 Model 선택</p>
						<ul>
							<li>한국어 - 여성 상담원 (baseline_kor)</li>
<%--							<li>영어 - 여성 성우 (baseline_eng)</li>
							<li>한국어 - 남자 아이 (kor_kids_m)</li>
							<li>한국어 - 여자 아이 (kor_kids_f)</li>--%>
						</ul>
						<span class="sub_title">
								 실행 가이드
							</span>
						<p class="sub_txt">① Request  </p>
						<ul>
							<li>Method : POST</li>
							<li>URL : https://api.maum.ai/tts/stream</li>
						</ul>
						<p class="sub_txt">② Request 파라미터 설명 </p>
						<table>
							<tr>
								<th>키</th>
								<th>설명</th>
								<th>type</th>
							</tr>
							<tr>
								<td>apiId</td>
								<td>사용자의 고유 ID. </td>
								<td>string</td>
							</tr>
							<tr>
								<td>apiKey</td>
								<td>사용자의 고유 key. </td>
								<td>string</td>
							</tr>
							<tr>
								<td>text</td>
								<td>문장 (*1000자 이내)</td>
								<td>string</td>
							</tr>
							<tr>
								<td>voiceName</td>
								<td>사용할 Voice Model Name (baseline_kor, baseline_eng, kor_kids_m, kor_kids_f)<br>
									<small>* 그 외 다른 모델 사용이 필요할 시에는 문의하세요 </small></td>
								<td>string</td>
							</tr>
						</table>
						<p class="sub_txt">③ Request 예제 </p>
						<div class="code_box">
<pre>
curl -X POST \
  https://api.maum.ai/tts/stream \
  -H 'Content-Type: application/json' \
  -d '{
    "apiId" : (*ID 요청필요),
    "apiKey" : (*Key 요청필요),
    "text" : 안녕하세요, 반갑습니다. Minds Lab 상담원입니다.,
    "voiceName" : baseline_kor
}'
</pre>
						</div>
						<p class="sub_txt">④ Response  예제 </p>
						<div class="code_box">
							audio/x-wav 타입의 chunked data (StreamBody)


						</div>
					</div>
					<!--//.guide_group-->
				</div>
				<!--//.guide_box-->
			</div>
			<!--//ttsmenu-->
			<!--.ttsexample-->
			<div class="demobox" id="ttsexample">
				<p><em style="font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>
				<div class="useCasesBox">
					<ul class="lst_useCases">
						<li>
							<dl>
								<dt>
									<em>CASE 01</em>
									<span>AI 자동 해피콜</span>
								</dt>
								<dd class="txt">설문조사, 상품 품질 조사와 같은 반복적인 성격의 전화를 인공지능으로 대신하는 AI 자동콜 해피콜에 응용할 수 있습니다.</dd>
								<dd class="api_itemBox">
									<ul class="lst_api">
										<li class="ico_tts"><span>TTS</span></li>
									</ul>
								</dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt>
									<em>CASE 02</em>
									<span>안내 서비스</span>
								</dt>
								<dd class="txt">다양한 목적의 안내나 어시스턴트 서비스에 실제처럼 자연스러운 TTS 음성을 활용할 수 있습니다.</dd>
								<dd class="api_itemBox">
									<ul class="lst_api">
										<li class="ico_tts"><span>TTS</span></li>
									</ul>
								</dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt>
									<em>CASE 03</em>
									<span>유튜버</span>
								</dt>
								<dd class="txt">유튜브나 브이로그 등 영상 콘텐츠에 즉각적으로 반영할 수 있습니다.</dd>
								<dd class="api_itemBox">
									<ul class="lst_api">
										<li class="ico_tts"><span>TTS</span></li>
									</ul>
								</dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt>
									<em>CASE 04</em>
									<span>게임</span>
								</dt>
								<dd class="txt">게임 상의 등장인물 음성이나 음향 효과 등을 만들 수 있습니다.</dd>
								<dd class="api_itemBox">
									<ul class="lst_api">
										<li class="ico_tts"><span>TTS</span></li>
									</ul>
								</dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt>
									<em>CASE 05</em>
									<span>아동 교육 콘텐츠</span>
								</dt>
								<dd class="txt">아이들을 대상으로 한 교육용 콘텐츠에 접목할 수 있습니다.</dd>
								<dd class="api_itemBox">
									<ul class="lst_api">
										<li class="ico_tts"><span>TTS</span></li>
									</ul>
								</dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt>
									<em>CASE 06</em>
									<span>뉴스 브리핑</span>
								</dt>
								<dd class="txt">고객이 원하는 목소리로 개인화된 뉴스 브리핑 서비스를 제공할 수 있습니다.</dd>
								<dd class="api_itemBox">
									<ul class="lst_api">
										<li class="ico_tts"><span>TTS</span></li>
										<li class="ico_xdc"><span>XDC</span></li>
									</ul>
								</dd>
							</dl>
						</li>
					</ul>
				</div>
				<!-- //음성 인식(TTS) -->
			</div>
		</div>
		<!-- //.content -->
	</div>
	<!-- //.contents -->

<%--<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/tts/app.js?20190816"></script>--%>

<script>

	var downloadLink;
	var dwnAudio = null;

jQuery.event.add(window, "load", function () {
	$(document).ready(function () {
		var $textArea = $('.demoBox .textArea');
		var $holeBox = $('.holeBox');

		var $ttsexample = $('#ttsexample');
		var $ttsmenu = $('#ttsmenu');

		$ttsexample.hide();
		$ttsmenu.hide();
		// $holeBox.hide();
		countTextAreaLength();
		init_tts_main();

		$('.selectarea .radio').on("click",function(){
			$textArea.val($('#voice'+$(this).children('input[type="radio"]').attr("id")).val());
			countTextAreaLength();
			$holeBox.show();
		});

		// textArea event
		$textArea.on('input keyup paste change', function () {
			if ($textArea.val().trim() !== "") {
				$holeBox.show();
			} else {
				$holeBox.hide();
			}
			countTextAreaLength(); // typing count
		});

		// step01 > step02  : 음성생성 버튼 클릭 event
		$('.tts_demo button.demoRecord').on('click', function () {
			if ($textArea.val().trim() === "") {
				alert("내용을 입력해 주세요.");
				$textArea.val("");
				$(this).focus();
				return 0;
			}

			var $checked = $("input:radio[name='voice']:checked");
			var text = $textArea.val();
			var s_id = $checked.data("speakerid");


			speak(text, s_id);
			$holeBox.hide();
			$('.desc_tts').hide();
			$('.demo_intro').hide();
			$('.selectarea').hide();
			$('.demo_recording').show();

			$('.demo_result').show();
		});

		// step04 > step01
		$('.tts_demo .demoReset').on('click', function () {
			$('#count').html(0);
			$textArea.val("");
			$('.demo_recording').hide();
			$('.demo_recording2').hide();
			$('.selectarea').show();
			$('.demo_intro').show();
			$('.desc_tts').show();

			init_tts_main();
		});
	});
});

function speak(text, s_id){

	var _url = "${pageContext.request.contextPath}/api/tts/ttsKidsApi";

	console.log(text);
	console.log(s_id);

	var formData = new FormData();

	formData.append('text', text);
	formData.append('voiceName', s_id);
	formData.append('${_csrf.parameterName}', '${_csrf.token}');

	var request = new XMLHttpRequest();

	request.onloadstart = function(ev) {
		request.responseType = "";
	}
	request.onreadystatechange = function() {
		if(request.readyState === 4) {
			if(request.status === 200){
				console.log(request.response);

				$('.demo_recording').hide();
				$('.demo_recording2').show();
				$('.demo_recording2 .demo_infoTxt').text(text);

				var responseJSON = JSON.parse(request.response);

				downloadLink = responseJSON['downloadLink'];

				let audioData = "data:audio/mp3;base64," + responseJSON['mp3File'].body;
				dwnAudio = "data:audio/wav;base64," + responseJSON['wavFile'].body; // 다운로드용 wavFile

				var tts_output = document.getElementById('tts_result_audio');
				tts_output.pause();
				tts_output.src = audioData;
				tts_output.play();

			}
			else{
				alert("결과를 받아오지 못했습니다. 다시 시도해 주세요.");
				dwnAudio = null;

				$('.demo_recording').hide();
				$('.demo_recording2').hide();
				$('.selectarea').show();
				$('.demo_intro').show();
				$('.desc_tts').show();

				init_tts_main();
			}
		}
	};
	request.open('POST', _url, true);
	request.timeout = 40000; // IE 11 에서 timeout이 InvalidStateError 발생 -> 눈속임?으로 open 뒤에 위치시키면 넘어감
	request.send(formData);

	request.ontimeout = function() {
		alert("응답 시간이 초과했습니다. 다시 시도해 주세요.");
		$('.demo_recording').hide();
		$('.demo_recording2').hide();
		$('.selectarea').show();
		$('.demo_intro').show();
		$('.desc_tts').show();

		init_tts_main();
		// window.location.reload();
	};


	request.addEventListener("abort", function(){
		console.log("abort!");
		// dwnAudio = null;
	});

}

function audioEnded_tts(audio) {
	$('.recording').hide();
	$('.btnBox').show();
}

function init_tts_main() {
	$('.demo_player').hide();
	$('.btnBox').hide();

	$('.demo_intro .demo_infoTxt').show();
	$('#btnBox_init').show();

}

function fileDownload() {
	let fileName = "TTS.wav";
	download(dwnAudio, fileName, "audio/wav");
}

function countTextAreaLength (){
	var content = $('#text-contents').val();
	if (content.length >= 1000) {
		$('#count').html(1000);
		return;
	}
	$('#count').html(content.length);
}

//API 탭
function openTap(evt, menu) {
	var i, demobox, tablinks;
	demobox = document.getElementsByClassName("demobox");
	for (i = 0; i < demobox.length; i++) {
		demobox[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(menu).style.display = "block";
	evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();

</script>