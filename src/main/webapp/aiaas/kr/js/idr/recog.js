var req;
var $cropper = null;
function activateCroppie(){
    $cropper = $('#previewImg').croppie({
        showZoomer: true,
        enableExif: true,
        enforceBoundary: false,
        enableOrientation: true,
        viewport: {
            width: 420,
            height: 180,
            type: 'square'
        },
        boundary: {
            width:600,
            height: 300
        }

    });
    $cropper.croppie('bind', {
        url: sample1SrcUrl,

    });

}

//
// function popCroppie(){
//     $cropper = $('#previewImg').croppie({
//         showZoomer: true,
//         enableExif: true,
//         enforceBoundary: false,
//         enableOrientation: true,
//         viewport: {
//             width: 420,
//             height: 180,
//             type: 'square'
//         },
//         boundary: {
//             width:600,
//             height: 300
//         }
//     });
// }

$("#rotate_left").on('click', function() {
    $cropper.croppie('rotate', 90);
});



$(document).ready(function () {

    // 크롭 취소
    $('#editCancel').on('click', function(){
        $('input[id="opt1"]').trigger("click");
        $('.pattern_2').hide();
        $('#previewImg').removeAttr('src');
        reset();
    });
    // 변환 중 취소
    $('#btn_back1').on('click', function () {
        req.abort();
        $('.loding_area').hide();
        $('#previewImg').removeAttr('src');
        reset();
    });
    // 완료 후 처음으로
    $('.btn_back2').on('click', function () {
        $('.pattern_3').hide();
        $('#sampleImg_1').trigger("click");
        $('input[id="opt1"]').trigger("click");
        $('.pattern_1').fadeIn(300);
        $('.fl_box').css('opacity', '1');
        $('.demo_layout ').css("border", "solid 1px #cfd5eb;");
        window.location.reload();
    });

});

function reset(){
    $cropper.croppie('destroy');
    $cropper = null;
    $('.pattern_1').fadeIn(300);
    $('.fl_box').css('opacity', '1');
    $('.demo_layout ').css("border", "solid 1px #cfd5eb;");
    // $('#demoFile').val('');
}

//API 탭
function openTap(evt, menu) {
    var i, demobox, tablinks;
    demobox = document.getElementsByClassName("demobox");
    for (i = 0; i < demobox.length; i++) {
        demobox[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(menu).style.display = "block";
    evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();