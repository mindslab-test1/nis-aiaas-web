
jQuery.event.add(window,"load",function(){
	$(document).ready(function (){
		
		$('.demo_recording').hide();
		$('.demo_result').hide();		
		$('.holeBox').hide();
		$('#lyr_lack').fadeIn(300);
		$('#lyr_upgrade').hide(300);

		// 녹음버튼 눌렀을 때
		$('.demo_intro .btnBox').on('click', function(){
			
			$('.btnupload').hide();											   
			$('.demo_fileTxt').hide();	
			$('.demo_recordTxt').fadeIn(200);
			$('.btnBox').hide();
			$('.btnreset .demoReset').hide();
			$('.btnreset .demoStop').show();
			$('.stt_demo .rightBox .selectarea').hide();	
			$('.holeBox').fadeIn(200);
			$('.progress li:nth-child(2)').addClass('active'); 
		});	
		
		$('.holeBox .hole i').on('click', function(){
			$(this).parent().parent().hide();
			$(this).parent().parent().parent().children('.btnBox').hide();
			$('.demo_recordTxt').hide();
			$('.btnreset').hide();
			$('.rightBox .demo_recording').hide();
			$('.rightBox .demo_result').show();
			$('.progress li:nth-child(3)').addClass('active'); 
			
		});	
		 
//		스탑버튼 누를때
		$('.btnreset .demoStop').on('click', function(){
			$(this).parent().parent().hide();
			$(this).parent().parent().parent().children('.btnBox').hide();
			$('.demo_recording').show();
			$('.progress li:nth-child(2)').addClass('active'); 
			
		});	
		
		
		//잘못된 파일 후 리셋 누를 때
		$('.btnreset').on('click', function(){			
			$('.btnupload').show();	
			$('.btnBox').show();											   
			$('.demo_fileTxt').show();													   
		});
		// 녹음 중
		$('.demo_recording .recording').on('click', function(){
			$('.rightBox .demo_recording').hide();
			$('.rightBox .demo_result').show();
			$('.btnBox').show();	
			$('.progress li:nth-child(2)').addClass('active'); 
			$('.progress li:nth-child(3)').addClass('active'); 
															   
		});
		// 리셋 버튼 누를때
		$('.demo_result button.demoReset').on('click', function(){
			$('.rightBox .demo_result').hide();
			$('.demo_recordTxt').hide();
			$('.stt_demo .rightBox .selectarea').show();
			$('.rightBox .demo_intro').show();
			$('.btnupload').show();	
			$('.demo_fileTxt').show();	
			$('.btnBox').show();	
			$('.progress li:nth-child(1)').addClass('active'); 	
			$('.progress li:nth-child(2)').removeClass('active');
			$('.progress li:nth-child(3)').removeClass('active');
		});
		
		// Demo 파일업로드
		$('.demoFile').on('change', function() {var fileTxt = $('.demoFile').val(); //파일을 추가한 input 박스의 값
			var fneTxt = fileTxt.split(".").pop(-1).toLowerCase();

			if ( fneTxt != "mp3" && fneTxt != "wav" && fneTxt != "pcm") {// 음성파일 체크
				// 실패
				$('#demo04 .demo_fileTxt').addClass('errorTxt').text('잘못된 파일입니다. mp3, wav, pcm 파일을 넣어주세요.');					
				$('.btnBox').hide();
				$('.holeBox').hide();
				$('.btnreset .demoReset').show();
				$('.btnreset .demoStop').hide();
				$('.btnreset').on('click', function(){	
					$('#demo04 .demo_fileTxt').removeClass('errorTxt').text('녹음 버튼을 클릭하고 문장을 말하거나,음성파일을 업로드 해 주세요.');
					$('.btnupload').show();	
					$('.btnBox').show();											   
					$('.demo_fileTxt').show();	
					$('.progress li:nth-child(2)').removeClass('active');
				});
				$('.progress li:nth-child(2)').addClass('active');
			} else {// 성공				
				$('.stt_demo .rightBox .demo_intro').hide();
				$('.stt_demo .rightBox .selectarea').hide();
				$('.stt_demo .rightBox .demo_recording').show();	
				$('.progress li:nth-child(2)').addClass('active');
				$('.progress li:nth-child(3)').addClass('active')
				
			}
		});
		// product layer popup     
		$('.btn_lyrWrap_close, .lyr_bg_notice').on('click', function () {
			$('#lyr_lack').fadeOut(300);
			$('#lyr_upgrade').fadeOut(300);
			$('body').css({
				'overflow': '',
			});
		});
	});	
});	
	