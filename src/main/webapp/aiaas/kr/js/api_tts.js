
	
jQuery.event.add(window,"load",function(){
	$(document).ready(function (){
		
	
		// text count
		$('.demoBox .textArea').on('input keyup paste', function() {			
			var txtValLth = $(this).val().length;
			
			if ( txtValLth > 0) {
				$('.demo_intro .btnBox .demoRecord').removeClass('disabled');	
				$('.demo_intro .btnBox .demoRecord').removeAttr('disabled');
				$('.demo_intro .btnBox .holeBox').show();
				$('.demo_intro .btnBox .disBox').remove();
				$('.progress li:nth-child(2)').addClass('active');
			} else {
				$('.demo_intro .btnBox .demoRecord').addClass('disabled');	
				$('.demo_intro .btnBox .demoRecord').attr('disabled');
				$('.demo_intro .btnBox .holeBox').hide();
				$('.demo_intro .btnBox').append('<span class="disBox"></span>');
				$('.progress li:nth-child(2)').removeClass('active');
			}
		});
				
		$('.holeBox').hide();
		// step01 > step02
		$('.tts_demo button.demoRecord').on('click', function(){	
			$(this).parent().parent('.demo_intro').hide();
			$(this).parent().parent().parent().children('.demo_recording').show();
			$('.selectarea').hide();
			$('.desc_tts').hide();
		});	
		// step02> step03
		$('.tts_demo .recording1').on('click', function(){	
			$(this).parent().parent('.demo_recording').hide();
			$(this).parent().parent().parent().children('.demo_recording2').show();
			$('.selectarea').hide();
			$('.desc_tts').hide();
			$('.progress li:nth-child(3)').addClass('active');
		});	
		
		// step03 > step04
		$('.tts_demo .recording').on('click', function(){	
			$(this).parent('.demo_recording2').hide();
			$(this).parent().parent().children('.demo_result').show();
			$('.selectarea').hide();
			$('.desc_tts').hide();
		});	
		
		
		// step04 > step01
		$('.tts_demo .demoReset').on('click', function(){	
			$('.tts_demo .demoBox .textArea').val('');
			$('.tts_demo .demoBox .textArea').attr('placeholder', "25자 이내로 자유롭게 텍스트를 입력해 보세요!");			
			$('.demo_intro .btnBox .demoRecord').addClass('disabled');	
			$('.demo_intro .btnBox .demoRecord').attr('disabled');
			$('.demo_intro .btnBox .holeBox').hide();
			$('.demo_recording2').hide();
			$('.selectarea').show();
			$('.desc_tts').show();
			$('.demo_intro .btnBox').append('<span class="disBox"></span>');
			$(this).parent().parent('.demo_result').hide();
			$(this).parent().parent().parent().children('.demo_intro').show();
			$('.progress li:nth-child(2)').removeClass('active');
			$('.progress li:nth-child(3)').removeClass('active');
			$('.tooltiptext').hide();
		});	
		
		
		// product layer popup     
		$('.btn_lyrWrap_close, .lyr_bg').on('click', function () {
			$('.lyr_lack').fadeOut(300);
			$('.lyr_deduct').fadeOut(300);
			$('body').css({
				'overflow': '',
			});
		});
		
		$('.selectarea .radio').on("click",function(){			
			$('.textArea').text($('#voice'+$(this).children('input[type="radio"]').attr("id")).val());
		});	  
	});	
});	
	
//API 탭  	
function openTap(evt, menu) {
  var i, demobox, tablinks;
  demobox = document.getElementsByClassName("demobox");
  for (i = 0; i < demobox.length; i++) {
    demobox[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(menu).style.display = "block";
  evt.currentTarget.className += " active";
}	
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();	
	
	
	