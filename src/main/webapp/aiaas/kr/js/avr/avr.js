// MINDsLab. UX/UI Team. mrs


var $cropper = null;
function activateCroppie_sample(){

    $cropper = $('#previewImg').croppie({
        showZoomer: true,
        enableExif: true,
        enforceBoundary: false,
        enableOrientation: true,
        viewport: {
            width: 480,
            height: 250,
            type: 'square',
        },
        boundary: {
            width:500,
            height: 330
        }
    });

    $cropper.croppie('bind', {
        url: sample1SrcUrl,

    });

}

function activateCroppie_upload(){
    console.log("uplaod croppie");

    $cropper = $('#previewImg').croppie({
        showZoomer: true,
        enableExif: true,
        enforceBoundary: false,
        enableOrientation: true,
        viewport: {
            width: 480,
            height: 250,
            type: 'square',
        },
        boundary: {
            width:500,
            height: 330
        }
    });
}
