package ai.square;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class RestTemplateEx4 {

	public static void main(String[] args) throws UnsupportedEncodingException {
		
		String url = "http://dev.board.com:9090/test.do";
//		String url = "https://api.maum.ai/api/mrc/";
		String sentence = "sentence";
		String question = "question";
        
		System.out.println("======sentence======:: "+sentence);
    	System.out.println("======question======:: "+question);
    	try {

			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
//			builder.addPart("sentence", new StringBody(sentence, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
//			builder.addPart("question", new StringBody(question, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			
//			builder.addPart("ID", new StringBody("minds-api-sales-demo", ContentType.TEXT_PLAIN.withCharset("UTF-8")));
//			builder.addPart("key", new StringBody("814826d160684a489dcbeb7445ecd644", ContentType.TEXT_PLAIN.withCharset("UTF-8")));
//			builder.addPart("cmd", new StringBody("runMRC", ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			
			
			HttpEntity entity = builder.build();
			
			post.setEntity(entity);
			HttpResponse response = client.execute(post);
			
			int responseCode = response.getStatusLine().getStatusCode();
			//System.out.println("\nSending 'POST' request to URL : " + url);
			//System.out.println("Post parameters : " + post.getEntity());
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

			if (responseCode == 200) {
				
				System.out.println("Response Result : " + responseCode);
				
			} else {
				System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
				
				System.out.println("Response Result : " + "{ \"status\": \"error\" }");
								
			}
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("Response Result : " + "{ \"status\": \"error\" }");
		}
        
		
	}
}
