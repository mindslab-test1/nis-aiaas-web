package ai.square;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RestTemplateEx6 {

	public static void main(String[] args) throws ClientProtocolException, IOException {
		
		String USER_AGENT = "Mozila/5.0";

		
		String url = "http://dev.board.com:9090/resttest3.do";
		
        //http client 생성
        CloseableHttpClient httpClient = HttpClients.createDefault();
        
        //get 메서드와 URL 설정
        HttpGet httpGet = new HttpGet(url);
        
        //agent 정보 설정
        httpGet.addHeader("User-Agent", USER_AGENT);
        httpGet.addHeader("Content-type", "application/json");
        
        
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", "데이터1");
		map.put("age", "데이터2");
		
		String requestJson = new ObjectMapper().writeValueAsString(map);


        HttpPost method = new HttpPost(url);
        StringEntity requestEntity = new StringEntity(requestJson , "utf-8");

        requestEntity.setContentType(new BasicHeader("Content-Type", "application/json"));

        method.setEntity(requestEntity);        
        
        //get 요청
        CloseableHttpResponse httpResponse = httpClient.execute(method);
        
        System.out.println("GET Response Status");
        System.out.println(httpResponse.getStatusLine().getStatusCode());
        String json = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
        
        System.out.println(json);
        
        httpClient.close();
	}

}
