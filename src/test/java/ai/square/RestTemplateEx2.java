package ai.square;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RestTemplateEx2 {

	public static void main(String[] args) throws JsonProcessingException {
		
		String url = "http://dev.board.com:9090/resttest3.do";
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", "데이터1");
		map.put("age", "데이터2");
		
		String requestJson = new ObjectMapper().writeValueAsString(map);
		
		System.out.println("requestJson :::: "+requestJson);
		
		HttpEntity<String> request = new HttpEntity<String>(headers);
	
		
		Object response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		
		System.out.println("response ::::::::::::: "+response.toString());
	}

}
