package ai.square;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class RestTemplateEx {

	public static void main(String[] args) {
		
		String url = "http://dev.board.com:9090/test.do";
		
		RestTemplate restTemplate = new RestTemplate();
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		map.add("키1", "데이터1");
		map.add("키2", "데이터2");
		
		String result1 = restTemplate.postForObject(url, map, String.class);
		
		System.out.println("result ::::: "+ result1);
	}

}
