package ai.square;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class RestTemplateEx5 {

	public static void main(String[] args) {
		
		String url = "http://dev.board.com:9090/resttest3.do";
		
        RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
        MultiValueMap map = new LinkedMultiValueMap();
        map.add("키1", "데이터1");
        map.add("키2", "데이터2");
//        String result = restTemplate.postForObject(url, map, String.class);
        HttpEntity<String> request = new HttpEntity<String>(map.toString(),headers);
        
        Object result = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
        
        
        System.out.println("response ::::::::::::: "+result.toString());
	}

}
